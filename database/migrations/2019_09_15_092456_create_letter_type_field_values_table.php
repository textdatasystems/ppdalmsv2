<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLetterTypeFieldValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letter_type_field_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('letter_id')->nullable();
            $table->integer('letter_type_id')->nullable();
            $table->integer('letter_type_field_id');
            $table->string('field_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letter_type_field_values');
    }
}
