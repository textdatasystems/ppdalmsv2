<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLetterTypeFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letter_type_fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('letter_type_id');
            $table->string('field_name');
            $table->string('field_type');
            $table->integer('field_order');
            $table->timestamps();

            $table->foreign('letter_type_id')->references('id')->on('letter_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letter_type_fields');
    }
}
