<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutgoingLettersLetterTypeFieldValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outgoing_letters_letter_type_field_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('outgoing_letter_id')->nullable();
            $table->integer('letter_type_id')->nullable();
            $table->integer('letter_type_field_id');
            $table->string('field_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outgoing_letters_letter_type_field_values');
    }
}
