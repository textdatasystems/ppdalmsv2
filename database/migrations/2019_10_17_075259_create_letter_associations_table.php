<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLetterAssociationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letter_associations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('associate_letter_id');
            $table->string('associate_letter_type');
            $table->bigInteger('associated_letter_id');
            $table->string('associated_letter_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letter_associations');
    }

}
