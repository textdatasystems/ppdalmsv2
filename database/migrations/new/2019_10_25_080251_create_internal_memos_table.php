<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternalMemosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internal_memos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('memo_type');
            $table->string('from_user')->nullable();
            $table->string('from_user_full_name')->nullable();
            $table->text('subject')->nullable();
            $table->date('memo_date')->nullable();
            $table->date('date_received')->nullable();
            $table->string('attention_to')->nullable();
            $table->string('attention_to_full_name')->nullable();
            $table->string('current_que')->nullable();

            $table->string('created_by_user')->nullable();
            $table->string('created_by_user_full_name')->nullable();

            $table->string('registry_entry_user')->nullable();
            $table->dateTime('registry_entry_date')->nullable();

            $table->string('eds_office_entry_user')->nullable();
            $table->date('eds_office_entry_date')->nullable();

            $table->string('memo_file_name',300)->nullable();
            $table->string('memo_file_path',400)->nullable();
            $table->string('memo_upload_user')->nullable();
            $table->date('memo_upload_date_time')->nullable();

            $table->string('edms_document_path_folder',400)->nullable();
            $table->string('edms_document_path',400)->nullable();
            $table->string('edms_document_id',200)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internal_memos');
    }
}
