<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomingLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incoming_letters', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('letter_category')->nullable();
            $table->string('org_type')->nullable(); //incoming letters
            $table->string('entity_name')->nullable(); //incoming letters
            $table->string('from_user')->nullable(); //internal memo
            $table->string('from_user_full_name')->nullable(); //internal memo
            $table->text('subject')->nullable();
            $table->date('date_sent')->nullable();

            $table->date('date_received_ppda')->nullable();
            $table->date('date_received_registry')->nullable();

            $table->string('sender_reference_number')->nullable();
            $table->string('ppda_reference_number')->nullable();
            $table->string('attention_to')->nullable();
            $table->string('attention_to_name')->nullable();
            $table->string('signatory')->nullable();
            $table->string('signatory_title')->nullable();

            $table->integer('letter_type_id')->nullable();

            $table->string('current_que',20)->nullable();
            $table->string('created_by_user')->nullable();
            $table->string('created_by_user_name')->nullable();
            $table->date('creation_date')->nullable();
            $table->string('registry_entry_user')->nullable();
            $table->string('eds_office_entry_user')->nullable();
            $table->date('eds_office_entry_date')->nullable();

            $table->string('incoming_letter_file_name',300)->nullable();
            $table->string('incoming_letter_file_path',400)->nullable();
            $table->string('incoming_letter_upload_user')->nullable();
            $table->date('upload_date_time')->nullable();

            $table->string('edms_document_path_folder',400)->nullable();
            $table->string('edms_document_path',400)->nullable();
            $table->string('edms_document_id',200)->nullable();

            $table->boolean('cbas_tag')->default(0)->nullable();
            $table->boolean('pm_tag')->default(0)->nullable();
            $table->boolean('advisory_tag')->default(0)->nullable();
            $table->boolean('legal_tag')->default(0)->nullable();
            $table->boolean('corp_tag')->default(0)->nullable();
            $table->boolean('operations_tag')->default(0)->nullable();

            $table->string('assign_to_legal')->nullable();
            $table->string('assign_to_mac')->nullable();
            $table->string('assign_to_corporate')->nullable();
            $table->string('assign_to_performance_monitoring')->nullable();
            $table->string('assign_to_advisory')->nullable();
            $table->string('assign_to_cbas')->nullable();
            $table->string('assign_to_operations')->nullable();

            $table->boolean('doc_fully_scanned')->default(0)->nullable();
            $table->boolean('entity_not_in_list_flag')->default(0)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incoming_letters');
    }
}
