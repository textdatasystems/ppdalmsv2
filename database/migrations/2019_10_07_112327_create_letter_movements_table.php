<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLetterMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letter_movements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('letter_id')->nullable();

            //new fields added
            $table->integer('moveable_id')->unsigned()->nullable();
            $table->string('moveable_type')->nullable();

            $table->string('to')->nullable();
            $table->string('to_name')->nullable();
            $table->string('to_department')->nullable();
            $table->string('from')->nullable();
            $table->string('from_name')->nullable();
            $table->string('from_department')->nullable();
            $table->dateTime('date_sent')->nullable();
            $table->text('required_action')->nullable();
            $table->date('deadline_for_action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letter_movements');
    }
}
