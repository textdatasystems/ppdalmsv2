<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutgoingLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outgoing_letters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('origin_department')->nullable();
            $table->string('origin_department_unit')->nullable();
            $table->string('origin_department_unit_name')->nullable();
            $table->string('org_type')->nullable();
            $table->string('entity_name')->nullable();
            $table->text('subject')->nullable();
            $table->date('date_sent')->nullable();
            $table->date('date_received')->nullable();

            $table->string('ppda_reference_number')->nullable();
            $table->string('recipient_reference_number')->nullable();
            $table->string('recipient_title')->nullable();
            $table->string('recipient_entity')->nullable();
            $table->string('signatory_username')->nullable();
            $table->string('signatory')->nullable();
            $table->string('signatory_title')->nullable();

            $table->string('current_que',20)->nullable();

            $table->integer('letter_type_id')->nullable();

            $table->string('outgoing_letter_file_name',300)->nullable();
            $table->string('outgoing_letter_file_path',400)->nullable();
            $table->string('outgoing_letter_upload_user')->nullable();
            $table->date('upload_date_time')->nullable();

            $table->string('edms_document_path_folder',400)->nullable();
            $table->string('edms_document_path',400)->nullable();
            $table->string('edms_document_id',200)->nullable();

            $table->string('outgoing_letter_receipt_file_name',300)->nullable();
            $table->string('outgoing_letter_receipt_file_path',400)->nullable();
            $table->string('outgoing_letter_receipt_upload_user')->nullable();
            $table->date('upload_receipt_date_time')->nullable();
            $table->string('edms_document_receipt_path',400)->nullable();

            $table->boolean('cbas_tag')->default(0)->nullable();
            $table->boolean('pm_tag')->default(0)->nullable();
            $table->boolean('advisory_tag')->default(0)->nullable();
            $table->boolean('legal_tag')->default(0)->nullable();
            $table->boolean('corp_tag')->default(0)->nullable();

            $table->boolean('private_file')->default(0)->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outgoing_letters');
    }
}
