<?php

use App\Helpers\EndPoints;
use App\Helpers\Security;
use Illuminate\Support\Facades\Redirect;

Route::get('/users/auth/{key}',
    [
        'uses' => 'UserController@authenticate',
        'as' => 'users.auth'
    ]);

/*
* This loads the home page
* */
//Route::get('/', function () {
//    return Redirect::to(EndPoints::$USER_MANAGEMENT_LOGOUT_LINK);
//})->name('login');

Route::get('/', 'UserController@home')->name('login');


/*
 * Only accessible once logged in
 * */
Route::group(['middleware' => ['web', 'users.authorized' /*'auth'*/]], function () {


//view email attachment
    Route::get('/lms-attachment-link/{letterCategory?}/{letterId?}', "IncomingAndOutgoingLetterController@viewAttachmentSentViaEmail")->name('attachments.email-link');

    /*
     * Test PDF View
     * */
    Route::get('/pdf-view', function () {

        $data = array(
            'menu_selected'=>'reception',
            'level'=> \App\Helpers\AppConstants::$ROLE_CODE_SUPER_USER,
        );

        return view('test/test')->with($data);
    })->name('pdf-view');


    Route::get('/user/logout',
        [
            'uses' => 'UserController@logoutUser',
            'as' => 'logout_user'
        ]);

    Route::get('/letter-movement','LetterMovementController@index')->name('letter_movement');
    Route::get('/memo-movement','LetterMovementController@indexInternalMemo')->name('letter_movement.memo');

    Route::prefix('letter-movement')->group(function(){
//        Route::get('/create/{letter}', 'LetterMovementController@create')->name('letter-movement.create');
        Route::get('/launch/{documentType}/{documentId}', 'LetterMovementController@launch')->name('letter-movement.launch');
        Route::get('/incoming-letter/view/{letter}/{menu}', 'LetterMovementController@viewLetterMovement')->name('letter-movement.view-movement');
        Route::get('/internal-memo/view/{memo}/{menu}', 'LetterMovementController@viewInternalMemoMovement')->name('memo-movement.view-movement');
        Route::get('/forward/{documentType}/{documentId}', 'LetterMovementController@forward')->name('letter-movement.forward');
        Route::get('/return-letter-movement/{documentId}/{returnTo}/{documentType}', 'LetterMovementController@returnIncomingLetter')->name('letter-movement.return');
        Route::get('/list/letter', 'LetterMovementController@letterMovementList')->name('letter-movement.list');
        Route::get('/list/memo', 'LetterMovementController@memoMovementList')->name('letter-movement.list.memo');
    });



    Route::get('/download-link-memo/{internalMemo}', "InternalMemoController@getInternalMemoDownloadLink")->name('download-link-memo');
    Route::get('/download-link-incoming-letter/{letter}', "IncomingLetterController@getLetterDownloadLink")->name('download-link-incoming');
    Route::get('/download-link-outgoing-letter/{outgoingLetter}', "OutgoingLetterController@getLetterDownloadLink")->name('download-link-outgoing');
    Route::get('/download-link-outgoing-letter/receipt/{outgoingLetter}', "OutgoingLetterController@getLetterReceiptDownloadLink")->name('download-link-outgoing.receipt');
    Route::get('/delete-temp-incoming-letter-doc/{documentName}', "IncomingLetterController@deleteTempDocument")->name('delete-temp-incoming-letter-doc');
    Route::get('/delete-temp-outgoing-letter-doc/{documentName}', "OutgoingLetterController@deleteTempDocument")->name('delete-temp-outgoing-letter-doc');

    Route::post('/store', 'ActionController@store')->name('store');
    Route::get('/delete/{table}/{id}', 'ActionController@delete')->name('delete');
    Route::get('/upload', 'FileUploadController@action')->name('upload');

    Route::get('/reception','ReceptionController@index')->name('home')->middleware('users.reception');
    Route::get('/registry','RegistryController@index')->name('registry')->middleware('users.registry');
    Route::get('/ed_office','EdController@index')->name('ed_office')->middleware('users.ed');

    Route::prefix('incoming-letter')->group(function(){
        Route::get('/create', 'IncomingLetterController@create')->name('letter.create');
        Route::get('/edit/{letter}', 'IncomingLetterController@edit')->name('letter.edit');
        Route::get('/readonly/{letter}', 'IncomingLetterController@readonly')->name('letter.readonly');
        Route::get('/upload/{letter}', 'IncomingLetterController@uploadIncomingLetter')->name('letter.upload');
        Route::get('/rename-attached-doc/{letter}', 'IncomingLetterController@renameAttachedDocumentView')->name('letter.rename-attached-doc.view');
        Route::get('/download/{letter}', 'IncomingLetterController@downloadAttachedIncomingLetter')->name('letter.download');
        Route::get('/remove-attached-upload/{letter}', 'IncomingLetterController@removeAttachedIncomingLetter')->name('letter.remove-attached-upload');
        Route::get('/reception/list', 'IncomingLetterController@reception_list')->name('reception.letters.list');
        Route::get('/registry/list', 'IncomingLetterController@registry_list')->name('registry.letters.list');
        Route::get('/ed/list', 'IncomingLetterController@ed_list')->name('ed.letters.list');
        Route::get('/forward/{letter}/{que}', 'IncomingLetterController@forward')->name('letter.forward');
        Route::get('/associations/{letter}','IncomingLetterController@associations')->name('letter.associations');
        Route::get('/history/view/{activeMenu}', 'IncomingLetterController@historyView')->name('letter.history.view');
        Route::get('/history/list/{activeMenu}', 'IncomingLetterController@historyList')->name('letter.history.list');
    });

    Route::prefix('outgoing-letter')->group(function(){
        Route::get('/','OutgoingLetterController@index')->name('outgoingletters');
        Route::get('/create', 'OutgoingLetterController@create')->name('outgoingletter.create');
        Route::get('/upload/{outgoingLetter}', 'OutgoingLetterController@uploadOutgoingLetter')->name('outgoingletter.upload');
        Route::get('/upload/receipt/{outgoingLetter}', 'OutgoingLetterController@uploadOutgoingLetterReceipt')->name('outgoingletter.upload.receipt');
        Route::get('/rename-attached-doc/{letter}', 'OutgoingLetterController@renameAttachedDocumentView')->name('outgoingletter.rename-attached-doc.view');
        Route::get('/download/{outgoingLetter}', 'OutgoingLetterController@downloadAttachedOutgoingLetter')->name('outgoingletter.download');
        Route::get('/remove-attached-upload/{outgoingLetter}', 'OutgoingLetterController@removeAttachedOutgoingLetter')->name('outgoingletter.remove-attached-upload');
        Route::get('/remove-attached-upload/receipt/{outgoingLetter}', 'OutgoingLetterController@removeAttachedOutgoingLetterReceipt')->name('outgoingletter.remove-attached-upload.receipt');
        Route::get('/edit/{outgoingLetter}', 'OutgoingLetterController@edit')->name('outgoingletter.edit');
        Route::get('/list', 'OutgoingLetterController@list')->name('outgoingletters.list');
        Route::get('/associations/{outgoingLetter}','OutgoingLetterController@associations')->name('outgoingletters.associations');

   });

    Route::prefix('internal-memo')->group(function(){
        Route::get('/create', 'InternalMemoController@create')->name('memo.create');
        Route::get('/edit/{internalMemo}', 'InternalMemoController@edit')->name('memo.edit');
        Route::get('/readonly/{internalMemo}', 'InternalMemoController@readonly')->name('memo.readonly');
        Route::get('/upload/{internalMemo}', 'InternalMemoController@uploadInternalMemo')->name('memo.upload');
        Route::get('/rename-attached-doc/{internalMemo}', 'InternalMemoController@renameAttachedDocumentView')->name('memo.rename-attached-doc.view');
        Route::get('/download/{internalMemo}', 'InternalMemoController@downloadAttachedIncomingLetter')->name('memo.download');
        Route::get('/remove-attached-upload/{internalMemo}', 'InternalMemoController@removeAttachedInternalMemo')->name('memo.remove-attached-upload');
        Route::get('/reception', 'InternalMemoController@receptionIndex')->name('memo.reception.index');
        Route::get('/registry', 'InternalMemoController@registryIndex')->name('memo.registry.index');
        Route::get('/ed', 'InternalMemoController@edOfficeIndex')->name('memo.ed.index');
        Route::get('/reception/list', 'InternalMemoController@receptionList')->name('memo.reception.list');
        Route::get('/registry/list', 'InternalMemoController@registryList')->name('memo.registry.list');
        Route::get('/ed/list', 'InternalMemoController@edOfficeList')->name('memo.ed.list');
        Route::get('/forward/{internalMemo}/{que}', 'InternalMemoController@forward')->name('memo.forward');
    });


    Route::prefix('letters')->group(function(){
        Route::get('/deletion/{letterCategory}/{letterId}', 'IncomingAndOutgoingLetterController@delete')->name('letters.deletion');
    });


    Route::prefix('letter-association')->group(function(){
        Route::get('/list/{letterId}/{letterCategory}', 'LetterAssociationController@associationList')->name('letter-association.list');
        Route::get('/create/{letterId}/{letterCategory}', 'LetterAssociationController@create')->name('letter-association.create');
        Route::get('/letters-incoming/{letter}', 'LetterAssociationController@listIncoming')->name('letter-association.letters-incoming');
        Route::get('/letters-outgoing/{outgoingLetter}', 'LetterAssociationController@listOutgoing')->name('letter-association.letters-outgoing');
    });

    Route::prefix('upload')->group(function(){
        Route::post('/incoming-letter', 'UploadController@uploadIncomingLetter')->name('upload.incoming-letter');
        Route::post('/internal-memo', 'UploadController@uploadInternalMemo')->name('upload.memo');
        Route::post('/outgoing-letter', 'UploadController@uploadOutgoingLetter')->name('upload.outgoing-letter');
        Route::post('/outgoing-letter/receipt', 'UploadController@uploadOutgoingLetterReceipt')->name('upload.outgoing-letter.receipt');
        Route::post('/incoming-letter/rename-attachment', 'UploadController@renameIncomingLetterAttachment')->name('upload.incoming-letter.rename-attachment');
        Route::post('/outgoing-letter/rename-attachment', 'UploadController@renameOutgoingLetterAttachment')->name('upload.outgoing-letter.rename-attachment');
    });

    Route::prefix('masterdata')->group(function(){
        Route::get('/', 'MasterDataController@index')->name('master_data_index');
        Route::get('letter_types', 'MasterDataController@index')->name('master_data.letter_types');
        Route::get('/general/{section}', 'MasterDataController@general')->name('general_section');
    });

    Route::prefix('letter_types')->group(function(){
        Route::get('/create','LetterTypeController@create')->name('create_letter_type');
        Route::get('/edit/{letterType}','LetterTypeController@letter_type_edit')->name('edit_letter_type');
        Route::get('/fields/{letterType}','LetterTypeController@letter_type_fields')->name('letter_type_fields');
        Route::get('/list','LetterTypeController@list')->name('letter_types.list');
    });
    Route::prefix('edms_doc_types')->group(function(){
        Route::get('/create','EdmsDocTypeController@create')->name('create_edms_doc_type');
        Route::get('/list','EdmsDocTypeController@list')->name('edms_doc_types.list');
        Route::get('/edit/{edmsdoctype}','EdmsDocTypeController@edit')->name('edmsdoctype.edit');
    });
    Route::prefix('mailing-lists')->group(function(){
        Route::get('/create','MailingListController@create')->name('create_mailing_list');
        Route::get('/list','MailingListController@list')->name('mailing_lists.list');
        Route::get('/edit/{mailingList}','MailingListController@edit')->name('mailing_list.edit');
        Route::get('/mailing-lists-delete/{mailingList}', 'MailingListController@removeMailingList')->name('mailing_list.remove_mailing_lists');
    });

    Route::prefix('global-values')->group(function(){
        Route::get('/create','GlobalValueController@create')->name('global-values.create');
        Route::get('/list','GlobalValueController@list')->name('global-values.list');
        Route::get('/edit/{globalValue}','GlobalValueController@edit')->name('global-values.edit');
        Route::get('/delete/{globalValue}', 'GlobalValueController@remove')->name('global-values.remove');
    });

    Route::prefix('providers')->group(function(){
        Route::get('/create/{origin}', 'ProviderController@other_entity_create')->name('provider-create');
    });

});


//test routes
Route::get('/edms-download', "UploadController@testEdmsDocumentDownload");

Route::get('/data', "LetterMovementController@test");


