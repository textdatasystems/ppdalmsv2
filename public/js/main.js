// JavaScript Document
function activate(){
alert("Sorry! you don't have permission to view this page. Your account is not activated yet. Contact the RoP administrator/Secretariat for details.")		
}

function fillqsearch(){
document.getElementById("searchquery").value="Enter search term:";
}

function clearqsearch(){
document.getElementById("searchquery").value="";
}

function blank2(){
		srch.un.value="";
		srch.pw.value="";		
}

function printThis(){
	obj = document.getElementById("printable");
	width = 790;
	//height = 900;
	//width = obj.offsetWidth;
	height = obj.offsetHeight;
	win = window.open('','','height='+height+',width='+width);
	if (window.focus) {win.focus()}
	win.document.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">');
	win.document.write('<html xmlns="http://www.w3.org/1999/xhtml">');
	win.document.write('<head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><title>Register of Providers: Print-Out</title><link rel="stylesheet" href="print.css" /></head>');
	// Body plus css to hide non-printables ...
	win.document.write('<body>' + '<style type="text/css">' + 'a{color: #000;text-decoration:none;cursor: pointer;} .noPrint{ display:none; visibility: false; }'+'</style>');
	win.document.write('<div align="left" style="height:auto; width:730px;">'+obj.innerHTML+'</div></body></html>');
	win.document.close();
	setTimeout(function() {
	    win.print();
	    win.close();
	}, 250);
}

/* function downloadThis()
{
	obj = document.getElementById("printable");
	width = 790;
	height = 900;
	//width = obj.offsetWidth;
	//height = obj.offsetHeight;
	win = window.open('','','height='+height+',width='+width);
	if (window.focus) {win.focus()}
	win.document.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">');
	win.document.write('<html xmlns="http://www.w3.org/1999/xhtml">');
	win.document.write('<head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><title>Register of Providers: Print-Out</title><link rel="stylesheet" href="print.css" /></head>');
	// Body plus css to hide non-printables ...
	win.document.write('<body>' + '<style type="text/css">' + 'a{color: #000;text-decoration:none;cursor: pointer;}.noPrint{ display:none; visibility: false; }'+'</style>');
	win.document.write('<div align="left" style="height:auto; width:790px;">'+obj.innerHTML+'</div></body></html>');
	win.document.close(); 
	win.stop()
	//win.print();
	//win.close();
} */

function downloadThis(){
	//return xepOnline.Formatter.Format('printable',{render:'download'});
	var pdf = new jsPDF('p', 'pt', 'a4');
	pdf.html(document.getElementById('printable'), {'width': 550, callback: function(pdf) {
		//var iframe = document.createElement('iframe');
		//iframe.setAttribute('style','position:absolute;right:0; top:0; bottom:0; height:100%; width:500px');
		//document.body.appendChild(iframe);
		//iframe.src = pdf.output('datauristring');
		pdf.output('dataurlnewwindow');
		}
	});
/*	var doc = new jsPDF();          
var elementHandler = {
  '#ignorePDF': function (element, renderer) {
    return true;
  }
};
var source = window.document.getElementById("printable")[0];
doc.fromHTML(
    source,
    15,
    15,
    {
      'width': 180,'elementHandlers': elementHandler
    });

doc.output("dataurlnewwindow");*/


}

/* function downloadThis(){
	var $html = $("#printable").html()
	var url = "plugins/html2pdf/generate.php"
	$.post(url,
		{
			data: $html,
			name: "This title"
		},
		function(data, status){
			window.open(url);
		}
	);
	
} */

/*function export_to_word(tableId,file_name){
	var tab_text="";
    //var textRange; var j=0;
    tab = document.getElementById(tableId).innerHTML; // id of table

	tab_text=tab_text+tab;
    


    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,file_name+".doc");
    }  
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/msword,' + encodeURIComponent(tab_text));  

    return (sa);
}*/

function export_to_word(tableId,filename){
	var preHtml = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'><head><meta charset='utf-8'><title>Export HTML To Doc</title></head><body>";
    var postHtml = "</body></html>";
    var html = preHtml+document.getElementById(tableId).innerHTML+postHtml;

    var blob = new Blob(['\ufeff', html], {
        type: 'application/msword'
    });
    
    // Specify link url
    var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);
    
    // Specify file name
    filename = filename?filename+'.doc':'document.doc';
    
    // Create download link element
    var downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob ){
        navigator.msSaveOrOpenBlob(blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = url;
        
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
    
    document.body.removeChild(downloadLink);
}
			
function currdays(){	
var i=0;
var dayArray= new Array(	
"","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31");	
		var day = document.getElementById("cdays");	
		var opt = day.options;
		opt.length=0;		
			for(i=0; i < dayArray.length; i++){	
				opt[opt.length] = new Option(dayArray[i],dayArray[i]);	
	}			
}

function currmonths(){	
var i=0;
var monthArray= new Array(	
"","January","February","March","April","May","June","July","August","September","October","November","December");	
		var month = document.getElementById("cmonths");	
		var opt = month.options;
		opt.length=0;		
			for(i=0; i < monthArray.length; i++){	
				opt[opt.length] = new Option(monthArray[i],monthArray[i]);	
	}			
}

function curryears(){
var d = new Date()
var i=0;
var cy=d.getYear();
var year = document.getElementById("cyears");
var year1 = document.getElementById("cyears1");
var opt = year.options;
var opt1 = year1.options;
for(i = cy;i>1929; i--){	
		opt[opt.length] = new Option(i,i);
		opt1[opt1.length] = new Option(i,i);	
	}				
}

function syears(){
var d = new Date()
var i=0;
var cy=d.getYear();
var year = document.getElementById("eyears");
var opt = year.options;
for(i = cy;i>1929; i--){	
		opt[opt.length] = new Option(i,i);
	}				
}

function pyears1(){
var d = new Date()
var i=0;
var cy=d.getYear()+10;
var year = document.getElementById("projyears1");
var opt = year.options;
for(i = cy;i>1900; i--){	
		opt[opt.length] = new Option(i,i);
	}				
}
function pyears2(){
var d = new Date()
var i=0;
var cy=d.getYear()+20;
var year = document.getElementById("projyears2");
var opt = year.options;
for(i = cy;i>1900; i--){	
		opt[opt.length] = new Option(i,i);
	}				
}

function pmonths(){	
var i=0;
var monthArray= new Array(	
"","January","February","March","April","May","June","July","August","September","October","November","December");	
		var month = document.getElementById("projmonth1");	
		var month1 = document.getElementById("projmonth2");	
		var opt = month.options;
		var opt1 = month1.options;
		opt.length=0;
		opt1.length=0;
			for(i=0; i < monthArray.length; i++){	
				opt[opt.length] = new Option(monthArray[i],monthArray[i]);
				opt1[opt1.length] = new Option(monthArray[i],monthArray[i]);
	}			
}

function curryears1(){
d = new Date()
var i=0;
var yearArray= new Array(	
"","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000","1999","1998","1997","1996","1995","1994","1993","1992","1991","1990","1989",
"1988","1987","1986","1985","1984","1983","1982","1981","1980","1979","1978","1977","1976","1975","1974","1973","1972","1971","1970","1969","1968","1967",
"1966","1965","1964","1963","1962","1961","1960","1959","1958","1957","1956","1955","1954","1953","1952","1951","1950","1949","1948","1947","1946","1945",
"1944","1943","1942","1941","1940","1939","1938","1937","1936","1935","1934","1933","1932","1931");	
		var year = document.getElementById("cyears");	
		var opt = year.options;
		opt.length=0;		
			for(i=0; i < yearArray.length; i++){	
				opt[opt.length] = new Option(yearArray[i],yearArray[i]);	
	}			
}

function countries(){	
var i=0; 
var countryArray= new Array( 
"Uganda","Afghanistan","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba (Neth.)","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia and Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei Darussalam","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central African Republic","Chad","Chile","China","Columbia","Comoros","Congo","Congo-Kinsasha","Cook Islands","Costa Rica","Cote d'Ivoire","Croatia","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","East Timor","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Fiji","Finland","France","French Guiana","French Guinea","French Polynesia","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guinea","Guinea Bissau","Guyana","Haiti","Heard and McDonald Islands","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Israel","Italy","Ivory Coast","Jamaica","Japan","Jordan","Kazakhstan","Kenya","Kiribati","Kuwait","Kyrgyzstan","Lao PDR","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique","Mauritania","Mauritius","Mayotte","Mexico","Micronesia, Fed Stat","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Niue","Norfolk Island","North Korea","Northern Mariana Islands","Norway","Oman","Pakistan","Palau Islands","Palestinian Authority","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russian Federation","Rwanda","Saint Lucia","Saint Tome & Principe","San Marino","Saudi Arabia","Scotland","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","Spain","Sri Lanka","St.Kitts & Nevis","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Tadjikistan","Taiwan","Tanzania","Thailand","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks and Caicos Islands","Tuvalu","Ukraine","United Arab Emirates","United Kingdom","United States","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Wallis & Futuna","Western Sahara","Western Samoa","Worldwide","Yemen","Zambia","Zimbabwe" );	

	var country = document.getElementById("country");		
	var opt = country.options; opt.length=0;		 	
	for(i=0; i < countryArray.length; i++){	 		
	opt[opt.length] = new Option(countryArray[i],countryArray[i]);	 	
		}	
		
	var bankcountry = document.getElementById("bankcountry");		
	var opt = bankcountry.options; opt.length=0;		 	
	for(i=0; i < countryArray.length; i++){	 		
	opt[opt.length] = new Option(countryArray[i],countryArray[i]);	 	
		}		
}
	//-->
	
