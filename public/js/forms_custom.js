
var new_provider_from_form = false;

$('body').on('click','#randomActionModal .btnSubmit,.btnSubmit',function(event){

	event.preventDefault();
	var formId = $(this).parents('form').attr('id');

	//alert(formId)
	//if validation fails return false else save_frm()
	if(!validate_form(formId)){
		return false
	}

	var origin = $(this).attr('data-origin');
	new_provider_from_form = /from_form/i.test(origin); // signifies that we have opened a new modal form to save the provider, so if origin has the string 'from_form' we set to true

	save_frm(formId);

});

$('body').on('change','.btn-condition', function () {
	//alert('open')
	if($('.btn-condition:checked').val() == 'Bad'){
		$('.bad-condition').toggle(true)
	}else{
		$('.bad-condition').toggle(false)
	}
});

$('body').on('change','.btn-sticker', function () {
	//alert('open')
	if($('.btn-sticker:checked').val() == 'Damaged'){
		$('.bad-sticker').toggle(true)
	}else{
		$('.bad-sticker').toggle(false)
	}

});

//function to save by ajax
function save_frm(frm){

	pushNotification('<i class="fa fa-spinner fa-spin"></i> Submitting info','Please wait as we submit your information!','info')

	var values = $('#'+frm).serialize();

	var frmAction = $('#'+frm).attr('action');

	if($('#'+frm).find($('.import')).length > 0){

		var formData = new FormData($('#'+frm)[0]);
		$.ajax({
			url: frmAction,  //Server script to process data
			type: 'POST',
			xhr: function() {  // Custom XMLHttpRequest
				var myXhr = $.ajaxSettings.xhr();
				if(myXhr.upload){ // Check if upload property exists
					myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
				}
				return myXhr;
			},

			//Ajax events
			/*beforeSend: beforeSendHandler,*/
			success: function (message){
				completeHandler(frm,message)
			},
			/*error: errorHandler,*/
			// Form data
			data: formData,
			//Options to tell jQuery not to process data or worry about content-type.
			cache: false,
			contentType: false,
			processData: false
		});
	}
	else{
		$.ajax(
		{
			type: "POST",
			url: frmAction,
			data: "myData=" + values,
			cache: false,
			success: function(message)
			{
				completeHandler(frm,message);
			},
			error: function(message)
			{
				pushNotification('<i class="fa fa-frown-o"></i> Error Submission','<strong>Oops</strong>, Looks like something is a mess!!<br>'+message,'error')
			}
		});
	}

	setTimeout(function(){$('.submit_msg').fadeOut('slow').html('')},10000);
}

function completeHandler(frm,message){

	var btnClass = 'alert-success';

	//if message cotains the String 'successfully'
	if (/successfully/i.test(message)){


		if(new_provider_from_form){

			//get value of new added provider
			var provider_name = $('#'+frm+' #orgname').val();

			//new select option
			var newOption;

			//identifier for selects where the new data came from e.g 'from_form:::provider_select' and we go ahead to split this guy
			var origin = $('#'+frm+' .btnSubmit').attr('data-origin');

			$.each(origin.split(':::'),function(index,selector_ele){


				if(index != 0){

					//if its the second option then it's the current select, so we set the new option to be selected
					if(index == 1){
						newOption = $('<option value="'+provider_name+'" selected="selected">'+provider_name+'</option>');
					}
					// if its not the second then we just add the new option to list
					else{
						newOption = $('<option value="'+provider_name+'" >'+provider_name+'</option>');
					}

					//add the new option and then trigger that we have selected something new
					$('#'+selector_ele).append(newOption);
					$('#'+selector_ele).trigger("chosen:updated");

				}

			});

			new_provider_from_form = false;
			$('#'+frm).closest(".modal").modal('hide');

		}


		loadListing();
		resetFrm(frm);
		$('#'+frm).closest(".modal").modal('hide');
		if($('#'+frm).hasClass('reload_page')){
			location.reload()
		}


	}else{

	}

	pushNotification('<i class="fa fa-smile-o"></i> Success',message,'success')

}

function resetFrm(frm){
	$('#'+frm+' input,#'+frm+' select,#'+frm+' checkbox').removeAttr('disabled');
	$('#'+frm).trigger("reset");
}

function pushNotification(title,body_text,type){
	//$.gritter.removeAll();

	var add_class = 'gritter-info'
	if(type == 'success'){
		add_class = 'gritter-success'
	}
	else if(type == 'danger'){
		add_class = 'gritter-danger'
	}
	$.gritter.add({
		// (string | mandatory) the heading of the notification
		title: '<h4>'+title+'</h4>',
		// (string | mandatory) the text inside the notification
		text: body_text,
		//sticky: true,
		 time: '',
		//time: 5000,
		class_name: 'gritter-center- gritter-'+type
	});

}
