/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 5.7.26 : Database - ppda_lms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `ppda_lms`;

/*Table structure for table `edms_doc_types` */

DROP TABLE IF EXISTS `edms_doc_types`;

CREATE TABLE `edms_doc_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `edms_doc_types` */

insert  into `edms_doc_types`(`id`,`type_name`,`created_at`,`updated_at`) values 
(1,'Invoice','2019-09-15 23:21:17','2019-09-15 23:21:17'),
(2,'Remove','2019-09-19 08:37:25','2019-10-11 12:22:51'),
(3,'Incoming Letter','2019-09-19 08:38:57','2019-10-11 12:17:36'),
(4,'Outgoing Letter','2019-10-11 12:17:24','2019-10-11 12:17:24');

/*Table structure for table `global_values` */

DROP TABLE IF EXISTS `global_values`;

CREATE TABLE `global_values` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `variable_name` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `global_values` */

insert  into `global_values`(`id`,`variable_name`,`value`,`created_at`,`updated_at`) values 
(4,'MAX_FILE_UPLOAD_SIZE_MBS','10','2019-10-23 09:49:17','2019-10-23 10:26:35');

/*Table structure for table `incoming_letters` */

DROP TABLE IF EXISTS `incoming_letters`;

CREATE TABLE `incoming_letters` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `letter_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `org_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entity_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_user_full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci,
  `date_sent` date DEFAULT NULL,
  `date_received_ppda` date DEFAULT NULL,
  `date_received_registry` date DEFAULT NULL,
  `sender_reference_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ppda_reference_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attention_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signatory` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signatory_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `letter_type_id` int(11) DEFAULT NULL,
  `current_que` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_user_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creation_date` date DEFAULT NULL,
  `registry_entry_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eds_office_entry_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eds_office_entry_date` date DEFAULT NULL,
  `incoming_letter_file_name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incoming_letter_file_path` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incoming_letter_upload_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upload_date_time` date DEFAULT NULL,
  `edms_document_path_folder` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edms_document_path` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edms_document_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cbas_tag` tinyint(1) DEFAULT '0',
  `pm_tag` tinyint(1) DEFAULT '0',
  `advisory_tag` tinyint(1) DEFAULT '0',
  `legal_tag` tinyint(1) DEFAULT '0',
  `corp_tag` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `assign_to_corporate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assign_to_performance_monitoring` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assign_to_advisory` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assign_to_cbas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operations_tag` tinyint(4) DEFAULT '0',
  `assign_to_operations` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assign_to_legal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assign_to_mac` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attention_to_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_fully_scanned` tinyint(4) DEFAULT '0',
  `entity_not_in_list_flag` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `incoming_letters` */

insert  into `incoming_letters`(`id`,`letter_category`,`org_type`,`entity_name`,`from_user`,`from_user_full_name`,`subject`,`date_sent`,`date_received_ppda`,`date_received_registry`,`sender_reference_number`,`ppda_reference_number`,`attention_to`,`signatory`,`signatory_title`,`letter_type_id`,`current_que`,`created_by_user`,`created_by_user_name`,`creation_date`,`registry_entry_user`,`eds_office_entry_user`,`eds_office_entry_date`,`incoming_letter_file_name`,`incoming_letter_file_path`,`incoming_letter_upload_user`,`upload_date_time`,`edms_document_path_folder`,`edms_document_path`,`edms_document_id`,`cbas_tag`,`pm_tag`,`advisory_tag`,`legal_tag`,`corp_tag`,`created_at`,`updated_at`,`assign_to_corporate`,`assign_to_performance_monitoring`,`assign_to_advisory`,`assign_to_cbas`,`operations_tag`,`assign_to_operations`,`assign_to_legal`,`assign_to_mac`,`attention_to_name`,`doc_fully_scanned`,`entity_not_in_list_flag`) values 
(1,'Incoming Letter','Government Entity','Abim DLG',NULL,NULL,'Test Abim Today','2019-10-11','2019-10-11','2019-10-11','BUVUMA0001','PPDA/02004','Executive Director','Timothy Kasaga','Town Clerk',2,'ed','julianahebwa@gmail.com',NULL,NULL,NULL,NULL,'2019-10-11','Test Abim Document .pdf',NULL,'julianahebwa@gmail.com','2019-10-11','/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Abim/Incoming/','/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Abim/Incoming/Test Abim Document .pdf',NULL,0,0,0,0,0,'2019-10-11 07:57:10','2019-10-11 07:59:03',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,0),
(2,'Incoming Letter','Government Entity','National Social Security Fund',NULL,NULL,'Monthly Procurement Report for the Month of September','2019-10-04','2019-10-10','2019-10-11','NSSF/PPDA/023','PPDA/NSSF/012',NULL,'DavidByarugaba','Managing Director',2,'ed','jkaggwa@ppda.go.ug',NULL,NULL,NULL,'julianahebwa@gmail.com','2019-10-16',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'2019-10-11 09:57:31','2019-10-16 17:20:07','emuhumuza@ppda.go.ug','hdalia@ppda.go.ug','rkemigisa@ppda.go.ug','rtumuhairwe@ppda.go.ug',0,'robasoni@ppda.go.ug','sabamu@ppda.go.ug','jkallemera@ppda.go.ug',NULL,0,0),
(3,'Incoming Letter','Government Entity','Luwero DLG',NULL,NULL,'Application for Administrative Review','2019-10-03','2019-10-11','2019-10-11',NULL,NULL,NULL,'Director Operations','Director',8,'ed','jkaggwa@ppda.go.ug',NULL,NULL,NULL,NULL,'2019-10-11','RoP_Registration_Procedure.pdf',NULL,'jkaggwa@ppda.go.ug','2019-10-11','/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Luwero/Incoming/','/PPDA DEPARTMENTAL WORKSPACES/EMIS/Letters/Incoming/RoP_Registration_Procedure.pdf',NULL,0,0,0,0,0,'2019-10-11 11:16:20','2019-10-19 11:50:17','marea@ppda.go.ug','hdalia@ppda.go.ug','rkemigisa@ppda.go.ug','rkemigisa@ppda.go.ug',0,'robasoni@ppda.go.ug','sabamu@ppda.go.ug','enamuddu@ppda.go.ug',NULL,0,0),
(4,'Incoming Letter','Government Entity','Mubende DLG',NULL,NULL,'Procurement Advice on the use of Preferential schemes','2019-09-18','2019-10-01','2019-10-11',NULL,NULL,'Executive Dirctor','Bagonza David','Chief Administrative Officer',3,'ed','jkaggwa@ppda.go.ug',NULL,NULL,NULL,NULL,'2019-10-11','Internal memo for renewal of VMware licence.pdf',NULL,'jkaggwa@ppda.go.ug','2019-10-11','/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Mubende/Incoming/','/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Mubende/Incoming/Internal memo for renewal of VMware licence.pdf',NULL,0,0,0,0,0,'2019-10-11 11:18:59','2019-10-11 11:28:41',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,0),
(5,'Incoming Letter','Government Entity','Luwero DLG',NULL,NULL,'Request for Standard Bidding Documents for non consultancy services','2019-10-07','2019-10-11','2019-10-14',NULL,NULL,NULL,'Singh Patel',NULL,3,'ed','jkaggwa@ppda.go.ug',NULL,NULL,'jahebwa@ppda.go.ug','julianahebwa@gmail.com','2019-10-19',NULL,NULL,NULL,NULL,'/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Luwero/Incoming/',NULL,NULL,0,0,0,0,0,'2019-10-11 11:30:32','2019-10-19 11:26:03',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,0),
(6,'Incoming Letter','Government Entity','Luwero DLG',NULL,NULL,'invitation letter to anti-corruption week','2019-10-02','2019-10-11','2019-10-11','unra/2019/001','ppda/unra/2019',NULL,'ED UNRA','Accounting Officer',3,'ed','jahebwa@ppda.go.ug',NULL,NULL,NULL,NULL,'2019-10-11','EDMS Form 5 Quick Guide.pdf',NULL,'jahebwa@ppda.go.ug','2019-10-11','/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Luwero/Incoming/','/PPDA DEPARTMENTAL WORKSPACES/EMIS/Letters/Incoming/EDMS Form 5 Quick Guide.pdf',NULL,0,1,1,0,1,'2019-10-11 11:31:39','2019-10-19 11:55:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,0),
(7,'Incoming Letter','Non - Government Entity','KK Security (U) LTD edited',NULL,NULL,'test subject + gerry','2019-10-07','2019-10-14','2019-10-14',NULL,NULL,NULL,NULL,NULL,3,'ed','pkakembo@ppda.go.ug',NULL,NULL,'pkakembo@ppda.go.ug','pkakembo@ppda.go.ug','2019-10-14','Index NITA PORTAL. (1).pdf',NULL,'pkakembo@ppda.go.ug','2019-10-14',NULL,'/PPDA DEPARTMENTAL WORKSPACES/EMIS/Letters/Incoming/Index NITA PORTAL. (1).pdf',NULL,0,0,0,0,0,'2019-10-14 07:02:45','2019-10-14 07:07:30','pkakembo@ppda.go.ug',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,0),
(8,'Incoming Letter','Non - Government Entity','UWEAL',NULL,NULL,'Application for Administrative Review','2019-10-09','2019-10-09','2019-10-14','JulzTest','PPDA/Provider/1111','bturamye@ppda.go.ug','Managing Director, Test Organization','Director',8,'ed','jahebwa@ppda.go.ug',NULL,NULL,'jahebwa@ppda.go.ug','jahebwa@ppda.go.ug','2019-10-14',NULL,NULL,NULL,NULL,'DEFAULT',NULL,NULL,0,0,0,0,0,'2019-10-14 08:25:35','2019-10-21 12:45:18',NULL,NULL,NULL,NULL,0,NULL,'enamuddu@ppda.go.ug','rmasajjage@ppda.go.ug','Benson Turamye',0,0),
(9,'Incoming Letter','Government Entity','National Social Security Fund',NULL,NULL,'Response to Audit findings','2019-10-01','2019-10-10','2019-10-14',NULL,NULL,'bturamye@ppda.go.ug','Byarugaba David','Managing Director',9,'ed','jkaggwa@ppda.go.ug',NULL,NULL,'jkaggwa@ppda.go.ug','julianahebwa@gmail.com','2019-10-16',NULL,NULL,NULL,NULL,'/REGISTRY/PRIVATE REGISTRY FILES/MDAs/National Social Security Fund/Incoming/',NULL,NULL,0,0,0,0,0,'2019-10-14 08:58:20','2019-10-29 09:48:04','jahebwa@ppda.go.ug',NULL,NULL,NULL,0,NULL,NULL,NULL,'Benson Turamye',0,0),
(10,'Incoming Letter','Non - Government Entity','IT Solutions U Limited',NULL,NULL,'Confirmation of attendance for the administrative Review hearing','2019-10-09','2019-10-10','2019-10-14',NULL,NULL,'bturamye@ppda.go.ug','Greig Klein','Managing Director',8,'ed','jkaggwa@ppda.go.ug',NULL,NULL,'jkaggwa@ppda.go.ug','jkaggwa@ppda.go.ug','2019-10-14','Rosemary Nalukwago_22 days annual leave_26th August 2019.pdf',NULL,'jkaggwa@ppda.go.ug','2019-10-14','/PPDA DEPARTMENTAL WORKSPACES/EMIS/Letters/Incoming/','/PPDA DEPARTMENTAL WORKSPACES/EMIS/Letters/Incoming/Rosemary Nalukwago_22 days annual leave_26th August 2019.pdf',NULL,0,0,0,0,0,'2019-10-14 09:04:52','2019-10-14 09:21:14',NULL,NULL,NULL,'mojambo@ppda.go.ug',0,NULL,'usegawa@ppda.go.ug',NULL,'Benson Turamye',0,0),
(11,'Incoming Letter','Government Entity','National Social Security Fund',NULL,NULL,'Monthly report for the month of August','2019-09-18','2019-10-22','2019-10-14',NULL,NULL,'bturamye@ppda.go.ug','Tonny Byamukama','Executive Director',2,'ed','jkaggwa@ppda.go.ug',NULL,NULL,'jkaggwa@ppda.go.ug','jkaggwa@ppda.go.ug','2019-10-14','Delivery note - Teltec.pdf',NULL,'jkaggwa@ppda.go.ug','2019-10-14',NULL,'/REGISTRY/PRIVATE REGISTRY FILES/MDAs/National Social Security Fund/Incoming/Delivery note - Teltec.pdf',NULL,0,0,0,0,0,'2019-10-14 09:52:23','2019-10-16 09:30:29','jkaggwa@ppda.go.ug',NULL,NULL,NULL,0,NULL,NULL,NULL,'Benson Turamye',1,0),
(12,'Incoming Letter','Government Entity','Abim DLG',NULL,NULL,'Abim Test DLG','2019-10-16','2019-10-24','2019-10-16','AbimDLG/09212','PPDA/02199','timothykasaga@gmail.com','Mukasa Joseph','Chief Administrative Officer',2,'ed','julianahebwa@gmail.com',NULL,NULL,'julianahebwa@gmail.com','julianahebwa@gmail.com','2019-10-16','Test Abim now today.pdf','D:\\All Projects\\AppServer\\applications\\lms\\public\\uploads/incoming_letter_uploads/Test Abim now today.pdf','julianahebwa@gmail.com','2019-10-23','/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Abim/Incoming/','/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Abim/Incoming/Test Abim now today.pdf',NULL,0,0,0,0,0,'2019-10-16 09:46:33','2019-10-23 07:34:26','admin@ppda.go.ug',NULL,NULL,NULL,0,NULL,NULL,NULL,'Test Ed',0,0),
(13,'Incoming Letter','Government Entity','Abim DLG',NULL,NULL,'Test Abim Subject','2019-10-23','2019-10-21','2019-10-21','ABim00921','PPDA009921','cbirungi@ppda.go.ug',NULL,'Town Clerk',2,'registry','julianahebwa@gmail.com',NULL,NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Abim/Incoming/',NULL,NULL,0,0,0,0,0,'2019-10-21 07:40:40','2019-10-21 08:35:28',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Arthur Clive Birungi',0,0),
(14,'Incoming Letter','Government Entity','Adjumani DLG',NULL,NULL,'Timothy test','2019-10-22','2019-10-21','2019-10-21','Abim','PPDARef','cbirungi@ppda.go.ug','ED','Town Clerk',2,'registry','julianahebwa@gmail.com',NULL,NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Adjumani/Incoming Documents/',NULL,NULL,0,0,0,0,0,'2019-10-21 08:48:48','2019-10-21 08:48:54',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Arthur Clive Birungi',0,0),
(15,'Incoming Letter','Government Entity','Adjumani DLG',NULL,NULL,'sasbab','2019-10-23','2019-10-21','2019-10-21','sasab','sabsbab','cbirungi@ppda.go.ug','sasasa','Town Clerk',2,'registry','julianahebwa@gmail.com',NULL,NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Adjumani/Incoming Documents/',NULL,NULL,0,0,0,0,0,'2019-10-21 08:52:47','2019-10-21 08:52:51',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Arthur Clive Birungi',0,0),
(16,'Incoming Letter','Government Entity','Abim DLG',NULL,NULL,'Test sasab','2019-10-24','2019-10-21','2019-10-21','sasnansnan','nsnansann','tkasaga6@gmail.com','sasajsjaj','Auditor General',4,'registry','julianahebwa@gmail.com',NULL,NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Abim/Incoming/',NULL,NULL,0,0,0,0,0,'2019-10-21 08:54:34','2019-10-21 08:55:15',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Test Initiator',0,0),
(17,'Incoming Letter','Government Entity','Abim DLG',NULL,NULL,'Test Abim now','2019-10-09','2019-10-21','2019-10-21','Sender','Ppda','cbirungi@ppda.go.ug','Test','Chief Administrative Officer',2,'registry','julianahebwa@gmail.com',NULL,NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Abim/Incoming/',NULL,NULL,0,0,0,0,0,'2019-10-21 08:56:41','2019-10-30 05:54:48',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Arthur Clive Birungi',0,0),
(18,'Incoming Letter','Government Entity','Adjumani DLG',NULL,NULL,'Test Adjumani','2019-10-24','2019-10-21','2019-10-21','ppsa','sasaska','cbirungi@ppda.go.ug','sasaskaks','Chairman',3,'ed','julianahebwa@gmail.com',NULL,NULL,'julianahebwa@gmail.com','admin@ppda.go.ug','2019-10-22','Test Adju now.pdf','D:\\All Projects\\AppServer\\applications\\lms\\public\\uploads/incoming_letter_uploads/Test Adju now.pdf','admin@ppda.go.ug','2019-10-22','/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Adjumani/Incoming/','/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Adjumani/Incoming/Test Adju now.pdf',NULL,0,0,0,0,0,'2019-10-21 09:00:04','2019-10-22 11:51:47',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Arthur Clive Birungi',0,0),
(19,'Incoming Letter','Government Entity','Alebtong DLG',NULL,NULL,'sasas','2019-10-23','2019-10-21','2019-10-21','sasa','sasasa','tkasaga4@gmail.com','sasasa','Town Clerk',2,'registry','julianahebwa@gmail.com',NULL,NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Alebtong/Incoming/',NULL,NULL,0,0,0,0,0,'2019-10-21 09:04:09','2019-10-21 09:04:15',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Test Supervisor',0,0),
(20,'Incoming Letter','Non - Government Entity','NINA INTERIORS LTD',NULL,NULL,'sasa','2019-10-17','2019-10-21','2019-10-21','sasa','assas','timothykasaga@gmail.com','sasas','Chief Administrative Officer',3,'registry','julianahebwa@gmail.com',NULL,NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'DEFAULT',NULL,NULL,0,0,0,0,0,'2019-10-21 09:06:02','2019-10-21 09:06:06',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Test Ed',0,0),
(21,'Incoming Letter','Government Entity','Abim DLG',NULL,NULL,'Test ED ML','2019-10-23','2019-10-21','2019-10-21','jsjaj','sasansna','cbirungi@ppda.go.ug','asasa','Chief Administrative Officer',2,'registry','julianahebwa@gmail.com',NULL,NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Abim/Incoming/',NULL,NULL,0,0,0,0,0,'2019-10-21 09:10:39','2019-10-21 09:13:05',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Arthur Clive Birungi',0,0),
(22,'Incoming Letter','Government Entity','Agago DLG',NULL,NULL,'Test invoice mailing list','2019-10-16','2019-10-21','2019-10-21','jsajsjj','sjasjajsja','cbirungi@ppda.go.ug','nsansnansna','Town Clerk',2,'registry','julianahebwa@gmail.com',NULL,NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Agago/Incoming/',NULL,NULL,0,0,0,0,0,'2019-10-21 09:11:25','2019-10-21 09:16:56',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Arthur Clive Birungi',0,0),
(23,'Incoming Letter','Government Entity','Amnesty Commission',NULL,NULL,'Test management mailing list','2019-10-31','2019-10-21','2019-10-21','sasjajsa','snansnan','bopany@ppda.go.ug','absbabsabsba','Accounting Officer',2,'registry','julianahebwa@gmail.com',NULL,NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'/REGISTRY/PRIVATE REGISTRY FILES/COMMISSIONS/Amnesty Commission/incoming/',NULL,NULL,0,0,0,0,0,'2019-10-21 09:12:18','2019-10-21 09:17:56',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Opany Bridget',0,0),
(28,'Incoming Letter','Government Entity','Wakiso DLG',NULL,NULL,'Test subject 2','2019-10-17','2019-10-22','2019-10-23',NULL,NULL,'timothykasaga@gmail.com',NULL,'Chief Administrative Officer',1,'ed','julianahebwa@gmail.com',NULL,NULL,'julianahebwa@gmail.com','julianahebwa@gmail.com','2019-10-23','2019.10.17-Procurement and disposal reports for the month of September 2019.pdf','D:\\All Projects\\AppServer\\applications\\lms\\public\\uploads/incoming_letter_uploads/2019.10.17-Procurement and disposal reports for the month of September 2019.pdf','julianahebwa@gmail.com','2019-10-22',NULL,'/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Wakiso DLG/Incoming/2019.10.17-Procurement and disposal reports for the month of September 2019.pdf',NULL,0,0,0,0,0,'2019-10-22 09:08:38','2019-10-27 22:44:36','tkasaga6@gmail.com',NULL,NULL,NULL,0,NULL,NULL,NULL,'Test Ed',0,0),
(29,'Incoming Letter','Government Entity','Abim DLG',NULL,NULL,'Test Subject Now','2019-10-09','2019-10-24','2019-10-24',NULL,NULL,'timothykasaga@gmail.com',NULL,'Chief Administrative Officer',2,'registry','julianahebwa@gmail.com','Test Hr',NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'2019-10-24 06:58:50','2019-10-30 05:54:29',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Test Ed',0,0),
(30,'Incoming Letter','Government Entity','Adjumani DLG',NULL,NULL,'TEST','2019-10-26','2019-10-25','2019-10-25',NULL,NULL,NULL,NULL,NULL,2,'ed','julianahebwa@gmail.com','Test Hr',NULL,'julianahebwa@gmail.com','tkasaga6@gmail.com','2019-11-04','Test Abim now today now now.pdf','D:\\All Projects\\AppServer\\applications\\lms\\public\\uploads/incoming_letter_uploads/Test Abim now today now now.pdf','julianahebwa@gmail.com','2019-10-31',NULL,'/REGISTRY/PRIVATE REGISTRY FILES/Letters - Incoming Default/Test Abim now today now now.pdf',NULL,0,0,0,0,0,'2019-10-25 11:31:12','2019-11-04 04:36:11',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,0),
(34,'Incoming Letter','Government Entity','Abim DLG',NULL,NULL,'TIMOTHY TEST SUBJECT','2019-10-31','2019-10-31','2019-10-31','ABIM/0003','PPDA/1980','bturamye@ppda.go.ug','James Male','Executive Director',3,'registry','julianahebwa@gmail.com','Test Hr',NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'/REGISTRY/PRIVATE REGISTRY FILES/DISTRICT LOCAL GOVERNMENTS/Abim/Incoming/',NULL,NULL,0,0,0,0,0,'2019-10-31 06:13:48','2019-10-31 06:15:53',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Benson Turamye',0,0),
(35,'Memo - To ED',NULL,NULL,'tkasaga6@gmail.com','Test Initiator','TEST RESIGNATION','2019-10-31','2019-10-31','2019-10-31',NULL,NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,'ed','julianahebwa@gmail.com','Test Hr',NULL,'julianahebwa@gmail.com','julianahebwa@gmail.com','2019-10-31','IT Test Document Today New New Memo Now.pdf','D:\\All Projects\\AppServer\\applications\\lms\\public\\uploads/incoming_letter_uploads/IT Test Document Today New New Memo Now.pdf','julianahebwa@gmail.com','2019-10-31','DEFAULT_MEMO','/REGISTRY/PRIVATE REGISTRY FILES/PPDA INTERNAL CORRESPONDENCES/IT Test Document Today New New Memo Now.pdf',NULL,0,0,0,0,0,'2019-10-31 06:14:37','2019-10-31 06:24:31',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Test Hr',0,0),
(36,'Memo - To ED',NULL,NULL,'tkasaga6@gmail.com','Test Initiator','TEST MEMO EMAIL','2019-10-31','2019-10-31','2019-10-31',NULL,NULL,'timothykasaga@gmail.com',NULL,NULL,NULL,'ed','julianahebwa@gmail.com','Test Hr',NULL,'julianahebwa@gmail.com','tkasaga6@gmail.com','2019-11-04','IT Test Document Today New New Memo Now Nu.pdf','D:\\All Projects\\AppServer\\applications\\lms\\public\\uploads/incoming_letter_uploads/IT Test Document Today New New Memo Now Nu.pdf','julianahebwa@gmail.com','2019-10-31','DEFAULT_MEMO','/REGISTRY/PRIVATE REGISTRY FILES/PPDA INTERNAL CORRESPONDENCES/IT Test Document Today New New Memo Now Nu.pdf',NULL,0,0,0,0,0,'2019-10-31 06:26:08','2019-11-04 04:35:29',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Test Ed',0,0),
(37,'Memo - To ED',NULL,NULL,'timothykasaga@gmail.com','Test Ed','TEST','2019-10-31','2019-10-31','2019-10-31',NULL,NULL,'timothykasaga@gmail.com',NULL,NULL,NULL,'registry','julianahebwa@gmail.com','Test Hr',NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'DEFAULT_MEMO',NULL,NULL,0,0,0,0,0,'2019-10-31 06:32:13','2019-10-31 06:32:19',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Test Ed',0,0),
(38,'Memo - To ED',NULL,NULL,'tkasaga4@gmail.com','Test Supervisor','TEST','2019-10-31','2019-10-31','2019-10-31',NULL,NULL,'timothykasaga@gmail.com',NULL,NULL,NULL,'registry','julianahebwa@gmail.com','Test Hr',NULL,'julianahebwa@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'DEFAULT_MEMO',NULL,NULL,0,0,0,0,0,'2019-10-31 06:34:59','2019-10-31 06:56:02',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Test Ed',0,0),
(39,'Memo - To ED',NULL,NULL,'tkasaga6@gmail.com','Test Initiator','SUBJECT','2019-10-31','2019-10-31','2019-11-04',NULL,NULL,'timothykasaga@gmail.com',NULL,NULL,NULL,'registry','julianahebwa@gmail.com','Test Hr',NULL,'tkasaga6@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,'DEFAULT_MEMO',NULL,NULL,0,0,0,0,0,'2019-10-31 06:37:36','2019-11-04 04:30:16',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'Test Ed',0,0);

/*Table structure for table `internal_memos` */

DROP TABLE IF EXISTS `internal_memos`;

CREATE TABLE `internal_memos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `memo_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_user_full_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci,
  `memo_date` date DEFAULT NULL,
  `date_received` date DEFAULT NULL,
  `attention_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attention_to_full_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_que` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_user_full_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registry_entry_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registry_entry_date` datetime DEFAULT NULL,
  `eds_office_entry_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eds_office_entry_date` date DEFAULT NULL,
  `memo_file_name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `memo_file_path` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `memo_upload_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `memo_upload_date_time` date DEFAULT NULL,
  `edms_document_path_folder` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edms_document_path` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edms_document_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `internal_memos` */

insert  into `internal_memos`(`id`,`memo_type`,`from_user`,`from_user_full_name`,`subject`,`memo_date`,`date_received`,`attention_to`,`attention_to_full_name`,`current_que`,`created_by_user`,`created_by_user_full_name`,`registry_entry_user`,`registry_entry_date`,`eds_office_entry_user`,`eds_office_entry_date`,`memo_file_name`,`memo_file_path`,`memo_upload_user`,`memo_upload_date_time`,`edms_document_path_folder`,`edms_document_path`,`edms_document_id`,`created_at`,`updated_at`) values 
(1,'To ED','timothykasaga@gmail.com','Test Ed','TEST','2019-10-24','2019-10-31','tkasaga6@gmail.com','Test Initiator','registry',NULL,NULL,'julianahebwa@gmail.com','2019-10-25 11:49:35',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-25 11:20:07','2019-10-25 11:49:35'),
(3,'To ED','timothykasaga@gmail.com','Test Ed','TEST INTERNAL MEMO EMAIL','2019-10-25','2019-10-24','bturamye@ppda.go.ug','Benson Turamye','registry',NULL,NULL,'tkasaga6@gmail.com','2019-10-25 12:14:17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-25 12:14:12','2019-10-25 12:14:17'),
(4,'To ED','timothykasaga@gmail.com','Test Ed','TEST MEMO NOTIFICATION VERSION 2','2019-10-25','2019-10-25','timothykasaga@gmail.com','Test Ed','ed','tkasaga6@gmail.com','Test Initiator','tkasaga6@gmail.com','2019-10-25 12:19:36','tkasaga6@gmail.com','2019-10-25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-25 12:19:32','2019-10-25 13:22:46'),
(5,'To ED','tkasaga4@gmail.com','Test Supervisor','BABSAB','2019-10-16','2019-10-25','patrick.kakembo@gmail.com','Test Hod','ed','tkasaga6@gmail.com','Test Initiator','tkasaga6@gmail.com','2019-10-25 12:53:37','tkasaga6@gmail.com','2019-10-25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-25 12:20:49','2019-10-25 13:22:39'),
(6,'To ED','timothykasaga@gmail.com','Test Ed','NEW MEMO TEST','2019-10-29','2019-10-28','cbirungi@ppda.go.ug','Arthur Clive Birungi','ed','Test Initiator','Test Initiator','tkasaga6@gmail.com','2019-10-28 06:02:47','tkasaga6@gmail.com','2019-10-28','Test Timothy Doc Long Try.pdf','D:\\All Projects\\AppServer\\applications\\lms\\public\\uploads/incoming_letter_uploads/Test Timothy Doc Long Try.pdf','tkasaga6@gmail.com','2019-10-28',NULL,'/REGISTRY/PRIVATE REGISTRY FILES/PPDA INTERNAL CORRESPONDENCES/Test Timothy Doc Long Try.pdf',NULL,'2019-10-28 06:02:08','2019-10-28 06:05:03');

/*Table structure for table `letter_associations` */

DROP TABLE IF EXISTS `letter_associations`;

CREATE TABLE `letter_associations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `associate_letter_id` bigint(20) NOT NULL,
  `associate_letter_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `associated_letter_id` bigint(20) NOT NULL,
  `associated_letter_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `letter_associations` */

insert  into `letter_associations`(`id`,`associate_letter_id`,`associate_letter_type`,`associated_letter_id`,`associated_letter_type`,`created_at`,`updated_at`) values 
(11,15,'outgoing',5,'incoming',NULL,NULL),
(2,15,'outgoing',6,'incoming',NULL,NULL),
(3,11,'incoming',15,'outgoing',NULL,NULL),
(4,13,'outgoing',15,'outgoing',NULL,NULL),
(12,15,'outgoing',8,'incoming','2019-10-18 12:22:42','2019-10-18 12:22:42'),
(13,13,'outgoing',1,'incoming','2019-10-19 11:19:17','2019-10-19 11:19:17'),
(14,13,'outgoing',12,'incoming','2019-10-19 11:19:17','2019-10-19 11:19:17'),
(15,13,'outgoing',5,'incoming','2019-10-19 11:28:24','2019-10-19 11:28:24'),
(16,13,'outgoing',3,'incoming','2019-10-19 11:50:37','2019-10-19 11:50:37'),
(17,13,'outgoing',6,'incoming','2019-10-19 11:55:56','2019-10-19 11:55:56'),
(18,5,'incoming',3,'incoming','2019-10-19 11:57:45','2019-10-19 11:57:45'),
(19,5,'incoming',6,'incoming','2019-10-19 11:57:52','2019-10-19 11:57:52'),
(20,16,'outgoing',14,'outgoing','2019-10-19 14:24:49','2019-10-19 14:24:49'),
(21,16,'outgoing',2,'incoming','2019-10-19 14:48:02','2019-10-19 14:48:02'),
(22,16,'outgoing',9,'incoming','2019-10-19 14:48:02','2019-10-19 14:48:02'),
(23,16,'outgoing',11,'incoming','2019-10-19 14:48:02','2019-10-19 14:48:02'),
(24,27,'incoming',26,'incoming','2019-10-22 08:45:13','2019-10-22 08:45:13'),
(25,18,'incoming',14,'incoming','2019-10-22 11:49:27','2019-10-22 11:49:27'),
(26,18,'incoming',15,'incoming','2019-10-22 11:49:27','2019-10-22 11:49:27'),
(27,17,'incoming',1,'incoming','2019-10-23 07:45:32','2019-10-23 07:45:32'),
(28,17,'incoming',12,'incoming','2019-10-23 07:45:32','2019-10-23 07:45:32'),
(29,11,'incoming',2,'incoming','2019-10-28 16:31:39','2019-10-28 16:31:39'),
(30,6,'incoming',3,'incoming','2019-10-28 16:32:23','2019-10-28 16:32:23');

/*Table structure for table `letter_movements` */

DROP TABLE IF EXISTS `letter_movements`;

CREATE TABLE `letter_movements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `letter_id` bigint(20) DEFAULT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  `required_action` text COLLATE utf8mb4_unicode_ci,
  `deadline_for_action` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `acted_upon` tinyint(1) DEFAULT '0',
  `moveable_id` bigint(20) unsigned DEFAULT NULL,
  `moveable_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `letter_movements` */

insert  into `letter_movements`(`id`,`letter_id`,`to`,`to_name`,`from`,`from_name`,`from_department`,`to_department`,`date_sent`,`required_action`,`deadline_for_action`,`created_at`,`updated_at`,`acted_upon`,`moveable_id`,`moveable_type`) values 
(1,1,'admin@ppda.go.ug','Admin Emis','gkawere@ppda.go.ug','Geraldine Kawere','Corporate Affairs','Corporate Affairs','2019-10-11 08:00:49','Action required from Admin Emis','2019-10-31','2019-10-11 08:00:49','2019-10-11 08:05:24',1,NULL,NULL),
(2,1,'julianahebwa@gmail.com','Test Hr','admin@ppda.go.ug','Admin Emis','Corporate Affairs','Test Department','2019-10-11 00:00:00','Action required from Test HR','2019-10-11','2019-10-11 08:05:24','2019-10-16 10:45:33',1,NULL,NULL),
(3,6,'mojambo@ppda.go.ug','Moses Ojambo','bturamye@ppda.go.ug','Benson Turamye','Performance Monitoring','Capacity Building And Advisory Services','2019-10-11 12:27:38','Handle','2019-10-15','2019-10-11 12:27:38','2019-10-11 12:27:38',0,NULL,NULL),
(4,7,'pkakembo@ppda.go.ug','Patrick Kakembo','pkakembo@ppda.go.ug','Patrick Kakembo','Corporate Affairs','Corporate Affairs','2019-10-14 07:07:30','ED has said .....','2019-10-14','2019-10-14 07:07:30','2019-10-14 07:07:30',0,NULL,NULL),
(5,10,'mojambo@ppda.go.ug','Moses Ojambo','bturamye@ppda.go.ug','Benson Turamye','Performance Monitoring','Capacity Building And Advisory Services','2019-10-14 09:21:14','Please advise','2019-10-14','2019-10-14 09:21:14','2019-10-14 09:21:14',0,NULL,NULL),
(6,10,'usegawa@ppda.go.ug','Uthman Segawa','bturamye@ppda.go.ug','Benson Turamye','Performance Monitoring','Legal And Investigations','2019-10-14 09:21:14','Please advise','2019-10-14','2019-10-14 09:21:14','2019-10-14 09:21:14',0,NULL,NULL),
(7,8,'enamuddu@ppda.go.ug','Eva Namuddu','tkasaga6@gmail.com','Test Initiator','Test Department','Legal And Investigations','2019-10-14 12:13:18','Please attend','2019-10-14','2019-10-14 12:13:18','2019-10-14 12:13:18',0,NULL,NULL),
(8,8,'rmasajjage@ppda.go.ug','Rebecca Masajjage','tkasaga6@gmail.com','Test Initiator','Test Department','Legal And Investigations','2019-10-14 12:13:18','Please attend','2019-10-14','2019-10-14 12:13:18','2019-10-14 12:13:18',0,NULL,NULL),
(9,11,'jkaggwa@ppda.go.ug','Jenipher Kaggwa','julianahebwa@gmail.com','Test Hr','Test Department','Corporate Affairs','2019-10-16 08:55:36','Please take action now','2019-10-16','2019-10-16 08:55:36','2019-10-16 08:55:36',0,NULL,NULL),
(10,1,'julianahebwa@gmail.com','Test Hr','Test Hr','Test Hr','Test Department','Test Department','2019-10-16 00:00:00','Action required','2019-10-16','2019-10-16 10:45:33','2019-10-16 10:46:56',1,NULL,NULL),
(11,1,'julianahebwa@gmail.com','Test Hr',NULL,'Test Hr','Test Department','Test Department','2019-10-16 00:00:00','asbabsa','2019-10-16','2019-10-16 10:46:56','2019-10-16 10:47:27',1,NULL,NULL),
(12,1,'julianahebwa@gmail.com','Test Hr',NULL,'Test Hr','Test Department','Test Department','2019-10-16 00:00:00','asbabsa','2019-10-16','2019-10-16 10:47:27','2019-10-16 10:49:29',1,NULL,NULL),
(13,1,'julianahebwa@gmail.com','Test Hr','julianahebwa@gmail.com','Test Hr','Test Department','Test Department','2019-10-16 00:00:00','Test HR action','2019-10-16','2019-10-16 10:49:29','2019-10-16 10:52:26',1,NULL,NULL),
(14,1,'julianahebwa@gmail.com','Test Hr','julianahebwa@gmail.com','Test Hr','Test Department','Test Department','2019-10-16 00:00:00','Action','2019-10-16','2019-10-16 10:52:26','2019-10-16 10:53:26',1,NULL,NULL),
(15,1,'julianahebwa@gmail.com','Test Hr','julianahebwa@gmail.com','Test Hr','Test Department','Test Department','2019-10-16 00:00:00','Action','2019-10-16','2019-10-16 10:53:26','2019-10-16 11:39:42',1,NULL,NULL),
(16,1,'gkawere@ppda.go.ug','Geraldine Kawere','julianahebwa@gmail.com','Test Hr','Test Department','Corporate Affairs','2019-10-16 00:00:00','This was sent to me by mistake','2019-10-16','2019-10-16 11:39:42','2019-10-16 11:39:42',0,NULL,NULL),
(17,12,'admin@ppda.go.ug','Admin Emis','gkawere@ppda.go.ug','Geraldine Kawere','Corporate Affairs','Corporate Affairs','2019-10-16 11:43:41','Take action','2019-10-16','2019-10-16 11:43:41','2019-10-16 11:44:36',1,NULL,NULL),
(18,12,'gkawere@ppda.go.ug','Geraldine Kawere','admin@ppda.go.ug','Admin Emis','Corporate Affairs','Corporate Affairs','2019-10-16 00:00:00','Returned: Returned from Admin Emis','2019-10-16','2019-10-16 11:46:32','2019-10-16 11:46:32',0,NULL,NULL),
(19,9,'jahebwa@ppda.go.ug','Julian Ahebwa','julianahebwa@gmail.com','Test Hr','Test Department','Corporate Affairs','2019-10-16 12:02:39','Action required from Test HR','2019-10-16','2019-10-16 12:02:39','2019-10-16 12:08:32',1,NULL,NULL),
(20,9,'jkaggwa@ppda.go.ug','Jenipher Kaggwa','jahebwa@ppda.go.ug','Julian Ahebwa','Corporate Affairs','Corporate Affairs','2019-10-16 00:00:00','Action from Jennifer','2019-10-16','2019-10-16 12:08:32','2019-10-16 12:09:42',1,NULL,NULL),
(21,9,'jahebwa@ppda.go.ug','Julian Ahebwa','jkaggwa@ppda.go.ug','Jenipher Kaggwa','Corporate Affairs','Corporate Affairs','2019-10-16 00:00:00','Returned: Returned from Jenni','2019-10-16','2019-10-16 12:09:42','2019-10-16 12:10:41',1,NULL,NULL),
(22,9,'julianahebwa@gmail.com','Test Hr','jahebwa@ppda.go.ug','Julian Ahebwa','Corporate Affairs','Test Department','2019-10-16 00:00:00','Returned: Returned from Julian Ahebwa','2019-10-16','2019-10-16 12:10:41','2019-10-16 12:10:41',0,NULL,NULL),
(23,2,'rtumuhairwe@ppda.go.ug','Ronald Tumuhairwe','julianahebwa@gmail.com','Test Hr','Test Department','Capacity Building And Advisory Services','2019-10-16 17:20:07','Test department sending','2019-10-16','2019-10-16 17:20:07','2019-10-16 17:20:07',0,NULL,NULL),
(24,2,'rkemigisa@ppda.go.ug','Ronah Kemigisa','julianahebwa@gmail.com','Test Hr','Test Department','Capacity Building And Advisory Services','2019-10-16 17:20:07','Test department sending','2019-10-16','2019-10-16 17:20:07','2019-10-16 17:20:07',0,NULL,NULL),
(25,2,'hdalia@ppda.go.ug','Hassam Dalia','julianahebwa@gmail.com','Test Hr','Test Department','Performance Monitoring','2019-10-16 17:20:07','Test department sending','2019-10-16','2019-10-16 17:20:07','2019-10-16 17:20:07',0,NULL,NULL),
(26,2,'emuhumuza@ppda.go.ug','Edwin Muhumuza','julianahebwa@gmail.com','Test Hr','Test Department','Corporate Affairs','2019-10-16 17:20:07','Test department sending','2019-10-16','2019-10-16 17:20:07','2019-10-16 17:20:07',0,NULL,NULL),
(27,2,'robasoni@ppda.go.ug','Richard Obasoni','julianahebwa@gmail.com','Test Hr','Test Department','Operations','2019-10-16 17:20:07','Test department sending','2019-10-16','2019-10-16 17:20:07','2019-10-16 17:20:07',0,NULL,NULL),
(28,2,'sabamu@ppda.go.ug','Sheila Abamu','julianahebwa@gmail.com','Test Hr','Test Department','Legal And Investigations','2019-10-16 17:20:07','Test department sending','2019-10-16','2019-10-16 17:20:07','2019-10-16 17:20:07',0,NULL,NULL),
(29,2,'jkallemera@ppda.go.ug','John Kallemera','julianahebwa@gmail.com','Test Hr','Test Department','MAC','2019-10-16 17:20:07','Test department sending','2019-10-16','2019-10-16 17:20:07','2019-10-16 17:20:07',0,NULL,NULL),
(30,2,'rtumuhairwe@ppda.go.ug','Ronald Tumuhairwe','julianahebwa@gmail.com','Test Hr','Test Department','Capacity Building And Advisory Services','2019-10-16 17:20:48','Test department sending','2019-10-16','2019-10-16 17:20:48','2019-10-16 17:20:48',0,NULL,NULL),
(31,2,'rkemigisa@ppda.go.ug','Ronah Kemigisa','julianahebwa@gmail.com','Test Hr','Test Department','Capacity Building And Advisory Services','2019-10-16 17:20:48','Test department sending','2019-10-16','2019-10-16 17:20:48','2019-10-16 17:20:48',0,NULL,NULL),
(32,2,'hdalia@ppda.go.ug','Hassam Dalia','julianahebwa@gmail.com','Test Hr','Test Department','Performance Monitoring','2019-10-16 17:20:48','Test department sending','2019-10-16','2019-10-16 17:20:48','2019-10-16 17:20:48',0,NULL,NULL),
(33,2,'emuhumuza@ppda.go.ug','Edwin Muhumuza','julianahebwa@gmail.com','Test Hr','Test Department','Corporate Affairs','2019-10-16 17:20:48','Test department sending','2019-10-16','2019-10-16 17:20:48','2019-10-16 17:20:48',0,NULL,NULL),
(34,2,'robasoni@ppda.go.ug','Richard Obasoni','julianahebwa@gmail.com','Test Hr','Test Department','Operations','2019-10-16 17:20:48','Test department sending','2019-10-16','2019-10-16 17:20:48','2019-10-16 17:20:48',0,NULL,NULL),
(35,2,'sabamu@ppda.go.ug','Sheila Abamu','julianahebwa@gmail.com','Test Hr','Test Department','Legal And Investigations','2019-10-16 17:20:48','Test department sending','2019-10-16','2019-10-16 17:20:48','2019-10-16 17:20:48',0,NULL,NULL),
(36,2,'jkallemera@ppda.go.ug','John Kallemera','julianahebwa@gmail.com','Test Hr','Test Department','MAC','2019-10-16 17:20:48','Test department sending','2019-10-16','2019-10-16 17:20:48','2019-10-16 17:20:48',0,NULL,NULL),
(37,2,'rtumuhairwe@ppda.go.ug','Ronald Tumuhairwe','julianahebwa@gmail.com','Test Hr','Test Department','Capacity Building And Advisory Services','2019-10-16 17:27:02','Test department sending','2019-10-16','2019-10-16 17:27:02','2019-10-16 17:27:02',0,NULL,NULL),
(38,2,'rkemigisa@ppda.go.ug','Ronah Kemigisa','julianahebwa@gmail.com','Test Hr','Test Department','Capacity Building And Advisory Services','2019-10-16 17:27:02','Test department sending','2019-10-16','2019-10-16 17:27:03','2019-10-16 17:27:03',0,NULL,NULL),
(39,2,'hdalia@ppda.go.ug','Hassam Dalia','julianahebwa@gmail.com','Test Hr','Test Department','Performance Monitoring','2019-10-16 17:27:02','Test department sending','2019-10-16','2019-10-16 17:27:03','2019-10-16 17:27:03',0,NULL,NULL),
(40,2,'emuhumuza@ppda.go.ug','Edwin Muhumuza','julianahebwa@gmail.com','Test Hr','Test Department','Corporate Affairs','2019-10-16 17:27:02','Test department sending','2019-10-16','2019-10-16 17:27:03','2019-10-16 17:27:03',0,NULL,NULL),
(41,2,'robasoni@ppda.go.ug','Richard Obasoni','julianahebwa@gmail.com','Test Hr','Test Department','Operations','2019-10-16 17:27:02','Test department sending','2019-10-16','2019-10-16 17:27:03','2019-10-16 17:27:03',0,NULL,NULL),
(42,2,'sabamu@ppda.go.ug','Sheila Abamu','julianahebwa@gmail.com','Test Hr','Test Department','Legal And Investigations','2019-10-16 17:27:02','Test department sending','2019-10-16','2019-10-16 17:27:03','2019-10-16 17:27:03',0,NULL,NULL),
(43,2,'jkallemera@ppda.go.ug','John Kallemera','julianahebwa@gmail.com','Test Hr','Test Department','MAC','2019-10-16 17:27:02','Test department sending','2019-10-16','2019-10-16 17:27:03','2019-10-16 17:27:03',0,NULL,NULL),
(44,2,'rtumuhairwe@ppda.go.ug','Ronald Tumuhairwe','julianahebwa@gmail.com','Test Hr','Test Department','Capacity Building And Advisory Services','2019-10-16 17:40:47','Test department sending','2019-10-16','2019-10-16 17:40:47','2019-10-16 17:40:47',0,NULL,NULL),
(45,2,'rkemigisa@ppda.go.ug','Ronah Kemigisa','julianahebwa@gmail.com','Test Hr','Test Department','Capacity Building And Advisory Services','2019-10-16 17:40:47','Test department sending','2019-10-16','2019-10-16 17:40:47','2019-10-16 17:40:47',0,NULL,NULL),
(46,2,'hdalia@ppda.go.ug','Hassam Dalia','julianahebwa@gmail.com','Test Hr','Test Department','Performance Monitoring','2019-10-16 17:40:47','Test department sending','2019-10-16','2019-10-16 17:40:47','2019-10-16 17:40:47',0,NULL,NULL),
(47,2,'emuhumuza@ppda.go.ug','Edwin Muhumuza','julianahebwa@gmail.com','Test Hr','Test Department','Corporate Affairs','2019-10-16 17:40:47','Test department sending','2019-10-16','2019-10-16 17:40:47','2019-10-16 17:40:47',0,NULL,NULL),
(48,2,'robasoni@ppda.go.ug','Richard Obasoni','julianahebwa@gmail.com','Test Hr','Test Department','Operations','2019-10-16 17:40:47','Test department sending','2019-10-16','2019-10-16 17:40:47','2019-10-16 17:40:47',0,NULL,NULL),
(49,2,'sabamu@ppda.go.ug','Sheila Abamu','julianahebwa@gmail.com','Test Hr','Test Department','Legal And Investigations','2019-10-16 17:40:47','Test department sending','2019-10-16','2019-10-16 17:40:47','2019-10-16 17:40:47',0,NULL,NULL),
(50,2,'jkallemera@ppda.go.ug','John Kallemera','julianahebwa@gmail.com','Test Hr','Test Department','MAC','2019-10-16 17:40:47','Test department sending','2019-10-16','2019-10-16 17:40:47','2019-10-16 17:40:47',0,NULL,NULL),
(51,3,'rkemigisa@ppda.go.ug','Ronah Kemigisa','julianahebwa@gmail.com','Test Hr','Test Department','Capacity Building And Advisory Services','2019-10-16 20:02:01','Action required from you','2019-10-16','2019-10-16 20:02:01','2019-10-16 20:02:01',0,NULL,NULL),
(52,3,'rkemigisa@ppda.go.ug','Ronah Kemigisa','julianahebwa@gmail.com','Test Hr','Test Department','Capacity Building And Advisory Services','2019-10-16 20:02:01','Action required from you','2019-10-16','2019-10-16 20:02:01','2019-10-16 20:02:01',0,NULL,NULL),
(53,3,'hdalia@ppda.go.ug','Hassam Dalia','julianahebwa@gmail.com','Test Hr','Test Department','Performance Monitoring','2019-10-16 20:02:01','Action required from you','2019-10-16','2019-10-16 20:02:01','2019-10-16 20:02:01',0,NULL,NULL),
(54,3,'marea@ppda.go.ug','Morris Area','julianahebwa@gmail.com','Test Hr','Test Department','Corporate Affairs','2019-10-16 20:02:01','Action required from you','2019-10-16','2019-10-16 20:02:01','2019-10-16 20:02:01',0,NULL,NULL),
(55,3,'robasoni@ppda.go.ug','Richard Obasoni','julianahebwa@gmail.com','Test Hr','Test Department','Operations','2019-10-16 20:02:01','Action required from you','2019-10-16','2019-10-16 20:02:01','2019-10-16 20:02:01',0,NULL,NULL),
(56,3,'sabamu@ppda.go.ug','Sheila Abamu','julianahebwa@gmail.com','Test Hr','Test Department','Legal And Investigations','2019-10-16 20:02:01','Action required from you','2019-10-16','2019-10-16 20:02:02','2019-10-16 20:02:02',0,NULL,NULL),
(57,3,'enamuddu@ppda.go.ug','Eva Namuddu','julianahebwa@gmail.com','Test Hr','Test Department','MAC','2019-10-16 20:02:01','Action required from you','2019-10-16','2019-10-16 20:02:02','2019-10-16 20:02:02',0,NULL,NULL),
(58,NULL,'tkasaga6@gmail.com','Test Initiator','tkasaga4@gmail.com','Test Supervisor','Corporate Affairs','Corporate Affairs','2019-10-27 22:44:36','Action requireed','2019-10-27','2019-10-27 22:44:36','2019-10-28 00:17:57',1,28,'App\\IncomingLetter'),
(59,NULL,'timothykasaga@gmail.com','Test Ed','timothykasaga@gmail.com','Test Ed','Corporate Affairs','Corporate Affairs','2019-10-27 22:52:49','Test action memo','2019-10-27','2019-10-27 22:52:49','2019-10-27 22:52:49',0,5,'App\\InternalMemo'),
(60,NULL,'timothykasaga@gmail.com','Test Ed','timothykasaga@gmail.com','Test Ed','Corporate Affairs','Corporate Affairs','2019-10-27 22:53:03','Test action memo','2019-10-27','2019-10-27 22:53:03','2019-10-27 22:53:03',0,5,'App\\InternalMemo'),
(61,NULL,'timothykasaga@gmail.com','Test Ed','timothykasaga@gmail.com','Test Ed','Corporate Affairs','Corporate Affairs','2019-10-27 22:53:35','Test action memo','2019-10-27','2019-10-27 22:53:35','2019-10-28 05:58:54',1,5,'App\\InternalMemo'),
(62,28,'timothykasaga@gmail.com','Test Ed','tkasaga6@gmail.com','Test Initiator','Corporate Affairs','Corporate Affairs','2019-10-28 00:00:00','Action required ED','2019-10-28','2019-10-28 00:11:26','2019-10-28 00:11:26',0,NULL,NULL),
(63,28,'timothykasaga@gmail.com','Test Ed','tkasaga6@gmail.com','Test Initiator','Corporate Affairs','Corporate Affairs','2019-10-28 00:00:00','Action from ED required','2019-10-28','2019-10-28 00:17:57','2019-10-28 06:17:24',1,28,'App\\IncomingLetter'),
(64,NULL,'julianahebwa@gmail.com','Test Hr','timothykasaga@gmail.com','Test Ed','Corporate Affairs','Corporate Affairs','2019-10-28 00:00:00','Action','2019-10-28','2019-10-28 05:49:54','2019-10-28 05:49:54',0,5,'App/IntenalMemo'),
(65,NULL,'timothykasaga@gmail.com','Test Ed','timothykasaga@gmail.com','Test Ed','Corporate Affairs','Corporate Affairs','2019-10-28 00:00:00','Returned: Action required','2019-10-28','2019-10-28 05:58:54','2019-10-28 06:15:02',1,5,'App\\InternalMemo'),
(66,NULL,'julianahebwa@gmail.com','Test Hr','timothykasaga@gmail.com','Test Ed','Corporate Affairs','Corporate Affairs','2019-10-28 00:00:00','sasa','2019-10-28','2019-10-28 05:59:55','2019-10-28 05:59:55',0,5,'App/IntenalMemo'),
(67,NULL,'timothykasaga@gmail.com','Test Ed','julianahebwa@gmail.com','Test Hr','Corporate Affairs','Corporate Affairs','2019-10-28 06:06:16','Action required from ED','2019-10-28','2019-10-28 06:06:16','2019-10-28 06:12:31',1,6,'App\\InternalMemo'),
(68,NULL,'tkasaga4@gmail.com','Test Supervisor','timothykasaga@gmail.com','Test Ed','Corporate Affairs','Corporate Affairs','2019-10-28 00:00:00','Action required from Test Supervisor','2019-10-28','2019-10-28 06:07:34','2019-10-28 06:07:34',0,6,'App/IntenalMemo'),
(69,NULL,'tkasaga4@gmail.com','Test Supervisor','timothykasaga@gmail.com','Test Ed','Corporate Affairs','Corporate Affairs','2019-10-28 00:00:00','Action required from Supervisor Again','2019-10-28','2019-10-28 06:10:20','2019-10-28 06:10:20',0,6,'App/InternalMemo'),
(70,NULL,'tkasaga4@gmail.com','Test Supervisor','timothykasaga@gmail.com','Test Ed','Corporate Affairs','Corporate Affairs','2019-10-28 00:00:00','Action required','2019-10-28','2019-10-28 06:12:31','2019-10-28 06:12:31',0,6,'App\\InternalMemo'),
(71,NULL,'patrick.kakembo@gmail.com','Test Hod','timothykasaga@gmail.com','Test Ed','Corporate Affairs','Corporate Affairs','2019-10-28 00:00:00','Test','2019-10-28','2019-10-28 06:15:02','2019-10-28 06:15:02',0,5,'App\\InternalMemo'),
(72,NULL,'tkasaga4@gmail.com','Test Supervisor','timothykasaga@gmail.com','Test Ed','Corporate Affairs','Corporate Affairs','2019-10-28 00:00:00','Action required','2019-10-28','2019-10-28 06:17:24','2019-10-28 06:17:24',0,28,'App\\IncomingLetter');

/*Table structure for table `letter_type_field_values` */

DROP TABLE IF EXISTS `letter_type_field_values`;

CREATE TABLE `letter_type_field_values` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `letter_id` int(11) DEFAULT NULL,
  `letter_type_id` int(11) DEFAULT NULL,
  `letter_type_field_id` int(11) NOT NULL,
  `field_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `letter_type_field_values` */

insert  into `letter_type_field_values`(`id`,`letter_id`,`letter_type_id`,`letter_type_field_id`,`field_value`,`created_at`,`updated_at`) values 
(1,1,1,1,'10000','2019-09-16 11:34:02','2019-09-16 11:34:02'),
(2,1,1,2,'4000','2019-09-16 11:34:03','2019-09-16 11:34:03'),
(3,1,1,3,'UGX','2019-09-16 11:34:03','2019-09-16 11:34:03'),
(4,11,1,1,'80000','2019-09-16 11:44:38','2019-09-16 11:44:38'),
(5,11,1,2,'200','2019-09-16 11:44:38','2019-09-16 11:44:38'),
(6,11,1,3,'UGX','2019-09-16 11:44:38','2019-09-16 11:44:38'),
(7,10,1,1,'80000','2019-09-16 11:47:06','2019-09-16 11:47:06'),
(8,10,1,2,'12','2019-09-16 11:47:07','2019-09-16 11:47:07'),
(9,10,1,3,'UGX','2019-09-16 11:47:07','2019-09-16 11:47:07'),
(10,5,1,1,'80000','2019-09-16 11:47:29','2019-09-16 11:47:29'),
(11,5,1,2,'14','2019-09-16 11:47:29','2019-09-16 11:47:29'),
(12,5,1,3,'UGX','2019-09-16 11:47:30','2019-09-16 11:47:30'),
(13,4,1,1,'80000','2019-09-16 11:47:56','2019-09-16 11:47:56'),
(14,4,1,2,'123','2019-09-16 11:47:56','2019-09-16 11:47:56'),
(15,4,1,3,'UGX','2019-09-16 11:47:57','2019-09-16 11:47:57'),
(16,2,1,1,'1200','2019-09-16 11:48:25','2019-09-16 11:48:25'),
(17,2,1,2,'12','2019-09-16 11:48:25','2019-09-16 11:48:25'),
(18,2,1,3,'UGX','2019-09-16 11:48:25','2019-09-16 11:48:25'),
(19,3,1,1,'80000','2019-09-16 11:49:03','2019-09-16 11:49:03'),
(20,3,1,2,'500','2019-09-16 11:49:03','2019-09-16 11:49:03'),
(21,3,1,3,'UGX','2019-09-16 11:49:03','2019-09-16 11:49:03'),
(22,6,1,1,'80000','2019-09-16 11:49:32','2019-09-16 11:49:32'),
(23,6,1,2,'300','2019-09-16 11:49:32','2019-09-16 11:49:32'),
(24,6,1,3,'UGX','2019-09-16 11:49:32','2019-09-16 11:49:32'),
(25,7,1,1,'80000','2019-09-16 11:50:01','2019-09-16 11:50:01'),
(26,7,1,2,'700','2019-09-16 11:50:01','2019-09-16 11:50:01'),
(27,7,1,3,'UGX','2019-09-16 11:50:02','2019-09-16 11:50:02'),
(28,8,1,1,'80000','2019-09-16 11:50:22','2019-09-16 11:50:22'),
(29,8,1,2,'540','2019-09-16 11:50:22','2019-09-16 11:50:22'),
(30,8,1,3,'UGX','2019-09-16 11:50:22','2019-09-16 11:50:22'),
(31,14,1,1,'123456','2019-09-19 08:13:45','2019-09-19 08:13:45'),
(32,14,1,2,'500000000','2019-09-19 08:13:45','2019-09-19 08:13:45'),
(33,14,1,3,'UGX','2019-09-19 08:13:45','2019-09-19 08:13:45'),
(34,28,1,1,'INV900','2019-10-23 07:37:52','2019-10-23 07:37:52'),
(35,28,1,2,'710000','2019-10-23 07:37:52','2019-10-23 07:37:52'),
(36,28,1,3,'UGX','2019-10-23 07:37:52','2019-10-23 07:37:52');

/*Table structure for table `letter_type_fields` */

DROP TABLE IF EXISTS `letter_type_fields`;

CREATE TABLE `letter_type_fields` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `letter_type_id` bigint(20) unsigned NOT NULL,
  `field_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `letter_type_fields_letter_type_id_foreign` (`letter_type_id`),
  CONSTRAINT `letter_type_fields_letter_type_id_foreign` FOREIGN KEY (`letter_type_id`) REFERENCES `letter_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `letter_type_fields` */

insert  into `letter_type_fields`(`id`,`letter_type_id`,`field_name`,`field_type`,`field_order`,`created_at`,`updated_at`) values 
(1,1,'Invoice Number','Text',1,'2019-09-15 23:22:24','2019-09-15 23:22:24'),
(2,1,'Invoice Value','Number',2,'2019-09-15 23:22:24','2019-09-15 23:22:24'),
(3,1,'Invoice Currency','Text',3,'2019-09-15 23:22:24','2019-09-15 23:22:24');

/*Table structure for table `letter_types` */

DROP TABLE IF EXISTS `letter_types`;

CREATE TABLE `letter_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `letter_type_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edms_doc_type_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `letter_types_edms_doc_type_id_foreign` (`edms_doc_type_id`),
  CONSTRAINT `letter_types_edms_doc_type_id_foreign` FOREIGN KEY (`edms_doc_type_id`) REFERENCES `edms_doc_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `letter_types` */

insert  into `letter_types`(`id`,`letter_type_name`,`edms_doc_type_id`,`created_at`,`updated_at`) values 
(1,'Invoice',1,'2019-09-15 23:22:24','2019-09-15 23:22:24'),
(2,'Monthly/Quarterly Report',2,'2019-09-19 08:37:03','2019-09-19 08:55:38'),
(3,'Incoming Letter-General',3,'2019-09-19 08:40:33','2019-09-19 08:55:27'),
(4,'Incoming Letter-Report',1,'2019-09-19 08:41:36','2019-09-19 08:50:29'),
(5,'Incoming Letter-Procurement Plan',3,'2019-09-19 08:42:09','2019-09-19 08:55:14'),
(6,'Incoming Letter-Report',3,'2019-09-19 08:43:25','2019-09-19 08:54:55'),
(7,'Incoming Letter-CC Minutes',3,'2019-09-19 08:45:15','2019-09-19 08:54:46'),
(8,'Incoming Letter-Administrative Review Issues',3,'2019-09-19 08:46:20','2019-09-19 08:54:34'),
(9,'Incoming Letter-Investigation Issue',3,'2019-09-19 08:46:38','2019-09-19 08:54:26'),
(10,'Incoming Letter-Tribunal Issue',3,'2019-09-19 08:46:52','2019-09-19 08:54:20'),
(11,'Incoming Letter-Court Issue',3,'2019-09-19 08:47:08','2019-09-19 08:53:49'),
(12,'Incoming Letter-Suspension',3,'2019-09-19 08:47:31','2019-09-19 08:54:07');

/*Table structure for table `mailing_lists` */

DROP TABLE IF EXISTS `mailing_lists`;

CREATE TABLE `mailing_lists` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `list_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_addresses` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `mailing_lists` */

insert  into `mailing_lists`(`id`,`list_name`,`email_addresses`,`created_at`,`updated_at`) values 
(4,'Registry_Mailing_List','it@ppda.go.ug,timothykasaga@gmail.com','2019-10-11 12:19:02','2019-10-25 12:13:29'),
(5,'Invoices_Mailing_List','it@ppda.go.ug,timothykasaga@gmail.com','2019-10-11 12:19:18','2019-10-25 12:13:12'),
(6,'ED_Office_Mailing_List','it@ppda.go.ug,timothykasaga@gmail.com','2019-10-11 12:19:55','2019-10-25 12:13:04'),
(7,'Management_Mailing_List','it@ppda.go.ug,timothykasaga@gmail.com','2019-10-11 12:20:28','2019-10-25 12:13:20');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2019_10_17_075259_create_letter_associations_table',1),
(2,'2019_10_23_090640_create_global_values_table',2),
(3,'2019_10_25_080251_create_internal_memos_table',3);

/*Table structure for table `outgoing_letters` */

DROP TABLE IF EXISTS `outgoing_letters`;

CREATE TABLE `outgoing_letters` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `origin_department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `origin_department_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `origin_department_unit_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `org_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entity_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci,
  `date_sent` date DEFAULT NULL,
  `date_received` date DEFAULT NULL,
  `ppda_reference_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recipient_reference_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recipient_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recipient_entity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signatory_username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signatory` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signatory_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_que` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `letter_type_id` int(11) DEFAULT NULL,
  `outgoing_letter_file_name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `outgoing_letter_file_path` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `outgoing_letter_upload_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upload_date_time` date DEFAULT NULL,
  `edms_document_path_folder` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edms_document_path` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edms_document_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `outgoing_letter_receipt_file_name` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `outgoing_letter_receipt_file_path` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `outgoing_letter_receipt_upload_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upload_receipt_date_time` date DEFAULT NULL,
  `edms_document_receipt_path` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cbas_tag` tinyint(1) DEFAULT '0',
  `pm_tag` tinyint(1) DEFAULT '0',
  `advisory_tag` tinyint(1) DEFAULT '0',
  `legal_tag` tinyint(1) DEFAULT '0',
  `corp_tag` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `private_file` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `outgoing_letters` */

insert  into `outgoing_letters`(`id`,`origin_department`,`origin_department_unit`,`origin_department_unit_name`,`org_type`,`entity_name`,`subject`,`date_sent`,`date_received`,`ppda_reference_number`,`recipient_reference_number`,`recipient_title`,`recipient_entity`,`signatory_username`,`signatory`,`signatory_title`,`current_que`,`letter_type_id`,`outgoing_letter_file_name`,`outgoing_letter_file_path`,`outgoing_letter_upload_user`,`upload_date_time`,`edms_document_path_folder`,`edms_document_path`,`edms_document_id`,`outgoing_letter_receipt_file_name`,`outgoing_letter_receipt_file_path`,`outgoing_letter_receipt_upload_user`,`upload_receipt_date_time`,`edms_document_receipt_path`,`cbas_tag`,`pm_tag`,`advisory_tag`,`legal_tag`,`corp_tag`,`created_at`,`updated_at`,`private_file`) values 
(15,'Capacity Building',NULL,NULL,'Non - Government Entity','UWEAL','Invitation for a one day training workshop, Women in Busniss','2019-10-09','2019-10-14','PPDA/UWEAL/012',NULL,'Other',NULL,NULL,'Moses Ojambo','Ag. Executive Director','reception',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'2019-10-14 09:28:17','2019-10-30 05:53:42',1),
(16,'Legal and Investigations',NULL,NULL,'Government Entity','National Social Security Fund','Testing 1234','2019-10-11','2019-10-14','PPDATest','Testing2019','Director',NULL,NULL,'ED PPDA','Executive Director','reception',NULL,'Test Deletion Now.pdf','D:\\All Projects\\AppServer\\applications\\lms\\public\\uploads/outgoing_letter_uploads/Test Deletion Now.pdf','julianahebwa@gmail.com','2019-10-31','/REGISTRY/PRIVATE REGISTRY FILES/MDAs/National Social Security Fund/Outgoing/','/REGISTRY/PRIVATE REGISTRY FILES/MDAs/National Social Security Fund/Outgoing/Test Deletion Now.pdf',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'2019-10-14 12:03:35','2019-10-31 06:07:28',0),
(17,'Advisory',NULL,NULL,'Government Entity','Abim DLG','Test','2019-10-31','2019-10-31','PPDA/0021',NULL,'Chief Administrative Officer',NULL,'bturamye@ppda.go.ug','Timothy Kasaga','Executive Director','reception',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'2019-10-31 07:03:22','2019-10-31 10:51:55',0),
(18,'Capacity Building',NULL,NULL,'Government Entity','Abim DLG','Now','2019-10-31','2019-10-31','PPDA/0021201',NULL,'Town Clerk',NULL,'bturamye@ppda.go.ug','Benson Turamye','Ag. Executive Director','reception',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'2019-10-31 08:18:37','2019-10-31 10:47:36',0),
(19,'Performance Monitoring','UNIT_7','Test','Government Entity','Abim DLG','subject','2019-10-31','1970-01-01','test',NULL,NULL,NULL,'issebabi@ppda.go.ug','Isaac Ssebabi','For Executive Director','reception',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,'2019-10-31 09:20:41','2019-11-04 05:13:02',0);

/*Table structure for table `outgoing_letters_letter_type_field_values` */

DROP TABLE IF EXISTS `outgoing_letters_letter_type_field_values`;

CREATE TABLE `outgoing_letters_letter_type_field_values` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `outgoing_letter_id` int(11) DEFAULT NULL,
  `letter_type_id` int(11) DEFAULT NULL,
  `letter_type_field_id` int(11) NOT NULL,
  `field_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `outgoing_letters_letter_type_field_values` */

insert  into `outgoing_letters_letter_type_field_values`(`id`,`outgoing_letter_id`,`letter_type_id`,`letter_type_field_id`,`field_value`,`created_at`,`updated_at`) values 
(1,1,1,1,'INV00219','2019-09-16 01:21:37','2019-09-16 01:21:37'),
(2,1,1,2,'120000','2019-09-16 01:21:37','2019-09-16 01:21:37'),
(3,1,1,3,'UGX','2019-09-16 01:21:38','2019-09-16 01:21:38');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

/* Procedure structure for procedure `GetIncomingLetterAssociations` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetIncomingLetterAssociations` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `GetIncomingLetterAssociations`(IN paramLetterId INTEGER(11))
BEGIN
	
	-- case 1 it's an associate letter
	SELECT 
	L.id, L.org_type, L.entity_name, L.subject, LT.letter_type_name, 'Incoming' AS letter_category,
	L.date_sent AS letter_date, L.date_received_ppda AS date_received, L.sender_reference_number AS entity_reference_number, L.ppda_reference_number, L.incoming_letter_file_name AS attached_doc 
	 FROM letter_associations LA 
	INNER JOIN incoming_letters L ON LA.associated_letter_id = L.id
	LEFT JOIN letter_types LT ON L.letter_type_id = LT.id
	WHERE LA.associate_letter_id = paramLetterId AND LA.associate_letter_type = 'incoming' AND LA.associated_letter_type = 'incoming' 
	
	UNION
	
        SELECT L.id, L.org_type, L.entity_name, L.subject,  'N/A' AS letter_type_name, 'Outgoing' AS letter_category,
	L.date_sent AS letter_date, L.date_received AS date_received, L.recipient_reference_number AS entity_reference_number, L.ppda_reference_number,  L.outgoing_letter_file_name AS attached_doc 
	FROM letter_associations LA 
	INNER JOIN outgoing_letters L ON LA.associated_letter_id = L.id
	LEFT JOIN letter_types LT ON L.letter_type_id = LT.id
	WHERE LA.associate_letter_id = paramLetterId AND LA.associate_letter_type = 'incoming' AND LA.associated_letter_type = 'outgoing' 
 
       
        -- case 2 it's an associated letter
	UNION
	 
	SELECT L.id, L.org_type, L.entity_name, L.subject, 'N/A' AS letter_type_name, 'Outgoing' AS letter_category,
	L.date_sent AS letter_date, L.date_received AS date_received, L.recipient_reference_number AS entity_reference_number, L.ppda_reference_number,  L.outgoing_letter_file_name AS attached_doc 
	FROM letter_associations LA 
	INNER JOIN outgoing_letters L ON LA.associate_letter_id = L.id
	LEFT JOIN letter_types LT ON L.letter_type_id = LT.id
	WHERE
	 LA.associated_letter_id = paramLetterId AND LA.associated_letter_type = 'incoming' AND LA.associate_letter_type = 'outgoing'
	 
	 UNION
	 
	SELECT 
	L.id, L.org_type, L.entity_name, L.subject, LT.letter_type_name, 'Incoming' AS letter_category,
	L.date_sent AS letter_date, L.date_received_ppda AS date_received, L.sender_reference_number AS entity_reference_number, L.ppda_reference_number, L.incoming_letter_file_name AS attached_doc 
	 FROM letter_associations LA 
	INNER JOIN incoming_letters L ON LA.associate_letter_id = L.id
	LEFT JOIN letter_types LT ON L.letter_type_id = LT.id
	WHERE
	 LA.associated_letter_id = paramLetterId AND LA.associated_letter_type = 'incoming' AND LA.associate_letter_type = 'incoming';

	END */$$
DELIMITER ;

/* Procedure structure for procedure `GetIncomingLettersNotYetAssociatedToLetter` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetIncomingLettersNotYetAssociatedToLetter` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `GetIncomingLettersNotYetAssociatedToLetter`(in paramEntityName varchar(255),IN paramLetterId INTEGER(11),IN paramLetterCategory VARCHAR(255))
BEGIN
	
SELECT L.*, LT.letter_type_name FROM incoming_letters L 
LEFT JOIN letter_types LT ON L.letter_type_id = LT.id
WHERE L.entity_name = paramEntityName and (paramLetterCategory = 'outgoing' or paramLetterId != L.id ) and
L.id NOT IN 
(
   SELECT associated_letter_id FROM letter_associations WHERE associate_letter_id = paramLetterId AND associate_letter_type = paramLetterCategory AND associated_letter_type = 'incoming' 
   union
   SELECT associate_letter_id FROM letter_associations WHERE associated_letter_id = paramLetterId AND associated_letter_type = paramLetterCategory AND associate_letter_type = 'incoming'
) ;

END */$$
DELIMITER ;

/* Procedure structure for procedure `GetOutgoingLetterAssociations` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetOutgoingLetterAssociations` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `GetOutgoingLetterAssociations`(IN paramLetterId Integer(11))
BEGIN
	
	-- case 1 it's an associate letter
	SELECT 
	L.id, L.org_type, L.entity_name, L.subject, LT.letter_type_name, 'Incoming' AS letter_category,
	L.date_sent AS letter_date, L.date_received_ppda AS date_received, L.sender_reference_number AS entity_reference_number, L.ppda_reference_number, L.incoming_letter_file_name AS attached_doc 
	 FROM letter_associations LA 
	INNER JOIN incoming_letters L ON LA.associated_letter_id = L.id
	LEFT JOIN letter_types LT ON L.letter_type_id = LT.id
	WHERE LA.associate_letter_id = paramLetterId AND LA.associate_letter_type = 'outgoing' AND LA.associated_letter_type = 'incoming' 
	
	UNION
	
        SELECT L.id, L.org_type, L.entity_name, L.subject,  'N/A' AS letter_type_name, 'Outgoing' AS letter_category,
	L.date_sent AS letter_date, L.date_received AS date_received, L.recipient_reference_number AS entity_reference_number, L.ppda_reference_number,  L.outgoing_letter_file_name AS attached_doc 
	FROM letter_associations LA 
	INNER JOIN outgoing_letters L ON LA.associated_letter_id = L.id
	LEFT JOIN letter_types LT ON L.letter_type_id = LT.id
	WHERE LA.associate_letter_id = paramLetterId AND LA.associate_letter_type = 'outgoing' AND LA.associated_letter_type = 'outgoing' 
 
       
        -- case 2 it's an associated letter
	UNION
	 
	SELECT L.id, L.org_type, L.entity_name, L.subject, 'N/A' as letter_type_name, 'Outgoing' AS letter_category,
	L.date_sent as letter_date, L.date_received as date_received, L.recipient_reference_number as entity_reference_number, L.ppda_reference_number,  L.outgoing_letter_file_name as attached_doc 
	FROM letter_associations LA 
	INNER JOIN outgoing_letters L ON LA.associate_letter_id = L.id
	LEFT JOIN letter_types LT ON L.letter_type_id = LT.id
	WHERE
	 LA.associated_letter_id = paramLetterId AND LA.associated_letter_type = 'outgoing' AND LA.associate_letter_type = 'outgoing'
	 
	 UNION
	 
	SELECT 
	L.id, L.org_type, L.entity_name, L.subject, LT.letter_type_name, 'Incoming' AS letter_category,
	L.date_sent AS letter_date, L.date_received_ppda AS date_received, L.sender_reference_number AS entity_reference_number, L.ppda_reference_number, L.incoming_letter_file_name AS attached_doc 
	 FROM letter_associations LA 
	INNER JOIN incoming_letters L ON LA.associate_letter_id = L.id
	LEFT JOIN letter_types LT ON L.letter_type_id = LT.id
	WHERE
	 LA.associated_letter_id = paramLetterId AND LA.associated_letter_type = 'outgoing' AND LA.associate_letter_type = 'incoming';

	END */$$
DELIMITER ;

/* Procedure structure for procedure `GetOutgoingLettersNotYetAssociatedToLetter` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetOutgoingLettersNotYetAssociatedToLetter` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `GetOutgoingLettersNotYetAssociatedToLetter`(in paramEntityName varchar(255),IN paramLetterId INTEGER(11),IN paramLetterCategory VARCHAR(255))
BEGIN
	
SELECT L.* FROM outgoing_letters L 
WHERE L.entity_name = paramEntityName and (paramLetterCategory = 'incoming' or paramLetterId != L.id ) and
L.id NOT IN 
(
   SELECT associated_letter_id FROM letter_associations WHERE associate_letter_id = paramLetterId AND associate_letter_type = paramLetterCategory AND associated_letter_type = 'outgoing' 
   union
   SELECT associate_letter_id FROM letter_associations WHERE associated_letter_id = paramLetterId AND associated_letter_type = paramLetterCategory AND associate_letter_type = 'outgoing'
) ;

END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
