<?php
/**
 * Common tags
 */
defined('INTERNAL_FLAG')		   			OR define('INTERNAL_FLAG', 'internal');
defined('EXTERNAL_FLAG')		   			OR define('EXTERNAL_FLAG', 'external');
defined('LOCAL_CURRENCY')		   			OR define('LOCAL_CURRENCY', 'UGX');
defined('CURRENT_DATE')		   			  OR define('CURRENT_DATE', date('Y-m-d'));
defined('CURRENT_YEAR')		   			  OR define('CURRENT_YEAR', date('Y'));
defined('LETTERS_STORAGE')		   		OR define('LETTERS_STORAGE', 'letters');
defined('PROCUREMENT_CHEEKLIST_FILE')		   		OR define('PROCUREMENT_CHEEKLIST_FILE', 'Procurement File Completeness checklist.docx');


defined('PUBLIC_DOCS_PATH')		   		OR define('PUBLIC_DOCS_PATH', '/docs');

/**
 * Audit tool tag names
 */
defined('AUDIT_SYSTEM_TAG')		   		OR define('AUDIT_SYSTEM_TAG', 'system');
defined('AUDIT_DISPOSAL_TAG')		   		OR define('AUDIT_DISPOSAL_TAG', 'disposal');
defined('AUDIT_PROCESS_TAG')		   		OR define('AUDIT_PROCESS_TAG', 'process');
defined('COMPLIANCE_STRUCTURE_TAG')		   		OR define('COMPLIANCE_STRUCTURE_TAG', 'compliancestructure');
defined('COMPLIANCE_PROCESS_TAG')		   		OR define('COMPLIANCE_PROCESS_TAG', 'complianceprocess');

/**
 * Letters /Report section
 */

defined('MANAGEMENT_LETTER_TAG')		   		OR define('MANAGEMENT_LETTER_TAG', 'management_letter');
defined('AUDIT_LAUNCH_LETTER_TAG')		   		OR define('AUDIT_LAUNCH_LETTER_TAG', 'audit_launch_letter');
defined('COMPLIANCE_CHECKLIST_LAUNCH_LETTER_TAG')		   		OR define('COMPLIANCE_CHECKLIST_LAUNCH_LETTER_TAG', 'compliance_checklist_launch_letter');
defined('CAPACITY_BUILDING_TRAINING_REPORT_TAG')		   		OR define('CAPACITY_BUILDING_TRAINING_REPORT_TAG', 'capacity_building_training_report');

defined('NEW_SECTION_TAG')		   		OR define('NEW_SECTION_TAG', '[NewSection]');
/**
 * Legal associated tags
 */
defined('LEGAL_ACCREDITATION_TAG')		   		OR define('LEGAL_ACCREDITATION_TAG', 'accreditation');
defined('LEGAL_ADMIN_REVIEW_TAG')		   		OR define('LEGAL_ADMIN_REVIEW_TAG', 'admin_review');
defined('LEGAL_DEVIATION_TAG')		   		OR define('LEGAL_DEVIATION_TAG', 'deviation');
defined('LEGAL_TRIBUNAL_TAG')		   		OR define('LEGAL_TRIBUNAL_TAG', 'tribunal');
defined('COURT_CASE_TAG')		   		OR define('COURT_CASE_TAG', 'court_case');
defined('LEGAL_MAC_TAG')		   		OR define('LEGAL_MAC_TAG', 'mac');
defined('ACCOUNTING_OFFICER_TAG')		   		OR define('ACCOUNTING_OFFICER_TAG', 'accounting_officer');
defined('AO_TAG')		   		OR define('AO_TAG', 'ao');
defined('LEGAL_INVESTIGATION_TAG')		   		OR define('LEGAL_INVESTIGATION_TAG', 'investigation');


/**
 * Performance monitoring letter variables
 */

defined('EMIS_GET_CURRENT_PM_ACTIVITY_ENTITY_OUTGOING_LETTER_CODE')	OR define('EMIS_GET_CURRENT_PM_ACTIVITY_ENTITY_OUTGOING_LETTER_CODE', '[EMIS:GetCurrentPMActivityEntityOutgoingLetterCode]');

defined('EMIS_GET_CURRENT_DATE') OR define('EMIS_GET_CURRENT_DATE', '[EMIS:GetCurrentDate]');

defined('EMIS_GET_CURRENT_PM_ACTIVITY_ENTITY') OR define('EMIS_GET_CURRENT_PM_ACTIVITY_ENTITY', '[EMIS:GetCurrentPMActivityEntity]');

defined('EMIS_GET_CURRENT_ENTITY_HEADQUATER_DISTRICT') OR define('EMIS_GET_CURRENT_ENTITY_HEADQUATER_DISTRICT', '[EMIS:GetCurrentEntityHeadquarterDistrict]');

defined('EMIS_GET_CURRENT_PM_ACTIVITY_AUDIT_START_DATE') OR define('EMIS_GET_CURRENT_PM_ACTIVITY_AUDIT_START_DATE', '[EMIS:GetCurrentPMActivityAuditStartDate]');

defined('EMIS_GET_CURRENT_PM_ACTIVITY_TEAM')	OR define('EMIS_GET_CURRENT_PM_ACTIVITY_TEAM', '[EMIS:GetCurrentPMActivityTeam]');
defined('EMIS_GET_CURRENT_PM_ACTIVITY_FY')	OR define('EMIS_GET_CURRENT_PM_ACTIVITY_FY', '[EMIS:GetCurrentPMActivityFY]');

defined('EMIS_GET_CURRENT_PM_ACTIVITY_ENTRY_MEETING_DATE') OR define('EMIS_GET_CURRENT_PM_ACTIVITY_ENTRY_MEETING_DATE', '[EMIS:GetCurrentPMActivityEntryMeetingDate]');

defined('EMIS_GET_CURRENT_PM_ACTIVITY_AUDIT_ENTRY_MEETING_TIME') OR define('EMIS_GET_CURRENT_PM_ACTIVITY_AUDIT_ENTRY_MEETING_TIME', '[EMIS:GetCurrentPMActivityAuditEntryMeetingTime]');

/**
 * Capacity building Training report variables
 */
defined('EMIS_GET_CURRENT_CB_ACTIVITY_NAME') OR define('EMIS_GET_CURRENT_CB_ACTIVITY_NAME', '[EMIS:GetCurrentCBActivityName]');

defined('EMIS_GET_CURRENT_CB_ACTIVITY_OWNER') OR define('EMIS_GET_CURRENT_CB_ACTIVITY_OWNER', '[EMIS:GetCurrentCBActivityOwner]');

defined('EMIS_GET_CURRENT_CB_ACTIVITY_LOCATION') OR define('EMIS_GET_CURRENT_CB_ACTIVITY_LOCATION', '[EMIS:GetCurrentCBActivityLocation]');

defined('EMIS_GET_CURRENT_CB_ACTIVITY_TOPICS_TABLE') OR define('EMIS_GET_CURRENT_CB_ACTIVITY_TOPICS_TABLE', '[EMIS:GetCurrentCBActivityTopicsTable]');

defined('EMIS_GET_CURRENT_CB_ACTIVITY_ATTENDANCE_TABLE') OR define('EMIS_GET_CURRENT_CB_ACTIVITY_ATTENDANCE_TABLE', '[EMIS:GetCurrentCBActivityWorkShopAttendanceTable]');

defined('EMIS_GET_CURRENT_CB_ACTIVITY_ISSUES_RAISED_TABLE') OR define('EMIS_GET_CURRENT_CB_ACTIVITY_ISSUES_RAISED_TABLE', '[EMIS:GetCurrentCBActivityParticipantIssuesRaisedTable]');

defined('EMIS_GET_CURRENT_CB_ACTIVITY_PARTICIPANTS_COUNT') OR define('EMIS_GET_CURRENT_CB_ACTIVITY_PARTICIPANTS_COUNT', '[EMIS:GetCurrentCBActivityParticipantsCount]');

defined('EMIS_GET_CURRENT_CB_ACTIVITY_FURTHER_TRAINING_AREAS') OR define('EMIS_GET_CURRENT_CB_ACTIVITY_FURTHER_TRAINING_AREAS', '[EMIS:GetCurrentCBActivityFurtherTrainingAreas]');

defined('EMIS_GET_CURRENT_CB_ACTIVITY_EVALUATIONS_SUMMARY') OR define('EMIS_GET_CURRENT_CB_ACTIVITY_EVALUATIONS_SUMMARY', '[EMIS:GetCurrentCBActivityEvaluationsSummary]');

defined('EMIS_GET_CURRENT_CB_ACTIVITY_RESPONSES_SUMMARY_TABLE') OR define('EMIS_GET_CURRENT_CB_ACTIVITY_RESPONSES_SUMMARY_TABLE', '[EMIS:GetCurrentCBActivityResponsesSummaryTable]');

defined('EMIS_GET_CURRENT_CB_ACTIVITY_CONTENT_SATISFACTORY_PERCENTAGE') OR define('EMIS_GET_CURRENT_CB_ACTIVITY_CONTENT_SATISFACTORY_PERCENTAGE', '[EMIS:GetCurrentCBActivityContentSatisfactoryPercetage]');

defined('EMIS_GET_CURRENT_CB_ACTIVITY_TRAINING_METHOD_SATISFACTORY_PERCENTAGE') OR define('EMIS_GET_CURRENT_CB_ACTIVITY_TRAINING_METHOD_SATISFACTORY_PERCENTAGE', '[EMIS:GetCurrentCBActivityTrainingMethodSatisfactoryPercetage]');

defined('EMIS_GET_CURRENT_CB_ACTIVITY_INSTRUCTOR_EFFECTIVENESS_HIGHLY_SATISFACTORY_PERCENTAGE') OR define('EMIS_GET_CURRENT_CB_ACTIVITY_INSTRUCTOR_EFFECTIVENESS_HIGHLY_SATISFACTORY_PERCENTAGE', '[EMIS:GetCurrentCBActivityInstructorEffectivenessHighlySatisfactoryPercetage]');
