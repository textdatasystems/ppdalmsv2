<?php


namespace App;


class ApiRespEdmsUpload
{
    public $statusCode = "";
    public $statusDescription = "";
    public $fileName = "";
    public $edmsFilePath = "";
    public $fileLocalPath = "";
    public $suggestedFileName = "";
}
