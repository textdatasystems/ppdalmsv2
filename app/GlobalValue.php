<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalValue extends Model
{
    //
    public static function laratablesCustomAction($globalValue)
    {
        $data = array(
            'model'=>'globalValue',
            'globalValue'=>$globalValue
        );
        return view('List_Actions.list_action_global_value')->with($data)->render();
    }

}
