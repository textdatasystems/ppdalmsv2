<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LetterTypeField extends Model
{

    public function letterTypeFieldValues(){
        return $this->hasMany('App\LetterTypeFieldValue');
    }


    public function outgoingLettersLetterTypeFieldValues(){
        return $this->hasMany('App\OutgoingLettersLetterTypeFieldValue');
    }
    
}
