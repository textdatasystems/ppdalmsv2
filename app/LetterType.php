<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LetterType extends Model
{
    public function fields(){
        return $this->hasMany('App\LetterTypeField');
    }
    public function edms_doc_type(){
        return $this->belongsTo('App\EdmsDocType');
    }
    public static function laratablesAdditionalColumns()
    {
        return ['letter_type_name'];
    }

    public function letters(){
        return $this->hasMany('App\IncomingLetter');
    }

    public function outgoingLetters(){
        return $this->hasMany('App\OutgoingLetter');
    }

    public static function laratablesCustomAction($record)
    {
        $model = 'edmsdoctype';
        $data = array(
            'model'=>'edmsdoctype',
            'record'=>$record
        );
        return view('List_Actions.general_action')->with($data)->render();
    }

    public static function laratablesCustomLetterType($record)
    {
        return view('master_data.general.letter_types.Col_LetterType',compact('record'))->render();
    }

    public static function laratablesCustomFields($record)
    {
        return view('master_data.general.letter_types.Col_Fields',compact('record'))->render();
    }
}
