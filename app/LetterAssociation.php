<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LetterAssociation extends Model
{
    //
    public static function laratablesQueryConditions($query)
    {
//        $query->join('outgoing_letters', 'outgoing_letters.id', 'letter_associations.associated_letter_id')
//            ->select(['letters.entity_name','letters.subject','letters.date_sent','letters.date_received_ppda','letters.date_received_registry',
//                'letters.sender_reference_number', 'letters.ppda_reference_number', 'letters.attention_to_name'])

        return $query->join('letters', 'letters.id', 'letter_associations.associated_letter_id')
                 ->select(['letters.entity_name','letters.subject','letters.date_sent','letters.date_received_ppda','letters.date_received_registry',
                'letters.sender_reference_number', 'letters.ppda_reference_number', 'letters.attention_to_name']);
    }


    public static function menuAction($associatedLetterId, $associatedLetterCategory, $fileName)
    {

        $level = 'ed';
        return view('List_Actions.list_action_letter_association', compact('associatedLetterId','level','associatedLetterCategory','fileName'))->render();

    }

}
