<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/16/19
 * Time: 3:59 PM
 */

namespace App;


class ApiUser
{

    public $id = "";
    public $username = "";
    public $firstName = "";
    public $lastName = "";
    public $otherName = "";
    public $email = "";
    public $phone = "";
    public $createdBy = "";
    public $orgCode = "";
    public $orgName = "";
    public $orgEdUsername = "";
    public $roleCode = "";
    public $roleName = "";
    public $roleLetterMovement = "";
    public $lmsRole = null;
    public $departmentCode = "";
    public $departmentName = "";
    public $departmentHeadUsername = "";
    public $departmentHeadFullName = "";
    public $regionalOfficeCode = "";
    public $regionalOfficeName = "";
    public $categoryCode = "";
    public $category = "";
    public $createdAt = "";

    public $fullName = "";

    public $staffNumber = "";
    public $designation = "";
    public $dateOfBirth = "";
    public $contractStartDate = "";
    public $contractExpiryDate = "";

}
