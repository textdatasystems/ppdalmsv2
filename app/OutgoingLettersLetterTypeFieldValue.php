<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutgoingLettersLetterTypeFieldValue extends Model
{

    public function letterTypeField(){
        return $this->belongsTo('App\LetterTypeField');
    }

}
