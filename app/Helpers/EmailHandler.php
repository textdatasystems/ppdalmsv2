<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/19/19
 * Time: 7:39 AM
 */

namespace App\Helpers;


use App\ApiResp;
use PHPMailer\PHPMailer\PHPMailer;

//use Illuminate\Support\Facades\Mail;


class EmailHandler {


    public static function sendPlainTextEmailPHPMAILER($mailTo, $subject, $message){


        $resp = new ApiResp();

        try{

            /*
             * Send the email
             * */
            $mail             = new PHPMailer(true);
            $mail->IsSMTP();
            $mail->SMTPDebug  = 0;  // debugging: 0 = off, 1 = errors and messages, 2 = messages only
            $mail->SMTPAuth   = true;  // authentication enabled
            $mail->SMTPSecure = env('MAIL_ENCRYPTION', 'tls');;  // secure transfer enabled REQUIRED for Gmail
            $mail->Host       = env('MAIL_HOST', 'smtp.hostinger.com');
            $mail->Port       = env('MAIL_PORT', 587);
            $mail->IsHTML(true);


            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->Username = env('MAIL_USERNAME', 'info@savemysoulinitiative.com');
            $mail->Password = env('MAIL_PASSWORD', 'Timothy93.com');
            $mail->SetFrom(env('MAIL_USERNAME', 'info@savemysoulinitiative.com'));
            $mail->Subject = $subject;
            $mail->Body    = $message;
            $mail->AddAddress($mailTo);


            /*
             * We attempt to send the email
             * */
            if (!$mail->Send()) {

                $resp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $resp->statusDescription = 'Failed to Send Email';
                return $resp;

            }


            /*
             * Build success response
             * */
            $resp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $resp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $resp;


        }catch (\Exception $exception){

            /*
             * Error occurred on sending the email
             * */
            $resp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $resp->statusDescription = $exception->getMessage();
            return $resp;

        }

    }

}
