<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/16/19
 * Time: 3:54 PM
 */

namespace App\Helpers;


class EndPoints
{

    const SERVER_IP = 'localhost';
    //const SERVER_IP = 'app-server';

    public static $BASE_URL = 'http:'.self::SERVER_IP.':9001/api';
    public static $BASE_URL_USER_MANAGEMENT = 'http:'.self::SERVER_IP.':7001/api';

    public static $USERS_GET_AUTHENTICATED_USER = '/users/auth-user';
    public static $USER_MANAGEMENT_LOGIN_PAGE =  'http://'.self::SERVER_IP.'/';
    public static $USER_MANAGEMENT_LOGOUT_LINK =  'http://'.self::SERVER_IP.'/admin/signout';

    public static $EMIS_ENTITIES_GET =  'http://'.self::SERVER_IP.':10000/api/entity_query/get_entities_short_list';

    public static $BASE_URL_EDMS_MIDDLEWARE_API = "http://192.168.33.45:801/api/edmstools";

    public static $EDMS_UPLOAD_DOCUMENT = "/upload-document";
    public static $EDMS_DOWNLOAD_DOCUMENT = "/download-document";
    public static $EDMS_DELETE_DOCUMENT = "/delete-document";
    public static $EMIS_SAVE_PROVIDER = 'http://'.self::SERVER_IP.':10000/api/general_query/add_providers';
    public static $EMIS_PROVIDERS_GET = 'http://'.self::SERVER_IP.':10000/api/general_query/get_providers';
    public static $EMIS_PPDA_USERS_GET = 'http://'.self::SERVER_IP.':7001/api/users';

}
