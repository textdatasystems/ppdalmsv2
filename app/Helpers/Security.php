<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/16/19
 * Time: 3:41 PM
 */

namespace App\Helpers;



class Security
{

    public static $COOKIE_TOKEN = 'token';
    public static $COOKIE_TOKEN_EXPIRY_MINUTES = 60;
    public static $SESSION_USER = 'user';
    public static $SESSION_ED = 'ed';
    public static $SESSION_DEPT_UNITS = 'department_units';
    public static $SESSION_ENABLE_ADMIN_DELETION = 'enable_admin_deletion';
    public static $SESSION_TEMP_USER = 'temp_user';
    public static $SESSION_USER_LEVEL = 'level';

    public static function dtEncrypt($plainData){

        return encrypt($plainData);

    }


    public static function dtDecrypt($encryptedValue){

//        try{

            return decrypt($encryptedValue);

//        }catch (\Exception $exception){
//            return $encryptedValue;
//        }

    }


}
