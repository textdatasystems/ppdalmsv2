<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/16/19
 * Time: 3:56 PM
 */

namespace App\Helpers;


use App\ApiDepartment;
use App\ApiDepartmentUnit;
use App\ApiLmsRole;
use App\ApiResp;
use App\ApiUser;
use App\Http\Controllers\ApiHandler;
use Illuminate\Support\Str;
use Unirest\Request;
use Unirest\Request\Body;

class DataLoader
{

    public static function getAuthenticatedUser($token) {

        $baseResp = new ApiResp();

        try{

            $action = EndPoints::$USERS_GET_AUTHENTICATED_USER;
            $resp = ApiHandler::makeGetRequest($action, true,$token, null, EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->result = self::getApiUser($apiResult['data']);
            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    /**
     * @param $userDataObjectFromApi
     * @return ApiUser
     */
    public static function getApiUser($userDataObjectFromApi) {

        $user = new ApiUser();
        $user->id = $userDataObjectFromApi['id'];
        $user->firstName = $userDataObjectFromApi['first_name'];
        $user->lastName = $userDataObjectFromApi['last_name'];
        $user->otherName = $userDataObjectFromApi['other_name'];
        $user->fullName = trim($user->firstName . ' ' . $user->lastName . ' ' . $user->otherName);
        $user->username = $userDataObjectFromApi['username'];
        $user->email = $userDataObjectFromApi['email'];
        $user->phone = $userDataObjectFromApi['phone'];
        $user->createdAt = $userDataObjectFromApi['created_at'];
        $user->createdBy = $userDataObjectFromApi['created_by'];

        $user->staffNumber = $userDataObjectFromApi['staff_number'];
        $user->designation = $userDataObjectFromApi['designation'];
        $user->dateOfBirth = $userDataObjectFromApi['date_of_birth'];
        $user->contractStartDate = $userDataObjectFromApi['contract_start_date'];
        $user->contractExpiryDate = $userDataObjectFromApi['contract_expiry_date'];

        $user->roleLetterMovement = self::getLetterMovementAppSpecificRoleCodes($userDataObjectFromApi['letter_movement_role_code']);

        $apiDept = self::getApiDepartment($userDataObjectFromApi['department']);
        $user->lmsRole = self::getApiLmsRole($userDataObjectFromApi['lms_role']);

        $user->departmentCode = is_null($apiDept) ? "" : $apiDept->departmentCode;
        $user->departmentName = is_null($apiDept) ? "" : $apiDept->name;
        $user->departmentHeadUsername = is_null($apiDept) ? "" : $apiDept->hodUsername;

        return $user;

    }


    public static function getApiDepartmentUnit($data) {

        $units = [];
       foreach ($data as $datum){

           $unit = new ApiDepartmentUnit();
           $unit->unit = $datum['name'];
           $unit->code = $datum['unit_code'];
           $unit->deptCode = array_key_exists('department',$datum) ?  $datum['department']['department_code'] : '';
           $unit->deptName = array_key_exists('department',$datum) ?  $datum['department']['name'] : '';

           $units[] = $unit;

       }
        return $units;

    }

    private static function getLetterMovementAppSpecificRoleCodes($letter_movement_role_code)
    {

        if($letter_movement_role_code == AppConstants::$API_ROLE_CODE_RECEPTION)
        {
            return AppConstants::$ROLE_CODE_RECEPTION;
        }
        else if($letter_movement_role_code == AppConstants::$API_ROLE_CODE_REGISTRY)
        {
            return AppConstants::$ROLE_CODE_REGISTRY;
        }
        else if($letter_movement_role_code == AppConstants::$API_ROLE_CODE_ED)
        {
            return AppConstants::$ROLE_CODE_ED;
        }
        else if($letter_movement_role_code == AppConstants::$API_ROLE_CODE_SUPER_USER)
        {
            return AppConstants::$ROLE_CODE_SUPER_USER;
        }
        else
        {
            return $letter_movement_role_code;
        }

    }

    /**
     * @param $item
     * @return ApiDepartment
     */
    public static function getApiDepartment($item) {

        $department = new ApiDepartment();
        $department->name = $item['name'];
        $department->departmentCode = $item['department_code'];
        $department->createdBy = $item['created_by'];
        $department->orgCode = $item['org_code'];
        $department->hodUsername = $item['head_of_department'];
        return $department;

    }

    public static function getApiLmsRole($item) {

        $role = new ApiLmsRole();

        if($item == null){

            $role->reception = false;
            $role->registry = false;
            $role->edOffice = false;
            $role->outLetters = false;
            $role->masterData = false;
            return $role;
        }else{
            $role->reception = $item['reception_flag'] == 1;
            $role->registry = $item['registry_flag'] == 1;
            $role->edOffice = $item['ed_office_flag'] == 1;
            $role->outLetters = $item['outgoing_letter_flag'] == 1;
            $role->masterData = $item['master_data_flag'] == 1;
            return $role;

        }

    }


    public static function getExecutiveDirector() {

        $baseResp = new ApiResp();

        try{

            $action = '/organization/ed';
            $identifier = 'PPDA';
            $resp = ApiHandler::makeGetRequest($action, false,null, $identifier, EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->result = self::getApiUser($apiResult['data']);
            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function getDepartmentUnits() {

        $baseResp = new ApiResp();

        try{

            $action = '/department-units';
            $resp = ApiHandler::makeGetRequest($action, false,null, null, EndPoints::$BASE_URL_USER_MANAGEMENT);

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->result = self::getApiDepartmentUnit($apiResult['data']);
            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


    public static function uploadFileToEdmsServer($data) {

        $baseResp = new ApiResp();

        try{

            $baseURL = EndPoints::$BASE_URL_EDMS_MIDDLEWARE_API;
            $action = EndPoints::$EDMS_UPLOAD_DOCUMENT;

            $resp = ApiHandler::makePostRequest($action, $data, false,null, $baseURL );

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $message = Str::contains($statusDescription, AppConstants::$DOCUMENT_NOT_CHECKED_OUT) ? AppConstants::$DOCUMENT_NOT_CHECKED_OUT_ENCAPSULATED : $statusDescription;
                $baseResp->statusDescription = $message;
                return $baseResp;

            }

            $baseResp->result = $statusDescription;
            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = $statusDescription;
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function uploadFileToEdmsServerV2($data) {

        $baseResp = new ApiResp();

        try{

            $baseURL = EndPoints::$BASE_URL_EDMS_MIDDLEWARE_API;
            $action = EndPoints::$EDMS_UPLOAD_DOCUMENT;

            $resp = self::makePostRequest($action, $data, false,null, $baseURL );

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $message = Str::contains($statusDescription, AppConstants::$DOCUMENT_NOT_CHECKED_OUT) ? AppConstants::$DOCUMENT_NOT_CHECKED_OUT_ENCAPSULATED : $statusDescription;
                $baseResp->statusDescription = $message;
                return $baseResp;

            }

            $baseResp->result = $statusDescription;
            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = $statusDescription;
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function makePostRequest($endPoint, $data, $authenticates = false, $authToken = null, $baseApiUrl = null ){

        $resp = new ApiResp();

        try{

            /*
             * If we require authentication we set the token header
             * */
            if($authenticates){

                $headers = [
                    'X-Requested-With'=>'XMLHttpRequest',
                    'Authorization'=>'Bearer ' . $authToken,
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'];

            }else{

                $headers =  [
                    'X-Requested-With'=>'XMLHttpRequest',
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'];

            }


            /*
             * Get the full URL
             * */
            $baseUrl = $baseApiUrl == null ? EndPoints::$BASE_URL : $baseApiUrl;
            $url = $baseUrl . $endPoint;


            /*
             * Build json request
             * */
            $body = Body::json($data);

            /*
             * Post the response to the API
             * */
            $response = Request::post($url, $headers, $body);

            /*
             * UniRest handles the response for us, so we have the fields below
             * */
            $respStatusCode = $response->code;        // HTTP Status code
            $respHeaders = $response->headers;     // Headers
            $respBody = $response->body;        // Parsed body
            $respRawBody = $response->raw_body;    // Unparsed body


            /*
             * Http status code is not 200, so an Http error occurred
             * */
            if($respStatusCode != \Illuminate\Http\Response::HTTP_OK){

                $resp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $resp->statusDescription = $respStatusCode;// .' '. json_encode($respBody);
                return $resp;

            }


            /*
             * Http status code is 200, build the success response
             * */
            $resp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $resp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            $resp->result = json_encode($respBody);

            return $resp;


        }catch (\Exception $exception){

            /*
             * We supposed to log this exception returned
             * */

            $resp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $resp->statusDescription = AppConstants::$GENERAL_ERROR_AT_TDS.' '.$exception->getMessage();
            return $resp;

        }

    }

    public static function downloadFileFromEdmsServer($data) {

        $baseResp = new ApiResp();

        try{

            $baseURL = EndPoints::$BASE_URL_EDMS_MIDDLEWARE_API;
            $action = EndPoints::$EDMS_DOWNLOAD_DOCUMENT;

            $resp = ApiHandler::makePostRequest($action, $data, false,null, $baseURL );

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->result = $apiResult['result'];//self::getApiUser($apiResult['data']);
            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function deleteDocumentEdmsServer($data) {

        $baseResp = new ApiResp();

        try{

            $baseURL = EndPoints::$BASE_URL_EDMS_MIDDLEWARE_API;
            $action = EndPoints::$EDMS_DELETE_DOCUMENT;

            $resp = ApiHandler::makePostRequest($action, $data, false,null, $baseURL );

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->result = $statusDescription;
            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = $statusDescription;
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function saveProviderInEmis($data) {

        $baseResp = new ApiResp();

        try{

            $baseURL = EndPoints::$EMIS_SAVE_PROVIDER;
            $action = '';

            $resp = ApiHandler::makePostRequest($action, $data, false,null, $baseURL );

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['success'];
            $statusDescription = $apiResult['message'];

            if($statusCode != true){

                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;

            }

            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = $statusDescription;
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }

    public static function renameFileInEdms($data) {

        $baseResp = new ApiResp();

        try{

            $baseURL = EndPoints::$BASE_URL_EDMS_MIDDLEWARE_API;
            $action = "/rename-document";

            $resp = ApiHandler::makePostRequest($action, $data, false,null, $baseURL );

            /*
             * Error occurred on sending the request, redirect to the page with data
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $resp->statusDescription;
                return $baseResp;
            }

            /*
             * Here got a response from the server, so we get the response from the server
             * */
            $apiResult = json_decode($resp->result, true);

            /*
             * Get the status code from the API
             * */
            $statusCode = $apiResult['statusCode'];
            $statusDescription = $apiResult['statusDescription'];

            if($statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
                $baseResp->statusDescription = $statusDescription;
                return $baseResp;
            }

            $baseResp->result = $statusDescription;
            $baseResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $baseResp->statusDescription = $statusDescription;
            return $baseResp;

        }catch (\Exception $exception){

            $baseResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $baseResp->statusDescription = AppConstants::generalError($exception);
            return $baseResp;
        }

    }


}
