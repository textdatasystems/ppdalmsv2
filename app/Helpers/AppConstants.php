<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/16/19
 * Time: 3:44 PM
 */

namespace App\Helpers;


class AppConstants
{


    /*
     [12:52 pm, 12/09/2019] ME: Username: Emis_User
      Password : Emis_User$123
     * */
    const IN_DEBUG = true;

    public static $STATUS_CODE_SUCCESS = "0";
    public static $STATUS_CODE_FAILED = "100";
    public static $STATUS_DESC_SUCCESS = "SUCCESS";

    public static $GENERAL_ERROR_AT_TDS = "SOMETHING WENT WRONG ON OUR SIDE";

    public static $API_ROLE_CODE_RECEPTION = "RECEPTION USER";
    public static $API_ROLE_CODE_ED = "EXECUTIVE DIRECTOR USER";
    public static $API_ROLE_CODE_NORMAL_USER = "NORMAL USER";
    public static $API_ROLE_CODE_REGISTRY = "REGISTRY USER";
    public static $API_ROLE_CODE_SUPER_USER= "SUPER USER";

    public static $ROLE_CODE_RECEPTION = 'reception';
    public static $ROLE_CODE_ED = 'ed_office';
    public static $ROLE_CODE_REGISTRY = 'registry';
    public static $ROLE_CODE_SUPER_USER = 'super_user';
    public static $EDMS_EMIS_DIRECTORY = "/PPDA DEPARTMENTAL WORKSPACES/EMIS/";

    public static $EDMS_DOCUMENT_BASE_URL = "http://edms-server:800/docs";
    public static $EDMS_EMIS_DIRECTORY_INCOMING_LETTERS_DEFAULT = "/REGISTRY/PRIVATE REGISTRY FILES/Letters - Incoming Default/";
    public static $EDMS_EMIS_DIRECTORY_OUTGOING_LETTERS_DEFAULT = "/REGISTRY/PRIVATE REGISTRY FILES/Letters - Outgoing Default/";
    public static $EDMS_EMIS_DIRECTORY_INTERNAL_MEMOS = "/REGISTRY/PRIVATE REGISTRY FILES/PPDA INTERNAL CORRESPONDENCES/";
    public static $DOCUMENT_NOT_CHECKED_OUT = "This document has not been checked out";
    public static $DOCUMENT_NOT_CHECKED_OUT_ENCAPSULATED = "The document with the same name already exists in EDMS for the given Entity";
    public static $MAX_FILE_LENGTH_EXCEEDED = "File name exceeds allowed maximum length of [100] characters";
    public static $LETTER_MOVT_DOC_TYPES_INCOMING_LETTER = "incoming_letter";
    public static $LETTER_MOVT_DOC_TYPES_INTERNAL_MEMO = "internal_memo";

    public static $LETTER_CATEGORY_INCOMING_LETTER = "Incoming Letter";
    public static $LETTER_CATEGORY_MEMO_TO_ED = "Memo - To ED";
    public static $LETTER_CATEGORY_MEMO_FROM_ED = "Memo - From ED";

    public static function generalError($actualError) {

        try{

            $data = [
                'error_message' => 'PORTAL-ERROR ' . $actualError
            ];
            //   $resp = DataLoader::saveErrorLog($data);

        }catch (\Exception $exception){

        }finally{
            return !AppConstants::IN_DEBUG ? AppConstants::$GENERAL_ERROR_AT_TDS : AppConstants::$GENERAL_ERROR_AT_TDS . ' ' .$actualError;
        }

    }

    /*
     * PLEASE NOTE IF YOU ADD A NEW GLOBAL VALUE, ADD TO THE THE LIST IN FUNCTION  {referencedGlobalValues} SUCH THAT IT'S NEVER DELETED
     * */
    public static $GLOBAL_VAL_MAX_FILE_UPLOAD_SIZE_MBS = "MAX_FILE_UPLOAD_SIZE_MBS";

    public static function referencedGlobalValues(){

        $referenceValues = [ self::$GLOBAL_VAL_MAX_FILE_UPLOAD_SIZE_MBS ];
        return $referenceValues;

    }

}
