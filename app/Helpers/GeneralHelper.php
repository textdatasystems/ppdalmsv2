<?php
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use App\SurveyEvalCriteria;

if (! function_exists ('date_picker_format_date')) {
    function date_picker_format_date($date)
    {
        if (!$date) return '';
        return date("d M Y", strtotime($date));
    }
}

if (! function_exists ('short_year_format_date')) {
    function short_year_format_date($date)
    {
        if(!$date){
            return '- -- ----';
        }
        return date("j M y", strtotime($date));
    }
}

if (! function_exists ('full_date')) {
    function full_date($date)
    {
        if(!$date){
            return '- -- ----';
        }
        return date("D, jS M Y", strtotime($date));
    }
}

if (! function_exists ('month_year_date')) {
    function month_year_date($date)
    {
        if(!$date){
            return '- -- ----';
        }
        return date("M Y", strtotime($date));
    }
}


if (! function_exists ('date_to_time')) {
    function date_to_time($date)
    {
        if(!$date){
            return '- -- ----';
        }
        return date("H:i a", strtotime($date));
    }
}

if (! function_exists ('full_year_format_date')) {
    function full_year_format_date($date)
    {
        if(!$date){
            return '- -- ----';
        }
        return date("j M Y", strtotime($date));
    }
}

// change date to database date format
if (! function_exists ('db_date_format')) {
    function db_date_format($date)
    {
        return date("Y-m-d", strtotime($date));
    }
}

if (! function_exists ('legal_no_date_format')) {
    function legal_no_date_format($date)
    {
        return date("j-M-Y", strtotime($date));
    }
}

if (! function_exists ('encrypt_data')) {
    function encrypt_data($param)
    {
      return Crypt::encrypt($param);
    }
}

if (! function_exists ('decrypt_data')) {
    function decrypt_data($param)
    {
      return Crypt::decrypt($param);
    }
}
/**
 * Function to return activityIds
 */
if (! function_exists ('get_activity_id')) {
    function get_activity_id($module,$id)
    {
        if($module == 'C'){
            $collection = App\CbActivity::find($id);
            @$from_date = $collection->cb_plan->financial_year->from_date;
            @$to_date = $collection->cb_plan->financial_year->to_date;
        }elseif($module == 'P'){
            $collection = App\PmActivity::find($id);
            @$from_date = $collection->pm_plan->financial_year->from_date;
            @$to_date = $collection->pm_plan->financial_year->to_date;
        }

        return $module.'-'.
                date("Y",strtotime($from_date)).'-'.
                    date("y",strtotime($to_date)).'-'.
                        $collection->id;
    }
}

/**
 * Function to return audit types
 */
if (! function_exists ('get_audit_types')) {
    function get_audit_types()
    {
        return array(
          'performance_audits'=> array('high_spend','usmid_entities','previous_ppda_performance','previous_oag_opinion','last_year_audited','all'),
          'compliance_inspection'=> array('new_entity','low_procurement_budget','previous_ppda_performance'),
          'bid_preparatory_audits'=> array('sectors'),
          'contract_audits'=> array('sectors')
        );
    }
}

if (! function_exists ('procurement_letter_key_records')) {
    function procurement_letter_key_records()
    {
        return array(
          "request_to_initiate_procurement_proceedings"=> "Request to initiate procurement proceedings",
          "demand_driven",""
        );
    }
}

if (! function_exists ('get_ppda_performance_sub_criteria')) {
    function get_ppda_performance_sub_criteria()
    {
        return array(
          'last_performance_rating',
          'last_performance_category',
          'number_of_legal_issues_in_last_year'
        );
    }
}

if (! function_exists ('format_audit_type')) {
    function format_audit_type($audit_type)
    {
        return ucwords(str_replace('_',' ',$audit_type));
    }
}

if (! function_exists ('stripUnderScore')) {
    function stripUnderScore($string)
    {
        return str_replace('_',' ',$string);
    }
}

if (! function_exists ('ucwords_strip_underscore')) {
    function ucwords_strip_underscore($string)
    {
        return ucwords(str_replace('_',' ',$string));
    }
}


if (! function_exists ('oag_opinions')) {
    function oag_opinions()
    {
        return array('qualified','unqualified','adverse');
    }
}

if (! function_exists ('get_current_year')) {
    function get_current_year(){return date('Y');}
}

if (! function_exists ('is_emis_file')) {
    function is_emis_file($filename){
      return (pathinfo($filename, PATHINFO_EXTENSION) == 'emis') ? true : false;
    }
}

if (! function_exists ('is_valid_json')) {
    function is_valid_json($data){
      json_decode($data);
      return (json_last_error() == JSON_ERROR_NONE);
    }
}

if (! function_exists ('validate_emis_file')) {
    function validate_emis_file($request){
        if($request->hasFile('emis_file_name')) {
            // Get filename
            $file = $request->file('emis_file_name');
            $filenameWithExt = $file->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
           // Get file extension
            $extension = $file->getClientOriginalExtension();

            // check if file is selected
            if (empty($file)) {
              return response()->json([
                'success' => false,
                'message' => 'No file uploaded'
              ]);
            }

            // upload or store the file
            $path = $file->store('uploads');

            // get file content
            $file_content = Storage::disk('local')->get($path);

            // check if file is emis file
            if(!is_emis_file($filenameWithExt)){
                return response()->json([
                  'success' => false,
                  'message' => 'This is not a valid .emis file'
                ]);
            }
            // check if file content is json data
            if(!is_valid_json($file_content)){
                return response()->json([
                  'success' => false,
                  'message' => 'File content must be json'
                ]);
            }

            return response()->json([
                'success'=>true,
                'data'=> $file_content
            ]);

        } else {
          return response()->json([
            'success' => false,
            'message' => 'File name is not properly set'
          ]);
        }
    }

    if (! function_exists ('request_loader')) {
        function request_loader(){
          return '
          <div id="" class="request_loader_ form-group row hide">
            <div class="col-md-6 col-md-offset-5">
                <i class="ace-icon fa fa-spinner fa-spin orange bigger-225"></i> Please wait...
            </div>
          </div>
          ';
        }
    }

    if (! function_exists ('get_months')) {
        function get_months(){
          return ['January','February','March','April','May','June','July','August','September','October','November','December'];
        }
    }
    if (! function_exists ('get_quarters')) {
        function get_quarters(){
          return ['Quarter 1','Quarter 2','Quarter 3','Quarter 4'];
        }
    }

    if (! function_exists ('get_activity_categories')) {
        function get_activity_categories()
        {
            return array(
              'supply_driven',
              'demand_driven',
            );
        }
    }

    if (! function_exists ('get_evaluation_criteria_options')) {
        function get_evaluation_criteria_options()
        {
            return array("Un-satisfactory","Moderately satisfactory","Satisfactory","Highly-Satisfactory");
        }
    }

    if (! function_exists ('customize_pm_activity_letter')) {
        function customize_pm_activity_letter($pm_activity, $html)
        {
            $start_date = @$pm_activity->pm_audit_dates()->where('tag','start_date')->get()->first();
            $entry_meeting = @$pm_activity->pm_audit_dates()->where('tag','entry_meeting')->get()->first();

            $activity_members = '';
            if($pm_activity->activity_team){
              foreach($pm_activity->activity_team->pm_team->members as $member){
                $activity_members .= $member->user->first_name." ".$member->user->last_name.", ";
              }
            }

            $html = str_replace(EMIS_GET_CURRENT_PM_ACTIVITY_ENTITY_OUTGOING_LETTER_CODE, $pm_activity->entity->outgoing_letter_code ,$html);
            $html = str_replace(EMIS_GET_CURRENT_DATE, full_date(date('Y-m-d')) ,$html);
            $html = str_replace(EMIS_GET_CURRENT_PM_ACTIVITY_ENTITY, $pm_activity->entity->entity_name ,$html);
            $html = str_replace(EMIS_GET_CURRENT_ENTITY_HEADQUATER_DISTRICT, @$pm_activity->entity->branch->branch_name ,$html);
            $html = str_replace(EMIS_GET_CURRENT_PM_ACTIVITY_FY, @$pm_activity->pm_plan->financial_year->financial_year ,$html);
            $html = str_replace(EMIS_GET_CURRENT_PM_ACTIVITY_AUDIT_START_DATE, full_date(@$start_date->activity_date) ,$html);
            $html = str_replace(EMIS_GET_CURRENT_PM_ACTIVITY_AUDIT_START_DATE, full_date(@$start_date->activity_date) ,$html);
            $html = str_replace(EMIS_GET_CURRENT_PM_ACTIVITY_ENTRY_MEETING_DATE, full_date(@$entry_meeting->activity_date) ,$html);
            $html = str_replace(EMIS_GET_CURRENT_PM_ACTIVITY_AUDIT_ENTRY_MEETING_TIME, date_to_time(@$entry_meeting->activity_time) ,$html);
            $html = str_replace(EMIS_GET_CURRENT_PM_ACTIVITY_TEAM, $activity_members,$html);
            return $html;
        }
    }

    if (! function_exists ('generate_cb_training_report')) {
        function generate_cb_training_report($cb_activity, $html)
        {
            $survey_criteria = SurveyEvalCriteria::with('children')->whereNull('parent_id')->get();
            $html = str_replace(EMIS_GET_CURRENT_DATE, month_year_date(date('Y-m-d')) ,$html);
            $html = str_replace(NEW_SECTION_TAG, "<p style='page-break-before: always'></p>" ,$html);

            $html = str_replace(EMIS_GET_CURRENT_CB_ACTIVITY_NAME, @$cb_activity->actual->activity_name,$html);
            $html = str_replace(EMIS_GET_CURRENT_CB_ACTIVITY_LOCATION, @$cb_activity->district->district_name.", ".@$cb_activity->town,$html);
            $html = str_replace(EMIS_GET_CURRENT_CB_ACTIVITY_PARTICIPANTS_COUNT, @$cb_activity->participants->count(),$html);
            $html = str_replace(EMIS_GET_CURRENT_CB_ACTIVITY_OWNER, ucwords(@$cb_activity->actual->user->full_name),$html);

            $html = str_replace(EMIS_GET_CURRENT_CB_ACTIVITY_FURTHER_TRAINING_AREAS, view('capacity_building.parts.survey.further_training_areas', compact('cb_activity'))->render(),$html);

            $html = str_replace(EMIS_GET_CURRENT_CB_ACTIVITY_TOPICS_TABLE, view('capacity_building.parts.survey.activity_modules', compact('cb_activity'))->render() ,$html);

            $html = str_replace(EMIS_GET_CURRENT_CB_ACTIVITY_ISSUES_RAISED_TABLE, view('capacity_building.parts.survey.issues_raised', compact('cb_activity'))->render() ,$html);

            $html = str_replace(EMIS_GET_CURRENT_CB_ACTIVITY_ATTENDANCE_TABLE, view('capacity_building.parts.survey.workshop_attendance', compact('cb_activity'))->render() ,$html);

            $html = str_replace(EMIS_GET_CURRENT_CB_ACTIVITY_EVALUATIONS_SUMMARY, view('capacity_building.parts.survey.evaluation_summary', compact('cb_activity','survey_criteria'))->render() ,$html);

            $html = str_replace(EMIS_GET_CURRENT_CB_ACTIVITY_RESPONSES_SUMMARY_TABLE, view('capacity_building.parts.survey.response_summary', compact('cb_activity','survey_criteria','html'))->render() ,$html);

            $html = str_replace(EMIS_GET_CURRENT_CB_ACTIVITY_CONTENT_SATISFACTORY_PERCENTAGE, get_percentage_cb_activity_survey_criteria($cb_activity,'Content', get_evaluation_criteria_options()[2]),$html);

            $html = str_replace(EMIS_GET_CURRENT_CB_ACTIVITY_TRAINING_METHOD_SATISFACTORY_PERCENTAGE, get_percentage_cb_activity_survey_criteria($cb_activity,'training', get_evaluation_criteria_options()[2]),$html);

            $html = str_replace(EMIS_GET_CURRENT_CB_ACTIVITY_INSTRUCTOR_EFFECTIVENESS_HIGHLY_SATISFACTORY_PERCENTAGE, get_percentage_cb_activity_survey_criteria($cb_activity,'instructor', get_evaluation_criteria_options()[3]),$html);

            // $html = str_replace(EMIS_GET_CURRENT_CB_ACTIVITY_CONTENT_SATISFACTORY_PERCENTAGE,20 ,$html);
            return $html;
        }
    }

    if (! function_exists ('get_cb_evaluation_report_training_numerics')) {
      function get_cb_evaluation_report_training_numerics($cb_activity,$child_count,$child,$total_unsat=0,$total_mod_sat=0,$total_sat=0,$total_h_sat=0){
        $unsat_count = 0;
        $mod_sat_count = 0;
        $sat_count = 0;
        $h_sat_count = 0;
        foreach ($cb_activity->participants as $person) {
          if($person->survey){
            $row_total = 0;
            $unsat_count += @$person->survey->training_evaluations->where('survey_eval_criteria_id',$child->id)->where('comment',get_evaluation_criteria_options()[0])->count();

            $mod_sat_count += @$person->survey->training_evaluations->where('survey_eval_criteria_id',$child->id)->where('comment',get_evaluation_criteria_options()[1])->count();

            $sat_count += @$person->survey->training_evaluations->where('survey_eval_criteria_id',$child->id)->where('comment',get_evaluation_criteria_options()[2])->count();

            $h_sat_count += @$person->survey->training_evaluations->where('survey_eval_criteria_id',$child->id)->where('comment',get_evaluation_criteria_options()[3])->count();
          }
        }

        $row_total = $unsat_count + $mod_sat_count + $sat_count + $h_sat_count;
        // $unsat_count++;
        $total_unsat += $unsat_count;
        $total_mod_sat += $mod_sat_count;
        $total_sat += $sat_count;
        $total_h_sat += $h_sat_count;

        $unsat_average = floor($total_unsat/$child_count);
        $mod_sat_average = floor($total_mod_sat/$child_count);
        $sat_average = floor($total_sat/$child_count);
        $h_sat_average = floor($total_h_sat/$child_count);

        $section_total_average = $unsat_average + $mod_sat_average + $sat_average + $h_sat_average;

        $unsat_percentage = floor(($unsat_average/$cb_activity->participants->count())*100);
        $mod_sat_percentage = floor(($mod_sat_average/$cb_activity->participants->count())*100);
        $sat_percentage = floor(($sat_average/$cb_activity->participants->count())*100);
        $h_sat_percentage = floor(($h_sat_average/$cb_activity->participants->count())*100);

        $row_percentage = $unsat_percentage + $mod_sat_percentage + $sat_percentage + $h_sat_percentage;

        return array(
          // Get counts
          'unsat_count'=>$unsat_count,
          'mod_sat_count'=>$mod_sat_count,
          'sat_count'=>$sat_count,
          'h_sat_count'=>$h_sat_count,
          'row_total'=>$row_total,

          // get totals
          'total_unsat'=>$total_unsat,
          'total_mod_sat'=>$total_mod_sat,
          'total_sat'=>$total_sat,
          'total_h_nsat'=>$total_h_sat,

          // get Averages
          'unsat_average'=>$unsat_average,
          'mod_sat_average'=>$mod_sat_average,
          'sat_average'=>$sat_average,
          'h_sat_average'=>$h_sat_average,
          'section_total_average'=>$section_total_average,

          // get percentages
          'unsat_percentage'=>$unsat_percentage,
          'mod_sat_percentage'=>$mod_sat_percentage,
          'sat_percentage'=>$sat_percentage,
          'h_sat_percentage'=>$h_sat_percentage

        );
      }
    }


    if (! function_exists ('get_cb_evaluation_report_training_numerics_2')) {
      function get_cb_evaluation_report_training_numerics_2($cb_activity){
        $survey_criteria = SurveyEvalCriteria::with('children')->whereNull('parent_id')->get();

        foreach($survey_criteria as $criteria){
            $child_count = $criteria->children->count();
            $total_unsat = 0;
            $total_mod_sat = 0;
            $total_sat = 0;
            $total_h_sat = 0;

            $total_percentage = 100;

          foreach($criteria->children as $child){

              $unsat_count = 0;
              $mod_sat_count = 0;
              $sat_count = 0;
              $h_sat_count = 0;
              foreach ($cb_activity->participants as $person) {
                if($person->survey){
                  $row_total = 0;
                  $unsat_count += @$person->survey->training_evaluations->where('survey_eval_criteria_id',$child->id)->where('comment',get_evaluation_criteria_options()[0])->count();
                  //
                  $mod_sat_count += @$person->survey->training_evaluations->where('survey_eval_criteria_id',$child->id)->where('comment',get_evaluation_criteria_options()[1])->count();

                  $sat_count += @$person->survey->training_evaluations->where('survey_eval_criteria_id',$child->id)->where('comment',get_evaluation_criteria_options()[2])->count();
                  //
                  $h_sat_count += @$person->survey->training_evaluations->where('survey_eval_criteria_id',$child->id)->where('comment',get_evaluation_criteria_options()[3])->count();
                }
              }

              // dd($unsat_count);


              $row_total = $unsat_count + $mod_sat_count + $sat_count + $h_sat_count;
              // $unsat_count++;
              $total_unsat += $unsat_count;
              $total_mod_sat += $mod_sat_count;
              $total_sat += $sat_count;
              $total_h_sat += $h_sat_count;

              $unsat_average = floor($total_unsat/$child_count);
              $mod_sat_average = floor($total_mod_sat/$child_count);
              $sat_average = floor($total_sat/$child_count);
              $h_sat_average = floor($total_h_sat/$child_count);

              $section_total_average = $unsat_average + $mod_sat_average + $sat_average + $h_sat_average;

              $unsat_percentage = floor(($unsat_average/$cb_activity->participants->count())*100);
              $mod_sat_percentage = floor(($mod_sat_average/$cb_activity->participants->count())*100);
              $sat_percentage = floor(($sat_average/$cb_activity->participants->count())*100);
              $h_sat_percentage = floor(($h_sat_average/$cb_activity->participants->count())*100);

              $row_percentage = $unsat_percentage + $mod_sat_percentage + $sat_percentage + $h_sat_percentage;

         }
       }
        return array(
          // Get counts
          'unsat_count'=>$unsat_count,
          'mod_sat_count'=>$mod_sat_count,
          'sat_count'=>$sat_count,
          'h_sat_count'=>$h_sat_count,
          'row_total'=>$row_total,

          // get totals
          'total_unsat'=>$total_unsat,
          'total_mod_sat'=>$total_mod_sat,
          'total_sat'=>$total_sat,
          'total_h_nsat'=>$total_h_sat,

          // get Averages
          'unsat_average'=>$unsat_average,
          'mod_sat_average'=>$mod_sat_average,
          'sat_average'=>$sat_average,
          'h_sat_average'=>$h_sat_average,
          'section_total_average'=>$section_total_average,

          // get percentages
          'unsat_percentage'=>$unsat_percentage,
          'mod_sat_percentage'=>$mod_sat_percentage,
          'sat_percentage'=>$sat_percentage,
          'h_sat_percentage'=>$h_sat_percentage

        );
      }
    }

    if(! function_exists('get_percentage_cb_activity_survey_criteria')){
        function get_percentage_cb_activity_survey_criteria($cb_activity,$section,$comment){
            // return $section;
            $criteria = 'App\SurveyEvalCriteria'::where("survey_question", "LIKE", "%".$section."%")->first();
            // dd($criteria);
            if($criteria){
            $child_count = $criteria->children->count();
            $total_unsat = 0;
            $total_mod_sat = 0;
            $total_sat = 0;
            $comment_total = 0;

            $total_percentage = 100;

            foreach($criteria->children as $child){
              $comment_count = 0;
                foreach ($cb_activity->participants as $person) {
                  if($person->survey){
                    $comment_count += @$person->survey->training_evaluations->where('survey_eval_criteria_id',$child->id)->where('comment',$comment)->count();
                  }
                }
                $comment_total += $comment_count;
            }
            $comment_average = floor($comment_total/$child_count);
            $comment_percentage = floor(($comment_average/$cb_activity->participants->count())*100);
            return $comment_percentage;
          }else{
            return "";
          }
        }
    }

    if(! function_exists('get_central_gov_id')){
        function get_central_gov_id(){
            return 'App\EntityType'::where("type_name", "LIKE", "%Central%")->orwhere("type_name", "LIKE", "%CG%")->first()->id;
        }
    }
    if(! function_exists('get_local_gov_id')){
        function get_local_gov_id(){
            return 'App\EntityType'::where("type_name", "LIKE", "%Local%")->orwhere("type_name", "LIKE", "%LG%")->first()->id;
        }
    }
    if(! function_exists('get_report_months')){
        function get_report_months(){
            return array(
                ['7','July'],
                ['8','August'],
                ['9','September'],
                ['10','October'],
                ['11','November'],
                ['12','December'],
                ['1','January'],
                ['2','February'],
                ['3','March'],
                ['4','April'],
                ['5','May'],
                ['6','June'],

            );
        }
    }

    if(! function_exists('get_month_name')){
        function get_month_name($monthInt){
            $months = get_months();
            return $months[$monthInt-1];
        }
    }

    if(! function_exists('get_response_types')){
        function get_response_types(){
            return array('Letter','Email','Phone Call');
        }
    }

    if(! function_exists('get_report_quarters')){
        function get_report_quarters(){
            return array(
                ['1','Quarter 1'],
                ['2','Quarter 2'],
                ['3','Quarter 3'],
                ['4','Quarter 4']

            );
        }
    }
    if(! function_exists('get_quarter_name')){
        function get_quarter_name($quarterInt){
            $quarters = get_quarters();
            return $quarters[$quarterInt-1];
        }
    }


    if (! function_exists ('enable_disable_admin_review')) {
      function enable_disable_admin_review($category,$from_admin_review){
        if($category== LEGAL_ADMIN_REVIEW_TAG || $category== LEGAL_INVESTIGATION_TAG){
          if($category== LEGAL_INVESTIGATION_TAG){
            if($from_admin_review){
                return "disabled";
            }
          }
        }else{
          return "disabled";
        }
      }
    }





}
