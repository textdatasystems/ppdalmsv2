<?php


namespace App\Helpers;


use App\ApiResp;
use App\ApiRespEdmsUpload;
use App\GlobalValue;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class EdmsDocumentUploadHandler
{

    //status codes
    public static  $STATUS_CODE_SUCCESS = "0";
    public static  $STATUS_CODE_FAILED = "1";
    public static $MAX_FILE_LENGTH = 100;

    public static function uploadDocumentToEdms(
        Request $request, $fileInputFieldName, $edmsSpecificBaseFolderName,
        $edmsDefaultBaseFolderName, $localStorageFolder = 'uploads/incoming_letter_uploads'){

        $resp = new ApiRespEdmsUpload();
        try{

            //no document uploaded
            if(!$request->hasFile($fileInputFieldName)){
                $resp->statusCode = self::$STATUS_CODE_FAILED;
                $resp->statusDescription = "File with input name[".$fileInputFieldName."] not found";
                return $resp;
            }

            //no EDMS folder specified
            if($edmsSpecificBaseFolderName == null && $edmsDefaultBaseFolderName == null){
                $resp->statusCode = self::$STATUS_CODE_FAILED;
                $resp->statusDescription = "Value not set for both [edmsSpecificBaseFolderName] and [edmsDefaultBaseFolderName]";
                return $resp;
            }

            //check file size
            $respFileSizeCheck = self::validateFileSize($request, $fileInputFieldName);
            if($respFileSizeCheck->statusCode != self::$STATUS_CODE_SUCCESS){
                return $respFileSizeCheck;
            }

            //get the file
            $file = $request->file($fileInputFieldName);

            //get base64 string content of the file
            $base64EncodedDocumentContents = base64_encode(file_get_contents($request->file($fileInputFieldName)->getPathname()));
            $fileName = $file->getClientOriginalName();
//            $fileName = preg_replace('/[^a-zA-Z0-9_. ]/', '_', $fileName);
            $fileName = preg_replace('/[^ .\-\w]+/', '_', $fileName);


            //check if the new file name field has data
            $newFileName = $request['new_upload_file_name'];
            if(isset($newFileName) && $newFileName != ''){

                $fileNameLengthCheckResp = self::validateFileNameLength($newFileName);
                if($fileNameLengthCheckResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                    return $fileNameLengthCheckResp;
                }

                $extension = pathinfo($fileName, PATHINFO_EXTENSION);
                $fileName = $newFileName.'.'.$extension;

            }else{

                $filenameWithOutExtension = pathinfo($fileName, PATHINFO_FILENAME);
                $extension = pathinfo($fileName, PATHINFO_EXTENSION);

                //check for the validity of the file name
                $fileNameLengthCheckResp = self::validateFileNameLength($filenameWithOutExtension);
                if($fileNameLengthCheckResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                    return $fileNameLengthCheckResp;
                }

            }

            //file path to store document in EDMS
            if($edmsSpecificBaseFolderName == 'DEFAULT_MEMO'){
                $edmsSpecificBaseFolderName = AppConstants::$EDMS_EMIS_DIRECTORY_INTERNAL_MEMOS;
            }

            $edmsBaseFolder = $edmsSpecificBaseFolderName == null || $edmsSpecificBaseFolderName == "DEFAULT" ? $edmsDefaultBaseFolderName : $edmsSpecificBaseFolderName;
            $pathToFileInEdms = $edmsBaseFolder . $fileName;

            //Move Uploaded File to the right folder
            $destinationPath = public_path($localStorageFolder);
            $file->move($destinationPath,$fileName);
            $fileLocalPath = $destinationPath . '/' . $fileName;

            //request to send to EDMS
            $data = [];
            $data['targetEdmsPath'] = $pathToFileInEdms;
            $data['documentContents'] = $base64EncodedDocumentContents;

            //send request to EDMS
            //todo implement GUZZLE request to send to EDMS, Post URL "http://192.168.33.45:801/api/edmstools/upload-document
            $apiResp  = DataLoader::uploadFileToEdmsServer($data) ;

            if($apiResp->statusCode != self::$STATUS_CODE_SUCCESS){

                //Upload to EDMS failed, Roll back actions
                self::deleteFile($fileLocalPath);

                $message = "Error occurred on sending document to EDMS [".$apiResp->statusDescription."], please contact the EDMS system administrator.";
                $resp->statusCode = self::$STATUS_CODE_FAILED;
                $resp->statusDescription = $message;
                return $resp;

            }else{

                //delete the path locally
                self::deleteFile($fileLocalPath);

                //return success
                $resp->statusCode = self::$STATUS_CODE_SUCCESS;
                $resp->statusDescription = $apiResp->statusDescription;
                $resp->edmsFilePath = $pathToFileInEdms;
                $resp->fileName = $fileName;
                $resp->fileLocalPath = $fileLocalPath;
                return $resp;

            }

        }catch (\Exception $exception){

            $resp->statusCode = self::$STATUS_CODE_FAILED;
            $resp->statusDescription = "Error occurred [".$exception->getMessage()."]";
            return $resp;

        }

    }

    /**
     * @param string $fileLocalPath
     */
    public static function deleteFile(string $fileLocalPath): void
    {
        if (file_exists($fileLocalPath)) {
            unlink($fileLocalPath);
        }
    }

    public static function validateFileNameLength($filenameWithOutExtension)
    {

        $nameLength = strlen($filenameWithOutExtension);
        $resp = new ApiRespEdmsUpload();
        if($nameLength >= self::$MAX_FILE_LENGTH){
            $resp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $resp->statusDescription = AppConstants::$MAX_FILE_LENGTH_EXCEEDED;
            $resp->fileName = $filenameWithOutExtension;
            $resp->suggestedFileName = FileHandler::truncate($filenameWithOutExtension,self::$MAX_FILE_LENGTH);
            return $resp;
        }

        $resp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
        $resp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
        return $resp;

    }

    private static function validateFileSize(Request $request, $fileInputFieldName)
    {

        $resp = new ApiRespEdmsUpload();

        try{

            $fileSize = $request->file($fileInputFieldName)->getSize();
            $sizeInMbs = $fileSize / 1048576;

            //get db setting
            $globalValue = GlobalValue::where('variable_name','=',AppConstants::$GLOBAL_VAL_MAX_FILE_UPLOAD_SIZE_MBS)->first();

            if($globalValue == null){
                $resp->statusCode = self::$STATUS_CODE_FAILED;
                $resp->statusDescription = "Global Value [".AppConstants::$GLOBAL_VAL_MAX_FILE_UPLOAD_SIZE_MBS."] not found in the database";
                return $resp;
            }

            $dbValueMaxUploadSize = $globalValue->value;

            if(!is_int((int)$dbValueMaxUploadSize)){
                $resp->statusCode = self::$STATUS_CODE_FAILED;
                $resp->statusDescription = "Invalid value for [".AppConstants::$GLOBAL_VAL_MAX_FILE_UPLOAD_SIZE_MBS."], value found [". $dbValueMaxUploadSize ."]";
                return $resp;
            }

            $valid = $sizeInMbs <= $dbValueMaxUploadSize;

            if(!$valid){
                $resp->statusCode = self::$STATUS_CODE_FAILED;
                $resp->statusDescription = "Uploaded file size exceeds allowed maximum of [". $dbValueMaxUploadSize ."MBS]";
                return $resp;
            }

            $resp->statusCode = self::$STATUS_CODE_SUCCESS;
            $resp->statusDescription = "SUCCESS";
            return $resp;

        }catch (\Exception $ex){

            $resp->statusCode = self::$STATUS_CODE_FAILED;
            $resp->statusDescription = AppConstants::generalError($ex->getMessage());
            return $resp;
        }

    }

}
