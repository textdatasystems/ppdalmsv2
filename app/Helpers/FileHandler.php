<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/25/19
 * Time: 12:13 PM
 */

namespace App\Helpers;


class FileHandler
{

    public static function deleteFile($filePath){

        if (file_exists($filePath)) {
            unlink($filePath);
        }

    }

    public  static function truncate($string, $length, $dots = "") {
        return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
    }

}
