<?php

namespace App;

use App\Helpers\AppConstants;
use App\Helpers\Security;
use Illuminate\Database\Eloquent\Model;

class IncomingLetter extends Model
{

    public static function laratablesCustomAction($letter)
    {
        $username = session(Security::$SESSION_USER) != null ? session(Security::$SESSION_USER)->username : '';
        $level = 'reception';

        $data = array(
            'username' => $username,
            'level' => $level,
            'letter' => $letter,
        );

        return view('List_Actions.list_action_new')->with($data)->render();

    }
    public static function laratablesCustomRegistryAction($letter)
    {
        $username = session(Security::$SESSION_USER) != null ? session(Security::$SESSION_USER)->username : '';
        $level = 'registry';
        return view('List_Actions.list_action_new', compact('letter','level','username'))->render();
    }
    public static function laratablesCustomEDAction($letter)
    {
        $username = session(Security::$SESSION_USER) != null ? session(Security::$SESSION_USER)->username : '';
        $level = 'ed';
        return view('List_Actions.list_action_new', compact('letter','level','username'))->render();
    }

    public static function laratablesCustomAssociationAction($letter)
    {
        $level = 'ed';
        return view('List_Actions.list_action_associate_incoming', compact('letter','level'))->render();
    }

    public static function laratablesCustomReadOnlyAction($letter)
    {
        $level = 'ed';
        $data = array(
            'level' => $level,
            'letter' => $letter,
        );
        return view('List_Actions.list_action_readonly_incoming')->with($data)->render();

    }

    public static function laratablesCustomCheckBox($letter)
    {
        $level = 'ed';
        $letterCategory = 'incoming';
        return view('extra_cols.associate_cbx', compact('letter','level','letterCategory'))->render();
    }

    public static function laratablesAdditionalColumns()
    {
        $columns = ['incoming_letter_file_name','incoming_letter_upload_user','from_user_full_name'];
        return $columns;
    }

    public function letterType(){
        return $this->belongsTo('App\LetterType');
    }

    /*public function letterMovements(){
        return $this->hasMany('App\LetterMovement','letter_id','id');
    }*/

    public function letterMovements(){
        return $this->morphMany('App\LetterMovement','moveable');
    }

    public static function laratablesCustomLetterTypeName($letter)
    {
        $type = LetterType::where('id','=', $letter->letter_type_id)->first();
        $data = $type != null ? $type->letter_type_name : '';
        return $data;
    }

    /**
     * Returns truncated name for the datatables.
     *
     * @param \App\User
     * @return string
     */
    public static function laratablesSubject($letter)
    {
        if($letter->letter_category == AppConstants::$LETTER_CATEGORY_INCOMING_LETTER){
            return $letter->subject;
        }else{
            return $letter->subject .' ('.$letter->from_user_full_name.')';
        }
    }

    public function getDateSentAttribute($value){
        return date("M d Y", strtotime($value));
    }

    public function getDateReceivedPpdaAttribute($value){
        return date("M d Y", strtotime($value));
    }

    public function setSubjectAttribute($value){

        $trimmedContent = trim($value);
        $trimmedContent = strtoupper($trimmedContent);
        $trimmedContent = preg_replace('/\s+/', ' ',$trimmedContent);
        $this->attributes['subject'] = $trimmedContent;

    }



}
