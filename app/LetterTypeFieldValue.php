<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LetterTypeFieldValue extends Model
{

    public function letterTypeField(){
        return $this->belongsTo('App\LetterTypeField');
    }

}
