<?php

namespace App\Http\Resources;

use App\LetterType;
use App\LetterTypeFieldValue;
use Illuminate\Http\Resources\Json\JsonResource;

class IncomingLetter extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $letterType = LetterType::where('id','=',$this->letter_type_id)->first();

        $extraFieldValues = LetterTypeFieldValue::with('letterTypeField')
            ->where('letter_id','=',$this->id)
            ->where('letter_type_id','=',$this->letter_type_id)
            ->get();

        $dateSent = date("F d Y", strtotime($this->date_sent));
        $dateReceivedPPDA = date("F d Y", strtotime($this->date_received_ppda));
        $dateReceivedRegistry = date("F d Y", strtotime($this->date_received_registry));

        return [
            'id'=>$this->id,
            'entity_name'=>$this->entity_name,
            'subject'=>$this->subject,
            'date_sent'=> $dateSent,//$this->date_sent,
            'date_received_ppda'=>$dateReceivedPPDA,//$this->date_received_ppda,
            'date_received_registry'=>$dateReceivedRegistry, //$this->date_received_registry,
            'sender_reference_number'=>$this->sender_reference_number,
            'ppda_reference_number'=>$this->ppda_reference_number,
            'attention_to'=>$this->attention_to,
            'signatory'=>$this->signatory,
            'signatory_title'=>$this->signatory_title,
            'incoming_letter_file_path'=>$this->incoming_letter_file_path,
            'incoming_letter_file_name'=>$this->incoming_letter_file_name,
            'letter_type'=>$letterType,
            'extra_fields'=>$extraFieldValues,
        ];
    }

}
