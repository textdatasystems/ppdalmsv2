<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LetterAssociation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'=>$this->id,
            'entity_name'=>$this->entity_name,
            'subject'=>$this->subject,
            'date_sent'=> $dateSent,//$this->date_sent,
            'date_received_ppda'=>$dateReceivedPPDA,//$this->date_received_ppda,
            'date_received_registry'=>$dateReceivedRegistry, //$this->date_received_registry,
            'sender_reference_number'=>$this->sender_reference_number,
            'ppda_reference_number'=>$this->ppda_reference_number,
            'attention_to'=>$this->attention_to,
            'signatory'=>$this->signatory,
            'signatory_title'=>$this->signatory_title,
            'incoming_letter_file_path'=>$this->incoming_letter_file_path,
            'incoming_letter_file_name'=>$this->incoming_letter_file_name,
            'letter_type'=>$letterType,
            'extra_fields'=>$extraFieldValues,
        ];

    }
}
