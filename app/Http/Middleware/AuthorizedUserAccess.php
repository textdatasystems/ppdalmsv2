<?php

namespace App\Http\Middleware;

use App\Helpers\Security;
use Closure;

class AuthorizedUserAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
        * If the user object is not set we redirect to the home page
        * */
        if (session(Security::$SESSION_USER) == null) {

            return redirect('/');

        }

        /*
         * Else we proceed with what we want to access
         * */
        return $next($request);

    }
}
