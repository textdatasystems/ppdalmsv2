<?php

namespace App\Http\Middleware;

use App\Helpers\AppConstants;
use App\Helpers\Security;
use Closure;

class EdAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
          * If the user object is not set we redirect to the home page
          * */
        $lmsRole = session(Security::$SESSION_USER)->lmsRole;
        if ($lmsRole == null || !$lmsRole->edOffice) {
            return redirect(route('letter_movement'));
        }

        /*
         * Else we proceed with what we want to access
         * */
        return $next($request);
    }
}
