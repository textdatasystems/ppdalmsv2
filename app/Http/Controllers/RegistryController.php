<?php

namespace App\Http\Controllers;

use App\Helpers\Security;
use Illuminate\Http\Request;

class RegistryController extends Controller
{
    public function index()
    {
        $data = array(
            'menu_selected'=>'registry',
            'level'=> session(Security::$SESSION_USER_LEVEL),
        );
        return view('Registry.index')->with($data);
    }
}
