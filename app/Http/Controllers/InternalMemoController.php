<?php

namespace App\Http\Controllers;

use App\ApiResp;
use App\Helpers\AppConstants;
use App\Helpers\DataLoader;
use App\Helpers\EmailHandler;
use App\Helpers\Security;
use App\IncomingLetter;
use App\InternalMemo;
use App\MailingList;
use Carbon\Carbon;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class InternalMemoController extends Controller
{

    public function receptionIndex(){
        return view('Memos.Reception.index');
    }

    public function registryIndex(){
        return view('Memos.Registry.index');
    }

    public function edOfficeIndex(){
        return view('Memos.Ed.index');
    }

    public function receptionList(){

        return Laratables::recordsOf(InternalMemo::class, function ($query) {
            return $query->where('current_que', 'reception')->orderBy('id','desc');
        });

    }

    public function registryList(){

        return Laratables::recordsOf(InternalMemo::class, function ($query) {
            return $query->where('current_que', 'registry')->orderBy('id','desc');
        });

    }

    public function edOfficeList(){

        return Laratables::recordsOf(InternalMemo::class, function ($query) {
            return $query->where('current_que', 'ed')->orderBy('id','desc');
        });

    }

    public function create(){
        $data = array(
            'users' => ApiController::getPpdaUsers()->data,
        );
        return view('Memos.create')->with($data);
    }

    public function edit(InternalMemo $internalMemo){
        $data = array(
            'users' => ApiController::getPpdaUsers()->data,
            'memo' => $internalMemo,
        );
        return view('Memos.create')->with($data);
    }

    public function forward(InternalMemo $internalMemo, $que)
    {

        $internalMemo->current_que = $que;

        if($que == 'registry'){

            $authenticatedUser = session(Security::$SESSION_USER);
            $internalMemo->registry_entry_user = $authenticatedUser != null ? $authenticatedUser->email : '';

            $dateNow = Carbon::now();
            $internalMemo->registry_entry_date = $dateNow;
            $internalMemo->save();

            $emailResp = $this->sendLetterForwardedToRegistryMailingList($internalMemo);

        }else if($que == 'ed'){

            $authenticatedUser = session(Security::$SESSION_USER);
            $internalMemo->eds_office_entry_user = $authenticatedUser != null ? $authenticatedUser->email : '';
            $internalMemo->eds_office_entry_date = Carbon::now();
            $internalMemo->save();

            $emailResp = $this->sendLetterForwardedToEdOfficeMailingList($internalMemo);

        }

        echo $internalMemo->subject . ' - forwarded successfully to '.$que;

    }

    private function sendLetterForwardedToEdOfficeMailingList($letter)
    {

        $apiResp = new ApiResp();
        try{

            /*
             * When email is received at ED's office, email should be to both management and Jerri
             * */
            self::notifyRegistrySender($letter);

            $mailingList = [];
            $mailingListName = 'ED_Office_Mailing_List';

            $customMailingList = MailingList::where('list_name','=', $mailingListName)->first();
            if($customMailingList != null){
                $customList = $customMailingList == null ? [] : explode(',', trim($customMailingList->email_addresses));
                $mailingList = $customList;
            }

            $letterType = $letter->letterType->letter_type_name;
            $subject = "Letter Movement: New Letter has been received at ED's Office(Letter Type: ".$letterType.")";
            $description = "New Letter has been received at ED's Office";
            $message = view('mails.ed_office_notification', compact('letter','description'))->render();

            $apiResp = $this->sendLetterForwardedNotificationEmail($subject, $message,$mailingList);
            return $apiResp;

        }catch (\Exception $exception){
            $apiResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $apiResp->statusDescription = $exception->getMessage();
            return $apiResp;
        }

    }


    private function sendLetterForwardedToRegistryMailingList($internalMemo)
    {

        $apiResp = new ApiResp();
        try{

            /*
             * Send to every one in Registry(Registry Mailing List), Send copy to sender from Reception
             * */
            $this->notifyReceptionSender($internalMemo);

            $mailingList = [];

            $mailingListName = 'Registry_Mailing_List';

            $customMailingList = MailingList::where('list_name','=', $mailingListName)->first();
            if($customMailingList != null){
                $customList = $customMailingList == null ? [] : explode(',', trim($customMailingList->email_addresses));
                $mailingList = $customList;
            }

            $memoType = $internalMemo->memo_type;
            $subject = "Letter Movement: New Internal Memo has been received at Registry(Memo Type: ".$memoType.")";
            $description = "New Internal Memo has been received at Registry";
            $message = view('mails.memo_notification', compact('internalMemo','description'))->render();

            $apiResp = $this->sendLetterForwardedNotificationEmail($subject, $message,$mailingList);
            $apiResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $apiResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $apiResp;

        }
        catch (\Exception $exception){
            $apiResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $apiResp->statusDescription = $exception->getMessage();
            return $apiResp;
        }

    }

    private function notifyReceptionSender($internalMemo){

        $createdBy = $internalMemo->created_by_user;

        $receptionSender = [
            $createdBy,
        ];

        $memoType = $internalMemo->memo_type;
        $subject = "Letter Movement: You have sent an Internal Memo to Registry(Memo Type: ".$memoType.")";
        $description = "New Internal Memo has been sent by you to Registry";
        $message = view('mails.memo_notification', compact('internalMemo','description'))->render();

        $apiResp = $this->sendLetterForwardedNotificationEmail($subject, $message,$receptionSender);
        $apiResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
        $apiResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
        return $apiResp;

    }

    private function notifyRegistrySender($internalMemo){

        $senderEmail = session(Security::$SESSION_USER)->username;

        $registrySender = [
            $senderEmail,
        ];

        $memoType = $internalMemo->memo_type;
        $subject = "Letter Movement: You have sent a New Internal Memo to ED's Office(Memo Type: ".$memoType.")";
        $description = "New Internal Memo has been sent by you to ED's Office";
        $message = view('mails.memo_notification', compact('internalMemo','description'))->render();

        $apiResp = $this->sendLetterForwardedNotificationEmail($subject, $message,$registrySender);
        $apiResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
        $apiResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
        return $apiResp;

    }

    private function sendLetterForwardedNotificationEmail($subject, $message, array $mailingList)
    {

        $apiResp = new ApiResp();
        foreach ($mailingList as $email){
            $apiResp = EmailHandler::sendPlainTextEmailPHPMAILER($email, $subject, $message);
        }

        return $apiResp;

    }

    public function uploadInternalMemo(InternalMemo $internalMemo)
    {
        $data = array(
            'memo'=>$internalMemo
        );
        return view('Memos.upload')->with($data);
    }

    public function getInternalMemoDownloadLink(InternalMemo $internalMemo){

        //get document from EDMS, result has base64String content of the document
        $data = [];
        $data['edmsDocumentPath'] = $internalMemo->edms_document_path;
        $apiResp  = DataLoader::downloadFileFromEdmsServer($data);

        if($apiResp->statusCode == AppConstants::$STATUS_CODE_SUCCESS){

            $documentBase64String = $apiResp->result;
            $decoded = base64_decode($documentBase64String);

            $timeNow = Carbon::now()->timestamp;
            $fileName = '/'.$timeNow.'.pdf';

            //temporarily store into a file
            $destinationPath = public_path('uploads/incoming_letter_uploads/temp');
            $filePath = $destinationPath . $fileName;

            file_put_contents($filePath, $decoded);

            $fileURL = asset('uploads/incoming_letter_uploads/temp'.$fileName);

            $data = [];
            $data['statusCode'] = 0;
            $data['statusDescription'] = "SUCCESS";
            $data['tempPath'] = $fileName; //used for deleting the temporarily file created
            $data['result'] = $fileURL; //used for rendering the PDF using Jquery UI - PDF Viewer
            return json_encode($data);

        }else{

            $data['statusCode'] = 1;
            $data['statusDescription'] = $apiResp->statusDescription;
            return json_encode($data);

        }

    }

    public function removeAttachedInternalMemo(InternalMemo $internalMemo){

        try{

            $loggedInUser = session(Security::$SESSION_USER)->username;
            $uploadUser = $internalMemo->memo_upload_user;

            //prevent remove action if not upload by you
            if($loggedInUser != $uploadUser){
                Session::flash('errorMessage', 'You cannot remove a document that was not uploaded by you.');
                return redirect()->back();
            }

            //First go to EDMS and remove the document, if successful, remove the upload details locally in LMS as well
            $edmsDocumentPath = $internalMemo->edms_document_path;
            if($edmsDocumentPath != null){

                //request to send to EDMS
                $data = [];
                $data['edmsDocumentPath'] = $edmsDocumentPath;

                //send request to EDMS
                $apiResp  = DataLoader::deleteDocumentEdmsServer($data);

                $statusDescription = $apiResp->statusDescription;
                if($apiResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS && !Str::contains(strtolower($statusDescription), 'document not found')){
                    //return error occurred on Delete document from EDMS
                    Session::flash('errorMessage', 'Error on deleting document. '.$apiResp->statusDescription);
                    return redirect()->back();
                }

            }

            //deletion in EDMS was successful, proceed locally
            $internalMemo->memo_file_name = null;
            $internalMemo->memo_file_path= null;
            $internalMemo->memo_upload_user= null;
            $internalMemo->memo_upload_date_time= null;
            $internalMemo->edms_document_path= null;
            $internalMemo->save();

            Session::flash('successMessage', 'Document successfully deleted');
            return redirect()->back();

        }catch (\Exception $exception){
            //return error occurred on Delete document from EDMS
            Session::flash('errorMessage', 'Error on deleting document ['.$exception->getMessage().']');
            return redirect()->back();
        }

    }

}
