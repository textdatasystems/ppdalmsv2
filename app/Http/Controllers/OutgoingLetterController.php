<?php

namespace App\Http\Controllers;

use App\Helpers\AppConstants;
use App\Helpers\DataGenerator;
use App\Helpers\DataLoader;
use App\Helpers\FileHandler;
use App\Helpers\Security;
use App\IncomingLetter;
use App\LetterType;
use App\OutgoingLettersLetterTypeFieldValue;
use Carbon\Carbon;

use Freshbitsweb\Laratables\Laratables;
use App\OutgoingLetter;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class OutgoingLetterController extends Controller
{
    public $apiService;

    public function __construct(ApiController $apiService)
    {
        $this->apiService = $apiService;
    }


    public function index()
    {
        $data = array(
            'menu_selected'=>'outgoingletters',
            'level'=> session(Security::$SESSION_USER_LEVEL),
        );
        return view('OutgoingLetters.index')->with($data);
    }

    public function create()
    {

        $letterTypes = LetterType::all();
        $signatoryTitles = DataGenerator::outgoingLetterSignatoryTitles();
        $recipientTitles = DataGenerator::signatoryTitles();
        $departmentsOfOrigin = DataGenerator::departmentOfOrigin();

        $data = array(
            'entities' => $this->apiService->getEntities()->payload,
            'letterTypes' => $letterTypes,
            'signatoryTitles' => $signatoryTitles,
            'recipientTitles' => $recipientTitles,
            'departmentsOfOrigin' => $departmentsOfOrigin,
            'providers' => ApiController::getProviders()->payload,
            'ed' => session(Security::$SESSION_ED),
            'units' => session(Security::$SESSION_DEPT_UNITS),
            'users' => ApiController::getPpdaUsers()->data,
        );

        return view('OutgoingLetters.create')->with($data);

    }

    public function edit(OutgoingLetter $outgoingLetter)
    {

        $letterTypes = LetterType::all();
        $signatoryTitles = DataGenerator::outgoingLetterSignatoryTitles();
        $departmentsOfOrigin = DataGenerator::departmentOfOrigin();
        $recipientTitles = DataGenerator::signatoryTitles();

        $extraFieldValues = OutgoingLettersLetterTypeFieldValue::with('letterTypeField')
            ->where('outgoing_letter_id','=',$outgoingLetter->id)
            ->where('letter_type_id','=',$outgoingLetter->letter_type_id)
            ->get();

        $data = array(
            'letter'=>$outgoingLetter,
            'entities' => $this->apiService->getEntities()->payload,
            'letterTypes' => $letterTypes,
            'signatoryTitles' => $signatoryTitles,
            'recipientTitles' => $recipientTitles,
            'departmentsOfOrigin' => $departmentsOfOrigin,
            'extraFieldValues' => $extraFieldValues,
            'providers' => ApiController::getProviders()->payload,
            'ed' => session(Security::$SESSION_ED),
            'users' => ApiController::getPpdaUsers()->data,
            'units' => session(Security::$SESSION_DEPT_UNITS),
        );

        return view('OutgoingLetters.create')->with($data);

    }

    public function uploadOutgoingLetter(OutgoingLetter $outgoingLetter)
    {

        $data = array(
            'letter'=>$outgoingLetter
        );

        return view('OutgoingLetters.upload')->with($data);
    }

    public function uploadOutgoingLetterReceipt(OutgoingLetter $outgoingLetter)
    {

        $data = array(
            'letter'=>$outgoingLetter
        );

        return view('OutgoingLetters.upload-receipt')->with($data);
    }

    public function renameAttachedDocumentView(OutgoingLetter $letter)
    {

        $data = array(
            'letter'=>$letter
        );

        return view('OutgoingLetters.rename-attached-doc')->with($data);
    }

    public function downloadAttachedOutgoingLetter(OutgoingLetter $outgoingLetter){

        try{

            $fileName = $outgoingLetter->outgoing_letter_file_name;

            if($fileName == null){
                return redirect()->back();
            }

            $path_to_file = public_path()."/uploads/outgoing_letter_uploads/".$fileName;
            return Response::download($path_to_file,$fileName);

        }catch (\Exception $ex){
            return redirect()->back();
        }

    }

    public function removeAttachedOutgoingLetter(OutgoingLetter $outgoingLetter){

        try {

            $loggedInUser = session(Security::$SESSION_USER)->username;
            $uploadUser = $outgoingLetter->outgoing_letter_upload_user;

            //prevent remove action if not upload by you
            if($loggedInUser != $uploadUser){
                Session::flash('errorMessage', 'You cannot remove a document that was not uploaded by you.');
                return redirect()->back();
            }

            //First go to EDMS and remove the document, if successful, remove the upload details locally in LMS as well
            $edmsDocumentPath = $outgoingLetter->edms_document_path;
            if($edmsDocumentPath != null){

                $data = [];
                $data['edmsDocumentPath'] = $edmsDocumentPath;

                //send request to EDMS
                $apiResp  = DataLoader::deleteDocumentEdmsServer($data);

                $statusDescription = $apiResp->statusDescription;
                if($apiResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS && !Str::contains(strtolower($statusDescription), 'document not found')){

                    //return error occurred on Delete document from EDMS
                    Session::flash('errorMessage', 'Error on deleting document. '.$apiResp->statusDescription);
                    return redirect()->back();

                }

            }

            //deletion in EDMS was successful, proceed locally
            $outgoingLetter->outgoing_letter_file_name = null;
            $outgoingLetter->outgoing_letter_file_path= null;
            $outgoingLetter->outgoing_letter_upload_user= null;
            $outgoingLetter->upload_date_time= null;
            $outgoingLetter->edms_document_path= null;
            $outgoingLetter->save();

            Session::flash('successMessage', 'Document successfully deleted');
            return redirect()->back();

        }catch (\Exception $exception){

            //return error occurred on Delete document from EDMS
            Session::flash('errorMessage', 'Error on deleting document ['.$exception->getMessage().']');
            return redirect()->back();

        }

    }


    public function removeAttachedOutgoingLetterReceipt(OutgoingLetter $outgoingLetter){

        try {

            $loggedInUser = session(Security::$SESSION_USER)->username;

            //get the user who uploaded the document
            $uploadUser = $outgoingLetter->outgoing_letter_receipt_upload_user;

            //prevent remove action if not by uploader
            if($loggedInUser != $uploadUser){
                Session::flash('errorMessage', 'You cannot remove a document that was not uploaded by you.');
                return redirect()->back();
            }

            //First go to EDMS and remove the document, if successful, remove the upload details locally in LMS as well
            $edmsDocumentPath = $outgoingLetter->edms_document_receipt_path;
            if($edmsDocumentPath != null){

                $data = [];
                $data['edmsDocumentPath'] = $edmsDocumentPath;

                //send request to EDMS
                $apiResp  = DataLoader::deleteDocumentEdmsServer($data);

                //removal from edms failed
                $statusDescription = $apiResp->statusDescription;
                if($apiResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS && !Str::contains(strtolower($statusDescription), 'document not found')){

                    //return error occurred on Delete document from EDMS
                    Session::flash('errorMessage', 'Error on deleting document. '.$apiResp->statusDescription);
                    return redirect()->back();

                }

            }

            //deletion in EDMS was successful, proceed to delete the details locally
            $outgoingLetter->outgoing_letter_receipt_file_name = null;
            $outgoingLetter->outgoing_letter_receipt_file_path= null;
            $outgoingLetter->outgoing_letter_receipt_upload_user= null;
            $outgoingLetter->upload_receipt_date_time= null;
            $outgoingLetter->edms_document_receipt_path= null;
            $outgoingLetter->save();

            Session::flash('successMessage', 'Document successfully deleted');
            return redirect()->back();

        }catch (\Exception $exception){

            //return error occurred on Delete document from EDMS
            Session::flash('errorMessage', 'Error on deleting document ['.$exception->getMessage().']');
            return redirect()->back();

        }

    }


    public function list()
    {
        return Laratables::recordsOf(OutgoingLetter::class, function ($query) {
            return $query->orderBy('id','desc');
        });
       // return Laratables::recordsOf(OutgoingLetter::class);
    }

    public function getLetterDownloadLink(OutgoingLetter $outgoingLetter){

        $data = [];
        $data['edmsDocumentPath'] = $outgoingLetter->edms_document_path;

        $apiResp  = DataLoader::downloadFileFromEdmsServer($data);

        if($apiResp->statusCode == AppConstants::$STATUS_CODE_SUCCESS){

            $documentBase64String = $apiResp->result;
            $decoded = base64_decode($documentBase64String);

            $timeNow = Carbon::now()->timestamp;
            $fileName = '/'.$timeNow.'.pdf';

            //temporarily store into a file
            $destinationPath = public_path('uploads/outgoing_letter_uploads/temp');
            $file = $destinationPath . $fileName;
            file_put_contents($file, $decoded);

            $fileURL = asset('uploads/outgoing_letter_uploads/temp'.$fileName);

            $data = [];
            $data['statusCode'] = 0;
            $data['statusDescription'] = "SUCCESS";
            $data['tempPath'] = $fileName;  //used for deleting the temporarily file created
            $data['result'] = $fileURL; //used for rendering the PDF using Jquery UI - PDF Viewer
            return json_encode($data);
        }else{

            $data['statusCode'] = 1;
            $data['statusDescription'] = $apiResp->statusDescription;
            return json_encode($data);

        }

    }

    public function getLetterReceiptDownloadLink(OutgoingLetter $outgoingLetter){

        $data = [];
        $data['edmsDocumentPath'] = $outgoingLetter->edms_document_receipt_path;

        $apiResp  = DataLoader::downloadFileFromEdmsServer($data);

        if($apiResp->statusCode == AppConstants::$STATUS_CODE_SUCCESS){

            $documentBase64String = $apiResp->result;
            $decoded = base64_decode($documentBase64String);

            $timeNow = Carbon::now()->timestamp;
            $fileName = '/'.$timeNow.'.pdf';

            //temporarily store into a file
            $destinationPath = public_path('uploads/outgoing_letter_uploads/temp');
            $file = $destinationPath . $fileName;
            file_put_contents($file, $decoded);

            $fileURL = asset('uploads/outgoing_letter_uploads/temp'.$fileName);

            $data = [];
            $data['statusCode'] = 0;
            $data['statusDescription'] = "SUCCESS";
            $data['tempPath'] = $fileName;  //used for deleting the temporarily file created
            $data['result'] = $fileURL; //used for rendering the PDF using Jquery UI - PDF Viewer
            return json_encode($data);
        }else{

            $data['statusCode'] = 1;
            $data['statusDescription'] = $apiResp->statusDescription;
            return json_encode($data);

        }

    }


    public function deleteTempDocument($documentName){

        $fileName = '/' . $documentName;
        $destinationPath = public_path('uploads/outgoing_letter_uploads/temp').$fileName;

        FileHandler::deleteFile($destinationPath);
        $data = [];
        $data['statusCode'] = 0;
        $data['statusDescription'] = "SUCCESS";
        return json_encode($data);

    }

    public function associations(OutgoingLetter $outgoingLetter){

        $data = array(
            'letterId'=> $outgoingLetter->id,
            'letterCategory' => 'outgoing'
        );
        return view('associations.associations')->with($data);

    }


}
