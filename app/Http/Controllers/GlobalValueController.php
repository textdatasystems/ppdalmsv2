<?php

namespace App\Http\Controllers;

use App\GlobalValue;
use App\Helpers\AppConstants;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;

class GlobalValueController extends Controller
{

    public function create(){
        return view('master_data.general.global_values.create');
    }

    public function edit(GlobalValue $globalValue){
        return view('master_data.general.global_values.create')->with(compact('globalValue'));
    }

    public function list()
    {
        return Laratables::recordsOf(GlobalValue::class);
    }

    public function remove(GlobalValue $globalValue){

        try {

            //if it exists in what's referenced we don't delete
            if(!in_array($globalValue->variable_name, AppConstants::referencedGlobalValues())){

                $globalValue->delete();

            }else{
                echo "Cannot delete the global value [".$globalValue->variable_name."]. Variable has references";
            }

        } catch (\Exception $e) {
        }

        return redirect()->back();

    }

}
