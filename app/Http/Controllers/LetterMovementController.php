<?php

namespace App\Http\Controllers;

use App\ApiResp;
use App\Helpers\AppConstants;
use App\Helpers\EmailHandler;
use App\Helpers\Security;
use App\IncomingLetter;
use App\InternalMemo;
use App\LetterMovement;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;

class LetterMovementController extends Controller
{

    public $menu_selected = 'letter_movements';

    public static function handleIncomingLetterNotificationForward(array $mailingList, $modelId, $senderName, $dateSent, $deadlineDate, $actionRequired): ApiResp
    {

        $letter = IncomingLetter::with('letterType')->find($modelId);

        $letterType = $letter->letterType->letter_type_name;
        $subject = "Letter Movement: New Letter has been forwarded to you(Letter Type: " . $letterType . ")";
        $description = "New Letter has been for forwarded to you";

        $apiResp = new ApiResp();

        foreach ($mailingList as $emailData) {

            $email = $emailData['email'];
            $department = $emailData['department'];

            $data = array(
                'letter' => $letter,
                'description' => $description,
                'senderName' => $senderName,
                'dateSent' => $dateSent,
                'deadlineDate' => $deadlineDate,
                'actionRequired' => $actionRequired,
                'department' => $department,
            );

            $message = view('mails.letter_forwarded_notification')->with($data)->render();

            $apiResp = EmailHandler::sendPlainTextEmailPHPMAILER($email, $subject, $message);

        }

        return $apiResp;

    }


    public static function handleInternalMemoNotificationForward(array $mailingList, $modelId, $senderName, $dateSent, $deadlineDate, $actionRequired): ApiResp
    {

        $internalMemo = InternalMemo::find($modelId);

        $subject = "Letter Movement: New Internal Memo has been forwarded to you(Memo Type: " . $internalMemo->memo_type . ")";
        $description = "New Internal Memo has been for forwarded to you";

        $apiResp = new ApiResp();

        foreach ($mailingList as $emailData) {

            $email = $emailData['email'];
            $department = $emailData['department'];

            $data = array(
                'memo' => $internalMemo,
                'description' => $description,
                'senderName' => $senderName,
                'dateSent' => $dateSent,
                'deadlineDate' => $deadlineDate,
                'actionRequired' => $actionRequired,
                'department' => $department,
            );

            $message = view('mails.memo_forwarded_notification')->with($data)->render();

            $apiResp = EmailHandler::sendPlainTextEmailPHPMAILER($email, $subject, $message);

        }

        return $apiResp;

    }

    public function index()
    {
        $data = array(
            'menu_selected' => $this->menu_selected,
            'section' => '',
            'level'=> session(Security::$SESSION_USER_LEVEL)
        );
        return view('letter_movements.index')->with($data);
    }

    public function indexInternalMemo()
    {
        $data = array(
            'menu_selected' => $this->menu_selected,
            'section' => '',
            'level'=> session(Security::$SESSION_USER_LEVEL)
        );
        return view('letter_movements.memo.index')->with($data);
    }

    public function launch($documentType, $documentId){

        $model = $documentType == AppConstants::$LETTER_MOVT_DOC_TYPES_INCOMING_LETTER ? 'App\IncomingLetter' : 'App\InternalMemo';
        $modelId = $documentId;

        $data = array(
            'menu_selected' => $this->menu_selected,
            'section' => '',
            'level'=> session(Security::$SESSION_USER_LEVEL),
            'model' => $model,
            'modelId' => $modelId,
            'users' => ApiController::getPpdaUsers()->data
        );

        return view('letter_movements.launch_letter_movement_dynamic')->with($data);

    }

    public function forward($documentType, $documentId){

        if($documentType == AppConstants::$LETTER_MOVT_DOC_TYPES_INCOMING_LETTER){
            $model = 'App\IncomingLetter';
        }else if($documentType == AppConstants::$LETTER_MOVT_DOC_TYPES_INTERNAL_MEMO){
            $model = 'App\InternalMemo';
        }else{
            $model = '';
        }

        $loggedInUser = session(Security::$SESSION_USER);

        $data = array(
            'menu_selected' => $this->menu_selected,
            'section' => '',
            'level'=> session(Security::$SESSION_USER_LEVEL),
            'model' => $model,
            'modelId' => $documentId,
            'users' => ApiController::getPpdaUsers()->data,
            'loggedInUser' => $loggedInUser
        );

        return view('letter_movements.create')->with($data);

    }

    public function returnIncomingLetter($documentId, $returnTo, $documentType){

        $loggedInUser = session(Security::$SESSION_USER);
        $returnToUser = [];
        $model = $documentType == AppConstants::$LETTER_MOVT_DOC_TYPES_INCOMING_LETTER ? 'App\IncomingLetter' : 'App\InternalMemo';
        $modelId = $documentId;

        if($returnTo == 'sender'){
            $letterMovt = LetterMovement::where('to','=',$loggedInUser->username)->where('moveable_id','=',$modelId)->where('moveable_type','=',$model)->orderBy('id','desc')->first();
            $returnToUser['name'] = $letterMovt->from_name;
            $returnToUser['username'] = $letterMovt->from;
            $returnToUser['department'] = $letterMovt->from_department;
        }else if($returnTo == 'ed'){
            $letterMovt = LetterMovement::where('moveable_id','=',$modelId)->where('moveable_type','=',$model)->orderBy('id','asc')->first();
            $returnToUser['name'] = $letterMovt->from_name;
            $returnToUser['username'] = $letterMovt->from;
            $returnToUser['department'] = $letterMovt->from_department;
        }

        $data = array(
            'menu_selected' => $this->menu_selected,
            'section' => '',
            'level'=> session(Security::$SESSION_USER_LEVEL),
            'model' => $model,
            'modelId' => $modelId,
            'users' => ApiController::getPpdaUsers()->data,
            'loggedInUser' => $loggedInUser,
            'returnToUser' => $returnToUser
        );

        return view('letter_movements.return_letter_movement')->with($data);

    }

    public static function sendLetterForwardedNotificationEmail(array $mailingList, $modelId, $senderName, $dateSent, $deadlineDate, $actionRequired, $emailDataModelType)
    {

        $timoTest = [
          'email' => 'tkasaga6@gmail.com',
        'department' => 'Test Department',
        ];
        array_push($mailingList,$timoTest);

        if($emailDataModelType == AppConstants::$LETTER_MOVT_DOC_TYPES_INCOMING_LETTER){

            return self::handleIncomingLetterNotificationForward($mailingList, $modelId, $senderName, $dateSent, $deadlineDate, $actionRequired);

        }else{

            return self::handleInternalMemoNotificationForward($mailingList, $modelId, $senderName, $dateSent, $deadlineDate, $actionRequired);

        }

    }

    public function viewLetterMovement(IncomingLetter $letter, $menu){

        $letterMovements = LetterMovement::where('moveable_id','=',$letter->id)->where('moveable_type','=','App\IncomingLetter')->orderBy('id','asc')->get();

        //most recent assignment
        $recentAssignment = LetterMovement::where('moveable_id','=',$letter->id)->where('moveable_type','=','App\IncomingLetter')->orderBy('id','desc')->first();

        //currentAssignee
        $currentAssignee = $recentAssignment != null ? $recentAssignment->to : '';

        $loggedInUser = session(Security::$SESSION_USER);

        //isCurrentAssignee
        $isCurrentAssignee = $currentAssignee == $loggedInUser->username;

        //check whether we should show the return to sender button
        $letterMovt = LetterMovement::where('to','=',$loggedInUser->username)->where('moveable_id','=',$letter->id)->where('moveable_type','=','App\IncomingLetter')->orderBy('id','desc')->first();
        if($letterMovt != null){
            $showReturnToSenderBtn = strpos($letterMovt->required_action, 'Returned:') === FALSE;
        }else{
            $showReturnToSenderBtn = false;
        }

        //check if letter is back to ED
        $showReturnToEdBtn = !($this->letterMovementReturnedToEdOfficeUser($letter, $loggedInUser));

        $data = array(
            'menu_selected' => $this->menu_selected,
            'section' => '',
            'level'=> session(Security::$SESSION_USER_LEVEL),
            'letter' => $letter,
            'letterMovements' => $letterMovements,
            'loggedInUser' => $loggedInUser,
            'isCurrentAssignee' => $isCurrentAssignee,
            'showReturnToSenderBtn' => $showReturnToSenderBtn,
            'showReturnToEdBtn' => $showReturnToEdBtn,
            'menu' => $menu,
        );

        return view('letter_movements.view_letter_movement')->with($data);

    }

    public function viewInternalMemoMovement(InternalMemo $memo, $menu){

        $memoMovements = LetterMovement::where('moveable_id','=',$memo->id)->where('moveable_type','=','App\InternalMemo')->orderBy('id','asc')->get();

        //most recent assignment
        $recentAssignment = LetterMovement::where('moveable_id','=',$memo->id)->where('moveable_type','=','App\InternalMemo')->orderBy('id','desc')->first();

        //currentAssignee
        $currentAssignee = $recentAssignment != null ? $recentAssignment->to : '';

        $loggedInUser = session(Security::$SESSION_USER);

        //isCurrentAssignee
        $isCurrentAssignee = $currentAssignee == $loggedInUser->username;

        //check whether we should show the return to sender button
        $letterMovt = LetterMovement::where('to','=',$loggedInUser->username)->where('moveable_id','=',$memo->id)->where('moveable_type','=','App\InternalMemo')->orderBy('id','desc')->first();
        if($letterMovt != null){
            $showReturnToSenderBtn = strpos($letterMovt->required_action, 'Returned:') === FALSE;
        }else{
            $showReturnToSenderBtn = false;
        }

        //check if letter is back to ED
        $showReturnToEdBtn = !($this->internalMemoReturnedToEdOfficeUser($memo, $loggedInUser));

        $data = array(
            'menu_selected' => $this->menu_selected,
            'section' => '',
            'level'=> session(Security::$SESSION_USER_LEVEL),
            'memo' => $memo,
            'memoMovements' => $memoMovements,
            'loggedInUser' => $loggedInUser,
            'isCurrentAssignee' => $isCurrentAssignee,
            'showReturnToSenderBtn' => $showReturnToSenderBtn,
            'showReturnToEdBtn' => $showReturnToEdBtn,
            'menu' => $menu,
        );

        return view('letter_movements.view_memo_movement')->with($data);

    }

    public function letterMovementList()
    {

       $loggedInUser = session(Security::$SESSION_USER)->username;

//        return Laratables::recordsOf(LetterMovement::class, function ($query) use($loggedInUser){
//            return $query->where('to','=', $loggedInUser)->where('acted_upon','=', 0)->whereHas('letter');
//        });

        return Laratables::recordsOf(LetterMovement::class, function ($query) use($loggedInUser){
            return $query->where('to','=', $loggedInUser)->where('acted_upon','=', 0)->whereHasMorph('moveable',['App\IncomingLetter']);
        });

    }

    public function memoMovementList()
    {

        $loggedInUser = session(Security::$SESSION_USER)->username;

        return Laratables::recordsOf(LetterMovement::class, function ($query) use($loggedInUser){
            return $query->where('to','=', $loggedInUser)->where('acted_upon','=', 0)->whereHasMorph('moveable',['App\InternalMemo']);
        });

    }

    public function test()
    {

        $lettersWithLetterMovts = LetterMovement::get('letter_id')->toArray();
        $ids = [];
        foreach ($lettersWithLetterMovts as $id){
            $ids[] = $id['letter_id'];
        }
        return $ids;


    }

    private function letterMovementReturnedToEdOfficeUser(IncomingLetter $letter, $loggedInUser)
    {

        $letterMovt = LetterMovement::where('to','=',$loggedInUser->username)->where('moveable_id','=',$letter->id)->where('moveable_type','=','App\IncomingLetter')->orderBy('id','desc')->first();

        //check if it's a return
        if($letterMovt != null && !(strpos($letterMovt->required_action, 'Returned:') === FALSE)){

            //it contains the return text, so it's been returned, now proceed to check if it's at the person who initiated it
            $letterMovt = LetterMovement::where('moveable_id','=',$letter->id)->where('moveable_type','=','App\IncomingLetter')->orderBy('id','asc')->first();
            if($letterMovt == null){
                return false;
            }

            $edUserInitiator = $letterMovt->from;
            return $edUserInitiator == $loggedInUser->username; //in this case, it's at the person who initiated it and it's a return

        }else{
            return false;
        }

    }

    private function internalMemoReturnedToEdOfficeUser(InternalMemo $memo, $loggedInUser)
    {

        $letterMovt = LetterMovement::where('to','=',$loggedInUser->username)->where('moveable_id','=',$memo->id)->where('moveable_type','=','App\InternalMemo')->orderBy('id','desc')->first();

        //check if it's a return
        if($letterMovt != null && !(strpos($letterMovt->required_action, 'Returned:') === FALSE)){

            //it contains the return text, so it's been returned, now proceed to check if it's at the person who initiated it
            $letterMovt = LetterMovement::where('moveable_id','=',$memo->id)->where('moveable_type','=','App\InternalMemo')->orderBy('id','asc')->first();
            if($letterMovt == null){
                return false;
            }

            $edUserInitiator = $letterMovt->from;
            return $edUserInitiator == $loggedInUser->username; //in this case, it's at the person who initiated it and it's a return

        }else{
            return false;
        }

    }

}
