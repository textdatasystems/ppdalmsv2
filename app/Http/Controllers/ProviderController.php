<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProviderController extends Controller
{

    public function other_entity_create($origin){
        $data = array(
            'org_types'=> [],//OrganizationType::all(),
            'origin'=>$origin
        );
        return view('entities.create_other_entity')->with($data);
    }

}
