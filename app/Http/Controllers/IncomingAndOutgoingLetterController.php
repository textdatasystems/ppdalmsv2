<?php

namespace App\Http\Controllers;

use App\Helpers\AppConstants;
use App\Helpers\DataLoader;
use App\IncomingLetter;
use App\OutgoingLetter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class IncomingAndOutgoingLetterController extends Controller
{

    public function delete($letterCategory, $letterId){

        try{

            if($letterCategory == 'incoming'){
                return $this->deleteIncomingLetter($letterId);
            }
            else if($letterCategory == 'outgoing'){
                return $this->deleteOutgoingLetter($letterId);
            }else{
                Session::flash('errorMessage', 'Unknown letter category ['.$letterCategory.']');
                return redirect()->back();
            }

        }catch (\Exception $exception){

            Session::flash('errorMessage', AppConstants::generalError($exception->getMessage()));
            return redirect()->back();

        }

    }

    /**
     * @param $letterId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteIncomingLetter($letterId): \Illuminate\Http\RedirectResponse
    {

        $letter = IncomingLetter::find($letterId);
        if ($letter == null) {
            Session::flash('errorMessage', 'Letter with [' . $letterId . '] not found');
            return redirect()->back();
        }

        //check if letter has document in edms
        if ($letter->edms_document_path == null || $letter->edms_document_path == '') {

            //it has no document in EDMS, just delete it
            $letter->delete();
            Session::flash('successMessage', 'Incoming Letter successfully deleted.');
            return redirect()->back();

        } else {

            //has document in EDMS, send request to EDMS
            $data = [];
            $data['edmsDocumentPath'] = $letter->edms_document_path;
            $apiResp = DataLoader::deleteDocumentEdmsServer($data);

            if ($apiResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS) {
                //return error occurred on Delete document from EDMS
                Session::flash('errorMessage', 'Error on deleting document. ' . $apiResp->statusDescription);
                return redirect()->back();
            }

            //delete in EDMS success, proceed locally
            $letter->delete();
            Session::flash('successMessage', 'Incoming Letter successfully deleted.');
            return redirect()->back();

        }

    }

    /**
     * @param $letterId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteOutgoingLetter($letterId): \Illuminate\Http\RedirectResponse
    {

        $letter = OutgoingLetter::find($letterId);
        if ($letter == null) {
            Session::flash('errorMessage', 'Letter with [' . $letterId . '] not found');
            return redirect()->back();
        }

        //check if letter has document in edms
        if ($letter->edms_document_path == null || $letter->edms_document_path == '') {

            $letter->delete();
            Session::flash('successMessage', 'Outgoing Letter successfully deleted.');
            return redirect()->back();

        } else {

            //has document in EDMS, send request to EDMS
            $data = [];
            $data['edmsDocumentPath'] = $letter->edms_document_path;

            //send request to EDMS
            $apiResp = DataLoader::deleteDocumentEdmsServer($data);

            if ($apiResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS) {
                //return error occurred on Delete document from EDMS
                Session::flash('errorMessage', 'Error on deleting document. ' . $apiResp->statusDescription);
                return redirect()->back();
            }

            //delete in EDMS success, proceed locally
            $letter->delete();
            Session::flash('successMessage', 'Outgoing Letter successfully deleted.');
            return redirect()->back();

        }

    }

    public function viewAttachmentSentViaEmail($letterCategory = null, $letterId = null){

        try{

            $data = array(
                'menu_selected' => 'letter_movement'
            );

            if($letterCategory == 'incoming'){
                return $this->viewIncomingLetterAttachment($letterId);
            }
            else if($letterCategory == 'outgoing'){
                return $this->viewOutgoingLetterAttachment($letterId);
            }else{
                Session::flash('errorMessage', 'Unknown letter category ['.$letterCategory.']');
                return view('links.index')->with($data);
            }

        }catch (\Exception $exception){

            Session::flash('errorMessage', AppConstants::generalError($exception->getMessage()));
            return redirect(route('attachments.email-link'));

        }

    }

    private function viewIncomingLetterAttachment($letterId)
    {

        $data = array(
            'menu_selected' => 'letter_movement',
            'tempPath' => route('delete-temp-incoming-letter-doc',['saaaa']),
            'fileLink' => "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf",
        );
        return view('links.index')->with($data);

        $letter = IncomingLetter::find($letterId);
        $data = [];
        $data['edmsDocumentPath'] = $letter->edms_document_path;
        $apiResp  = DataLoader::downloadFileFromEdmsServer($data);

        if($apiResp->statusCode == AppConstants::$STATUS_CODE_SUCCESS){

            $documentBase64String = $apiResp->result;
            $decoded = base64_decode($documentBase64String);

            $timeNow = Carbon::now()->timestamp;
            $fileName = '/'.$timeNow.'.pdf';

            //temporarily store into a file
            $destinationPath = public_path('uploads/incoming_letter_uploads/temp');
            $filePath = $destinationPath . $fileName;

            file_put_contents($filePath, $decoded);

            $fileURL = asset('uploads/incoming_letter_uploads/temp'.$fileName);

            $tempPath = $fileName; //used for deleting the temporarily file created
            $fileLink = $fileURL; //used for rendering the PDF using Jquery UI - PDF Viewer

            $data = array(
                'menu_selected' => 'letter_movement',
                'tempPath' => route('delete-temp-incoming-letter-doc',[$tempPath]),
                'fileLink' => $fileLink,
            );
            return view('links.index')->with($data);

        }else{

            $data = array(
                'menu_selected' => 'letter_movement'
            );

            Session::flash('errorMessage', $apiResp->statusDescription);
            return redirect(route('attachments.email-link'))-with($data);

        }

    }

    private function viewOutgoingLetterAttachment($letterId)
    {

        $data = array(
            'menu_selected' => 'letter_movement',
            'tempPath' => route('delete-temp-incoming-letter-doc',['saaaa']),
            'fileLink' => "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf",
        );
        return view('links.index')->with($data);

        $letter = OutgoingLetter::find($letterId);
        $data = [];
        $data['edmsDocumentPath'] = $letter->edms_document_path;
        $apiResp  = DataLoader::downloadFileFromEdmsServer($data);

        if($apiResp->statusCode == AppConstants::$STATUS_CODE_SUCCESS){

            $documentBase64String = $apiResp->result;
            $decoded = base64_decode($documentBase64String);

            $timeNow = Carbon::now()->timestamp;
            $fileName = '/'.$timeNow.'.pdf';

            //temporarily store into a file
            $destinationPath = public_path('uploads/outgoing_letter_uploads/temp');
            $filePath = $destinationPath . $fileName;

            file_put_contents($filePath, $decoded);

            $fileURL = asset('uploads/outgoing_letter_uploads/temp'.$fileName);

            $tempPath = $fileName; //used for deleting the temporarily file created
            $fileLink = $fileURL; //used for rendering the PDF using Jquery UI - PDF Viewer

            $data = array(
                'menu_selected' => 'letter_movement',
                'tempPath' => route('delete-temp-outgoing-letter-doc',[$tempPath]),
                'fileLink' => $fileLink,
            );
            return view('links.index')->with($data);

        }else{

            $data = array(
                'menu_selected' => 'letter_movement'
            );

            Session::flash('errorMessage', $apiResp->statusDescription);
            return redirect(route('attachments.email-link'))-with($data);

        }

    }

    public static function getEmailAttachmentLink(string $letterCategory, $id)
    {
        try{

            $params = $letterCategory == 'incoming' ? ['incoming',$id] : ['outgoing',$id];

            if($letterCategory == 'incoming'){

                $letter = IncomingLetter::find($id);
                if($letter == null || $letter->incoming_letter_file_name == null || $letter->incoming_letter_file_name == ''  ){
                    return '';
                }

            }else{
                $letter = OutgoingLetter::find($id);

                if($letter == null || $letter->outgoing_letter_file_name == null || $letter->incoming_letter_file_name == ''  ){
                    return '';
                }

            }

            //the letter has an attachment
            return route('attachments.email-link',$params);

        }catch (\Exception $e){
            return '';
        }

    }

}
