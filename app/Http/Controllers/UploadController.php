<?php

namespace App\Http\Controllers;

use App\Helpers\AppConstants;
use App\Helpers\DataLoader;
use App\Helpers\EdmsDocumentUploadHandler;
use App\Helpers\FileHandler;
use App\Helpers\Security;
use App\IncomingLetter;
use App\InternalMemo;
use App\OutgoingLetter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class UploadController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadIncomingLetter(Request $request){

        try{

            $letterId = $request['fld_id'];
            $uploadedBy = $request['uploaded_by'];

            $letter = IncomingLetter::find($letterId);

            //letter not found
            if($letter == null){
                return Response::json(['success' => false,'message'=>'Failed to find letter with ID['.$letterId.']'], 200); // Status code here
            }

            $edmsBaseFolder = $letter->edms_document_path_folder;
            $fileInputFieldName = 'incoming_letter_file';
            $edmsSpecificBaseFolderName = $edmsBaseFolder;
            $edmsDefaultBaseFolderName = AppConstants::$EDMS_EMIS_DIRECTORY_INCOMING_LETTERS_DEFAULT;
            $localStorageFolder = 'uploads/incoming_letter_uploads';

            $uploadResp = EdmsDocumentUploadHandler::uploadDocumentToEdms($request, $fileInputFieldName, $edmsSpecificBaseFolderName,$edmsDefaultBaseFolderName,$localStorageFolder);

            if($uploadResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $message = $uploadResp->statusDescription;
                return Response::json(['success' => false,'message'=>$message,'suggestedFileName' => $uploadResp->suggestedFileName], 200); // Status code here

            }else{

                //save the upload details
                $letter->incoming_letter_file_name = $uploadResp->fileName;
                $letter->incoming_letter_file_path = $uploadResp->fileLocalPath;
                $letter->incoming_letter_upload_user = $uploadedBy;
                $letter->upload_date_time = db_date_format(Carbon::now());
                $letter->edms_document_path = $uploadResp->edmsFilePath;
                $letter->save();

                if($letter->org_type == 'Non - Government Entity'){
                    $message = "Operation successfully completed. Saved to default folder.";
                }else{
                    $msgEdmsPathMissing = "Document uploaded successfully to default folder. Please setup the EDMS letter folder path for Entity: [".$letter->entity_name."] in EMIS";
                    $message = $edmsBaseFolder == null || $edmsBaseFolder == "DEFAULT" ? $msgEdmsPathMissing : "Operation successfully completed";
                }

                return Response::json(['success' => true,'message' => $message], 200);

            }

        }catch (\Exception $exception){

            return Response::json(['success' => false,'message'=>$exception->getMessage()], 200);

        }

    }

    public function uploadOutgoingLetter(Request $request){

        try{

            $letterId = $request['fld_id'];
            $uploadedBy = $request['uploaded_by'];

            $letter = OutgoingLetter::find($letterId);

            //letter not found
            if($letter == null){
                return Response::json(['success' => false,'message'=>'Failed to find letter with ID['.$letterId.']'], 200); // Status code here
            }

            $edmsBaseFolder = $letter->edms_document_path_folder;
            $fileInputFieldName = 'outgoing_letter_file';
            $edmsSpecificBaseFolderName = $edmsBaseFolder;
            $edmsDefaultBaseFolderName = AppConstants::$EDMS_EMIS_DIRECTORY_OUTGOING_LETTERS_DEFAULT;
            $localStorageFolder = 'uploads/outgoing_letter_uploads';

            $uploadResp = EdmsDocumentUploadHandler::uploadDocumentToEdms(
                $request, $fileInputFieldName, $edmsSpecificBaseFolderName,$edmsDefaultBaseFolderName,$localStorageFolder);

            if($uploadResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $message = $uploadResp->statusDescription;
                return Response::json(['success' => false,'message'=>$message,'suggestedFileName' => $uploadResp->suggestedFileName], 200); // Status code here

            }else{

                //save the upload details

                $letter->outgoing_letter_file_name = $uploadResp->fileName;
                $letter->outgoing_letter_file_path = $uploadResp->fileLocalPath;
                $letter->outgoing_letter_upload_user = $uploadedBy;
                $letter->upload_date_time = db_date_format(Carbon::now());
                $letter->edms_document_path = $uploadResp->edmsFilePath;
                $letter->save();

                if($letter->org_type == 'Non - Government Entity'){
                    $message = "Operation successfully completed. Saved to default folder.";
                }else{
                    $msgEdmsPathMissing = "Document uploaded successfully to default folder. Please setup the EDMS letter folder path for Entity: [".$letter->entity_name."] in EMIS";
                    $message = $edmsBaseFolder == null || $edmsBaseFolder == "DEFAULT" ? $msgEdmsPathMissing : "Operation successfully completed";
                }

                return Response::json(['success' => true,'message' => $message], 200);

            }

        }catch (\Exception $exception){

            return Response::json(['success' => false,'message'=>$exception->getMessage()], 200);

        }

    }

    public function uploadOutgoingLetterReceipt(Request $request){

        try{

            $letterId = $request['fld_id'];
            $uploadedBy = $request['uploaded_by'];

            $letter = OutgoingLetter::find($letterId);

            //letter not found
            if($letter == null){
                return Response::json(['success' => false,'message'=>'Failed to find letter with ID['.$letterId.']'], 200); // Status code here
            }

            $edmsBaseFolder = $letter->edms_document_path_folder;
            $fileInputFieldName = 'outgoing_letter_file';
            $edmsSpecificBaseFolderName = $edmsBaseFolder;
            $edmsDefaultBaseFolderName = AppConstants::$EDMS_EMIS_DIRECTORY_OUTGOING_LETTERS_DEFAULT;
            $localStorageFolder = 'uploads/outgoing_letter_uploads';

            //set the name to be used for the receipt letter from the letter of the attached document
            if(isset($letter->outgoing_letter_file_name) && $letter->outgoing_letter_file_name != ''){
                $request['new_upload_file_name'] = 'RECEIPT_'.str_replace('.pdf','',$letter->outgoing_letter_file_name);
            }

            //to incase the length was adjusted we need to be able to pass the check. note max is 110 in EDMS
            EdmsDocumentUploadHandler::$MAX_FILE_LENGTH = EdmsDocumentUploadHandler::$MAX_FILE_LENGTH + 8;

            $uploadResp = EdmsDocumentUploadHandler::uploadDocumentToEdms(
                $request, $fileInputFieldName, $edmsSpecificBaseFolderName,$edmsDefaultBaseFolderName,$localStorageFolder);

            if($uploadResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $message = $uploadResp->statusDescription;
                return Response::json(['success' => false,'message'=>$message,'suggestedFileName' => $uploadResp->suggestedFileName], 200); // Status code here

            }else{

                //save the upload details for the receipt document
                $letter->outgoing_letter_receipt_file_name = $uploadResp->fileName;
                $letter->outgoing_letter_receipt_file_path = $uploadResp->fileLocalPath;
                $letter->outgoing_letter_receipt_upload_user = $uploadedBy;
                $letter->upload_receipt_date_time = db_date_format(Carbon::now());
                $letter->edms_document_receipt_path = $uploadResp->edmsFilePath;
                $letter->save();

                if($letter->org_type == 'Non - Government Entity'){
                    $message = "Operation successfully completed. Saved to default folder.";
                }else{
                    $msgEdmsPathMissing = "Document uploaded successfully to default folder. Please setup the EDMS letter folder path for Entity: [".$letter->entity_name."] in EMIS";
                    $message = $edmsBaseFolder == null || $edmsBaseFolder == "DEFAULT" ? $msgEdmsPathMissing : "Operation successfully completed";
                }

                return Response::json(['success' => true,'message' => $message], 200);

            }

        }catch (\Exception $exception){

            return Response::json(['success' => false,'message'=>$exception->getMessage()], 200);

        }

    }


    public function testEdmsDocumentDownload(){

        $data = [];
        $data['edmsDocumentPath'] = "/PPDA DEPARTMENTAL WORKSPACES/EMIS/PI3 Team Objectives.pdf";

        $apiResp  = DataLoader::downloadFileFromEdmsServer($data);

        if($apiResp->statusCode == AppConstants::$STATUS_CODE_SUCCESS){

            $documentBase64String = $apiResp->result;
            $decoded = base64_decode($documentBase64String);

            //temporarily store into a file
            $destinationPath = public_path('uploads/incoming_letter_uploads');
            $file = $destinationPath . '/test1.pdf';
            file_put_contents($file, $decoded);

            //download the file
            $headers = array(
                'Content-Type: application/pdf',
            );

            return Response::download($file, 'download.pdf', $headers);

        }

        return $apiResp->statusDescription;

    }

    /**
     * @param string $fileLocalPath
     * @param $letter
     */
    public function rollbackIncomingLetterLocalChanges(string $fileLocalPath, $letter): void
    {

        FileHandler::deleteFile($fileLocalPath);
        $letter->incoming_letter_file_name = null;
        $letter->incoming_letter_file_path = null;
        $letter->incoming_letter_upload_user = null;
        $letter->incoming_letter_file_path = null;
        $letter->edms_document_path = null;
        $letter->edms_document_path_folder = null;
        $letter->upload_date_time = null;
        $letter->save();

    }

    /**
     * @param string $fileLocalPath
     * @param $letter
     */
    public function rollOutgoingLetterLocalChanges(string $fileLocalPath, $letter): void
    {

        FileHandler::deleteFile($fileLocalPath);
        $letter->outgoing_letter_file_name = null;
        $letter->outgoing_letter_upload_user = null;
        $letter->upload_date_time = null;
        $letter->edms_document_path_folder = null;
        $letter->outgoing_letter_file_path = null;
        $letter->edms_document_path = null;
        $letter->save();

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function renameIncomingLetterAttachment(Request $request){

        try{

            $letterId = $request['fld_id'];
            $newFileName = $request['new_file_name'];
            $renamedBy = session(Security::$SESSION_USER) == null ? "" : session(Security::$SESSION_USER)->fullName;

            $letter = IncomingLetter::find($letterId);

            //letter not found
            if($letter == null){
                return Response::json(['success' => false,'message'=>'Failed to find letter with ID['.$letterId.']'], 200); // Status code here
            }

            //check for the validity of the file name
            $fileNameLengthCheckResp = EdmsDocumentUploadHandler::validateFileNameLength($newFileName);
            if($fileNameLengthCheckResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return Response::json(['success' => false,'message'=>$fileNameLengthCheckResp->statusDescription], 200); // Status code here
            }

            $currentEdmsFilePath = $letter->edms_document_path;
            $newDocumentNameWithExtension = $newFileName . '.pdf';
            $data = [
                'currentDocumentPath' => $currentEdmsFilePath,
                'newDocumentName' => $newDocumentNameWithExtension,
                'newDescription' => 'Renamed by: '.$renamedBy,
            ];

            //send request to EDMS
            $resp = DataLoader::renameFileInEdms($data);

            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return Response::json(['success' => false,'message'=>$resp->statusDescription], 200);
            }else{

                //update the edms path in EMIS
                $oldFileName = $letter->incoming_letter_file_name;
                $updatedEdmsPath = str_replace($oldFileName, $newDocumentNameWithExtension, $letter->edms_document_path);

                $letter->incoming_letter_file_name = $newDocumentNameWithExtension;
                $letter->edms_document_path = $updatedEdmsPath;
                $letter->save();

                return Response::json(['success' => true,'message' => "Document successfully renamed"], 200);
            }

        }catch (\Exception $exception){

            return Response::json(['success' => false,'message'=>$exception->getMessage()], 200);

        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function renameOutgoingLetterAttachment(Request $request){

        try{

            $letterId = $request['fld_id'];
            $newFileName = $request['new_file_name'];
            $renamedBy = session(Security::$SESSION_USER) == null ? "" : session(Security::$SESSION_USER)->fullName;

            $letter = OutgoingLetter::find($letterId);

            //letter not found
            if($letter == null){
                return Response::json(['success' => false,'message'=>'Failed to find letter with ID['.$letterId.']'], 200); // Status code here
            }

            //check for the validity of the file name
            $fileNameLengthCheckResp = EdmsDocumentUploadHandler::validateFileNameLength($newFileName);
            if($fileNameLengthCheckResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return Response::json(['success' => false,'message'=>$fileNameLengthCheckResp->statusDescription], 200); // Status code here
            }

            $currentEdmsFilePath = $letter->edms_document_path;
            $newDocumentNameWithExtension = $newFileName . '.pdf';
            $data = [
                'currentDocumentPath' => $currentEdmsFilePath,
                'newDocumentName' => $newDocumentNameWithExtension,
                'newDescription' => 'Renamed by: '.$renamedBy,
            ];

            //send request to EDMS
            $resp = DataLoader::renameFileInEdms($data);

            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return Response::json(['success' => false,'message'=>$resp->statusDescription], 200);
            }else{

                //update the edms path in EMIS
                $oldFileName = $letter->outgoing_letter_file_name;
                $updatedEdmsPath = str_replace($oldFileName, $newDocumentNameWithExtension, $letter->edms_document_path);

                $letter->outgoing_letter_file_name = $newDocumentNameWithExtension;
                $letter->edms_document_path = $updatedEdmsPath;
                $letter->save();

                return Response::json(['success' => true,'message' => "Document successfully renamed"], 200);
            }

        }catch (\Exception $exception){

            return Response::json(['success' => false,'message'=>$exception->getMessage()], 200);

        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadInternalMemo(Request $request){

        try{

            $memoId = $request['fld_id'];
            $uploadedBy = $request['uploaded_by'];

            $internalMemo = InternalMemo::find($memoId);

            //memo not found
            if($internalMemo == null){
                return Response::json(['success' => false,'message'=>'Failed to find Memo with ID['.$memoId.']'], 200); // Status code here
            }

            $fileInputFieldName = 'upload_file';
            $edmsSpecificBaseFolderName = AppConstants::$EDMS_EMIS_DIRECTORY_INTERNAL_MEMOS;;
            $edmsDefaultBaseFolderName = AppConstants::$EDMS_EMIS_DIRECTORY_INCOMING_LETTERS_DEFAULT;
            $localStorageFolder = 'uploads/incoming_letter_uploads';

            $uploadResp = EdmsDocumentUploadHandler::uploadDocumentToEdms($request, $fileInputFieldName, $edmsSpecificBaseFolderName,$edmsDefaultBaseFolderName,$localStorageFolder);

            if($uploadResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){

                $message = $uploadResp->statusDescription;
                return Response::json(['success' => false,'message'=>$message,'suggestedFileName' => $uploadResp->suggestedFileName], 200); // Status code here

            }else{

                //save the upload details
                $internalMemo->memo_file_name = $uploadResp->fileName;
                $internalMemo->memo_file_path = $uploadResp->fileLocalPath;
                $internalMemo->memo_upload_user = $uploadedBy;
                $internalMemo->memo_upload_date_time = db_date_format(Carbon::now());
                $internalMemo->edms_document_path = $uploadResp->edmsFilePath;
                $internalMemo->save();

                $message = "Operation successfully completed. Saved to default folder.";
                return Response::json(['success' => true,'message' => $message], 200);

            }

        }catch (\Exception $exception){
            return Response::json(['success' => false,'message'=>$exception->getMessage()], 200);
        }

    }


}
