<?php

namespace App\Http\Controllers;

use App\IncomingLetter;
use App\LetterAssociation;
use App\OutgoingLetter;
use Freshbitsweb\Laratables\Laratables;
use Yajra\DataTables\Facades\DataTables;

class LetterAssociationController extends Controller
{

    public function create($letterId, $letterCategory)
    {

        $letter = $letterCategory == 'incoming' ? IncomingLetter::find($letterId) :  OutgoingLetter::find($letterId);
        $entityName = $letter != null ? $letter->entity_name : '';

        $data = array(
            'letterId' => $letterId,
            'letterCategory' => $letterCategory,
            'entityName' => $entityName,
        );

        return view('associations.create')->with($data);

    }

    public function associationList($letterId, $letterCategory)
    {

        /*
         * Get the stored proc to call
         * */
        $storedProcToCall = $letterCategory == 'incoming' ? 'GetIncomingLetterAssociations' : 'GetOutgoingLetterAssociations';

        /*
         * Get stored procedure to return list of letters
         * */
        $associatedLetters = \DB::select('call '.$storedProcToCall.'(?)',array($letterId));

        /*
         * Convert response to normal array
         * */
        $dataArray = array_map(function ($value) {  return (array)$value;  }, $associatedLetters);

        /*
         * Attach additional data
         * */
        $updatedArray = [];
        $timo = '';
        foreach ($dataArray as $item){

            $associatedLetterCategory = $item['letter_category'];
            $associatedLetterId = $item['id'];

            if($associatedLetterCategory == 'Incoming'){
                $letter = IncomingLetter::find($item['id']);
                $fileName = $letter != null ? $letter->incoming_letter_file_name : '';
            }else{
                $outgoingLetter = OutgoingLetter::find($item['id']);
                $fileName = $outgoingLetter != null ? $outgoingLetter->outgoing_letter_file_name : '';
            }

            $item['menu_action'] = LetterAssociation::menuAction($associatedLetterId, $associatedLetterCategory,$fileName);
            $timo .= $associatedLetterId;
            $updatedArray[] = $item;
        }

        $collection = collect($updatedArray);
        return DataTables::collection($collection)->rawColumns(['menu_action'])->toJson();

    }

    public function listIncoming($letter){

        $data = explode(':::', $letter);
        $letterId = $data[0];
        $category = $data[1];

        $letter = $category == 'incoming' ? IncomingLetter::find($letterId) : OutgoingLetter::find($letterId);
        $entityName = $letter->entity_name;

        /*
         * Get stored procedure to return list of letters
         * */
        $letters = \DB::select('call GetIncomingLettersNotYetAssociatedToLetter(?,?,?)',array($entityName, $letterId,$category));

        /*
         * Convert response to normal array
         * */
        $dataArray = array_map(function ($value) {  return (array)$value;  }, $letters);

        /*
         * Attach additional data
         * */
        $updatedArray = [];
        foreach ($dataArray as $item){
            $currentLetter = IncomingLetter::find($item['id']);
            $item['AssociationAction'] = IncomingLetter::laratablesCustomAssociationAction($currentLetter);
            $item['CheckBox'] = IncomingLetter::laratablesCustomCheckBox($currentLetter);
            $updatedArray[] = $item;
        }

        $collection = collect($updatedArray);
        return DataTables::collection($collection)->rawColumns(['AssociationAction','CheckBox'])->toJson();

    }

    public function listIncomingOld($letter){

        $data = explode(':::', $letter);
        $letterId = $data[0];
        $category = $data[1];

        $letter = $category == 'incoming' ? IncomingLetter::find($letterId) : OutgoingLetter::find($letterId);
        $entityName = $letter->entity_name;

        return Laratables::recordsOf(IncomingLetter::class, function ($query) use($entityName){
            return $query->where('current_que', 'ed')->where('entity_name', $entityName)->orderBy('id','desc');
        });

    }

    public function listOutgoing($letter){

        $data = explode(':::', $letter);
        $letterId = $data[0];
        $category = $data[1];

        $letter = $category == 'incoming' ? IncomingLetter::find($letterId) : OutgoingLetter::find($letterId);
        $entityName = $letter->entity_name;

        /*
         * Get stored procedure to return list of letters
         * */
        $letters = \DB::select('call GetOutgoingLettersNotYetAssociatedToLetter(?,?,?)',array($entityName, $letterId,$category));

        /*
         * Convert response to normal array
         * */
        $dataArray = array_map(function ($value) {  return (array)$value;  }, $letters);

        /*
         * Attach additional data
         * */
        $updatedArray = [];
        foreach ($dataArray as $item){
            $currentLetter = OutgoingLetter::find($item['id']);
            $item['AssociationAction'] = OutgoingLetter::laratablesCustomAssociationAction($currentLetter);
            $item['CheckBox'] = OutgoingLetter::laratablesCustomCheckBox($currentLetter);
            $updatedArray[] = $item;
        }

        $collection = collect($updatedArray);
        return DataTables::collection($collection)->rawColumns(['AssociationAction','CheckBox'])->toJson();

    }

}
