<?php

namespace App\Http\Controllers;

use App\MailingList;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;

class MailingListController extends Controller
{

    public function create(){
        return view('master_data.general.mailing_lists.create');
    }

    public function edit(MailingList $mailingList){
        return view('master_data.general.mailing_lists.create')->with(compact('mailingList'));
    }

    public function list()
    {
        return Laratables::recordsOf(MailingList::class);
    }

    public function removeMailingList(MailingList $mailingList){

        try {

            $mailingList->delete();

        } catch (\Exception $e) {
        }

        return redirect()->back();

    }

}
