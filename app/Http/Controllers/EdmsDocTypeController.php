<?php

namespace App\Http\Controllers;

use App\EdmsDocType;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;

class EdmsDocTypeController extends Controller
{
    public function create(){
        return view('master_data.general.edms_document_types.create');
    }

    public function edit(EdmsDocType $edmsdoctype){
        return view('master_data.general.edms_document_types.create')->with(compact('edmsdoctype'));
    }

    public function list()
    {
        return Laratables::recordsOf(EdmsDocType::class);
    }
}
