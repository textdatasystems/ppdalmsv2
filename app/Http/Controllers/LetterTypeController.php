<?php

namespace App\Http\Controllers;

use App\LetterType;
use App\EdmsDocType;

use App\LetterTypeField;
use Freshbitsweb\Laratables\Laratables;

class LetterTypeController extends Controller
{
    public function create(){
        $data = array(
            'edmsdocs'=>EdmsDocType::all()->pluck('type_name','id')
        );
        return view('master_data.general.letter_types.create')->with($data);
    }

    public function letter_type_edit (LetterType $letterType){

        $data = array(
            'lettertype'=>$letterType,
            'edmsdocs'=>EdmsDocType::all()->pluck('type_name','id')
        );

        return view('master_data.general.letter_types.create')->with($data);

    }

    public function letter_type_fields (LetterType $letterType){

        $data = array(
            'fields'=>LetterTypeField::where('letter_type_id','=',$letterType->id)->get()
        );

        return view('master_data.general.letter_types.letter_type_fields')->with($data);

    }

      public function list()
      {
          return Laratables::recordsOf(LetterType::class);
      }
      
}
