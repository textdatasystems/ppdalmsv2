<?php

namespace App\Http\Controllers;

use App\Helpers\Security;
use Illuminate\Http\Request;

class EdController extends Controller
{
    public function index()
    {
        $data = array(
            'menu_selected'=>'ed_office',
            'level'=> session(Security::$SESSION_USER_LEVEL),
        );
        return view('Ed.index')->with($data);
    }
}
