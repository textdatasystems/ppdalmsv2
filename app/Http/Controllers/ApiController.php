<?php

namespace App\Http\Controllers;

use App\Helpers\DataGenerator;
use App\Helpers\EndPoints;
use App\IncomingLetter;
use GuzzleHttp\Client;
use App\OutgoingLetter;
use App\Http\Resources\IncomingLetter as IncomingLetterResource;
use App\Http\Resources\OutgoingLetter as OutgoingLetterResource;

class ApiController extends Controller
{
    public function getEntities(){

        $url = EndPoints::$EMIS_ENTITIES_GET;
        $client = new Client([
          'headers' => ["content-type"=>"application/json", "Accept"=>"application/json"]
        ]);

        $response = $client->request("GET", $url, [
          'json'=>["x-code"=>"&80@#74!@3$"]
        ]);

        //todo remove
        return json_decode(DataGenerator::dummyEntities());

        if($response->getStatusCode() == 200){
          return json_decode($response->getBody());
        }else{
            $data = array(
                'payload'=>[]
            );
            return json_decode(json_encode($data));
        }

    }

    public static function getProviders(){

        $url = EndPoints::$EMIS_PROVIDERS_GET;
        $client = new Client([
            'headers' => ["content-type"=>"application/json", "Accept"=>"application/json"]
        ]);

        $response = $client->request("GET", $url, [
            'json'=>["x-code"=>"&80@#74!@3$"]
        ]);

        //todo remove
        return DataGenerator::dummyProviders();

        if($response->getStatusCode() == 200){
            return json_decode($response->getBody());
        }else{
            $data = array(
                'payload'=>[]
            );
            return json_decode(json_encode($data));
        }

    }

    public static function getPpdaUsers(){

        $url = EndPoints::$EMIS_PPDA_USERS_GET;

        $client = new Client([
            'headers' => ["content-type"=>"application/json", "Accept"=>"application/json"]
        ]);

        $response = $client->request("GET", $url, [
            'json'=>["x-code"=>"&80@#74!@3$"]
        ]);

        //todo remove
        return json_decode(DataGenerator::dummyUsers());

        if($response->getStatusCode() == 200){
            return json_decode($response->getBody());
        }else{
            $data = array(
                'data'=>[]
            );
            return json_decode(json_encode($data));
        }

    }



    /**
    ** Get all incoming letters for a department
     * API expects one of the following parameters(departments) are one of the following; pm_tag
        legal_tag,
        corp_tag,
        advisory_tag,
        cbas_tag
    **/
    public function get_department_incoming_letters($department){
      $letters = IncomingLetter::where($department,1)->get();
      if(!$letters){
          return array("success"=>false, "message"=>"Entity not found");
      }
      $data = IncomingLetterResource::collection($letters);
      return response()->json(array("success"=>true, "payload"=>$data));
    }

    /**
    ** Get all outgoing letters for a department
    **/
    public function get_department_outgoing_letters($department){
      $letters = OutgoingLetter::where($department,1)->get();
      if(!$letters){
          return array("success"=>false, "message"=>"No letters found");
      }
      $data = OutgoingLetterResource::collection($letters);
      return response()->json(array("success"=>true, "payload"=>$data));
    }



    public static function uploadFile($fileName, $authenticationTicket,$pathToFileInEdms){

        $url = EndPoints::$BASE_URL_EDMS_MIDDLEWARE_API . EndPoints::$EDMS_UPLOAD_DOCUMENT;
        $client = new Client([
            'headers' => ["content-type"=>"application/json", "Accept"=>"application/json"]
        ]);

        $response =  $client->request('POST', $url, [
            'multipart' => [
                [
                    'name' => 'documentContents',
                    'contents' => fopen($fileName, 'r'),
                ],
                [
                    'name' => 'authenticationTicket',
                    'contents' => json_encode($authenticationTicket),
                ],
                [
                    'name' => 'pathToFileInEdms',
                    'contents' => json_encode($pathToFileInEdms),
                ]
            ]
        ]);

        if($response->getStatusCode() == 200){
            return json_decode($response->getBody());
        }else{
            return [];
        }

    }

    public static function getAccountingOfficerTitles(){

        $url = 'http://'. EndPoints::SERVER_IP . ':10000/api/entity_query/get_accounting_officer_titles';
        $client = new Client([
            'headers' => ["content-type"=>"application/json", "Accept"=>"application/json"]
        ]);

        $response = $client->request("GET", $url, [
            'json'=>["x-code"=>"&80@#74!@3$"]
        ]);

        //todo remove
        return json_decode(DataGenerator::dummAoTitles());

        if($response->getStatusCode() == 200){
            return json_decode($response->getBody());
        }else{
            $data = array(
                'payload'=>[]
            );
            return json_decode(json_encode($data));
        }

    }


}
