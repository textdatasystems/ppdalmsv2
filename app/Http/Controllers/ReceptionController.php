<?php

namespace App\Http\Controllers;

use App\Helpers\Security;
use Illuminate\Http\Request;

class ReceptionController extends Controller
{
    public function index()
    {


        $data = array(
            'menu_selected'=>'reception',
            'level'=> session(Security::$SESSION_USER_LEVEL),
        );
        return view('Reception.index')->with($data);
    }
}
