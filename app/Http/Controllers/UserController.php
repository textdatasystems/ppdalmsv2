<?php

namespace App\Http\Controllers;

use App\ApiLmsRole;
use App\ApiUser;
use App\Helpers\AppConstants;
use App\Helpers\DataLoader;
use App\Helpers\EndPoints;
use App\Helpers\Security;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{

    public function authenticate($key){

        try{

            $token = Security::dtDecrypt($key);

            $resp = DataLoader::getAuthenticatedUser($token);
            $respED = DataLoader::getExecutiveDirector();
            $respDepartmentUnits = DataLoader::getDepartmentUnits();

            /*
             * Authentication failed
             * */
            if($resp->statusCode != AppConstants::$STATUS_CODE_SUCCESS){
                return $resp->statusDescription;
            }

            $user = $resp->result;
            $ed = $respED->statusCode != AppConstants::$STATUS_CODE_SUCCESS ? null : $respED->result;
            $units = $respDepartmentUnits->statusCode != AppConstants::$STATUS_CODE_SUCCESS ? [] : $respDepartmentUnits->result;

            /*
             * Save the user in the session
             * */
            session([Security::$SESSION_USER => $user]);

            /*
             * Save variable for enabling admin deletion
             * */
            $enableAdminDeletion = isset($user->lmsRole) && $user->lmsRole->masterData;
            session([Security::$SESSION_ENABLE_ADMIN_DELETION => $enableAdminDeletion]);

            /*
             * Save the ED in the session
             * */
            session([Security::$SESSION_ED => $ed]);

            /*
             * Save the Department Units in the session
             * */
            session([Security::$SESSION_DEPT_UNITS => $units]);

            /*
             * Redirect the user base on the their LMS role code
             * */
            return $this->redirectUserBasedOnTheUserLevel();


        }catch (\Exception $exception){

            $error = AppConstants::generalError($exception->getMessage());
            return $error;

        }

    }


    public function home()
    {

        $user = new ApiUser();
        $user->firstName = "Admin";
        $user->lastName = "Admin";
        $user->username = "admin@ppda.go.ug";
        $user->email = "admin@ppda.go.ug";

        $role = new ApiLmsRole();
        $role->masterData = true;
        $role->outLetters = true;
        $role->edOffice = true;
        $role->registry = true;
        $role->reception = true;

        $user->lmsRole = $role;

        /*
         * Save the user in the session
         * */
        session([Security::$SESSION_USER => $user]);

        /*
         * Save variable for enabling admin deletion
         * */
        $enableAdminDeletion = isset($user->lmsRole) && $user->lmsRole->masterData;
        session([Security::$SESSION_ENABLE_ADMIN_DELETION => $enableAdminDeletion]);

        $lmsRole = session(Security::$SESSION_USER)->lmsRole;

        /*
         * New role format not define for user
         * */
        if ($lmsRole == null) {
            return redirect(route('letter_movement'));
        }

        /*
         * Open the left most tab the user has access to
         * */
        if ($lmsRole->reception) {

            return redirect(route('home'));

        } else if ($lmsRole->registry) {

            return redirect(route('registry'));

        } else if ($lmsRole->edOffice) {

            return redirect(route('ed_office'));

        } else if ($lmsRole->outLetters) {

            return redirect(route('outgoingletters'));

        } else if ($lmsRole->masterData) {

            return redirect(route('master_data_index'));

        } else {
            //any other thing we redirect to letter movements
            return redirect(route('letter_movement'));
        }

    }


    private function redirectUserBasedOnTheUserLevel()
    {

        $lmsRole = session(Security::$SESSION_USER)->lmsRole;

        /*
         * New role format not define for user
         * */
        if($lmsRole == null){
            return redirect(route('letter_movement'));
        }

        /*
         * Open the left most tab the user has access to
         * */
        if($lmsRole->reception)
        {

            return redirect(route('home'));

        }
        else if($lmsRole->registry){

            return redirect(route('registry'));

        }
        else if($lmsRole->edOffice){

            return redirect(route('ed_office'));

        }
        else if($lmsRole->outLetters){

            return redirect(route('outgoingletters'));

        }
        else if($lmsRole->masterData){

            return redirect(route('master_data_index'));

        }
        else{
            //any other thing we redirect to letter movements
            return redirect(route('letter_movement'));
        }

    }


    public function logoutUser(Request $request){

        session()->flush();
        return Redirect::to(EndPoints::$USER_MANAGEMENT_LOGOUT_LINK); // redirect(route('login'));

    }

}
