<?php

namespace App\Http\Controllers;

use App\IncomingLetter;
use App\OutgoingLetter;
use Illuminate\Http\Request;
use App\ApiResp;
use App\Helpers\AppConstants;
use App\Helpers\DataGenerator;
use App\Helpers\DataLoader;
use App\Helpers\EmailHandler;
use App\Helpers\FileHandler;
use App\Helpers\Security;
use App\LetterType;
use App\LetterTypeField;
use App\LetterTypeFieldValue;
use App\MailingList;
use Carbon\Carbon;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class IncomingLetterController extends Controller
{

    public function __construct(ApiController $apiService)
    {
        $this->apiService = $apiService;
    }

    public function create()
    {

        //return ApiController::getProviders()->payload;

        $letterTypes = LetterType::all();
        $signatoryTitles = DataGenerator::signatoryTitles();

        $data = array(
            'entities' => $this->apiService->getEntities()->payload,
            'letterTypes' => $letterTypes,
            'signatoryTitles' => $signatoryTitles,
            'providers' => ApiController::getProviders()->payload,
            'users' => ApiController::getPpdaUsers()->data,
        );

        return view('IncomingLetters.create')->with($data);
    }

    public function edit(IncomingLetter $letter)
    {

        $letterTypes = LetterType::all();
        $signatoryTitles = DataGenerator::signatoryTitles();

        $extraFieldValues = LetterTypeFieldValue::with('letterTypeField')
            ->where('letter_id','=',$letter->id)
            ->where('letter_type_id','=',$letter->letter_type_id)
            ->get();

        $fields = LetterTypeField::where('letter_type_id','=',$letter->letter_type_id)->get();

        $data = array(
            'letter'=>$letter,
            'entities' => $this->apiService->getEntities()->payload,
            'letterTypes' => $letterTypes,
            'signatoryTitles' => $signatoryTitles,
            'extraFieldValues' => $extraFieldValues,
            'fields' => $fields,
            'providers' => ApiController::getProviders()->payload,
            'users' => ApiController::getPpdaUsers()->data,
        );

        return view('IncomingLetters.create')->with($data);
    }

    public function readonly(IncomingLetter $letter)
    {

        // return json_encode($letter);

        $letterTypes = LetterType::all();
        $signatoryTitles = DataGenerator::signatoryTitles();

        $extraFieldValues = LetterTypeFieldValue::with('letterTypeField')
            ->where('letter_id','=',$letter->id)
            ->where('letter_type_id','=',$letter->letter_type_id)
            ->get();

        $fields = LetterTypeField::where('letter_type_id','=',$letter->letter_type_id)->get();

        $data = array(
            'letter'=>$letter,
            'entities' => $this->apiService->getEntities()->payload,
            'letterTypes' => $letterTypes,
            'signatoryTitles' => $signatoryTitles,
            'extraFieldValues' => $extraFieldValues,
            'fields' => $fields,
            'providers' => ApiController::getProviders()->payload,
            'users' => ApiController::getPpdaUsers()->data,
        );

        return view('IncomingLetters.view')->with($data);

    }


    public function forward(IncomingLetter $letter,$que)
    {

        $letter = IncomingLetter::with('letterType')->find($letter->id);
        $letter->current_que = $que;

        if($que == 'registry'){

            $authenticatedUser = session(Security::$SESSION_USER);
            $letter->registry_entry_user = $authenticatedUser != null ? $authenticatedUser->email : '';

            $dateNow = Carbon::now();
            $letter->date_received_registry = $dateNow;
            $letter->save();

            $emailResp = $this->sendLetterForwardedToRegistryMailingList($letter);


        }else if($que == 'ed'){

            $authenticatedUser = session(Security::$SESSION_USER);
            $letter->eds_office_entry_user = $authenticatedUser != null ? $authenticatedUser->email : '';
            $letter->eds_office_entry_date = Carbon::now();
            $letter->save();

            $emailResp = $this->sendLetterForwardedToEdOfficeMailingList($letter);

        }

        echo $letter->subject . ' - forwarded successfully to '.$que;

    }

    public function uploadIncomingLetter(IncomingLetter $letter)
    {

        $data = array(
            'letter'=>$letter
        );

        return view('IncomingLetters.upload')->with($data);
    }

    public function renameAttachedDocumentView(IncomingLetter $letter)
    {

        $data = array(
            'letter'=>$letter
        );

        return view('IncomingLetters.rename-attached-doc')->with($data);
    }

    public function downloadAttachedIncomingLetter(IncomingLetter $letter){

        try{

            $fileName = $letter->incoming_letter_file_name;

            if($fileName == null){
                return redirect()->back();
            }

            $path_to_file = public_path()."/uploads/incoming_letter_uploads/".$fileName;
            return Response::download($path_to_file,$fileName);

        }catch (\Exception $ex){
            return redirect()->back();
        }

    }

    public function removeAttachedIncomingLetter(IncomingLetter $letter){

        try{

            $loggedInUser = session(Security::$SESSION_USER)->username;
            $uploadUser = $letter->incoming_letter_upload_user;

            //prevent remove action if not upload by you
            if($loggedInUser != $uploadUser){
                Session::flash('errorMessage', 'You cannot remove a document that was not uploaded by you.');
                return redirect()->back();
            }

            //First go to EDMS and remove the document, if successful, remove the upload details locally in LMS as well
            $edmsDocumentPath = $letter->edms_document_path;
            if($edmsDocumentPath != null){

                //request to send to EDMS
                $data = [];
                $data['edmsDocumentPath'] = $edmsDocumentPath;

                //send request to EDMS
                $apiResp  = DataLoader::deleteDocumentEdmsServer($data);

                $statusDescription = $apiResp->statusDescription;
                if($apiResp->statusCode != AppConstants::$STATUS_CODE_SUCCESS && !Str::contains(strtolower($statusDescription), 'document not found')){
                    //return error occurred on Delete document from EDMS
                    Session::flash('errorMessage', 'Error on deleting document. '.$apiResp->statusDescription);
                    return redirect()->back();
                }

            }

            //deletion in EDMS was successful, proceed locally
            $letter->incoming_letter_file_name = null;
            $letter->incoming_letter_file_path= null;
            $letter->incoming_letter_upload_user= null;
            $letter->upload_date_time= null;
            $letter->edms_document_path= null;
            $letter->save();

            Session::flash('successMessage', 'Document successfully deleted');
            return redirect()->back();

        }catch (\Exception $exception){

            //return error occurred on Delete document from EDMS
            Session::flash('errorMessage', 'Error on deleting document ['.$exception->getMessage().']');
            return redirect()->back();

        }

    }

    public function reception_list()
    {
        return Laratables::recordsOf(IncomingLetter::class, function ($query) {
            return $query->where('current_que', 'reception')->orderBy('id','desc');
        });
    }

    public function registry_list()
    {
        return Laratables::recordsOf(IncomingLetter::class, function ($query) {
            return $query->where('current_que', 'registry')->orderBy('id','desc');
        });
    }

    public function ed_list()
    {
        return Laratables::recordsOf(IncomingLetter::class, function ($query) {
            return $query->where('current_que', 'ed')->orderBy('id','desc');
        });
    }

    public function historyView($activeMenu){
        return view('Reception.history-incoming', compact('activeMenu'));
    }

    public function historyList($activeMenu)
    {

        if($activeMenu == 'registry'){
            return Laratables::recordsOf(IncomingLetter::class, function ($query) {
                return $query->where('current_que','=', 'ed')->orderBy('id','desc');
            });
        }else if($activeMenu == 'reception'){
            return Laratables::recordsOf(IncomingLetter::class, function ($query) {
                return $query->where('current_que','!=', 'reception')->orderBy('id','desc');
            });
        }

        return Laratables::recordsOf(IncomingLetter::class, function ($query) {
            return $query->where('current_que','!=', '')->orderBy('id','desc');
        });

    }

    private function sendLetterForwardedNotificationEmail($subject, $message, array $mailingList)
    {

        $apiResp = new ApiResp();

        //remove if in production, this is just to test that the emails are sent
        // array_push($mailingList, 'tkasaga6@gmail.com');

        foreach ($mailingList as $email){

            $apiResp = EmailHandler::sendPlainTextEmailPHPMAILER($email, $subject, $message);

        }

        return $apiResp;

    }

    private function sendLetterForwardedToEdOfficeMailingList($letter)
    {


        $apiResp = new ApiResp();
        try{

            /*
             * When email is received at ED's office, email should be to both management and Jerri
             * */
            self::notifyRegistrySender($letter);

            $mailingList = [];
            $mailingListName = 'ED_Office_Mailing_List';

            $customMailingList = MailingList::where('list_name','=', $mailingListName)->first();
            if($customMailingList != null){
                $customList = $customMailingList == null ? [] : explode(',', trim($customMailingList->email_addresses));
                $mailingList = $customList;
            }

            $letterType = $letter->letter_category != AppConstants::$LETTER_CATEGORY_INCOMING_LETTER ? $letter->letter_category : $letter->letterType->letter_type_name;
            $subject = "Letter Movement: New Letter has been received at ED's Office(Letter Type: ".$letterType.")";
            $description = "New Letter has been received at ED's Office";
            $downloadLink = IncomingAndOutgoingLetterController::getEmailAttachmentLink('incoming', $letter->id);
            $message = view('mails.ed_office_notification', compact('letter','description','downloadLink'))->render();

            $apiResp = $this->sendLetterForwardedNotificationEmail($subject, $message,$mailingList);
            return $apiResp;

        }catch (\Exception $exception){
            $apiResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $apiResp->statusDescription = $exception->getMessage();
            return $apiResp;
        }

    }

    private function sendLetterForwardedToRegistryMailingList($letter)
    {

        $apiResp = new ApiResp();
        try{

            /*
             * Send to every one in Registry(Registry Mailing List), Send copy to sender from Reception
             * */

            $this->notifyReceptionSender($letter);

            $mailingList = [];

            $mailingListName = 'Registry_Mailing_List';

            $customMailingList = MailingList::where('list_name','=', $mailingListName)->first();
            if($customMailingList != null){
                $customList = $customMailingList == null ? [] : explode(',', trim($customMailingList->email_addresses));
                $mailingList = $customList;
            }

            $letterType = $letter->letter_category != AppConstants::$LETTER_CATEGORY_INCOMING_LETTER ? $letter->letter_category : $letter->letterType->letter_type_name;
            //todo, remove mailing list append in below line
            $subject = "Letter Movement: New Letter has been received at Registry(Letter Type: ".$letterType.")";
            $description = "New Letter has been received at Registry";
            $downloadLink = IncomingAndOutgoingLetterController::getEmailAttachmentLink('incoming', $letter->id);
            $message = view('mails.ed_office_notification', compact('letter','description','downloadLink'))->render();

            $apiResp = $this->sendLetterForwardedNotificationEmail($subject, $message,$mailingList);

            $apiResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
            $apiResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
            return $apiResp;

        }
        catch (\Exception $exception){
            $apiResp->statusCode = AppConstants::$STATUS_CODE_FAILED;
            $apiResp->statusDescription = $exception->getMessage();
            return $apiResp;
        }

    }

    private function notifyReceptionSender($letter){

        $createdBy = $letter->created_by_user;

        $receptionSender = [
            $createdBy,
        ];

        $letterType = $letter->letter_category != AppConstants::$LETTER_CATEGORY_INCOMING_LETTER ? $letter->letter_category : $letter->letterType->letter_type_name;
        $subject = "Letter Movement: You have sent a New Letter to Registry(Letter Type: ".$letterType.")";
        $description = "New Letter has been sent by you to Registry";
        $message = view('mails.ed_office_notification', compact('letter','description'))->render();

        $apiResp = $this->sendLetterForwardedNotificationEmail($subject, $message,$receptionSender);

        $apiResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
        $apiResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
        return $apiResp;

    }

    private function notifyRegistrySender($letter){

        $senderEmail = session(Security::$SESSION_USER)->username;

        $registrySender = [
            $senderEmail,
        ];

        $letterType = $letter->letter_category != AppConstants::$LETTER_CATEGORY_INCOMING_LETTER ? $letter->letter_category : $letter->letterType->letter_type_name;
        $subject = "Letter Movement: You have sent a New Letter to ED's Office(Letter Type: ".$letterType.")";
        $description = "New Letter has been sent by you to ED's Office";
        $message = view('mails.ed_office_notification', compact('letter','description'))->render();

        $apiResp = $this->sendLetterForwardedNotificationEmail($subject, $message,$registrySender);

        $apiResp->statusCode = AppConstants::$STATUS_CODE_SUCCESS;
        $apiResp->statusDescription = AppConstants::$STATUS_DESC_SUCCESS;
        return $apiResp;

    }

    public function getLetterDownloadLink(IncomingLetter $letter){


        //get document from EDMS, result has base64String content of the document
        $data = [];
        $data['edmsDocumentPath'] = $letter->edms_document_path;
        $apiResp  = DataLoader::downloadFileFromEdmsServer($data);

        if($apiResp->statusCode == AppConstants::$STATUS_CODE_SUCCESS){

            $documentBase64String = $apiResp->result;
            $decoded = base64_decode($documentBase64String);

            $timeNow = Carbon::now()->timestamp;
            $fileName = '/'.$timeNow.'.pdf';

            //temporarily store into a file
            $destinationPath = public_path('uploads/incoming_letter_uploads/temp');
            $filePath = $destinationPath . $fileName;

            file_put_contents($filePath, $decoded);

            $fileURL = asset('uploads/incoming_letter_uploads/temp'.$fileName);

            $data = [];
            $data['statusCode'] = 0;
            $data['statusDescription'] = "SUCCESS";
            $data['tempPath'] = $fileName; //used for deleting the temporarily file created
            $data['result'] = $fileURL; //used for rendering the PDF using Jquery UI - PDF Viewer
            return json_encode($data);

        }else{

            $data['statusCode'] = 1;
            $data['statusDescription'] = $apiResp->statusDescription;
            return json_encode($data);

        }

    }

    public function deleteTempDocument($documentName){

        $fileName = '/' . $documentName;
        $destinationPath = public_path('uploads/incoming_letter_uploads/temp').$fileName;

        FileHandler::deleteFile($destinationPath);
        $data = [];
        $data['statusCode'] = 0;
        $data['statusDescription'] = "SUCCESS";
        return json_encode($data);

    }

    public function associations(IncomingLetter $letter){

        $data = array(
            'letterId'=> $letter->id,
            'letterCategory' => 'incoming'
        );
        return view('associations.associations')->with($data);
    }


}
