<?php

namespace App\Http\Controllers;

use App\Helpers\Security;
use Illuminate\Http\Request;

class MasterDataController extends Controller
{
    public $menu_selected = 'master_data';

    public function __construct()
    {
        /* $this->middleware('auth'); */
    }

    public function index()
    {
        $data = array(
            'menu_selected' => $this->menu_selected,
            'section' => '',
            'level'=> session(Security::$SESSION_USER_LEVEL)
        );
        return view('master_data.master_data_index')->with($data);
    }

    /**
      * master method for general data
      * Here you have to pass a parameter to the route
     **/
    public function general($section)
    {
        $data = array(
            'section' => $section,
            'menu_selected' => $this->menu_selected,
            'level'=> session(Security::$SESSION_USER_LEVEL)
        );

        return view('master_data.general.'.$section.'.index')->with($data);
    }

}
