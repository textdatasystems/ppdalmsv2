<?php

namespace App;

use App\Helpers\Security;
use Illuminate\Database\Eloquent\Model;

class OutgoingLetter extends Model
{
    public static function laratablesCustomAction($letter)
    {
        $username = session(Security::$SESSION_USER) != null ? session(Security::$SESSION_USER)->username : '';
        $level = 'outgoing';
        return view('List_Actions.list_action_outgoing_letter', compact('letter','level','username'))->render();
    }

    public static function laratablesCustomAssociationAction($letter)
    {
        $level = 'outgoing';
        return view('List_Actions.list_action_associate_outgoing', compact('letter','level'))->render();
    }

    public function letterType(){
        return $this->belongsTo('App\LetterType');
    }

    public static function laratablesCustomCheckBox($letter)
    {
        $level = 'ed';
        $letterCategory = 'outgoing';
        return view('extra_cols.associate_cbx', compact('letter','level','letterCategory'))->render();
    }

    public function getDateSentAttribute($value){
        return date("M d Y", strtotime($value));
    }

    public function getDateReceivedAttribute($value){
        return date("M d Y", strtotime($value));
    }

    public static function laratablesAdditionalColumns()
    {
        $columns = ['outgoing_letter_file_name','outgoing_letter_upload_user','outgoing_letter_receipt_file_name','outgoing_letter_receipt_upload_user'];
        return $columns;
    }

}
