<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EdmsDocType extends Model
{
    public static function laratablesCustomAction($record)
    {
        $model = 'edmsdoctype';
        $data = array(
            'model'=>'edmsdoctype',
            'record'=>$record
        );
        return view('List_Actions.general_action')->with($data)->render();
    }
}
