<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LetterMovement extends Model
{

    public function moveable(){
        return $this->morphTo();
    }

    public function letter(){
        return $this->belongsTo('App\IncomingLetter','moveable_id','id');
    }

    public function memo(){
        return $this->belongsTo('App\InternalMemo','moveable_id','id');
    }

    public static function laratablesCustomLetterMovtAction($letterMovement)
    {
        $letter = IncomingLetter::find($letterMovement->moveable_id);
        $level = 'ed';
        return view('List_Actions.list_action_letter_movt', compact('letter','level'))->render();
    }

    public static function laratablesCustomMemoMovtAction($letterMovement)
    {
        $memo = InternalMemo::find($letterMovement->moveable_id);
        $level = 'ed';
        return view('List_Actions.list_action_memo_movt', compact('memo','level'))->render();
    }

    public static function laratablesCustomLetterTypeName($letterMovement)
    {

        $letter = IncomingLetter::where('id','=',$letterMovement->moveable_id)->first();

        if($letter == null){
            return '';
        }

        $type = LetterType::where('id','=', $letter->letter_type_id)->first();
        $data = $type != null ? $type->letter_type_name : '';
        return $data;
    }

}
