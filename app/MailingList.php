<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailingList extends Model
{

    public static function laratablesCustomAction($mailingList)
    {
        $data = array(
            'model'=>'mailinglist',
            'mailingList'=>$mailingList
        );
        return view('List_Actions.list_action_mailing_list')->with($data)->render();
    }


    public function setEmailAddressesAttribute($value)
    {

        $validEmails = '';
        $noSpaces = preg_replace('/\s+/', '', $value);
        $noSpacesArr = explode(',',$noSpaces);

        $i = 0;
        foreach ($noSpacesArr as $item){
            $validEmails .= $i == 0 ? $item : ','.$item;
            $i++;
        }

        $this->attributes['email_addresses'] = $validEmails;
    }


}
