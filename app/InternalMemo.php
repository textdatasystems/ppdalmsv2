<?php

namespace App;

use App\Helpers\Security;
use Illuminate\Database\Eloquent\Model;

class InternalMemo extends Model
{

    public function letterMovements(){
        return $this->morphMany('App\LetterMovement','moveable');
    }

    public static function laratablesCustomAction($memo)
    {
        $username = session(Security::$SESSION_USER) != null ? session(Security::$SESSION_USER)->username : '';
        $level = 'reception';

        $data = array(
            'username' => $username,
            'level' => $level,
            'memo' => $memo,
        );

        return view('List_Actions.list_action_memo')->with($data)->render();

    }

    public static function laratablesCustomRegistryAction($memo)
    {
        $username = session(Security::$SESSION_USER) != null ? session(Security::$SESSION_USER)->username : '';
        $level = 'registry';
        return view('List_Actions.list_action_memo', compact('memo','level','username'))->render();
    }
    public static function laratablesCustomEDAction($memo)
    {
        $username = session(Security::$SESSION_USER) != null ? session(Security::$SESSION_USER)->username : '';
        $level = 'ed';
        return view('List_Actions.list_action_memo', compact('memo','level','username'))->render();
    }

    public function getMemoDateAttribute($value){
        return date("M d Y", strtotime($value));
    }

    public function getDateReceivedAttribute($value){
        return date("M d Y", strtotime($value));
    }

    public function setSubjectAttribute($value){

        $trimmedContent = trim($value);
        $trimmedContent = strtoupper($trimmedContent);
        $trimmedContent = preg_replace('/\s+/', ' ',$trimmedContent);
        $this->attributes['subject'] = $trimmedContent;

    }

    public static function laratablesAdditionalColumns()
    {
        $columns = ['memo_file_name','memo_upload_user'];
        return $columns;
    }

}
