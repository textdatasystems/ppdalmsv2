<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/16/19
 * Time: 3:52 PM
 */

namespace App;


class ApiResp
{
    public $statusCode = "";
    public $statusDescription = "";
    public $result;
}