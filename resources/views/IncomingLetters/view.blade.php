<div class="">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body smaller_accordions">


                    <form action="{{route('store')}}" class="form-horizontal" method="post" id="form_{{time()}}">

                        <input type="hidden" name="ext" value="1">
                        @csrf
                        <input type="hidden" name="table" value="IncomingLetter">
                        <input type="hidden" name="r_fld[current_que]" value="{{ (isset($letter)?$letter->current_que:'reception') }}">

                        @if(isset($letter))
                            <input type="hidden" name="fld_id" value="{{ $letter->id }}">
                        @endif

                        <div id="accordion_ad_review" class="accordion-style1 panel-group accordion-style2 management_letter_accordion">
                            <!-- panel 1 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle " data-parent="#accordion_ad_review" href="#!" aria-expanded="false">Letter Details</a>
                                    </h4>
                                </div>
                                <div class="panel-collapse" id="section31_ad_review" aria-expanded="false">
                                    <div class="panel-body">

                                        <div class="form-group row">
                                            <label for="letter_category" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Letter Category:') }}</label>
                                            <div class="col-md-6">
                                                <select readonly="" id="select_letter_category" class="form-control selectize select_letter_category" name="r_fld[letter_category]" required>
                                                    <option value="">Select Letter Category</option>
                                                    <option value="Incoming Letter" {{((@$letter->letter_category == 'Incoming Letter' ) ? 'selected="selected"':'')}} >Incoming Letter</option>
                                                    <option value="Memo - To ED" {{((@$letter->letter_category == 'Memo - To ED' ) ? 'selected="selected"':'')}} >Memo - To ED</option>
                                                    <option value="Memo - From ED" {{((@$letter->letter_category == 'Memo - From ED' ) ? 'selected="selected"':'')}} >Memo - From ED</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row ctn-memo-specific-fields" style="display: none">
                                            <label for="from_user" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('From User:') }}</label>
                                            <div class="col-md-6">
                                                <select readonly id="from_user" name="r_fld[from_user]" class="form-control selectize select-fill-hidden-input" data-hidden-input-id="from_user_full_name" required>
                                                    <option value="">Select User</option>
                                                    @if(isset($users))
                                                        @foreach($users as $user)
                                                            <option value="{{ $user->email }}" {{((@$letter->from_user == $user->email ) ? 'selected="selected"':'')}}>{{ $user->first_name .' '.$user->last_name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <input id="from_user_full_name" name="r_fld[from_user_full_name]" value="{{ (isset($letter)?$letter->from_user_full_name:'') }}" type="hidden"/>
                                            </div>
                                        </div>

                                        <div class="ctn-letter-specific-fields" style="display: none">

                                            <div class="form-group row ctn-org-type">
                                                <label for="entity_name" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Organization Type:') }}</label>
                                                <div class="col-md-6">
                                                    <select readonly id="org_type_selector" class="form-control org_type_selector" name="r_fld[org_type]" required>
                                                        <option value="">Select Organization type</option>
                                                        <option value="Government Entity" {{ (@$letter->org_type == 'Government Entity') ? 'selected="selected"':'' }} >Government Entity</option>
                                                        <option value="Non - Government Entity" {{ (@$letter->org_type == 'Non - Government Entity') ? 'selected="selected"':'' }}>Non - Government Entity</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="display:none">
                                                <label for="c_f_name"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Government Entity:') }}</label>
                                                <div class="col-md-6">
                                                    <select readonly name="r_fld[entity_name]" class="government_entity_select selectize form-control">
                                                        <option value="">Select Government Entity</option>
                                                        @foreach($entities as $entity)
                                                            <option value="{{ $entity->entity_name }}"
                                                                    data-edms-doc-path="{{$entity->edms_incoming_letter_path}}"
                                                                    data-entity-id="{{$entity->id}}"
                                                                {{((@$letter->entity_name == $entity->entity_name) ? 'selected="selected"':'')}} >
                                                                {{ $entity->entity_name .' - '. $entity->acronym }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group row" >
                                                    <label for="entity_not_in_list_flag" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Entity not in list:') }}</label>
                                                    <div class="col-md-6 checkbox">
                                                        <label><input readonly name="r_fld[entity_not_in_list_flag]" type="checkbox" {{(@$letter->entity_not_in_list_flag ? 'checked':'')}}  value="1">Create Entity At Registry</label>
                                                    </div>
                                                </div>

                                                <input type="hidden" id="edms_document_path_folder" name="r_fld[edms_document_path_folder]" value=""/>
                                            </div>

                                            {{--Begin new non govt code--}}

                                            <div class="form-group row" style="display:none">
                                                <label for="c_f_name"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Non Government Entity:') }}</label>
                                                <div class="col-md-6 ">
                                                    <select readonly name="r_fld[entity_name]" id="provider_select" class="form-control selectize non_government_entity_select" >
                                                        <option value=""></option>
                                                        @if(isset($providers))
                                                            @foreach($providers as $provider)
                                                                <option value="{{$provider->orgname}}" {{((@$letter->entity_name == $provider->orgname) ? 'selected="selected"':'')}} >{{ $provider->orgname }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>

                                            {{-- End new non govt code--}}

                                        </div>

                                        <div class="form-group row">
                                            <label for="c_l_name"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Subject:') }}</label>
                                            <div class="col-md-6">
                                                <textarea readonly name="r_fld[subject]" id="" class="form-control" rows="3" required>{{ (isset($letter)?$letter->subject:'') }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="c_title"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Letter Date:') }}</label>
                                            <div class="col-md-6">
                                                <input readonly id="c_title" type="text" class="form-control calendar" name="r_fld[date_sent]" value="{{ (isset($letter)?full_year_format_date($letter->date_sent): '' ) }}" placeholder=""  required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="date_received_ppda"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Date Received:') }}</label>
                                            <div class="col-md-6">
                                                <input readonly id="date_received_ppda" type="text" class="form-control calendar" name="r_fld[date_received_ppda]" value="{{ (isset($letter)?full_year_format_date($letter->date_received_ppda): date('j M Y') ) }}" placeholder="" required>
                                            </div>
                                        </div>

                                        {{-- Added fields --}}
                                        <div class="form-group row ctn-letter-specific-fields" >
                                            <label for="sender_reference_number"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Sender Reference Number:') }}</label>
                                            <div class="col-md-6">
                                                <input readonly id="sender_reference_number" type="text" class="form-control " name="r_fld[sender_reference_number]" value="{{ (isset($letter)?$letter->sender_reference_number:'') }}" placeholder="" autofocus >
                                            </div>
                                        </div>
                                        <div class="form-group row ctn-letter-specific-fields" >
                                            <label for="ppda_reference_number"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('PPDA Reference Number:') }}</label>
                                            <div class="col-md-6">
                                                <input readonly id="ppda_reference_number" type="text" class="form-control " name="r_fld[ppda_reference_number]" value="{{ (isset($letter)?$letter->ppda_reference_number:'') }}" placeholder="" autofocus >
                                            </div>
                                        </div>
                                        <div class="form-group row" >
                                            <label for="attention_to"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Attention To:') }}</label>
                                            <div class="col-md-6">
                                                <select readonly name="r_fld[attention_to]" class="form-control selectize select-fill-hidden-input" data-hidden-input-id="attention_to_name">
                                                    <option value="">Select User</option>
                                                    @if(isset($users))
                                                        @foreach($users as $user)
                                                            <option value="{{ $user->email }}" {{((@$letter->attention_to == $user->email ) ? 'selected="selected"':'')}}>{{ $user->first_name .' '.$user->last_name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <input id="attention_to_name" name="r_fld[attention_to_name]" value="{{ (isset($letter)?$letter->attention_to_name:'') }}" type="hidden" />
                                            </div>

                                        </div>
                                        <div class="form-group row ctn-letter-specific-fields" >
                                            <label for="signatory"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Signatory:') }}</label>
                                            <div class="col-md-6">
                                                <input readonly id="signatory" type="text" class="form-control " name="r_fld[signatory]" value="{{ (isset($letter)?$letter->signatory:'') }}" placeholder="" autofocus >
                                            </div>
                                        </div>
                                        <div class="form-group row ctn-letter-specific-fields">
                                            <label for="signatory_title" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Sender Signatory Title:') }}</label>
                                            <div class="col-md-6">
                                                <select readonly class="form-control selectize" name="r_fld[signatory_title]">
                                                    <option value="">Select Signatory Title</option>
                                                    @if(isset($signatoryTitles))
                                                        @foreach($signatoryTitles as $signatoryTitle)
                                                            <option value="{{ $signatoryTitle['title'] }}" {{((@$letter->signatory_title == $signatoryTitle['title'] ) ? 'selected="selected"':'')}} >{{ $signatoryTitle['title']}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row " >
                                            <label for="created_by_user" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Receiving Officer:') }}</label>
                                            <div class="col-md-6">
                                                <input readonly id="created_by_user" type="text" class="form-control " name="r_fld[created_by_user]" value="{{ (isset($letter)?$letter->created_by_user: session('user')->username) }}" placeholder="" autofocus readonly >
                                                <input id="created_by_user_name" type="hidden" class="form-control " name="r_fld[created_by_user_name]" value="{{ (isset($letter)?$letter->created_by_user_name: session('user')->fullName) }}" placeholder=""  >
                                            </div>
                                        </div>

                                        <div class="form-group row ctn-letter-specific-fields">
                                            <label for="letter_type_id"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Letter Type:') }}</label>
                                            <div class="col-md-6">
                                                <select required data-show-extra-fields = "{{((@$letter->current_que == 'registry' || @$letter->current_que == 'ed' ) ? 'true':'false')}}" data-url = "{{route('letter_type_fields','')}}" id="dropdown_letter_types" class="form-control selectize" name="r_fld[letter_type_id]">
                                                    <option value="">Select Letter Type</option>
                                                    @if(isset($letterTypes))
                                                        @foreach($letterTypes as $letterType)
                                                            <option value="{{ $letterType->id }}" {{((@$letter->letter_type_id == $letterType->id ) ? 'selected="selected"':'')}} >{{ $letterType->letter_type_name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>


                                        @if(isset($letter) && ($letter->current_que == 'registry' || $letter->current_que == 'ed'))
                                            <div class="form-group row">
                                                <label for="doc_fully_scanned" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Have All Documents Been Scanned:') }}</label>
                                                <div class="col-md-6">
                                                    <div class="radio">
                                                        <label>
                                                            <input name="r_fld[doc_fully_scanned]" type="radio" class="ace " value="1" {{ (@$letter->doc_fully_scanned == 1) ? 'checked':'' }} >
                                                            <span class="lbl"> Yes</span>
                                                        </label>
                                                        <label>
                                                            <input name="r_fld[doc_fully_scanned]" type="radio" class="ace " value="0" {{ (@$letter->doc_fully_scanned == 0 || @$letter->doc_fully_scanned === null ) ? 'checked':'' }} >
                                                            <span class="lbl"> No</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        {{-- Begin section for extra fields --}}
                                        <div id="extra_fields">
                                            @if(isset($letter) && ($letter->current_que == 'registry' || $letter->current_que == 'ed'))
                                                @include('master_data.general.letter_types.letter_type_fields')
                                            @endif
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

