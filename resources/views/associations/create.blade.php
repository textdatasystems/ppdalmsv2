
    <div class="form-actions no-margin-bottom">
        <div class="float-alert center">
            <div class="float-alert-container">
                <div class="col-md-12">
                    <a data-table-to-show="TableIncomingLetters" data-table-to-hide="TableOutgoingLetters" href="#!" class="btn btn-primary btn-sm btn-association-letters" title="Incoming Letters">Incoming Letters</a>
                    <a data-table-to-show="TableOutgoingLetters" data-table-to-hide="TableIncomingLetters" href="#!" class="btn btn-primary btn-sm btn-association-letters" title="Outgoing Letter">Outgoing Letters</a>
                    <hr>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

    <form action="{{route('store')}}" class="form-horizontal" method="post" id="form_{{time()}}">

        <input type="hidden" name="ext" value="1">
        @csrf
        <input type="hidden" name="table" value="LetterAssociation">
        <input type="hidden" name="letter_id" value="{{$letterId}}">
        <input type="hidden" name="letter_category" value="{{$letterCategory}}">

    <div style="display: block;width: 100%" id="TableIncomingLetters" >

        <div style="font-weight: bold; margin-bottom: 5px" class="col-md-12 text-md-left">{{ __('Incoming Letters') }}</div>
        <table  class="data-table letter_associations_letters_incoming {{$letterId.':::'.$letterCategory}} table table-striped table-bordered hide_first_column table-hover no-margin-bottom no-border-top" >
            <thead>
            <tr>
                <th style="vertical-align: top"></th>
                <th style="vertical-align: top;width: 4%">Select</th>
                <th style="vertical-align: top">Entity Name</th>
                <th style="vertical-align: top;width: 25%">Subject</th>
                <th style="vertical-align: top">Date Sent</th>
                <th style="vertical-align: top">Date Received PPDA</th>
                <th style="vertical-align: top">Date Received Registry</th>
                <th style="vertical-align: top">Sender Reference No.</th>
                <th style="vertical-align: top">PPDA Reference No.</th>
                <th style="vertical-align: top">Attention To</th>
                <th style="vertical-align: top">Letter Type</th>
                <th style="vertical-align: top">Attached Document</th>
                <th style="vertical-align: top"></th>
            </tr>
            </thead>
        </table>

        <div class="form-group row mb-0">
            <div class="col-md-12 left">
                <input type="submit" value="Add Association" class="btn btn-sm btn-primary btnSubmit">
            </div>
        </div>

    </div>

    <div style="display: none;width: 100%" id="TableOutgoingLetters" >
        <div style="font-weight: bold; margin-bottom: 5px" class="col-md-12 text-md-left">{{ __('Outgoing Letters') }}</div>
        <table  class="data-table letter_associations_letters_outgoing {{$letterId.':::'.$letterCategory}} table table-striped table-bordered hide_first_column table-hover no-margin-bottom no-border-top" >
            <thead>
            <tr>
                <th style="vertical-align: top"></th>
                <th style="vertical-align: top;width: 4%">Select</th>
                <th style="vertical-align: top">Entity <br>Name</th>
                <th style="vertical-align: top;width: 25%">Subject</th>
                <th style="vertical-align: top">Date <br>Sent</th>
                <th style="vertical-align: top">Date Received</th>
                <th style="vertical-align: top">PPDA Reference No</th>
                <th style="vertical-align: top">Recipient Reference No</th>
                <th style="vertical-align: top">Recipient Title</th>
                <th style="vertical-align: top">Department of Origin</th>
                <th style="vertical-align: top">Attached <br>Doc</th>
                <th style="vertical-align: top"></th>
            </tr>
            </thead>
        </table>

        <div class="form-group row mb-0">
            <div class="col-md-12 left">
                <input type="submit" value="Add Association" class="btn btn-sm btn-primary btnSubmit">
            </div>
        </div>

    </div>

    </form>
