

    <div class="form-actions no-margin-bottom">
        <div class="float-alert center">
            <div class="float-alert-container">
                <div class="col-md-12">
                    <a href="{{ route('letter-association.create',[$letterId, $letterCategory]) }}" class="clarify_secondary btn btn-primary btn-sm" title="Add Associated Letters">Add Associated Letters</a>
                    <hr>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

    <table class="data-table letter_associations {{$letterId}} {{$letterCategory}} table table-striped table-bordered table-hover hide_first_column no-margin-bottom no-border-top" >
        <thead>
            <tr>
                <th style="vertical-align: top"></th>
                <th style="vertical-align: top">Entity <br>Name</th>
                <th style="vertical-align: top;width: 25%">Subject</th>
                <th style="vertical-align: top">Letter Type</th>
                <th style="vertical-align: top">Letter Category</th>
                <th style="vertical-align: top">Letter Date</th>
                <th style="vertical-align: top">Date Received/Posted</th>
                <th style="vertical-align: top">Entity Ref</th>
                <th style="vertical-align: top">PPDA Ref</th>
                <th style="vertical-align: top">AttachedDoc</th>
                <th style="vertical-align: top"></th>
            </tr>
        </thead>
    </table>

