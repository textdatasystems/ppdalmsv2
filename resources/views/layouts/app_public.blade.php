<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Language" content="en" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<META name=GENERATOR content="MSHTML 8.00.7600.16385" />

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="title" content="@yield('title')" />

<meta property="og:site_name" content="@yield('title')" />

<meta name="revisit-after" content="3 days" />

<title>@yield('title')</title>

<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

<link rel="stylesheet" href="{{ asset('css/jquery.gritter.css') }}" />
<link rel="stylesheet" href="{{ asset('css/app-styles.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/app-custom-styles.min.css') }}" />


		<!-- bootstrap & fontawesome -->
		<!-- <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}" /> -->
		<!-- <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" /> -->
		<!-- <link rel="stylesheet" href="{{ asset('css/datepicker.css') }}" /> -->
		<!-- <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}" /> -->

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<!-- <link rel="stylesheet" href="{{ asset('css/ace-fonts.css') }}" /> -->

		<!-- ace styles -->
		<!-- <link rel="stylesheet" href="{{ asset('css/ace.css') }}" /> -->

		<!-- <link rel="stylesheet" href="{{ asset('css/ace.css') }}" class="ace-main-stylesheet" id="main-ace-style" /> -->

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="{{ asset('css/ace-part2.css') }}" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="{{ asset('css/ace-ie.css') }}" />
		<![endif]-->

        <!-- inline styles related to this page -->
        @yield('stylesheets')




		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="{{ asset('js/html5shiv.js') }}"></script>
		<script src="{{ asset('js/respond.js') }}"></script>
        <![endif]-->

        <style>
            .dataTables_wrapper{
                background: #F5F5F5;
            }
            .dataTables_wrapper .row:last-child{
                border-bottom: none;
                background: #F5F5F5;
            }
            .form-actions:last-child{
                border-top: none;
                border-bottom: 1px solid #E5E5E5;
                margin-top: 0;
                padding-top: 0;
            }
            .entity-selector{
                margin-top: 5px;
            }
            .chosen-container > .chosen-single, [class*="chosen-container"] > .chosen-single{
                height: 34px;
                padding-top: 2px;
            }
            .submit_msg{
                display: none;
                left: 0;
                margin: 0 25%;
                position: fixed;
                top: 0;
                width: 50%;
                z-index: 1000000000;
            }
            .tab-pane{
                overflow: hidden;
            }
            .master_data .sidebar{
                background: none;
                width: inherit;
                padding-right:8px;
            }
            .master_data .sidebar ul{
                list-style:none;
            }
            .master_data .sidebar ul li{
                margin-bottom: 5px;
            }
            .master_data .tab-content{
                margin-left: 200px;
                border: none;
                overflow: hidden;
                padding-top: 0;
            }
            .master_data .form-actions{
                margin-top: 0;
            }
            .form-horizontal .col-form-label{
                margin-bottom: 0;
                padding-top: 7px;
                font-size: 11px;
                padding-top: 0 !important;
            }
            .text-md-right{
                text-align: right;
            }
            .gritter-item-wrapper{
                top: auto;
            }
            .text-select-entity{
                font-size: 70px;
                color: #a19898;
                text-align: center;
            }
            .selected-entity-header{
                text-align: right;
                float: right;
            }
            .data-table .dropdown-toggle{
                border: none;
                background-color: transparent !important;
                padding: 0 5px 0 8px;
            }
            .main-content .btn, #randomActionModal .btn{
                border-radius: 4px;
            }
            .profile-info-row .profile-info-value, .profile-info-row .profile-info-name{
                vertical-align: top;
            }
            .profile-info-row span.editable{width:100%;}
            .editable-container.editable-inline, .form-horizontal .editable.actual{
                display: none;
            }
            form .editable-container.editable-inline{
                display: inline-block;
                width: 100%;
            }

            .activity-profile .profile-info-row div.profile-info-value:nth-child(2){
                background: rgba(195, 32, 4,.2);
                width: 35%
            }
            body.public .activity-profile .profile-info-row div.profile-info-value:nth-child(2){
                display:none;
            }

            .activity-profile .profile-info-row div.profile-info-name:nth-child(1){
                font-size: 100%;
                font-weight: bold;
            }

            .activity-profile .profile-info-row div.profile-info-value:nth-child(3){
                padding-bottom: 0px;
                padding-top: 3px;
            }

            .activity-profile input, .activity-profile select{
                font-size: 100%;
                padding: 5px 10px;
            }

            .text-white,.text-white-50{
                color: #ffffff !important;
            }




            .actions {
                position: relative;
                z-index: 0;
                top: 10px;
                max-width: 60px;
                /* right: 0px; */
                -webkit-transform: translateY(-50%);
                transform: translateY(-50%);
                font-size: 18px;
                color: #047bf8;
                text-decoration: none;
                cursor: pointer;
            }
            .actions .actions-list {
                position: absolute;
                background-color: #0068B3 /* #0f2338 */;
                color: #fff;
                font-size: 0.9rem;
                padding: 12px 12px;
                border-radius: 5px;
                visibility: hidden;
                opacity: 0;
                -webkit-transform: translateY(10px);
                transform: translateY(10px);
                -webkit-transition: all 0.2s ease;
                transition: all 0.2s ease;
                top: 20px;
                right: 0px;
            }
            .actions:hover{
                z-index: 10;
            }
            .actions:hover > i {
                transform: rotate(180deg);
            }
            .actions:hover .actions-list{
                visibility: visible;
                transform: translateY(0px);
                opacity: 1;
            }
            .actions-list a {
                display: block;
                padding: 5px 10px;
                border-bottom: 1px solid rgba(255,255,255,0.05);
                color: #fff;
                text-decoration: none;
                white-space: nowrap;
            }
            .actions-list a:last-child{
                border-bottom: none;
            }
            .actions-list a i {
                font-size: 17px;
                display: inline-block;
                vertical-align: middle;
                margin-right: 10px;
                color: #fff;
            }
			.actions-list a span {
                color: rgba(255,255,255,0.7);
                display: inline-block;
                vertical-align: middle;
                transition: all 0.2s ease;
            }
            .actions-list a:hover span {
                color: #fff;
                transform: translateX(-3px);
            }
			.actions-list a.danger i {
                color: #ff5b5b;
            }
            .actions-list a.danger span {
                color: #ff5b5b;
            }


        </style>

</head>
<body class="no-skin public">
    <div class="submit_msg"></div>
    <!-- #section:basics/navbar.layout -->
    @include('layouts.webparts.titlebar_public')
    <!-- /section:basics/navbar.layout -->
    <div class="main-container" id="main-container">
        <script type="text/javascript">
            try{ace.settings.check('main-container' , 'fixed')}catch(e){}
        </script>
        <!-- #section:basics/sidebar.horizontal -->
        @include('layouts.webparts.navbar_public')

        <!-- /section:basics/sidebar.horizontal -->
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="row">
                        <div class="col-xs-12">
                            @yield('content')
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content-inner -->
        </div><!-- /.main-content -->

        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
            <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
        </a>
    </div><!-- /.main-container -->

    <!-- basic scripts -->
    @include('layouts.webparts.footer')

    @yield('scripts')

    <!-- ace settings handler -->
	<!-- <script src="{{ asset('js/ace-extra.js') }}"></script> -->
</body>
</html>
