
{{-- Start Dialog for rendering PDFS --}}

<div id="pdf-dialog" style="display:none;">
	<div >
		<iframe id="iframe-pdf-viewer" style="width: 100%;height: 600px" src=""></iframe>
	</div>
</div>

{{-- End Dialog for rendering PDFS --}}

<!-- The Random Action Modal -->
<div class="modal container fade" id="randomActionModal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
		    <!-- Modal Header -->
		    <div class="modal-header">
		        <h4 class="modal-title">Modal Heading</h4>
		        <button type="button" class="close" data-dismiss="modal" style="margin-top: -35px;">&times;</button>
		    </div>
		    <!-- Modal body -->
		    <div class="modal-body">
		        Modal body..
		    </div>

		    <!-- Modal footer -->
		    <div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		    </div>
		</div>
	</div>
</div>
<!-- End the Random Action modal -->
<!-- Second model -->
<div class="modal fade bs-fill-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" style="margin-top: -35px;">&times;</button>
      </div>
      <div class="modal-body">
	  </div>
	  <!-- Modal footer -->
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
    </div>
  </div>
</div>
<!-- End the Second model -->

<!-- Tertiary model -->
<div class="modal fade" id="TertiaryModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button" style="margin-top: -35px;">&times;</button>
            </div>
            <div class="modal-body">
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End the Tertiary modal -->

<div class="footer">
	<div class="footer-inner">
		<!-- #section:basics/footer -->
		<div class="footer-content">
			<a href="#"><img src="{{ asset('img/logo3.png') }}" style="height:20px;"></a>
			<span class="bigger-120 blue">
				<span style="font-size:60%;">Entity Management System</span>
				&copy; {{ date("Y") }}
			</span>
		</div>
		<!-- /section:basics/footer -->
	</div>
</div>

<!-- Hidden form for image uploads -->
<div style="display:none;">
	<form name="frmPhoto" enctype="multipart/form-data" method="post" id="target">
		@csrf
		<div id="target_input"></div>
	</form>
</div>
<!-- End hidden form -->



<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script type='text/javascript' src='{{ asset('js/jquery.js') }}'>"+"<"+"/script>");
			window.jQuery || document.write("<script type='text/javascript' src='{{ asset('js/jquery-ui.js') }}'>"+"<"+"/script>");
			window.jQuery || document.write("<script type='text/javascript' src='{{ asset('js/jquery-ui.custom.js') }}'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script type='text/javascript' src='{{ asset('js/jquery1x.js') }}'>"+"<"+"/script>");
</script>
<![endif]-->
		<!-- <script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script type='text/javascript' src='{{ asset('js/jquery.mobile.custom.js') }}'>"+"<"+"/script>");
		</script> -->
		<!-- <script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script> -->

		<script type="text/javascript" src="{{ asset('js/app-scripts.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/app-custom-scripts.min.js') }}"></script>

		<script type="text/javascript" src="{{ asset('js/chosen.jquery.js') }}"></script>

		<script type="text/javascript" src="{{ asset('js/jquery.gritter.js') }}"></script>

		<script type="text/javascript" src="{{ asset('js/date-time/bootstrap-timepicker.js') }}"></script>

		<script type="text/javascript" src="{{ asset('js/form_validate.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/forms_custom.js') }}"></script>

		<script type="text/javascript" src="{{ asset('js/tinymce/jquery.tinymce.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/inc/ajaxupload.js') }}"></script>

		<script type="text/javascript" src="{{ asset('js/jquery-ui.js') }}"></script>


		<!-- inline scripts related to this page -->
		<script type="text/javascript">


            function handleLetterCategorySelectionLogic() {

                var selected = $('#select_letter_category').children("option:selected").val();
                if (selected === 'Incoming Letter') {

                    //show incoming letter specific fields
                    $('.ctn-org-type').css('display', 'block');
                    $('.ctn-letter-specific-fields').css('display', 'block');
                    $('.ctn-memo-specific-fields').css('display', 'none');

                    //enable required fields
                    $("#org_type_selector").prop('required', true);
                    $("#dropdown_letter_types").prop('required', true);
                    $("#from_user").prop('required', false);

                } else if (selected === 'Memo - To ED' || selected === 'Memo - From ED') {

                    //show memo specific fields
                    $('.ctn-memo-specific-fields').css('display', 'block');
                    $('.ctn-letter-specific-fields').css('display', 'none');

                    //enable required fields
                    $("#org_type_selector").prop('required', false);
                    $("#dropdown_letter_types").prop('required', false);
                    $("#from_user").prop('required', true);

                    var folderField = $("#edms_document_path_folder");
                    folderField.val("DEFAULT_MEMO");

                } else {
                    $('.ctn-org-type').css('display', 'none');
                    $('.ctn-letter-specific-fields').css('display', 'block');
                    $('.ctn-memo-specific-fields').css('display', 'none');

                    //enable required fields
                    $("#org_type_selector").prop('required', true);
                    $("#dropdown_letter_types").prop('required', true);
                    $("#from_user").prop('required', false);
                }

            }

            function showEmailLinkAttachment(){

                var deleteTempDocumentURL = $("#pdf-dialog-1").attr('data-remove-temp-url');
                var dialog1 = $("#pdf-dialog-1").dialog({
                    width: 1300,
                    height: 600,
                    top:['center',20],
                    close: function(event) {

                        $.get(deleteTempDocumentURL, function(data ) {
                            var resp = jQuery.parseJSON(data);
                        });
                    }
                });

                $("#pdf-dialog-1").show();
                // $(".ui-dialog").show();

            }


            jQuery(function($) {

                showEmailLinkAttachment();

			$('body').on('click','.clarify,.clarify_secondary,.clarify_tertiary,.clarify_form',function(event){
				event.preventDefault()
				var modal_selector =  (/clarify_secondary/i.test($(this).attr('class') ) ? '.bs-fill-modal' : '#randomActionModal' )

                modal_selector = ''
                if(/clarify_secondary/i.test($(this).attr('class') )){
                    modal_selector = '.bs-fill-modal'
                }else if(/clarify_form/i.test($(this).attr('class') )){
                    modal_selector = '#saveAndStickModal'
                }else if(/clarify_tertiary/i.test($(this).attr('class') )){
                    modal_selector = '#TertiaryModal'
                }else{
                    modal_selector = '#randomActionModal'
                }

				var $msg_load_preview_wait = "" +
						"<div class='float-alert'>" +
							"<div class='float-alert-container'>" +
								"<div class='col-sm-9-'>" +
									"<h3 class='header smaller lighter green text-center'><i class='fa fa-globe'></i>&nbsp;Request Submitted </h3>" +
									"<p class='text-center'>" +
										"<i class='ace-icon fa fa-spinner fa-spin orange bigger-225'></i> " +
										"Loading details! Please wait..." +
									"</p>" +
								"</div>" +
							"</div>" +
						"</div>";

				$(modal_selector +' .modal-body').html($msg_load_preview_wait)
				$(modal_selector +' .modal-title').html('Please wait...')
				$(modal_selector).modal('show');

				var url = $(this).attr('href')
				var title = $(this).attr('title')

				$.get(url,function(result){
					$(modal_selector +' .modal-body').html(result);
					$(modal_selector +' .modal-title').html(title);
					//activateForm()
					initiateDateRangePicker()
					initializeTimePicker()
					selectize()
					loadListing()
					initiateWYSIWYG()
					initializeFilePicker()
					form_element_show_hide()
				})
			})

			$(window).on('resize.ace.top_menu', function() {
				//$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			});


		  //timothy custom script

                $(document).on('change','#dropdown_letter_types', function() {

                    var selectedLetterType = $(this).val();
                    var showExtraFields = $(this).attr('data-show-extra-fields');

                    //no selection clear the extra fields
                    if(selectedLetterType == '0' || showExtraFields != 'true'){
                        $('#extra_fields').html('');
                        return;
                    }

                    var dataURL = $(this).attr('data-url') + '/'+selectedLetterType;

                    $.get(dataURL, function (data) {
                        // use this method you need to handle the response from the view
                        $('#extra_fields').html(data);
                    });

                });

                $(document).on('change','#dropdown_letter_types_outgoing', function() {

                    var selectedLetterType = $(this).val();

                    //no selection clear the extra fields
                    if(selectedLetterType == '0'){ $('#extra_fields').html('');return;  }

                    var dataURL = $(this).attr('data-url') + '/'+selectedLetterType;

                    $.get(dataURL, function (data) {
                        // use this method you need to handle the response from the view
                        $('#extra_fields').html(data);
                    });

                });

                /*
                * Handles PDF viewing using the Jquery-ui library
                * */
				$(document).on('click','.pdf-link', function() {

					//go to EDMS API and get the HREF
					var downloadLinksURL = $(this).attr('data-download-url');
					var deleteTempDocumentURL = $(this).attr('data-remove-temp-url');
                    var enableToolBar = $(this).attr('data-enable-toolbar');
					var tempPath = '';

					var dialog1 = $("#pdf-dialog").dialog({
						width: 900,
						height: 550,
						top:['center',20],
						close: function(event) {

							var removeURL = deleteTempDocumentURL + tempPath;
							$.get(removeURL, function(data ) {
								var resp = jQuery.parseJSON(data);
							});

						}
					});

					//show progress dialog
					pushNotification('<i class="fa fa-spinner fa-spin"></i> Loading info','Please wait as we load the requested PDF!','info');

					// use other ajax submission type for post, put ...
					$.get( downloadLinksURL, function(data ) {

						// use this method you need to handle the response from the controller, in this case I am returning a json response
						var resp = jQuery.parseJSON(data);
						if(resp.statusCode == 0){

							$(".ui-dialog").show();
							var pdfURL = resp.result;
							tempPath = resp.tempPath;

							//disable the download and print link on the PDF Viewer
							pdfURL = enableToolBar === "yes" ? pdfURL : pdfURL + "?page=hsn#toolbar=0"; //https://stackoverflow.com/questions/41586093/disable-hide-download-button-in-iframe-on-default-pdf-viewer //https://stackoverflow.com/questions/20328820/how-to-disable-pdf-toolbar-download-button-in-iframe
							$('#iframe-pdf-viewer').attr('src', pdfURL);

						}else{

                           $(".ui-dialog").hide();
                           pushNotification('Operation Failed',resp.statusDescription,'error');
                          // alert(resp.statusDescription);

						}

					});

				});


				/*
				* Refresh entity list
				* */
				$(document).on('click','.btnRefreshEntities', function() {

					alert("Refreshing Entities");

				});

				/*
				* For setting EDMS Folder path for the Government entities
				* */
                $('body').on('change','.government_entity_select',function(){

                    var folderField = $("#edms_document_path_folder");
                    var entityType = $('.org_type_selector').children("option:selected").val();
                    if(entityType == 'Government Entity'){

                        var selected = $(this).children("option:selected");
                        var dataEntityId = selected.attr('data-entity-id');
                        var dataEdmsDocRootPath = selected.attr('data-edms-doc-path');
                        dataEdmsDocRootPath = dataEdmsDocRootPath == null || dataEdmsDocRootPath === "" ? "DEFAULT" : dataEdmsDocRootPath;
                        folderField.val(dataEdmsDocRootPath);

                    }else{
                        folderField.val("DEFAULT");
                    }

                });

                /*
				* For setting EDMS Folder path for the Non government to DEFAULT
				* */
                $('body').on('change','.org_type_selector',function(){

                    var selected = $(this).children("option:selected").val();
                    if(selected === 'Non - Government Entity'){

                        var folderField = $("#edms_document_path_folder");
                        folderField.val("DEFAULT");

                    }

                });

				/*
				* For handling TAG selection
				* */
				$('body').on('change','.tag-rbtns',function(){

					var cbxRowId = $(this).attr('data-cbx-container-id');
					var selectClassId = $(this).attr('data-dropdown-class');

					if(this.checked) {

						var selected = $(this).val() === "1";
						if(selected){

							$('#'+cbxRowId).css('display','block'); //show the check boxes

						}else{

							//$("#"+selectClassId).prop("selectedIndex", 0);
							//$("#"+selectClassId)[0].selectedIndex = 0;
							$('#'+cbxRowId).css('display','none'); //Hide the check boxes

						}

					}

				});

				/*
				* For setting name value of the selected user
				* */
				$('body').on('change','.select-fill-hidden-input',function(){

					try{

						//autofill the fullName
						var textValue = $(this).children("option:selected").text();
						var idOfInputToFill = $(this).attr('data-hidden-input-id');
						$('#'+idOfInputToFill).val(textValue);

						//autofill the department
						var selectedOption = $(this).children("option:selected");
						var dataDept = selectedOption.attr('data-dept');
						var idOfInputDept= $(this).attr('data-hidden-input-dept');
						$('#'+idOfInputDept).val(dataDept);

					}catch (e) {
						pushNotification("Error occurred", e.message,'danger')
					}

				});

                $('body').on('click','.btn-association-letters',function(){

                    var elemShow = $(this).attr('data-table-to-show');
                    var elemHide = $(this).attr('data-table-to-hide');

                    $('#'+elemHide).css('display','none');
                    $('#'+elemShow).css('display','block');

                    loadListing();

                });

                // Call delete URL with confirmation
                $('body').on('click','.delete-timo',function(){

                    var deleteURL = $(this).attr('data-url');
                    var modelToDel = $(this).attr('data-model');

                    bootbox.confirm('Are you sure you want to delete '+modelToDel+'!', function(result) {
                        if(result) {
                            actionAuthorized = true;
                            document.location = deleteURL;
                        }
                    });

                    return false;
                });

                /*
				* For setting EDMS Folder path for the Non government to DEFAULT
				* */
                $('body').on('change','.org_type_selector',function(){

                    var selected = $(this).children("option:selected").val();
                    if(selected === 'Non - Government Entity'){

                        var folderField = $("#edms_document_path_folder");
                        folderField.val("DEFAULT");

                    }

                });

                /*
				* For handling letter category selection
				* */
                $('body').on('change','.select_letter_category',function(){
                    handleLetterCategorySelectionLogic();
                });

                $("body").on('change', ".select_out_letters_signatory_title", function() {
                    try{

                        form_element_show_hide();
                    }catch($ex){
                    }
                });

                /*
                * When the modal is loaded, with server data, handle show
                * */
                $("body").on('DOMSubtreeModified', ".modal-body", function() {
                    try{
                        handleLetterCategorySelectionLogic();
                    }catch($ex){
                    }
                });


                /*
				* For setting EDMS Folder path for the Government entities
				* */
                $('body').on('change','#select_departments',function(){

                    var selectedDepartment = $('#select_departments').children("option:selected").val();

                    $('option[data-dept="'+selectedDepartment+'"]').hide();

                    // $('#origin_department_unit').children().hide();
                    // $('#origin_department_unit').children('option[data-dept="'+selectedDepartment+'"]').hide();
                    //
                    // $("#origin_department_unit").trigger('contentChanged');

                });


          //end timothy custom scripts

		});

		</script>

		<script type="text/javascript">

			var actionAuthorized = false
			$(document).on('click','button[value=Delete],button[value=Deactivate],button[value="Auto Deactivate"]',function(e){
				var action = $(this).val()
				var btn = $(this)
				if(actionAuthorized){
					actionAuthorized = false
					return
				}
				e.preventDefault()
				if(action == 'Deactivate'){
					bootbox.prompt("Give reasons for Deactivation?", function(result) {
						alert(result)
						if (result === null) {
							alert('Please specify reason for deactivation')
						}else{
							$('.reason_text').val(result)
							actionAuthorized = true
							btn.trigger('click')
							//alert()
						}
					});
				}else{
					bootbox.confirm('Are you sure you want to '+action+'!', function(result) {
						if(result) {
							actionAuthorized = true
							btn.trigger('click')
						}
					});
				}

			})

			actionAuthorized = false
			$(document).on('click','.btn-delete',function(e){
				var action = "Delete";
				var deleteURL = $(this).attr('href');

				var btn = $(this)
				if(actionAuthorized){
					actionAuthorized = false
					return
				}
				e.preventDefault()
				if(action == 'Delete'){
					bootbox.confirm('Are you sure you want to '+action+'!', function(result) {
						if(result) {
							actionAuthorized = true
							document.location = deleteURL
						}
					});
				}

			})

			$(document).on('change','.chk-other',function(){
				if($(this).prop('checked')){
					$(this).parents('label').find('.specify-other').show()
				}else{
					$(this).parents('label').find('.specify-other').val('').hide()

				}
			});

			$(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			});

// datepicker
			$('.date-picker').datepicker({
				autoclose: true,
				todayHighlight: true,
				format: "d M yyyy"
			})
			//show datepicker when clicking on the icon
			.next().on(ace.click_event, function(){
				$(this).prev().focus();
			});
//////////////////////////////////////////////
/* $(document).on('focus','.date-picker',function(){
	$(this).datepicker()
})
$(document).on('focus','.date-range-picker',function(){
	$(this).daterangepicker()
}) */

// initialize date pickers
			function initiateDateRangePicker(){
				$('.calendar').datepicker({
					autoclose: true,
					todayHighlight: true,
					format: "d M yyyy"
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});

				$('.date-range-picker').daterangepicker({
					'applyClass' : 'btn-sm btn-success',
					'cancelClass' : 'btn-sm btn-default',
					format: "D MMM YYYY",
					locale: {
						applyLabel: 'Apply',
						cancelLabel: 'Cancel',
					}
				})
				.prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
			}
////////////////////////////////////////

// initialize time pickers
			function initializeTimePicker(){
				$('.clock').timepicker({
					minuteStep: 1,
					showSeconds: true,
					showMeridian: false
				}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
			}

//Stylize our selects
function selectize(){

    //$('.timo').chosen({allow_single_deselect:true});
	//chosen select
	$('.chosen-select, .selectize:not(tr.hide .selectize)').chosen({allow_single_deselect:true});

}



			function changeInput(str){
				alert(str)
			}

			function addRow(tableID) {
	        	var table = document.getElementById(tableID);
				var rowCount = table.rows.length;
				var cellContent = ''
	        	if(rowCount < 100){							// limit the user from creating fields more than your limits
	        		var row = table.insertRow(rowCount);
	        		    row.id = (rowCount-1)+'_rowdata';
					var colCount = table.rows[0].cells.length;
	        		for(var i=0; i<colCount; i++) {
						cellContent = ''
	        			var newcell = row.insertCell(i);
		              	if(tableID=="dataTable3"){
							cellContent = table.rows[1].cells[i].innerHTML;
						}
						else if(tableID=="activity_dates_table" && i==1){
							cellContent = table.rows[1].cells[i].innerHTML;
							cellContent = cellContent.replace("[DAY]","Day "+(rowCount-1));
						}else{
							cellContent = table.rows[1].cells[i].innerHTML;
						}


						//$(cellContent).find('.chosen-container').remove()
						//$(cellContent).find('select').css('display','block')
						//cellContent = cellContent

						newcell.innerHTML = cellContent;


					}
					if(tableID == "activity_dates_table"){
						initiateDateRangePicker(); //add calendar for dates
						initializeTimePicker();
					}
	        	}else{
	        		 alert("Maximum is 100.");
				}

				//style_select()
				selectize()
	    }

			function deleteRow(tableID) {
				var table = document.getElementById(tableID);
				var rowCount = table.rows.length;
				for(var i=1; i<rowCount; i++) { //0
					var row = table.rows[i];
					var chkbox = row.cells[0].childNodes[0];
					if(null != chkbox && true == chkbox.checked) {
						if(rowCount <= 2) { 						// limit the user from removing all the fields
							alert("Cannot Remove all.");
							break;
						}
						table.deleteRow(i);
						rowCount--;
						i--;
					}
				}
			}

			$('[data-rel=tooltip]').tooltip();


		</script>



<!-- query analyzer code -->
		<script type="text/javascript">
			var $validation = true;

				$('#validation-form').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: false,
					ignore: "",
					rules: {
						email: {
							required: true,
							email:true
						},
						password: {
							required: true,
							minlength: 5
						},
						password2: {
							required: true,
							minlength: 5,
							equalTo: "#password"
						},
						name: {
							required: true
						},
						phone: {
							required: true,
							phone: 'required'
						},
						url: {
							required: true,
							url: true
						},
						comment: {
							required: true
						},
						state: {
							required: true
						},
						service: {
							required: true
						},
						description: {
							required: true
						},
						age: {
							required: true,
						},
						agree: {
							required: true,
						}
					},

					messages: {
						email: {
							required: "Please provide a valid email.",
							email: "Please provide a valid email."
						},
						password: {
							required: "Please specify a password.",
							minlength: "Please specify a secure password."
						},
						state: "Please choose state",
						subscription: "Please choose at least one option",
						gender: "Please choose gender",
						agree: "Please accept our policy"
					},


					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},

					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
						$(e).remove();
					},

					errorPlacement: function (error, element) {
						if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},

					submitHandler: function (form) {
						$('.results_container').html('<div class="center"><i class="fa fa-spinner fa-spin bigger-230"></i></div>')
						$('#accordion').find('.accordion-toggle').addClass('collapsed');
						$('#accordion').find('.panel-collapse').removeClass('in');
						$('#accordion').find('.ace-icon').addClass('fa-angle-right').removeClass('fa-angle-down');
						var values = $(form).serialize();
						$.ajax(
							{
								type: "POST",
								url: 'qry_results.php',
								data: "myData=" + values,
								cache: false,
								success: function(message)
								{
									$('.results_container').html(message)
									$('html').animate({scrollTop:$('.results_container').offset().top},600,'swing')
								},
								error: function(message)
								{
									alert('Query failure!! Please make sure all the required fields are filled before Submitting.');
								}
							});
					},
					invalidHandler: function (form) {
					}
				});


			$(document).on('change','#qry_dataset',function(){
			 	//alert('am in')
			 	var fields_html = '';
			 	var txt_dataset = $(this).val()
				 if($(this).val() != ''){
					$.post('qry_fields.php',{dataset:txt_dataset},function(result){
						fields_html = result;
						$('.fields_container').html(fields_html);
						$('#accordion').toggle(true)
						$('#accordion').find('.accordion-toggle').removeClass('collapsed');
						$('#accordion').find('.panel-collapse').addClass('in');
						$('#accordion').find('.ace-icon').removeClass('fa-angle-right').addClass('fa-angle-down');

						//initiateDateRangePicker()
					})
				 }else{
					$('.fields_container').html(fields_html);
					$('#accordion').toggle(false)
				 }

			});
			$(document).on('change','.query_operator',function(){
				try {
					//$(this).parents('tr').find('.calendar').datepicker('destroy')
					$(this).parents('tr').find('.calendar').data('datepicker').remove()
				} catch (error) {
					console.log(error);
				}
				try {
					$(this).parents('tr').find('.calendar').data('daterangepicker').remove()
				} catch (error) {
					console.log(error);
				}

				if($(this).val() == 'BETWEEN'){
					//$(this).parents('tr').find('.date-picker').removeClass('date-picker').addClass('date-range-picker').off()
					$(this).parents('tr').find('.calendar').daterangepicker({
						'applyClass' : 'btn-sm btn-success',
						'cancelClass' : 'btn-sm btn-default',
						format: "D MMM YYYY",
						locale: {
							applyLabel: 'Apply',
							cancelLabel: 'Cancel',
						}
					})
				}else{
					//$(this).parents('tr').find('.date-range-picker').removeClass('date-range-picker').removeClass('hasDatepicker').addClass('date-picker').off()
					//alert('am in')
					$(this).parents('tr').find('.calendar').datepicker({
						autoclose: true,
						todayHighlight: true,
						format: "d M yyyy"
					})
					//alert('ok')
				}
				//initiateDateRangePicker()
			})

			/*$(document).on("submit", "form", function(e){
			    e.preventDefault();
			    return  false;
			});*/

			// $('body').on('click','.btnFilter',function(event){
			// 	event.preventDefault();
			// });

			$(document).on("change", ".chk_select_all", function(){
			    if($(this).prop('checked')){
			    	$('.chk_fields').prop('checked',true)
			    }else{
			    	$('.chk_fields').prop('checked',false)
			    }
			});

			$(document).on("keypress","#pass1,#pass2",function(e){
				if(e.which == 118){
					e.preventDefault()
					alert("You can not paste URA Payment NO. Please type.")
					return false
				}

			})

			$(document).on("contextmenu","#pass1,#pass2",function(e){
				return false
			})



			function export_to(tableId){
				var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
			    var textRange; var j=0;
			    tab = document.getElementById(tableId); // id of table

			    for(j = 0 ; j < tab.rows.length ; j++)
			    {
			        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
			        //tab_text=tab_text+"</tr>";
			    }

			    tab_text=tab_text+"</table>";
			    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
			    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
			    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

			    var ua = window.navigator.userAgent;
			    var msie = ua.indexOf("MSIE ");

			    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
			    {
			        txtArea1.document.open("txt/html","replace");
			        txtArea1.document.write(tab_text);
			        txtArea1.document.close();
			        txtArea1.focus();
			        sa=txtArea1.document.execCommand("SaveAs",true,"save report.xls");
			    }
			    else                 //other browser not tested on IE 11
			        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

			    return (sa);
			}
		</script>

		<script>
			$(document).ready(function(){
				//data tables
				loadListing()

				//chosen select
				selectize()

				// Delete row in a table
				$('body').on('click','.delete-row',function(){

					var details = $(this).attr('class').split(' ')
					var route = details[1]
					var table = details[2]
					var pK = details[3]
					var id = details[4]

					bootbox.confirm('Are you sure you want to delete '+table+'!', function(result) {
						if(result) {
							//var url = '/'+route+'/'+id
							var url = '/delete/'+table+'/'+id

							$.get(url,function(message){
								completeHandler(table,message)
							})

							$(this).closest('tr').fadeOut(function(){
								$(this).remove();
							});
						}
					});
					return false;
				});

				$('body').on('click','.btn-reveal-form',function(){
					$('.form-container').toggle();
				})

				$('body').on('click','.btnUploadSubmit',function(event){

					event.preventDefault();
					var formId = $(this).parents('form').attr('id');
					var url = $(this).parents('form').attr('action');
					// var formData = new FormData($(this)[0]);
					var formData = new FormData($('#'+formId)[0]);
					console.log("formData "+ formData);
				  $.ajax({
				   url:url,
				   method:"POST",
				   data:formData,
				   // dataType:'JSON',
				   contentType: false,
				   cache: false,
				   processData: false,
					 beforeSend: function(data)
					 {
						 pushNotification('<i class="fa fa-spinner fa-spin"></i> Submitting info','Please wait as we submit your information!','info')
					 },
				   success:function(response)
				   {
						 	// response = JSON.parse(response);
							if(response.success){
								completeHandler('frm',response.message);
								setTimeout(function(){
                                    location.reload();
								}, 1000);
							}else{

							    if(response.message === 'File name exceeds allowed maximum length of [100] characters'){

							        try{

							            alert('Please note that the uploaded filename is longer than the allowed maximum filename. Please rename the file.');

                                        //show field for new file name
                                        $('#container_new_upload_name').css('display','block');
                                        $('#new_upload_file_name').val(response.suggestedFileName);
                                        $('#new_upload_file_name').focus();

                                    }catch(ex){

                                    }

                                }else{
                                    pushNotification('<i class="fa fa-frown-o"></i> Error Submission',response.message,'error');
                                }

							}

				   },

					 error: function(error)
					 {
						 alert('Seems like something went wrong ' + error);
					 }
				  })
				});






				$('body').on('change','#GetEntityDetails',function() {
					console.log("show_entity_details");
				    var entity_id = $(this).val();
					var section = 'details';
					GetEntityDetails(entity_id,section);
				});

				function GetEntityDetails(entity_id,section){
					$.ajax({
							type: "GET",
							url: '/get_entity_details/'+entity_id+"/"+section,
							cache: false,
							success: function(data) {
								if(section == 'details'){
										$('#show_entity_details').html(data);
										GetEntityDetails(entity_id,"last_audited")
								}else{
										$('#last_audited_details').html(data);
								}
							},
							error: function(error) {
								alert('Please select an Entity.');
							}
						});
				}

				$('body').on('change','#select_entity_compliance',function() {
						var loading_ = $('.request_loader_');
						var get_entity_compliance = $("#get_entity_compliance");
						var show_entity_compliance = $("#show_entity_compliance");
				    var entity_id = $(this).val();
						$.ajax({
								type: "GET",
								url: '/get_entity_compliance/'+entity_id,
								cache: false,
								beforeSend: function (){
									loading_.removeClass('hide');
								},
								success: function(data) {
									loading_.addClass('hide');
									show_entity_compliance.html(data);
								},
								error: function(error) {
									loading_.addClass('hide');
									alert('Please select an Entity.');
								}
							});
				});

				$('body').on('change','#get_selection_criteria',function(event) {
					event.preventDefault()
					console.log("selection_criteria");
				    var audit_type = $(this).val();
						var loading_ = $('.request_loader_');
						loading_.removeClass('hide');
						$("#showSelectedCriteria").html('');
						$("#showCriteriaEntities").html('');
						$("#showOagOpinions").addClass('hide');
						$("#showYearLastAudited").addClass('hide');
						$("#showPreviousPpdaPerformance").addClass('hide');

						var url = "{ route('get_selection_criteria','[ID]') }"
						url = url.replace("%5BID%5D",audit_type)

						$.ajax(
							{
								type: "GET",
								url: url,
								cache: false,
								success: function(data)
								{
									$("#showSelectedCriteria").html(data);
									loading_.addClass('hide');
								},
								error: function(error)
								{
									loading_.addClass('hide');
									alert('Seems like something went wrong');
								}
							});
				});

				$('body').on('change','#selection_criteria',function(event) {
					event.preventDefault()
					var selection_criteria = $(this).val()
					$("#showOagOpinions").addClass('hide');
					$("#showYearLastAudited").addClass('hide');
					$("#showPreviousPpdaPerformance").addClass('hide');
					if(selection_criteria == 'previous_oag_opinion'){
							$("#showOagOpinions").removeClass('hide');
					}else if(selection_criteria == 'last_year_audited'){
							$("#showYearLastAudited").removeClass('hide');
					}else if (selection_criteria == 'previous_ppda_performance') {
							$("#showPreviousPpdaPerformance").removeClass('hide');
					}
				});


				$('body').on('click','#getCriteriaEntities',function(event) {
					event.preventDefault()
					var loading_ = $('.request_loader_');
					var formId = $(this).parents('form').attr('id');
					var values = $('#'+formId).serialize();
					var frmAction = $('#'+formId).attr('action');
					$("#showCriteriaEntities").html('');
					$.ajax({
						type: "POST",
						url: frmAction,
						data: "myData=" + values,
						cache: false,
						beforeSend: function(data)
						{
							loading_.removeClass('hide');
						},

						success: function (data){
							loading_.addClass('hide');
							$("#showCriteriaEntities").html(data);
						},
						error: function(message)
						{
							loading_.addClass('hide');
							pushNotification('<i class="fa fa-frown-o"></i> Error Submission','<strong>Oops</strong>, Looks like something is a mess!!<br>'+message,'error')
						}
					});

				});

				$('body').on('change','#selectPreviousPpdaPerformance',function(event) {
					event.preventDefault()
					var performance_sub_criteria = $(this).val();
					// $("#PreviousPerformanceComparisonSign").addClass('hide');
					var PM_CategorySection = $("#PM_CategorySection");
					var PM_ComparisonSignValueSection = $("#PM_ComparisonSignValueSection");
					var PM_ComparisonSignSection = $("#PM_ComparisonSignSection");
					var PM_CategorySection = $("#PM_CategorySection");
					PM_CategorySection.addClass('hide');
					PM_ComparisonSignSection.find("select").val('');
					PM_ComparisonSignValueSection.addClass('hide');
					if(performance_sub_criteria == 'last_performance_category'){
						PM_CategorySection.removeClass('hide');
						PM_ComparisonSignValueSection.addClass('hide');
						PM_ComparisonSignSection.find("select").val('=');
						PM_ComparisonSignSection.find("*").prop("disabled", true);
					}else{
						PM_ComparisonSignValueSection.removeClass('hide');
						PM_ComparisonSignSection.find("*").prop("disabled", false);
						PM_ComparisonSignValueSection.find("*").prop("disabled", false);
					}
				});

				$('body').on('change','#pm_interval_selector',function(event) {
					event.preventDefault();
					var pm_interval_selector = $(this);
					var pm_interval_selector_vaule = $(this).val();
					pm_interval_selector_vaule = (pm_interval_selector_vaule == "") ? "interval above" : pm_interval_selector_vaule;
					$('.pm_interval').val('').addClass('hide');
					$('#pm_interval_' + pm_interval_selector_vaule).val(pm_interval_selector_vaule).removeClass('hide');
					$('label#pm_interval_label').html("Select " + pm_interval_selector_vaule);
				});

				// $('body').on('click','.pmFilterBtn',function(event) {
				// 	event.preventDefault()
				// 	$('#compliance_monthly_reports').addClass('hide');
				// 	$('#entities_interval_month').removeClass('hide');
				// 	loadListing();
				// });

				$('body').on('change','.check_entity_audit_type',function(event) {
					event.preventDefault()
					var audit_type = $(this).attr('audit_type');
					var pm_plan_id = $(this).attr('pm_plan_id');
					var entity_id = $(this).val();

					var selected = $(this)

					if($(this).prop('checked')){
						$.ajax({
							type: "GET",
							url: '/check_if_pm_entity_audit_type_exists/'+pm_plan_id+'/'+entity_id+'/'+audit_type,
							cache: false,
							success: function (response){
								if(response == "true"){
										// Entity already mapped on  Audit type"
										bootbox.confirm('Entity already exists for this Audit type !', function(result) {
											selected.removeAttr('checked');
										});
								}
							},
							error: function(message)
							{
								pushNotification('<i class="fa fa-frown-o"></i> Error Submission','<strong>Oops</strong>, Looks like something is a mess!!<br>'+message,'error')
							}
						});
					}
				});

				$('body').on('change','#cb_select_entity',function(){
					var entity_type = $(this).children("option:selected").attr('entity_type');
					var input = $(this).parents('tr').find('.display_entity_type').val(entity_type);
				})

				$('body').on('change','.fuel_cost_select',function(){
					$(this).parents('tr').find('.cost_per_litre').val($(this).children("option:selected").val())
				})

				$('body').on('change','.mgt-letter-section-select',function(){
					$('.section-exceptions-select').val('')
					form_element_show_hide()
					/* $('.section-exceptions-select').children("option").hide()
					$('.section-exceptions-select').children("option:selected").text('')
					var section = $(this).val();
					$('.section-exceptions-select').children("option[data-section="+section+"]").show()
					$('.section-exceptions-container').show() */
				})

				$('body').on('change','.attachment-module-select',function(){
					$('.module-sections-select').val('')
					form_element_show_hide()
				})



				$('body').on('click','.make_team_leader',function(){

					pushNotification('<i class="fa fa-spinner fa-spin"></i> Submitting info','Please wait as we submit your information!','info')

					var teamMemberId = $(this).attr('class').split(' ')[1]
					var url = "{ route('pm.make_team_leader','[ID]') }"
					url = url.replace("%5BID%5D",teamMemberId)
					//alert(url)
					$.get(url,function(data){
						completeHandler('frm',data)
						}
					)
				})

				$('body').on('change','#provider_select',function(){
					//alert($(this).children("option:selected").text());
					$('.provider_name').val( $(this).children("option:selected").text() )
				})

				$('body').on('change','.contract_value_selector',function(){
					//alert($(this).children("option:selected").text());
					var selected = $(this).children("option:selected").text()
					$('.known_values,.unknown_values').attr('disabled','disabled').hide()
					if(selected == 'Known'){
						$('.known_values').removeAttr('disabled').show()
						$('.unknown_values').attr('disabled','disabled').hide()
					} else if(selected == 'Unknown'){
						$('.unknown_values').removeAttr('disabled').show()
						$('.known_values').attr('disabled','disabled').hide()
					}
				})

				$('body').on('change','.attendee_type_select',function(){
					//alert($(this).children("option:selected").text());
					var selected = $(this).children("option:selected").val()
					$('.ppda_staff_select,.entity_person_select').removeAttr('name').attr('disabled','disabled').parents('.form-group.row').hide()
					if(selected == 'App\\User'){
						$('.ppda_staff_select').removeAttr('disabled').attr('name','r_fld[commentable_id]').parents('.form-group.row').show()
					} else if(selected == 'App\\Person'){
						$('.entity_person_select').removeAttr('disabled').attr('name','r_fld[commentable_id]').parents('.form-group.row').show()
					}
					$('.chosen-container').css('width','100%')
				})

				$('body').on('change','.org_type_selector',function(){

                    form_element_show_hide()

				})

				$('body').on('click','.btn-people-form',function(){
					$(this).toggleClass('active').toggleClass('btn-danger')
					$(this).parents('.input-group').find('.chosen-container').toggleClass('chosen-disabled')
					$(this).parents('.input-group').find('.form-heading').toggleClass('active')
					$('.people-form').toggle()
					$('.people_form_active').prop('checked', (i, v) => !v);
				})
				$('body').on('click','.btn-user-form',function(){
					$('.user-form').toggle()
				})

				$('body').on('keyup change','.budget_items_table .quantity, .budget_items_table .unit_price, .budget_items_table .budget_item_selector',function(){
					var $tblrows = $(".budget_items_table tbody tr");
					$tblrows.each(function (index) {
					    var $tblrow = $(this);
						var quantity = $tblrow.find(".quantity").val();
						var unit_price = $tblrow.find(".unit_price").val();
						var subTotal = parseInt(quantity,10) * parseFloat(unit_price);

						if (!isNaN(subTotal)) {
							$tblrow.find('.sub_total').val(subTotal.toFixed(2));
								get_budget_items_table_grand_total();
						}
					});
				});

				$('body').on('change','.budget_item_selector',function(){
					var unit_price = $(this).children("option:selected").attr('unit_price');
					unit_price = parseInt(unit_price);
					$(this).parents('tr').find('.unit_price').val(unit_price);
				})

				$('body').on('change','.allowances_budget_item',function(){
					var rate = $(this).children("option:selected").attr('rate');
					$(this).parents('tr').find('.rate').val(rate);
					computeAllowanceBudgets();
				})



				$('body').on('keyup','.entity_allowances_budgets .staff, .entity_allowances_budgets .days',function(){
					computeAllowanceBudgets();
				})

				function computeAllowanceBudgets(){
					var $tblrows = $(".entity_allowances_budgets tbody tr");
					$tblrows.each(function (index) {
							var $tblrow = $(this);
							// $tblrow.find('.quantity').on('change', function () {
								var rate = $tblrow.find(".rate").val();
								var days = $tblrow.find(".days").val();
								var staff = $tblrow.find(".staff").val();
								var subTotal = parseInt(rate) * parseFloat(days) * parseFloat(staff);

								if (!isNaN(subTotal)) {
									$tblrow.find('.allowance_sub_total').html(subTotal);
									var allowance_grandTotal = 0;

									$(".allowance_sub_total").each(function () {
											var stval = parseFloat($(this).html());
											allowance_grandTotal += isNaN(stval) ? 0 : stval;
									});
									$('.allowances_budget_total').html(allowance_grandTotal);
										$tblrow.find('.allowance_sub_total').html(subTotal);
										var allowance_grandTotal = 0;

										$(".allowance_sub_total").each(function () {
												var stval = parseFloat($(this).html());
												allowance_grandTotal += isNaN(stval) ? 0 : stval;
										});
										$('.allowances_budget_total').html(number_format(allowance_grandTotal));
								}
								var fuel_grandTotal = parseInt($('.fuel_budget_total').html());
								var grandTotal = allowance_grandTotal + (isNaN(fuel_grandTotal) ? 0 : fuel_grandTotal);
								$('.grandTotal').html(number_format(grandTotal));

							// });
					});
				}

				$('body').on('keyup','.entity_fuel_budgets .litres',function(){
					calculate_fuel_budgets()
				})
				$('body').on('change','.entity_fuel_budgets .fuel_cost_select',function(){
					calculate_fuel_budgets()
				})
				$('body').on('change','.grounds_table input[type=checkbox]',function(){
					var data_sample = $(this).parents('table').attr('data-sample')
					var parent_tr_id = $(this).parents('tr').attr('id')
					var this_checked = $(this).prop('checked')
					$('.grounds_table[data-sample='+data_sample+'] tr#'+parent_tr_id+' input[type=checkbox]').each(function(){
						$(this).prop('checked',this_checked)
					})
				})
				$('body').on('change keyup','.grounds_table input[type=text]',function(){
					var data_sample = $(this).parents('table').attr('data-sample')
					var parent_tr_id = $(this).parents('tr').attr('id')
					var this_value = $(this).val()
					var this_class = $(this).attr('class')
					$('.grounds_table[data-sample='+data_sample+'] tr#'+parent_tr_id+' input[class="'+this_class+'"]').each(function(){
						$(this).val(this_value)
					})
				})

				$('body').on('click','.close_tab',function(){
					$('.administration_tab,.master_data_tab').toggleClass('hide')
				})

				function calculate_fuel_budgets(){
					var $tblrows = $(".entity_fuel_budgets tbody tr");
					$tblrows.each(function (index) {
					    var $tblrow = $(this);
							// $tblrow.find('.quantity').on('change', function () {
								var litres = $tblrow.find(".litres").val();
								var cost_per_litre = $tblrow.find(".cost_per_litre").val();
								var subTotal = parseFloat(litres) * parseFloat(cost_per_litre);

								if (!isNaN(subTotal)) {
								    $tblrow.find('.fuel_sub_total').html(subTotal);
								    var fuel_grandTotal = 0;

								    $(".fuel_sub_total").each(function () {
								        var stval = parseFloat($(this).html());
								        fuel_grandTotal += isNaN(stval) ? 0 : stval;
								    });
								    $('.fuel_budget_total').html(number_format(fuel_grandTotal));
								}

								var allowance_grandTotal = parseFloat($('.allowances_budget_total').html());
								var grandTotal = fuel_grandTotal + (isNaN(allowance_grandTotal) ? 0 : allowance_grandTotal);
								$('.grandTotal').html(number_format(grandTotal));

							// });
					});
				}


				$('body').on('keyup','.entity_budget_plan_table .amount, .entity_budget_plan_table .rate',function(){
					var $tblrows = $(".entity_budget_plan_table tbody tr");
					$tblrows.each(function (index) {
					    var $tblrow = $(this);
								var amount = $tblrow.find(".amount").val();
								var rate = $tblrow.find(".rate").val();
								var subTotal = parseInt(amount) * parseFloat(rate);

								if (!isNaN(subTotal)) {
								    $tblrow.find('.sub_total').val(subTotal.toFixed(2));
								}
					});
				})


				$('body').on('click','.audit_counts_continue',function(){
					var counts = parseInt($(this).parents('.form-group').find('.audit_plan_counts').val());
					if(isNaN(counts)){
						alert("Please provide count");
						return;
					}

					$(this).parents('.form-group').addClass('hide');
					$('.entity_counts_table').removeClass('hide');
					$('.display-planned-count').html(counts);
				})

				$('body').on('keyup','.entity_audit_count_size',function(){
					var $tblrows = $(".entity_audit_count_size_table tbody tr");
					$tblrows.each(function (index) {
						var totalCount = 0;
						$(".entity_audit_count_size").each(function () {
								var stval = parseFloat($(this).val());
								totalCount += isNaN(stval) ? 0 : stval;
						});

						$(this).parents('table').find('.display_audit_total_count').html(totalCount);
						// console.log("total count ", totalCount);
					});
				})

				$('body').on('change','.entity_type_selector',function(){
					var entity_id = $(this).val();
					$('.entity_type_selected_row').removeClass('hide');
					var entity_type_category = $('.entity_type_category');
					entity_type_category.addClass('hide').removeAttr('required');
					$('#entity_type_selected_'+entity_id).removeClass('hide');
				})

				$('body').on('change','#select_cb_activity_facilitator',function(){
					var type = $(this).children("option:selected").attr('type');
					var new_class = type+'_facilitators';
					$(this).parents('tr').find('.facilitator').addClass('hide').attr('disabled');
					$(this).parents('tr').find('#'+new_class).removeClass('hide').removeAttr('disabled');
				})

				$('body').on('change','.row-findings-select',function(){

					loadListing()
				})

				// control for image upload
				$('body').on('change','.uploadimg',function(){

					var selectedFileName = $(this).val();

					if(selectedFileName != null && selectedFileName != ""){

						var filename = $(this).val().split('\\').pop();

                        if( $('#letter_file_name').length ){
                            $('#letter_file_name').val(filename);  // it exists
                        }

						$('#fancy_file_name').attr('data-title', filename);

					}else{
						$('#fancy_file_name').attr('data-title', "No File ...");
					}

				});

				$('body').on('keyup','.search_entity_selector_filter',function(){
				var value = $(this).val().toLowerCase();
					$("#entity_selector_table tbody tr ").filter(function() {
						$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
					});
				});



				$('body').on('click','.forward-action',function(event){
					event.preventDefault()
					var letter_id = $(this).attr('data-id')
					var que = $(this).attr('data-que')

					pushNotification('<i class="fa fa-spinner fa-spin"></i> Submitting info','Please wait as we submit your information!','info')

					var url = "{{ route('letter.forward',['[ID]','[QUE]']) }}"
					url = url.replace("%5BID%5D",letter_id)
					url = url.replace("%5BQUE%5D",que)

					//alert(url)
					$.get(url,function(data){
						completeHandler('frm',data)
						}
					)
				});

                $('body').on('click','.forward-action-memo',function(event){
                    event.preventDefault()
                    var letter_id = $(this).attr('data-id')
                    var que = $(this).attr('data-que')

                    pushNotification('<i class="fa fa-spinner fa-spin"></i> Submitting info','Please wait as we submit your information!','info')

                    var url = "{{ route('memo.forward',['[ID]','[QUE]']) }}"
                    url = url.replace("%5BID%5D",letter_id)
                    url = url.replace("%5BQUE%5D",que)

                    //alert(url)
                    $.get(url,function(data){
                            completeHandler('frm',data)
                        }
                    )
                });



			})// end document ready function

			// function to load datatable lists on pages
			function loadListing(){
				if($('.data-table').length > 0){
					$('.data-table,.inner-table').each(function(){

						var table = $(this).attr('class').split(' ')[1]
						var id = $(this).attr('class').split(' ')[2]
						var extraParam = $(this).attr('class').split(' ')[3]

						//alert(id);
						//alert(table);
						//alert(extraParam);

						//initiateDataTable('data-table',table,id)
						initiateDataTable(table,table,id,extraParam)
					})
				}
				if($('.ajaxLoad').length > 0){
					$('.ajaxLoad').each(function(){
						var route = $(this).attr('class').split(' ')[1]

						loadPart($(this),route)
					})
				}


			}

			function get_budget_items_table_grand_total(){
				var grandTotal = 0;
				$('.budget_items_table').find(".sub_total").each(function () {
						var stval = parseFloat($(this).val());
						grandTotal += isNaN(stval) ? 0 : stval;
				});
				console.log("grandTotal ", grandTotal);
				$('.grand_total').html(number_format(grandTotal));
			}

			function loadPart(ele,url){
				$.get(url,function(response){
					ele.html(response)
				} )
			}


			function initiateDataTable(selector,table,id,extraParam){
				@include('layouts.webparts.listings')

				var url = listing[table]['url'].replace("%5BID%5D",id)
					url = url.replace("%5BEXTRA%5D",extraParam)
				var cols = listing[table]['cols']

				if( $.fn.DataTable.isDataTable('.'+selector)  ){
					$('.'+selector).DataTable().ajax.reload();
					return
				}


				$('.'+selector).DataTable({
					"bDestroy": true,
					responsive: false,
					"bProcessing": true,
					serverSide: true,
					ajax: url,
					columns: cols,
					"buttons": [
						{ extend: 'copyHtml5', 'footer': true, exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ] } },
						{ extend: 'excelHtml5', 'footer': true, exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ] } },
						{ extend: 'csvHtml5', 'footer': true, exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ] } },
						{ extend: 'pdfHtml5', orientation: 'landscape', pageSize: 'A4', 'footer': true,
						exportOptions: { columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ] } },
						{ extend: 'colvis', text: 'Columns'},
					],
				}, function(data){
					console.log("Data :::", data);
				});
			}

			//function to initiate rich text
			function initiateWYSIWYG(){
				$('textarea.wysiwyg').tinymce({
					width: '100%',
					height: 300,
					toolbar: "responsivefilemanager  undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image table fullscreen | forecolor backcolor | code | mybutton",
					menubar: false,

					setup: function(editor) {
						editor.addButton('mybutton', {
						type: 'menubutton',
						text: 'Place holders',
						icon: false,
						menu: [
							{
								text: 'Insert New Section',
								onclick: function() {
									editor.insertContent('&nbsp;{{NEW_SECTION_TAG}}&nbsp;');
								}
							},
							{
							text: 'Insert PM Activity Entity Outgoing letter code',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_PM_ACTIVITY_ENTITY_OUTGOING_LETTER_CODE}}&nbsp;');
							}
						}, {
							text: 'Insert Current date',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_DATE}}&nbsp;');
							}
						},

						{
							text: 'Insert Current PM Activity Entity',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_PM_ACTIVITY_ENTITY}}&nbsp;');
							}
						},
						{
							text: 'Insert Current Entity Headquater District',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_ENTITY_HEADQUATER_DISTRICT}}&nbsp;');
							}
						},

						{
							text: 'Insert Current PM Activity Audit Start date',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_PM_ACTIVITY_AUDIT_START_DATE}}&nbsp;');
							}
						},

						{
							text: 'Insert Current PM Activity Team',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_PM_ACTIVITY_TEAM}}&nbsp;');
							}
						},

						{
							text: 'Insert Current PM Activity FY',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_PM_ACTIVITY_FY}}&nbsp;');
							}
						},

						{
							text: 'Insert Current PM Activity Entry Meeting Date',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_PM_ACTIVITY_ENTRY_MEETING_DATE}}&nbsp;');
							}
						},

						{
							text: 'Insert Current PM Activity Audit Entry Meeting Time',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_PM_ACTIVITY_AUDIT_ENTRY_MEETING_TIME}}&nbsp;');
							}
						},

						{
							text: 'Insert Current CB Activity Name',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_CB_ACTIVITY_NAME}}&nbsp;');
							}
						},
						{
							text: 'Insert Current CB Activity Owner',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_CB_ACTIVITY_OWNER}}&nbsp;');
							}
						},
						{
							text: 'Insert Current CB Activity Location',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_CB_ACTIVITY_LOCATION}}&nbsp;');
							}
						},
						{
							text: 'Insert Current CB Activity discussed Topics',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_CB_ACTIVITY_TOPICS_TABLE}}&nbsp;');
							}
						},
						{
							text: 'Insert Current CB Activity Workshop attendance, participant details ',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_CB_ACTIVITY_ATTENDANCE_TABLE}}&nbsp;');
							}
						},
						{
							text: 'Insert Current CB Activity Key Issues Identified and Responses/Advice',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_CB_ACTIVITY_ISSUES_RAISED_TABLE}}&nbsp;');
							}
						},
						{
							text: 'Insert Current CB Activity number of participants',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_CB_ACTIVITY_PARTICIPANTS_COUNT}}&nbsp;');
							}
						},
						{
							text: 'Insert Current CB Activity summary of evaluations',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_CB_ACTIVITY_EVALUATIONS_SUMMARY}}&nbsp;');
							}
						},
						{
							text: 'Insert Current CB Activity Areas for Further Training',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_CB_ACTIVITY_FURTHER_TRAINING_AREAS}}&nbsp;');
							}
						},
						{
							text: 'Insert Current CB Activity summary of responses by Percentage for the participants',
							onclick: function() {
								editor.insertContent('&nbsp;{{EMIS_GET_CURRENT_CB_ACTIVITY_RESPONSES_SUMMARY_TABLE}}&nbsp;');
							}
						}
					]
						});
					}
				});

			}

			function number_format(num) {
			  return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
			}

			function initializeFilePicker(){
				$('.input-file').ace_file_input({
					no_file:'No File ...',
					btn_choose:'Choose',
					btn_change:'Change',
					droppable:false,
					onchange:null,
					thumbnail:false //| true | large
					//whitelist:'gif|png|jpg|jpeg'
					//blacklist:'exe|php'
					//onchange:''
					//
				});
			}

			function form_element_show_hide(){
				//.mgt-letter-section-select
				$('.section-exceptions-select').children("option").hide()
				var section = $('.mgt-letter-section-select').val();
				$('.section-exceptions-select').children("option[data-section="+section+"]").show()
				$('.section-exceptions-container').show()

				//file upload sections
				$('.module-sections-select').children("option").hide()
				var module = $('.attachment-module-select').val();
				$('.module-sections-select').children("option[data-module='"+module+"']").show()
				$('.module-sections-container').show()

				//entity_type
				var selected = $('.org_type_selector').children("option:selected").val()
				$('.government_entity_select,.non_government_entity_select').removeAttr('name').attr('disabled','disabled').parents('.form-group.row').hide()
				if(selected == 'Government Entity'){
					$('.government_entity_select').removeAttr('disabled').attr('name','r_fld[entity_name]').parents('.form-group.row').show()
				} else if(selected == 'Non - Government Entity'){
					$('.non_government_entity_select').removeAttr('disabled').attr('name','r_fld[entity_name]').parents('.form-group.row').show()
				}

				//signatory title
                var selectedTitle = $('.select_out_letters_signatory_title').children("option:selected").val();
                $('.ctn_out_letter_signatory_ed,.ctn_out_letter_signatory_others,.ctn_out_letter_signatory_username_ed').removeAttr('name').attr('disabled','disabled').parents('.form-group.row').hide();
                if(selectedTitle === 'Executive Director'){

                    $('.ctn_out_letter_signatory_username_ed').removeAttr('disabled').attr('name','r_fld[signatory_username]').parents('.form-group.row').show();
                    $('.ctn_out_letter_signatory_ed').removeAttr('disabled').attr('name','r_fld[signatory]');
                    //update the ED user name
                    var edUsername = $('.select_out_letters_signatory_title').attr('data-ed-username');
                    var edName = $('.select_out_letters_signatory_title').attr('data-ed-name');

                    $('#signatory_username_ed').val(edUsername);
                    $('#signatory_ed').val(edName);

                } else if(!(selectedTitle === '')){
                    $('.ctn_out_letter_signatory_others').removeAttr('disabled').attr('name','r_fld[signatory_username]').parents('.form-group.row').show();
                    $('.ctn_out_letter_signatory_name').removeAttr('disabled').attr('name','r_fld[signatory]').parents('.form-group.row').show();
                    $(".ctn_out_letter_signatory_username_ed").attr('disabled','disabled');
                }

				$('.chosen-container').css('width','100%')
			}

		</script>


@if(session('successMessage') != null)
	<div class="modal container fade" id="modalSuccess" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Operation Successful</h4>
				</div>
				<div class="modal-body">
					{{session('successMessage')}}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$('#modalSuccess').modal('show');
	</script>
@elseif(session('errorMessage'))
	<div class="modal container fade" id="modalFailed" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Operation Failed</h4>
				</div>
				<div class="modal-body">
					{{session('errorMessage')}}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$('#modalFailed').modal('show');
	</script>
@endif
