var listing = {
    'reception_letters' : {
        'url' : "{{ route('reception.letters.list') }}",
        'cols' : [
                    { name: 'letter_category' },
                    { name: 'entity_name' },
                    { name: 'subject' },
                    { name: 'date_sent' },
                    { name: 'date_received_ppda' },
                    { name: 'sender_reference_number' },
                    { name: 'ppda_reference_number' },
                    { name: 'attention_to_name' },
                    { name: 'letterType.letter_type_name' , orderable :false, searchable:false },
                    { name: "Action", "searchable": false, "orderable": false}
                ]
    },
    'registry_letters' : {
        'url' : "{{ route('registry.letters.list') }}",
        'cols' : [
                    { name: 'letter_category' },
                    { name: 'entity_name' },
                    { name: 'subject' },
                    { name: 'date_sent' },
                    { name: 'date_received_ppda' },
                    { name: 'sender_reference_number' },
                    { name: 'ppda_reference_number' },
                    { name: 'attention_to_name' },
                    { name: 'letterType.letter_type_name' , orderable :false, searchable:false },
                    { name: 'incoming_letter_file_name' },
                    { name: "RegistryAction", "searchable": false, "orderable": false}
                ]
    },
    'ed_letters' : {
        'url' : "{{ route('ed.letters.list') }}",
        'cols' : [
                    { name: 'letter_category' },
                    { name: 'entity_name' },
                    { name: 'subject' },
                    { name: 'date_sent' },
                    { name: 'date_received_ppda' },
                    { name: 'sender_reference_number' },
                    { name: 'ppda_reference_number' },
                    { name: 'attention_to_name' },
                    { name: 'letterType.letter_type_name', orderable :false, searchable:false },
                    { name: 'incoming_letter_file_name' },
                    { name: "EDAction", "searchable": false, "orderable": false}
                ]
    },
    'outgoingletters' : {
        'url' : "{{ route('outgoingletters.list') }}",
        'cols' : [
                    { name: 'entity_name' },
                    { name: 'subject' },
                    { name: 'date_sent' },
                    { name: 'date_received' },
                    { name: 'ppda_reference_number' },
                    { name: 'recipient_reference_number' },
                    { name: 'recipient_title' },
                    { name: 'origin_department' },
                    { name: 'outgoing_letter_file_name' },
                    { name: "Action", "searchable": false, "orderable": false}
                ]
    },
    'edms_doc_types' : {
        'url' : "{{ route('edms_doc_types.list') }}",
        'cols' : [
                    { name: 'type_name' },
                    { name: "Action", "searchable": false, "orderable": false}
                ]
    },
    'mailing_lists' : {
        'url' : "{{ route('mailing_lists.list') }}",
        'cols' : [
                    { name: 'list_name' },
                    { name: 'email_addresses' },
                    { name: "Action", "searchable": false, "orderable": false}
                ]
    },
    'global_values' : {
        'url' : "{{ route('global-values.list') }}",
        'cols' : [
                    { name: 'variable_name' },
                    { name: 'value' },
                    { name: "Action", "searchable": false, "orderable": false}
                ]
    },
    'letter_types' : {
        'url' : "{{ route('letter_types.list') }}",
        'cols' : [
                    { name: 'id' },
                    { name: 'LetterType', orderable :false, searchable:false },
                    { name: 'edms_doc_type.type_name', orderable :false, searchable:false },
                    { name: 'Fields', orderable :false, searchable:false },
                ]
    },
    'letter_associations' : {
        'url' : "{{ route('letter-association.list',['[ID]','[EXTRA]']) }}",
        'cols' : [
                { data: 'id' },
                { data: 'entity_name' },
                { data: 'subject' },
                { data: 'letter_type_name' },
                { data: 'letter_category' },
                { data: 'letter_date' },
                { data: 'date_received' },
                { data: 'entity_reference_number' },
                { data: 'ppda_reference_number' },
                { data: 'attached_doc' , orderable :false, searchable:false },
                { data: "menu_action", "searchable": false, "orderable": false}
        ]
    },
    'letter_associations_letters_incoming' : {
    'url' : "{{ route('letter-association.letters-incoming',['[ID]']) }}",
        'cols' : [
                { data: 'id' },
                { data: "CheckBox", "searchable": false, "orderable": false},
                { data: 'entity_name' },
                { data: 'subject' },
                { data: 'date_sent' },
                { data: 'date_received_ppda' },
                { data: 'date_received_registry' },
                { data: 'sender_reference_number' },
                { data: 'ppda_reference_number' },
                { data: 'attention_to_name' },
                { data: 'letter_type_name', orderable :false, searchable:false },
                { data: 'incoming_letter_file_name' },
                { data: "AssociationAction", "searchable": false, "orderable": false}
        ]
    },

    'letter_associations_letters_outgoing' : {
        'url' : "{{ route('letter-association.letters-outgoing',['[ID]']) }}",
            'cols' : [
                { data: 'id' },
                { data: "CheckBox", "searchable": false, "orderable": false},
                { data: 'entity_name' },
                { data: 'subject' },
                { data: 'date_sent' },
                { data: 'date_received' },
                { data: 'ppda_reference_number' },
                { data: 'recipient_reference_number' },
                { data: 'recipient_title' },
                { data: 'origin_department' },
                { data: 'outgoing_letter_file_name' },
                { data: "AssociationAction", "searchable": false, "orderable": false}
        ]
    },

    'old_incoming_letters' : {
        'url' : "{{ route('letter.history.list',['[ID]']) }}",
        'cols' : [
                { name: 'letter_category' },
                { name: 'entity_name' },
                { name: 'from_user_full_name' },
                { name: 'subject' },
                { name: 'date_sent' },
                { name: 'date_received_ppda' },
                { name: 'sender_reference_number' },
                { name: 'ppda_reference_number' },
                { name: 'attention_to_name' },
                { name: 'letterType.letter_type_name' , orderable :false, searchable:false },
                { name: "ReadOnlyAction", "searchable": false, "orderable": false}
            ]
    },

    'memos_reception' : {
        'url' : "{{ route('memo.reception.list') }}",
        'cols' : [
                { name: 'memo_type' },
                { name: 'from_user_full_name' },
                { name: 'subject' },
                { name: 'memo_date' },
                { name: 'date_received' },
                { name: 'attention_to_full_name' },
                { name: "Action", "searchable": false, "orderable": false}
        ]
    },

    'memos_registry' : {
        'url' : "{{ route('memo.registry.list') }}",
        'cols' : [
            { name: 'memo_type' },
            { name: 'from_user_full_name' },
            { name: 'subject' },
            { name: 'memo_date' },
            { name: 'date_received' },
            { name: 'attention_to_full_name' },
            { name: "RegistryAction", "searchable": false, "orderable": false}
        ]
    },

    'memos_ed' : {
        'url' : "{{ route('memo.ed.list') }}",
        'cols' : [
            { name: 'memo_type' },
            { name: 'from_user_full_name' },
            { name: 'subject' },
            { name: 'memo_date' },
            { name: 'date_received' },
            { name: 'attention_to_full_name' },
            { name: "EDAction", "searchable": false, "orderable": false}
        ]
    },
    'letter_movements' : {
    'url' : "{{ route('letter-movement.list') }}",
        'cols' : [
            { name: 'id' },
            { name: 'letter.entity_name', "searchable": false, "orderable": false},
            { name: 'letter.subject' , "searchable": false, "orderable": false},
            { name: 'letter.date_sent', "searchable": false, "orderable": false},
            { name: 'letter.date_received_ppda', "searchable": false, "orderable": false},
            { name: 'letter.date_received_registry' , "searchable": false, "orderable": false},
            { name: 'letter.sender_reference_number', "searchable": false, "orderable": false},
            { name: 'letter.ppda_reference_number', "searchable": false, "orderable": false},
            { name: 'letter.attention_to', "searchable": false, "orderable": false},
            { name: 'LetterTypeName' , "searchable": false, "orderable": false},
            { name: 'letter.incoming_letter_file_name', "searchable": false, "orderable": false},
            { name: "LetterMovtAction", "searchable": false, "orderable": false},
        ]
    },
'memo_movements' : {
    'url' : "{{ route('letter-movement.list.memo') }}",
    'cols' : [
    { name: 'id' },
    { name: 'memo.from_user_full_name', "searchable": false, "orderable": false},
    { name: 'memo.subject' , "searchable": false, "orderable": false},
    { name: 'memo.memo_date', "searchable": false, "orderable": false},
    { name: 'memo.date_received', "searchable": false, "orderable": false},
    { name: 'memo.attention_to_full_name', "searchable": false, "orderable": false},
    { name: 'memo.memo_type', "searchable": false, "orderable": false},
    { name: "MemoMovtAction", "searchable": false, "orderable": false},
    ]
},



}
