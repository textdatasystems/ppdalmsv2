@extends('layouts.app')

@section('content')
    <div class="page-header">
        <h1>
            Master Data
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Management Letter Sections
            </small>
        </h1>
    </div>

    <div class="tabbable master_data">
        @include('master_data.menu')

        <div class="tab-content">
        <div class="form-actions no-margin-bottom">
        <div class="float-alert center">
            <div class="float-alert-container">
                <div class="col-md-12">
                    <a href="{{ route('management_letter_section_create') }}" class="clarify btn btn-primary btn-sm" title="Add Management Letter Section">Add Section</a>
                    <hr>
                </div>
            </div>
        </div>
     
        <div class="clearfix"></div>
    </div>
            <table class="data-table mgt_letter_sections table table-striped table-bordered table-hover no-margin-bottom no-border-top" style="table-layout:fixed">
                <thead>
                    <tr>
                        <th></th>
                        <th width="20%">Section</th>
                        <th width="80%">Exceptions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
