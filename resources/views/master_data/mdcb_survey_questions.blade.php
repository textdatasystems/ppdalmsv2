<ol>
  @foreach($survey_criteria->children as $child)
    <li> {{$child->survey_question}}  -
      <a class="delete-row survey_criteria SurveyEvalCriteria id {{ $child->id }} danger" title="Delete {{ $child->survey_question }}" href="javascript:void(0)"> <i class="fa fa-pencil text-primary"></i> </a>

      <a class="delete-row survey_criteria SurveyEvalCriteria id {{ $child->id }} danger" title="Delete {{ $child->survey_question }}" href="javascript:void(0)"> <i class="fa fa-trash-o red"></i> </a>

    </li>
  @endforeach
</ol>
