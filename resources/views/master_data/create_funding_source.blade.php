<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                  <!-- open form using laravelcollective -->
                      {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                        {{Form::hidden('ext',null, ['value'=>'0'])}}
                        @csrf

                        {{Form::hidden('table','FundingSource')}}

                        @if(isset($funding_source))
                            {{Form::hidden('fld_id',$funding_source->id)}}
                        @endif

                        <div class="form-group row">
                          {{Form::label('source_name','Source of funding', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                          <div class="col-md-6">
                              {{Form::text('r_fld[source_name]', isset($funding_source)?$funding_source->source_name:"" , ["class"=>"form-control","required"])}}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($funding_source)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                    <!-- close form -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
