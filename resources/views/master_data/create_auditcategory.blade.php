<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                  <!-- open form using laravelcollective -->
                      {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                        {{Form::hidden('ext',null, ['value'=>'0'])}}
                        @csrf

                        {{Form::hidden('table','AuditCategory')}}

                        @if(isset($audit_category))
                            {{Form::hidden('fld_id',$audit_category->id)}}
                        @endif

                        <div class="form-group row">
                          {{Form::label('category_name','Category name', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                            <div class="col-md-6">
                              {{Form::text('r_fld[category_name]', isset($audit_category)?$audit_category->category_name:"" , ["class"=>"form-control $errors->has('category_name') ? ' is-invalid' : '' ","required"])}}

                                @if ($errors->has('category_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('category_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($audit_category)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                    <!-- close form -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
