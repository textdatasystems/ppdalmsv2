@extends('layouts.app')
@section('title') Master Data @endsection
@section('content')
    <div class="page-header">
        <h1>
            Master Data
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Letter Types
            </small>
        </h1>
    </div>

    <div class="tabbable master_data">
        @include('master_data.menu')

        <div class="tab-content">
        <div class="form-actions no-margin-bottom">
        <div class="float-alert center">
            <div class="float-alert-container">
                <div class="col-md-12">
                    <a href="{{ route('create_letter_type') }}" class="clarify btn btn-primary btn-sm" title="Create New Letter Type">Create Letter Type</a>
                    <hr>
                </div>
            </div>
        </div>
     
        <div class="clearfix"></div>
    </div>
            <table class="data-table letter_types table table-striped table-bordered table-hover no-margin-bottom no-border-top hide_first_column" style="table-layout:fixed">
                <thead>
                    <tr>
                        <th></th>
                        <th width="20%">Letter Type</th>
                        <th width="20%">EDMS Type</th>
                        <th width="60%">Fields</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
