
@if(isset($extraFieldValues) && count($extraFieldValues)>0)

    <input name="has_extra_fields" hidden value="true"/>

    @foreach($extraFieldValues as $field)

        <div class="form-group row" >
            <label  style="font-weight: bold" for="{{$field->letterTypeField->field_name}}" class="col-md-4 col-form-label text-md-right">{{ __(($field->letterTypeField->field_name).':') }}</label>
            <div class="col-md-6">
                @if($field->field_type == 'date')
                    <input required id="{{$field->letterTypeField->field_name}}" type="text" class="form-control calendar" name="r_fld_extra_fields[{{$field->letterTypeField->id}}]" value="{{ (isset($field->field_value)?full_year_format_date($field->field_value): date('j M Y') ) }}" placeholder=""  autofocus>
                @elseif($field->field_type == 'number')
                    <input required id="{{$field->letterTypeField->field_name}}" type="number" step="any" class="form-control " name="r_fld_extra_fields[{{$field->letterTypeField->id}}]" value="{{$field->field_value }}" placeholder="" autofocus >
                @else
                    <input required id="{{$field->letterTypeField->field_name}}" type="text" class="form-control " name="r_fld_extra_fields[{{$field->letterTypeField->id}}]" value="{{$field->field_value }}" placeholder="" autofocus >
                @endif

            </div>
        </div>

    @endforeach

@elseif(isset($fields) && count($fields) > 0)

    <input name="has_extra_fields" hidden value="true"/>

    @foreach($fields as $field)

        <div class="form-group row" >
            <label  style="font-weight: bold" for="{{$field->field_name}}" class="col-md-4 col-form-label text-md-right">{{ __(($field->field_name).':') }}</label>
            <div class="col-md-6">
                @if($field->field_type == 'Date')
                    <input required id="{{$field->field_name}}" type="text" class="form-control calendar" name="r_fld_extra_fields[{{$field->id}}]" value="{{ date('j M Y') }}" placeholder=""  autofocus>
                @elseif($field->field_type == 'Number')
                    <input required id="{{$field->field_name}}" type="number" step="any" class="form-control " name="r_fld_extra_fields[{{$field->id}}]" value="" placeholder="" autofocus >
                @else
                    <input required id="{{$field->field_name}}" type="text" class="form-control " name="r_fld_extra_fields[{{$field->id}}]" value="" placeholder="" autofocus >
                @endif

            </div>
        </div>

    @endforeach

@endif
