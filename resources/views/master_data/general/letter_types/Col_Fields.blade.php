<a href="" class="btn btn-info btn-minier clarify_secondary" title="Add new Field to {{ $record->letter_type_name }}"><i class="fa fa-plus-square-o"></i> Add New field to letter type</a>
<hr style="margin: 10px auto 12px;">
<table class="inner-list-with-action table">
    <tr>
        <th></th>
        <th>Field Name</th>
        <th>Field Type</th>
        <th>Field Order</th>
    </tr>
    @foreach($record->fields as $field)
        <tr>
            <td></td>
            <td>{{ $field->field_name }}</td>
            <td>{{ $field->field_type }}</td>
            <td>{{ $field->field_order }}</td>
        </tr>
    @endforeach
</table>
