<div class="">
    <div class="row justify-content-center">
        <div class="col-md-10" style="margin:auto; float:none;">
            <div class="card">
                <div class="card-body">
                  <!-- open form using laravelcollective -->
                      {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                        {{Form::hidden('ext',null, ['value'=>'0'])}}
                        @csrf

                        {{Form::hidden('table','LetterType')}}
                        
                        @if(!isset($lettertype))
                            {{Form::hidden('delete_rows',1)}}
                        @endif

                        @if(isset($lettertype))
                            {{Form::hidden('fld_id',$lettertype->id)}}
                        @endif

                        <div class="form-group row">
                          {{Form::label('letter_type_name','Letter Type', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                          <div class="col-md-8">
                              {{Form::text('r_fld[letter_type_name]', isset($lettertype)?$lettertype->letter_type_name:"" , ["class"=>"form-control"])}}
                            </div>
                        </div>
                        <div class="form-group row">
                          {{Form::label('edms_doc_type_id','EDMS Document Type', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                          <div class="col-md-8">
                              {{Form::select('r_fld[edms_doc_type_id]', $edmsdocs , null , ["class"=>"form-control"])}}
                            </div>
                        </div>

@if(!isset($lettertype))
                        <hr class="separator">

                        <h5 class="text-center">Letter Type Fields</h5>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <table class="table table-condensed table-striped mb30" id="type_fields_table">
                                  <thead>
                                  <tr>
                                      <th></th>
                                      <th>Field Name</th>
                                      <th>Field Type</th>
                                      <th>Field Order</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                      <tr class="hide">
                                          <td><input name="form-field-checkbox" class="ace" type="checkbox">
                                                      <span class="lbl"></span>
                                                <input type="hidden" name="type_fields_count[]" value="1">
                                                <input type="hidden" name="type_fields[letter_type_id][]" value="">
                                                <input type="hidden" name="type_fields[id][]" value="">
                                          </td>
                                          <td>
                                              {{Form::text('type_fields[field_name][]', "" , ["class"=>"form-control"])}}
                                          </td>
                                          <td>
                                              {{Form::select('type_fields[field_type][]', ["Text"=>"Text","Number"=>"Number","Date"=>"Date"] , null , ["class"=>"form-control"])}}
                                          </td>
                                          <td>
                                              {{Form::text('type_fields[field_order][]', "" , ["class"=>"form-control"])}}
                                          </td>
                                      </tr>
                                  </tbody>
                                </table>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-white btn-primary btn-sm" onClick="addRow('type_fields_table');">
                                        <i class="fa fa-plus"></i> Add Type Field
                                    </button>
                                    <button type="button" class="btn btn-white btn-danger btn-sm" onClick="deleteRow('type_fields_table');">
                                        <i class="fa fa-remove"></i> Remove selected
                                    </button>
                                </div>
                            </div>
                        </div>

@endif

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($lettertype)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                    <!-- close form -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
