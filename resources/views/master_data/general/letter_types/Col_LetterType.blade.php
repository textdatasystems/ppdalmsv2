{{$record->letter_type_name}}
<div class="actions">
    <i class="fa fa-list"></i>
    <div class="actions-list">

        <a class="clarify" title="View/Edit {{$record->letter_type_name}}" href="{{route('edit_letter_type',$record->id)}}"><i class="fa fa-edit"></i> <span>View/Edit Letter type name</span> </a>

        <a class="delete-row legal_delete LetterType id {{ $record->id }} danger hide" title="Delete" href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Letter type</span> </a>
    </div>
</div>
