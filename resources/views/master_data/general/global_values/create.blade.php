<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">

                  <!-- open form using laravelcollective -->
                  {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                    {{Form::hidden('ext',null, ['value'=>'0'])}}
                    @csrf

                        {{Form::hidden('table','GlobalValue')}}

                        @if(isset($globalValue))
                            {{Form::hidden('fld_id',$globalValue->id)}}
                        @endif

                        <div class="form-group row">
                            {{Form::label('variable_name','Variable Name', ['class'=>'col-md-5 col-form-label text-md-right'])}}
                            <div class="col-md-7">
                                @if(isset($globalValue))
                                <input name="r_fld[variable_name]" class="form-control" readonly required value="{{$globalValue->variable_name}}"/>
                                @else
                                {{Form::text('r_fld[variable_name]', isset($globalValue)?$globalValue->variable_name:"" , ["class"=>"form-control","required"])}}
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            {{Form::label('value','Value', ['class'=>'col-md-5 col-form-label text-md-right'])}}
                            <div class="col-md-7">
                                {{Form::text('r_fld[value]', isset($globalValue)?$globalValue->value:"" , ["class"=>"form-control","required"])}}
                            </div>
                        </div>


                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($globalValue)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                        <!-- close form -->
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
