<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">

                  <!-- open form using laravelcollective -->
                  {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                    {{Form::hidden('ext',null, ['value'=>'0'])}}
                    @csrf

                        {{Form::hidden('table','MailingList')}}

                        @if(isset($mailingList))
                            {{Form::hidden('fld_id',$mailingList->id)}}
                        @endif

                        <div class="form-group row">
                            {{Form::label('list_name','Mailing List Name', ['class'=>'col-md-5 col-form-label text-md-right'])}}
                            <div class="col-md-7">
                                @if(isset($mailingList))
                                <input name="r_fld[list_name]" class="form-control" readonly required value="{{$mailingList->list_name}}"/>
                                @else
                                {{Form::text('r_fld[list_name]', isset($mailingList)?$mailingList->list_name:"" , ["class"=>"form-control","required"])}}
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            {{Form::label('email_addresses','Email Addresses (Comma separated , no spaces)', ['class'=>'col-md-5 col-form-label text-md-right'])}}
                            <div class="col-md-7">
                                {{Form::textarea('r_fld[email_addresses]', isset($mailingList)?$mailingList->email_addresses:"" , ["class"=>"form-control","required"])}}
                            </div>
                        </div>

                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($mailingList)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                        <!-- close form -->
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
