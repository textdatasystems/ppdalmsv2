<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <!-- <div class="card-header">{{ __('bodycat') }}</div> -->

                <div class="card-body">
                  <!-- open form using laravelcollective -->
                  {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                    {{Form::hidden('ext',null, ['value'=>'0'])}}
                    @csrf

                    {{Form::hidden('table','BodyCategory')}}

                    @if(isset($bodycategory))
                        {{Form::hidden('fld_id',$bodycategory->id)}}
                    @endif

                        <div class="form-group row">
                            {{Form::label('category_name','Entity Category name', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                            <div class="col-md-6">
                              {{Form::text('r_fld[category_name]', isset($bodycategory)?$bodycategory->category_name:"" , ["class"=>"form-control","required"])}}
                            </div>
                        </div>

                        <div class="form-group row">
                            {{Form::label('region_id','Entity Type', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                            <div class="col-md-6">

                              {{Form::select('r_fld[entity_type_id]', $entity_types, isset($bodycategory) ? $bodycategory->entity_type_id : null, ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($bodycategory)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                        <!-- close form -->
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
