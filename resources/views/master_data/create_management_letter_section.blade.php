<div class="">
    <div class="row justify-content-center">
        <div class="col-md-10" style="margin:auto; float:none;">
            <div class="card">
                <div class="card-body">
                  <!-- open form using laravelcollective -->
                      {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                        {{Form::hidden('ext',null, ['value'=>'0'])}}
                        @csrf

                        {{Form::hidden('table','MgtLetterSection')}}
                        
@if(!isset($mgtlettersection))
                        {{Form::hidden('delete_rows',1)}}
@endif


                        @if(isset($mgtlettersection))
                            {{Form::hidden('fld_id',$mgtlettersection->id)}}
                        @endif

                        <div class="form-group row">
                          {{Form::label('section_title','Section Title', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                          <div class="col-md-8">
                              {{Form::text('r_fld[section_title]', isset($mgtlettersection)?$mgtlettersection->section_title:"" , ["class"=>"form-control"])}}
                            </div>
                        </div>

@if(!isset($mgtlettersection))
                        <hr class="separator">

                        <h5 class="text-center">Exceptions</h5>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <table class="table table-condensed table-striped mb30" id="exception_table">
                                  <thead>
                                  <tr>
                                      <th></th>
                                      <th class="col-md-12"></th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                      <tr class="hide">
                                          <td><input name="form-field-checkbox" class="ace" type="checkbox">
                                                      <span class="lbl"></span>
                                                <input type="hidden" name="exception_count[]" value="1">
                                                <input type="hidden" name="exception[mgt_letter_section_id][]" value="">
                                                <input type="hidden" name="exception[id][]" value="">
                                          </td>
                                          <td>
                                              <div class="clearfix">
                                                <div class="form-group row">
                                                    {{Form::label('exception_title','Exception title', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                                                    <div class="col-md-8">
                                                        {{Form::text('exception[exception_title][]', "" , ["class"=>"form-control"])}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    {{Form::label('exception_standard_implication','Exception Standard implication', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                                                    <div class="col-md-8">
                                                        {{Form::textarea('exception[exception_standard_implication][]', isset($distance)?$distance->distance:"" , ["class"=>"form-control","rows"=>"3"])}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    {{Form::label('exception_standard_recommendation','Exception Standard Recommendation', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                                                    <div class="col-md-8">
                                                        {{Form::textarea('exception[exception_standard_recommendation][]', isset($distance)?$distance->distance:"" , ["class"=>"form-control","rows"=>"3"])}}
                                                    </div>
                                                </div>
                                              </div>
                                          </td>
                                      </tr>
                                  </tbody>
                                </table>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-white btn-primary btn-sm" onClick="addRow('exception_table');">
                                        <i class="fa fa-plus"></i> Add exception
                                    </button>
                                    <button type="button" class="btn btn-white btn-danger btn-sm" onClick="deleteRow('exception_table');">
                                        <i class="fa fa-remove"></i> Remove selected
                                    </button>
                                </div>
                            </div>
                        </div>

@endif

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($mgtlettersection)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                    <!-- close form -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
