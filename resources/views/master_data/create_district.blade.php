<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                  <!-- open form using laravelcollective -->
                  {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                    {{Form::hidden('ext',null, ['value'=>'0'])}}
                    @csrf

                    {{Form::hidden('table','District')}}

                    @if(isset($district))
                        {{Form::hidden('fld_id',$district->id)}}
                    @endif

                        <div class="form-group row">
                            {{Form::label('district_name','District name', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                            <div class="col-md-6">
                                {{Form::text('r_fld[district_name]', isset($district)?$district->district_name:"" , ["class"=>"form-control","required"])}}
                            </div>
                        </div>

                        <div class="form-group row">
                            {{Form::label('region_id','Region', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                            <div class="col-md-6">

                              {{Form::select('r_fld[region_id]', $regions, isset($district) ? $district->region_id : null, ['class' => 'form-control',"required"]) }}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($district)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                        <!-- close form -->
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
