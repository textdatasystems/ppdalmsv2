<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                  <!-- open form using laravelcollective -->
                  {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal reload_page', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                    {{Form::hidden('ext',null, ['value'=>'0'])}}
                    @csrf

                    {{Form::hidden('table','AuditRouteStop')}}
                    {{Form::hidden('r_fld[audit_route_id]',$auditroute)}}
                    {{Form::hidden('r_fld[route_order]',$auditroute)}}

                    @if(isset($auditroutestop))
                        {{Form::hidden('fld_id',$auditroutestop->id)}}
                    @endif

                        <div class="form-group row">
                            {{Form::label('district_id','District', ['class'=>'col-md-2 col-form-label text-md-right'])}}
                            <div class="col-md-9">
                                {{Form::select('r_fld[district_id]', $districts, isset($cb_activity) ? $cb_activity->district_id : null, ['class' => 'form-control selectize', "required"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            {{Form::label('town','Town', ['class'=>'col-md-2 col-form-label text-md-right'])}}
                            <div class="col-md-9">
                                {{Form::text('r_fld[town]', isset($audit_category)?$audit_category->category_name:"" , ["class"=>"form-control $errors->has('category_name') ? ' is-invalid' : '' ","required"])}}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($auditroutestop)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                        <!-- close form -->
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
