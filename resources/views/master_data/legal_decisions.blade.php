@extends('layouts.app')

@section('content')
<div class="page-header">
    <h1>
        Master Data
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i> Legal
        </small>
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
        </small>
        {{ucwords(stripUnderScore($decision_type))}} decisions
    </h1>
</div>

<div class="tabbable master_data">
    @include('master_data.menu')

    <div class="tab-content">
    <div class="form-actions no-margin-bottom">
    <div class="float-alert center">
        <div class="float-alert-container">
            <div class="col-md-12">
                <a href="{{ route('legal_decision_create', $decision_type) }}" class="clarify btn btn-primary btn-sm" title="Create {{ucwords(stripUnderScore($decision_type))}} Decision">Create {{ucwords(stripUnderScore($decision_type))}} Decision</a>
                <hr>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>
        <table class="data-table legal_decisions {{$decision_type}} table table-striped table-bordered table-hover no-margin-bottom no-border-top hide_first_column">
            <thead>
                <tr>
                    <th width="70">ID</th>
                    <th>Decision</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
