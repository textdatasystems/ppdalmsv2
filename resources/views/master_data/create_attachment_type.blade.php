<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                  <!-- open form using laravelcollective -->
                    {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                        {{Form::hidden('ext',null, ['value'=>'0'])}}
                        @csrf

                        {{Form::hidden('table','AttachmentType')}}

                        @if(isset($attachment_type))
                            {{Form::hidden('fld_id',$attachment_type->id)}}
                        @endif

                        <div class="form-group row">
                            {{Form::label('emis_module','EMIS Module', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                            <div class="col-md-6">
                                <select name="r_fld[emis_module]" class="form-control attachment-module-select">
                                    <option value=""></option>
                                    @foreach($modules as $module=>$sections)
                                    <option value="{{$module}}" {{ ( ( @$attachment_type->emis_module == $module ) ? 'selected="selected"':'' ) }}>{{$module}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            {{Form::label('emis_module_section','EMIS Module Section', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                            <div class="col-md-6">
                                <select name="r_fld[emis_module_section]" class="form-control module-sections-select">
                                    <option value=""></option>
                                    @foreach($modules as $module=>$sections)
                                        @foreach($sections as $section)
                                            <option value="{{$section}}" {{ ( ( @$attachment_type->emis_module_section == $section ) ? 'selected="selected"':'' ) }} data-module="{{ $module }}">{{$section}}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                          {{Form::label('attachment_type','Attachment type', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                          <div class="col-md-6">
                              {{Form::text('r_fld[attachment_type]', isset($attachment_type)?$attachment_type->attachment_type:"" , ["class"=>"form-control","required"])}}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($attachment_type)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                    <!-- close form -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
