@extends('layouts.app')

@section('content')
    <div class="page-header">
        <h1>
            Master Data
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Letter Templates
            </small>
        </h1>
    </div>

    <div class="tabbable master_data">
        @include('master_data.menu')

        <div class="tab-content">
        <div class="form-actions no-margin-bottom">
        <div class="float-alert center">
            <div class="float-alert-container">
                <div class="col-md-12">
                    <a href="{{ route('letter_template_create') }}" class="clarify btn btn-primary btn-sm" title="Create Letter Template">Create Letter Template</a>
                    <hr>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
            <table class="data-table template_letters table table-striped table-bordered table-hover no-margin-bottom no-border-top">
                <thead>
                    <tr>
                        <th width="10"></th>
                        <th>Template type</th>
                        <th>Template Sections</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
