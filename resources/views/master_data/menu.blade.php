<div id="sidebar" class="sidebar responsive col-md-4" data-sidebar="true" data-sidebar-scroll="true" data-sidebar-hover="true">
    <ul class="nav nav-list">
        <li class="{{ (($section == 'letter_types' || $section == 'edms_document_types' || $section == 'mailing_lists'  || $section == 'global_values' )?'active open':'' ) }}">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-caret-right"></i>
                General
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="{{ (($section == 'letter_types')?'active':'') }}">
                    <a href="{{ route('general_section','letter_types') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Letter Types
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="{{ (($section == 'edms_document_types')?'active':'') }}">
                    <a href="{{ route('general_section','edms_document_types') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        EDMS Document Types
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{ (($section == 'mailing_lists')?'active':'') }}">
                    <a href="{{ route('general_section','mailing_lists') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Mailing Lists
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="{{ (($section == 'global_values')?'active':'') }}">
                    <a href="{{ route('general_section','global_values') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Global Values
                    </a>

                    <b class="arrow"></b>
                </li>

            </ul>
        </li>

    </ul>
</div>
