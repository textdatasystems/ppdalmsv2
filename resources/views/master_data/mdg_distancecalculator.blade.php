@extends('layouts.app')

@section('content')
    <div class="page-header">
        <h1>
            Master Data
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Distance Calculator
            </small>
        </h1>
    </div>

    <div class="tabbable master_data">
        @include('master_data.menu')

        <div class="tab-content">
        
            <table class="distancecalculator table table-striped table-bordered table-hover no-margin-bottom no-border-top">
                <tr>
                    <th></th>
                    @foreach($offices as $office)
                        <th>{{ $office->office_name }}</th>
                    @endforeach
                </tr>
                @foreach($districts as $district)
                    <tr>
                        <th>{{ $district->district_name }}</th>
                        @foreach($offices as $office)
                            <td>
                                {{ @$distances[$district->id][$office->id] }}
                                <div class="action-links">
                                    <a href="{{ @route('create_distance',[$district->id , $office->id]) }}" class="clarify" title="Edit distance between {{$office->office_name}} and {{ $district->district_name }}"><i class="fa fa-edit"></i></a> | 
                                    <a href="" class="delete-row Distance red" title="Delete"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </td>
                        @endforeach
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
