<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                  <!-- open form with laravelcollective -->
                      {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                        {{Form::hidden('ext',null, ['value'=>'0'])}}
                        @csrf

                        {{Form::hidden('table','PmRole')}}

                        @if(isset($pm_role))
                            {{Form::hidden('fld_id',$pm_role->id)}}
                        @endif

                        <div class="form-group row">
                          {{Form::label('role_name','Role', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                          <div class="col-md-6">
                              {{Form::text('r_fld[role_name]', isset($pm_role)? $pm_role->role_name: "" , ["class"=>"form-control","required"])}}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($pm_role)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                    <!-- close form -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
