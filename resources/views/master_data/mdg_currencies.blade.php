@extends('layouts.app')

@section('content')
    <div class="page-header">
        <h1>
            Master Data
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Currencies
            </small>
        </h1>
    </div>

    <div class="tabbable master_data">
        @include('master_data.menu')

        <div class="tab-content">
        <div class="form-actions no-margin-bottom">
        <div class="float-alert center">
            <div class="float-alert-container">
                <div class="col-md-12">
                    <a href="{{ route('currency_create') }}" class="clarify btn btn-primary btn-sm" title="Add Currency">Add Currency</a>
                    <hr>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
            <table class="data-table currencies table table-striped table-bordered table-hover no-margin-bottom no-border-top">
                <thead>
                    <tr>

                        <th>ID</th>
                        <th>Currency</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
