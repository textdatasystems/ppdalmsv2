<div class="">
    <div class="row justify-content-center">
        <div class="col-md-10" style="margin:auto; float:none;">
            <div class="card">
                <div class="card-body">
                  <!-- open form using laravelcollective -->
                      {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                        {{Form::hidden('ext',null, ['value'=>'0'])}}
                        @csrf

                        {{Form::hidden('table','Exception')}}

                        {{Form::hidden('r_fld[mgt_letter_section_id]',$mgtlettersection)}}

                        @if(isset($exception))
                            {{Form::hidden('fld_id',$exception->id)}}
                        @endif

                        <div class="clearfix">
                            <div class="form-group row {{ ($section == 'all')?'':'hide'}}">
                                {{Form::label('exception_title','Exception title', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                                <div class="col-md-8">
                                    {{Form::text('r_fld[exception_title]', isset($exception)?$exception->exception_title:"" , ["class"=>"form-control"])}}
                                </div>
                            </div>
                            <div class="form-group row {{ ($section == 'implication')?'':'hide'}}">
                                {{Form::label('exception_standard_implication','Exception Standard implication', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                                <div class="col-md-8">
                                    {{Form::textarea('r_fld[exception_standard_implication]', isset($exception)?$exception->exception_standard_implication:"" , ["class"=>"form-control","rows"=>"3"])}}
                                </div>
                            </div>
                            <div class="form-group row {{ ($section == 'recommendation')?'':'hide'}}">
                                {{Form::label('exception_standard_recommendation','Exception Standard Recommendation', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                                <div class="col-md-8">
                                    {{Form::textarea('r_fld[exception_standard_recommendation]', isset($exception)?$exception->exception_standard_recommendation:"" , ["class"=>"form-control","rows"=>"3"])}}
                                </div>
                            </div>
                        </div>
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($exception)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                    <!-- close form -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
