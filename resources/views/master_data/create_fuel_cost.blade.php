<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                  <!-- open form using laravelcollective -->
                  {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                    {{Form::hidden('ext',null, ['value'=>'0'])}}
                    @csrf

                    {{Form::hidden('table','FuelCost')}}

                    @if(isset($fuel_cost))
                        {{Form::hidden('fld_id',$fuel_cost->id)}}
                    @endif

                        <div class="form-group row">
                            {{Form::label('cost','Fuel cost', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                            <div class="col-md-6">
                                {{Form::number('r_fld[cost]', isset($fuel_cost)?$fuel_cost->cost:"" , ["class"=>"form-control","required"])}}
                            </div>
                        </div>

                        <div class="form-group row">
                            {{Form::label('financial_year_id','Financial year', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                            <div class="col-md-6">

                              {{Form::select('r_fld[financial_year_id]', $financial_years, isset($fuel_cost) ? $fuel_cost->financial_year_id : null, ['class' => 'form-control',"required"]) }}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($fuel_cost)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                        <!-- close form -->
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
