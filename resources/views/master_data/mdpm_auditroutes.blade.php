@extends('layouts.app')

@section('content')
<style>
    .step-containers .step{
        /* width: 242px; */
        box-shadow: 1px 1px 15px #ccc inset;
        border-radius: 9px;
        padding: 0;
        margin-right: 30px;
    }
    .step-containers .step:last-child {
        margin-right: 0;
    }
    .step-containers .step .btn-app:first-child:not(:last-child):not(.dropdown-toggle){
        margin-right: 10px;
    }
    .step-containers .step .attributes{
        display: block;
        float: left;
        height: 75px;
        width: 95px;
        font-size: 70%;
        padding-top: 5px;
    }
    .step-containers .step .btn-app.dropdown-toggle{
        width: 40px;
        height: 70px;
        float:right;
        display: none;
    }
    .step-containers .step .actions{
        width: 40px;
        height: 70px;
        float: right;
        top: 40px;
        padding-top: 20px;
        text-align: center;
    }

    .step-containers .step .actions-list{
        text-align: left;
    }

    .step-containers .step:before{
        display: block;
        content: "";
       /*  width: 35px;
        height: 1px; */
        width:0;
        height:0;

        font-size: 0;
        overflow: hidden;
        /* border-top: 4px solid #CED1D6;
            border-top-color: rgb(206, 209, 214); */
        border-top: 20px solid transparent;
        border-bottom: 20px solid transparent;
        border-left: 20px solid #ccc;

        position: absolute;
        top: 17px;
        z-index: 1;
        left: 105%;
    }
    .step-containers .step:after{
        display: block;
        content: "";
        width: 35px;
        height: 1px;
        font-size: 0;
        overflow: hidden;
        border-top: 4px solid #CED1D6;
            border-top-color: #ccc;
        position: absolute;
        top: 35px;
        z-index: 1;
        left: 100%;
    }
    .step-containers .step:last-child::before, .step-containers .step:last-child::after{
        display:none;
    }
    
</style>
    <div class="page-header">
        <h1>
            Master Data
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Audit Routes
            </small>
        </h1>
    </div>

    <div class="tabbable master_data">
        @include('master_data.menu')

        <div class="tab-content">
            <div class="form-actions no-margin-bottom">
                <div class="float-alert center">
                    <div class="float-alert-container">
                        <div class="col-md-12">
                            <a href="{{ route('pm_route_create') }}" class="clarify btn btn-primary btn-sm" title="Add New Route">Add Route</a>
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="routes-table">
                <table class="table table-stripped step-containers">
                    @foreach($routes as $route)
                        <?php $index = 0; ?>
                        <tr>
                            <td>{{ $route->id }}</td>
                            <td>
                                <div class="btn-group step well">
                                    <button class="btn btn-app btn-success btn-xs">
                                        <span class="bigger-175">{{ ++$index }}</span><br>
                                        <span class="smaller-70">Start Point</span>
                                    </button>
                                    <span class="attributes">
                                        {{ $route->start_node->office_name }}
                                    </span>

                                    <div class="actions">
                                        <i class="fa fa-list"></i>
                                        <div class="actions-list">
                                            <a class="clarify" title="Edit" href=""><i class="fa fa-edit"></i> <span>Edit Point</span></a>
                                            <a class="clarify" title="Add Route stop point" href="{{ route('create_audit_route_stop',$route->id) }}"><i class="fa fa-plus"></i> <span>Add Stop after</span></a>
                                            <a class="delete-row activity_category_delete ActivityCategory id danger" title="Delete " href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Point</span></a>
                                        </div>
                                    </div>

                                </div><!-- /.btn-group -->

                                @foreach($route->stops as $stop)
                                    <div class="btn-group step well">
                                        <button class="btn btn-app btn-inverse btn-xs">
                                            <span class="bigger-175">{{ ++$index }}</span><br>
                                            <span class="smaller-70">Stop Point</span>
                                        </button>
                                        <span class="attributes">
                                            {{ $stop->town .', '. $stop->district->district_name}}
                                        </span>
                                        <div class="actions">
                                            <i class="fa fa-list"></i>
                                            <div class="actions-list">
                                                <a class="clarify" title="Edit Point" href="{{ route('create_audit_route_stop',$route->id) }}"><i class="fa fa-edit"></i> <span>Edit Point</span></a>
                                                <a class="clarify" title="Add Stop point before" href="{{ route('create_audit_route_stop',$route->id) }}"><i class="fa fa-plus"></i> <span>Add Stop before</span></a>
                                                <a class="clarify" title="Add Stop point after" href="{{ route('create_audit_route_stop',$route->id) }}"><i class="fa fa-plus"></i> <span>Add Stop after</span></a>
                                                <a class="clarify" title="Audit Route fuel calculations for {{ $stop->town .', '. $stop->district->district_name}}" href="{{ route('audit_route_fuel_calculations',$stop->id) }}"><i class="fa fa-plus"></i> <span>Fuel Calculations</span></a>
                                                <a class="delete-row activity_category_delete ActivityCategory id danger" title="Delete " href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Point</span></a>
                                            </div>
                                        </div>
                                    </div><!-- /.btn-group -->
                                @endforeach



                                <div class="btn-group step well">
                                    <button class="btn btn-app btn-danger btn-xs">
                                        <span class="bigger-175">{{ ++$index }}</span><br>
                                        <span class="smaller-70">End Point</span>
                                    </button>
                                    <span class="attributes">
                                        {{ $route->end_node->office_name }}
                                    </span>
                                    <div class="actions">
                                        <i class="fa fa-list"></i>
                                        <div class="actions-list">
                                            <a class="clarify" title="Edit" href=""><i class="fa fa-edit"></i> <span>Edit Point</span></a>
                                            <a class="clarify" title="Add Route stop point" href="{{ route('create_audit_route_stop',$route->id) }}"><i class="fa fa-plus"></i> <span>Add Point before</span></a>
                                            <a class="delete-row activity_category_delete ActivityCategory id danger" title="Delete " href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Point</span></a>
                                        </div>
                                    </div>
                                </div><!-- /.btn-group -->
                            </td>
                        </tr>
                    @endforeach
                    
                </table>
            </div>
    
        </div>
    </div>
@endsection
