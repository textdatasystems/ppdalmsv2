<div class="">
    <div class="row justify-content-center">
        <div class="col-md-10" style="margin:auto; float:none;">
            <div class="card">
                <div class="card-body">
                  <!-- open form using laravelcollective -->
                      {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                        {{Form::hidden('ext',null, ['value'=>'0'])}}
                        @csrf

                        {{Form::hidden('table','LetterSectionContent')}}
                        {{Form::hidden('r_fld[letter_template_section_id]', $section->id)}}

                        @if(isset($content))
                            {{Form::hidden('fld_id',$content->id)}}
                        @endif

                        <div class="clearfix">
                            <div class="form-group row">
                                {{Form::label('body','Body text', ['class'=>'col-md-6 col-form-label text-md-left'])}}
                                <div class="col-md-12">
                                    {{Form::textarea('r_fld[body_text]', isset($content)?$content->body_text:"" , ["class"=>"form-control wysiwyg","rows"=>"3"])}}
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($letter_template)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                    <!-- close form -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
