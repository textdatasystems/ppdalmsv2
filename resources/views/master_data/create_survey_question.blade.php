<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                  <!-- open form using laravelcollective -->
                    {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                        {{Form::hidden('ext',null, ['value'=>'0'])}}
                        @csrf

                        {{Form::hidden('table','SurveyEvalCriteria')}}

                        {{Form::hidden('r_fld[parent_id]', $parent->id)}}

                        @if(isset($criteria))
                            {{Form::hidden('fld_id',$criteria->id)}}
                        @endif

                        <div class="form-group row">
                          {{Form::label('survey_question','Category', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                          <div class="col-md-6">
                              {{Form::text('r_fld[survey_question]', isset($criteria)?$criteria->survey_question:"" , ["class"=>"form-control","required"])}}
                          </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($criteria)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                    <!-- close form -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
