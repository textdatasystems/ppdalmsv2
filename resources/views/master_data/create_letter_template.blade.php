<div class="">
    <div class="row justify-content-center">
        <div class="col-md-10" style="margin:auto; float:none;">
            <div class="card">
                <div class="card-body">
                  <!-- open form using laravelcollective -->
                      {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                        {{Form::hidden('ext',null, ['value'=>'0'])}}
                        @csrf

                        {{Form::hidden('table','LetterTemplate')}}

                        @if(isset($letter_template))
                            {{Form::hidden('fld_id',$letter_template->id)}}
                        @endif

                        <div class="clearfix">
                            <div class="form-group row">
                                {{Form::label('template_type','Template Type', ['class'=>'col-md-6 col-form-label text-md-left'])}}
                                <div class="col-md-12">
                                    {{Form::select('r_fld[template_type]', get_letter_templates_options(), isset($letter_template) ? $letter_template->template_type : null, ["class"=>"form-control","required"])}}
                                </div>
                            </div>
                            <div class="form-group row">
                                {{Form::label('body','Body text', ['class'=>'col-md-6 col-form-label text-md-left'])}}
                                <div class="col-md-12">
                                    {{Form::textarea('r_fld[body_text]', isset($letter_template)?$letter_template->body_text:"" , ["class"=>"form-control wysiwyg","rows"=>"3"])}}
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($letter_template)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                    <!-- close form -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
