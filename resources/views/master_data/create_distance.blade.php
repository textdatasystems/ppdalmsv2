<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                  <!-- open form with laravelcollective -->
                      {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal reload_page', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                        {{Form::hidden('ext',null, ['value'=>'0'])}}
                        @csrf
                        {{Form::hidden('table','Distance')}}

                        {{Form::hidden('r_fld[district_id]',$district)}}
                        {{Form::hidden('r_fld[ppda_office_id]',$office)}}


                        @if(isset($distance))
                            {{Form::hidden('fld_id',$distance->id)}}
                        @endif

                        <div class="form-group row">
                            {{Form::label('distance','Distance (KM)', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                            <div class="col-md-6">
                                {{Form::text('r_fld[distance]', isset($distance)?$distance->distance:"" , ["class"=>"form-control","required"])}}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($distance)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                    <!-- close form -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
