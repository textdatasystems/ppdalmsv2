<div class="">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                  <!-- open form using laravelcollective -->
                      {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                        {{Form::hidden('ext',null, ['value'=>'0'])}}
                        @csrf

                        {{Form::hidden('table','LetterTemplateSection')}}
                        {{Form::hidden('r_fld[letter_template_id]', $letter_template->id)}}

                        @if(isset($letter_section))
                            {{Form::hidden('fld_id',$letter_section->id)}}
                        @endif

                        <div class="form-group row">
                            {{Form::label('section_name','Section name', ['class'=>'col-md-3 col-form-label text-md-right'])}}
                            <div class="col-md-7">
                              {{Form::text('r_fld[section_name]', isset($letter_section)?$letter_section->section_name: null , ["class"=>"form-control","required"])}}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($letter_section)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                        <!-- close form -->
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
