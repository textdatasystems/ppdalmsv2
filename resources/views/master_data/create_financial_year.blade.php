<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                  <!-- open form using laravelcollective -->
                      {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                        {{Form::hidden('ext',null, ['value'=>'0'])}}
                        @csrf

                        {{Form::hidden('table','FinancialYear')}}

                        @if(isset($financial_year))
                            {{Form::hidden('fld_id',$financial_year->id)}}
                        @endif

                        <div class="form-group row">
                            {{Form::label('financial_year','Financial year', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                            <div class="col-md-6">
                              {{Form::text('r_fld[financial_year]', isset($financial_year)?$financial_year->financial_year:"" , ["class"=>"form-control","required"])}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="from_date" class="col-md-4 col-form-label text-md-right">{{ __('From') }}</label>

                            <div class="col-md-6">
                              {{Form::text('r_fld[from_date]', isset($financial_year)? date("d M Y",strtotime($financial_year->from_date)):"" , ["class"=>"calendar form-control $errors->has('from_date') ? ' is-invalid' : '' "])}}
                                @if ($errors->has('from_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('from_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="to_date" class="col-md-4 col-form-label text-md-right">{{ __('To') }}</label>

                            <div class="col-md-6">
                              {{Form::text('r_fld[to_date]', isset($financial_year)? date_picker_format_date($financial_year->to_date):"" , ["class"=>"calendar form-control $errors->has('from_date') ? ' is-invalid' : '' "])}}

                                @if ($errors->has('to_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('to_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($financial_year)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                        <!-- close form -->
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
