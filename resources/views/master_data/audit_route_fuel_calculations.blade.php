<h5>Fuel Calculation from previous stop to {{$auditroutestop->town . ', ' .$auditroutestop->district->district_name}}</h5>
<div class="form-group row">
    <div class="col-md-12">
        <table class="table table-condensed table-striped mb30 entity_fuel_budgets" id="budget_items_table_fuel">
            <thead>
            <tr>
                <th width="1">*</th>
                <th width="300">Item</th>
                <th width="100">Distance(Km)</th>
                <th width="100">Litres</th>
                <th width="100">Cost per litre</th>
                <th class="text-right">Total</th>
            </tr>
            </thead>
            <tbody>

              <tr class="">
                  <td><input name="form-field-checkbox" class="ace" type="checkbox">
                      <span class="lbl"></span>
                      <input type="hidden" name="fuel_budget_count[]" value="1">
                      <input type="hidden" name="fuel_budget[entity_id][]" value="{{@$entity->id}}">
                      <input type="hidden" name="fuel_budget[id][]" value="">
                  </td>
                  <td>
                      <div class="clearfix">
                          <select class="fuel_budget_item form-control" name="fuel_budget[budget_item_id][]">
                              <option value=""></option>
                              @foreach($budget_items_row as $item_row)
                              <option value="{{$item_row->id}}" rate="{{$item_row->rate}}">{{$item_row->item_name}}</option>
                              @endforeach
                          </select>
                      </div>
                  </td>
                  <td> {{Form::text('fuel_budget[distance][]', null , ["class"=>"form-control distance"])}} </td>
                  <td> {{Form::text('fuel_budget[litres][]', null , ["class"=>"form-control litres"])}} </td>
                  <td> {{Form::text('fuel_budget[cost_per_litre][]', @$fuel_cost->cost , ["class"=>"form-control cost_per_litre", "readonly"])}} </td>
                  <td><span class="fuel_sub_total pull-right">0</span></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<hr>

<h5>Fuel Calculation for town running in {{$auditroutestop->town . ', ' .$auditroutestop->district->district_name}}</h5>
<div class="form-group row">
    <div class="col-md-12">
        <table class="table table-condensed table-striped mb30 entity_fuel_budgets" id="budget_items_table_fuel">
            <thead>
            <tr>
                <th width="1">*</th>
                <th width="300">Item</th>
                <th width="100">Distance(Km)</th>
                <th width="100">Litres</th>
                <th width="100">Cost per litre</th>
                <th class="text-right">Total</th>
            </tr>
            </thead>
            <tbody>

              <tr class="">
                  <td><input name="form-field-checkbox" class="ace" type="checkbox">
                      <span class="lbl"></span>
                      <input type="hidden" name="fuel_budget_count[]" value="1">
                      <input type="hidden" name="fuel_budget[entity_id][]" value="{{@$entity->id}}">
                      <input type="hidden" name="fuel_budget[id][]" value="">
                  </td>
                  <td>
                      <div class="clearfix">
                          <select class="fuel_budget_item form-control" name="fuel_budget[budget_item_id][]">
                              <option value=""></option>
                              @foreach($budget_items_row as $item_row)
                              <option value="{{$item_row->id}}" rate="{{$item_row->rate}}">{{$item_row->item_name}}</option>
                              @endforeach
                          </select>
                      </div>
                  </td>
                  <td> {{Form::text('fuel_budget[distance][]', null , ["class"=>"form-control distance"])}} </td>
                  <td> {{Form::text('fuel_budget[litres][]', null , ["class"=>"form-control litres"])}} </td>
                  <td> {{Form::text('fuel_budget[cost_per_litre][]', @$fuel_cost->cost , ["class"=>"form-control cost_per_litre", "readonly"])}} </td>
                  <td><span class="fuel_sub_total pull-right">0</span></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>