<div class="">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                  <!-- open form using laravelcollective -->
                  {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal reload_page', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                    {{Form::hidden('ext',null, ['value'=>'0'])}}
                    @csrf

                    {{Form::hidden('table','AuditRoute')}}

                    @if(isset($auditroute))
                        {{Form::hidden('fld_id',$auditroute->id)}}
                    @endif

                        <div class="form-group row">
                            {{Form::label('start_node','Start Point', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                            <div class="col-md-6">
                                {{Form::select('r_fld[start_node_id]', $offices, isset($auditroute) ? $auditroute->start_node : null, ['class' => 'form-control',"required"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            {{Form::label('end_node','End Point', ['class'=>'col-md-4 col-form-label text-md-right'])}}
                            <div class="col-md-6">
                              {{Form::select('r_fld[end_node_id]', $offices, isset($auditroute) ? $auditroute->end_node : null, ['class' => 'form-control',"required"]) }}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{Form::submit(isset($auditroute)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit'])}}
                            </div>
                        </div>
                        <!-- close form -->
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
