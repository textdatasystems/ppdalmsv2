<div class="">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body smaller_accordions">

                    <form action="{{route('store')}}" class="form-horizontal" method="post" id="form_{{time()}}">

                        <input type="hidden" name="ext" value="1">
                        @csrf
                        <input type="hidden" name="table" value="LetterMovement">
{{--                        <input type="hidden" name="r_fld[letter_id]" value="{{ $letter->id }}">--}}
                        <input type="hidden" name="model" value="{{$model}}">
                        <input type="hidden" name="model_id" value="{{$modelId}}">

                        @if(isset($letterMovement))
                            <input type="hidden" name="fld_id" value="{{ $letterMovement->id }}">
                        @endif

                        <div id="accordion_ad_review"
                             class="accordion-style1 panel-group accordion-style2 management_letter_accordion">
                            <!-- panel 1 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle "
                                           data-parent="#accordion_ad_review" href="#section31_ad_review"
                                           aria-expanded="false">
                                            &nbsp;Movement Details
                                        </a>
                                    </h4>
                                </div>

                                <div class="panel-collapse " id="section31_ad_review" aria-expanded="false">
                                    <div class="panel-body">

                                        <div class="form-group row">
                                            <label for="from" style="font-weight: bold"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('From:') }}</label>
                                            <div class="col-md-6">
                                                <input class="form-control" value="@if(isset($loggedInUser)) {{ $loggedInUser->fullName }}@endif" disabled />
                                                <input id="from" name="r_fld[from]"  type="hidden" value="{{$loggedInUser->username}}"/>
                                                <input id="from_name" name="r_fld[from_name]" type="hidden" value="@if(isset($loggedInUser)){{$loggedInUser->fullName}}@endif"/>
                                                <input id="from_department" name="r_fld[from_department]" type="hidden" value="@if(isset($loggedInUser)){{$loggedInUser->departmentName}}@endif"/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="to" style="font-weight: bold"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Send To:') }}</label>
                                            <div class="col-md-6">
                                                <select id="to" class="form-control select-fill-hidden-input selectize timo-select" data-hidden-input-dept="to_department"
                                                        data-hidden-input-id="to_name" name="r_fld[to]" required>
                                                    <option value="">Select User</option>
                                                    @if(isset($users))
                                                        @foreach($users as $user)
                                                            @if($user->email != $loggedInUser->username)
                                                            <option data-dept="{{isset($user->department)?$user->department->name:''}}" value="{{$user->email}}" {{((@$letterMovement->from == $user->email) ? 'selected="selected"':'')}} >{{ $user->first_name .' '.$user->last_name }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <input id="to_name" name="r_fld[to_name]" type="hidden"/>
                                                <input id="to_department" name="r_fld[to_department]" type="hidden"/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="deadline_for_action" style="font-weight: bold"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Deadline for Action:') }}</label>
                                            <div class="col-md-6">
                                                <input id="deadline_for_action" type="text"
                                                       class="form-control calendar" name="r_fld[deadline_for_action]"
                                                       value="{{ (isset($letterMovement)?full_year_format_date($letterMovement->deadline_for_action): date('j M Y') ) }}"
                                                       placeholder="" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="required_action" style="font-weight: bold"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Required Action:') }}</label>
                                            <div class="col-md-6">
                                                <textarea rows="4" id="required_action" type="text" class="form-control"
                                                          name="r_fld[required_action]">{{ (isset($letterMovement)?$letterMovement->required_action:'')}}</textarea>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-3 center">
                                <input type="submit" value="Save" class="btn btn-primary btnSubmit">
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
