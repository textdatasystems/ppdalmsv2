<div class="">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body smaller_accordions">


                    <form action="{{route('store')}}" class="form-horizontal" method="post" id="form_{{time()}}">

                        <input type="hidden" name="ext" value="1">
                        @csrf
                        <input type="hidden" name="table" value="LetterMovement">
                        <input type="hidden" name="r_fld[letter_id]" value="{{ $letter->id }}">

                        <div id="accordion_ad_review"
                             class="accordion-style1 panel-group accordion-style2 management_letter_accordion">
                            <!-- panel 1 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        @if(isset($letter->incoming_letter_file_name))
                                        <div class="row" style="vertical-align: middle">
                                            <div class="col-md-9"><a class="accordion-toggle" data-parent="#accordion_ad_review" href="#section31_ad_review" aria-expanded="false">Letter Movements</a></div>
                                            <div class="col-md-3"><a href="#!" class="btn btn-danger align-right pdf-link" data-remove-temp-url="{{route('delete-temp-incoming-letter-doc','')}}" data-download-url = "{{route('download-link-incoming',$letter->id)}}" data-pdf-url = "" data-id="{{ $letter->id }}">View Attached Document</a></div>
                                        </div>
                                        @else
                                            <div class="col-md-12"><a class="accordion-toggle" data-parent="#accordion_ad_review" href="#section31_ad_review" aria-expanded="false">Letter Movements</a></div>
                                        @endif
                                    </h4>
                                </div>

                                <div class="panel-collapse " id="section31_ad_review" aria-expanded="false">
                                    <div class="panel-body">

                                        <table class="table table-striped " style="width: 98%; margin: 0 auto">
                                            <tr>
                                                <th>From</th>
                                                <th>From Department</th>
                                                <th>Sent To</th>
                                                <th>To Department</th>
                                                <th>Date Sent</th>
                                                <th>Deadline for Action</th>
                                                <th>Required Action</th>
                                            </tr>
                                            @if(isset($letterMovements) && count($letterMovements) >0)
                                                @foreach($letterMovements as $movt)
                                                    <tr style="height: 70px;">
                                                        <td style="padding-top: 5px">
                                                            {{$movt->from_name}}
                                                        </td>
                                                        <td style="padding-top: 5px">
                                                            {{$movt->from_department}}
                                                        </td>
                                                        <td style="padding-top: 5px">
                                                            {{$movt->to_name}}
                                                        </td>
                                                        <td style="padding-top: 5px">
                                                            {{$movt->to_department}}
                                                        </td>
                                                        <td style="padding-top: 5px">
                                                            {{date("F d Y", strtotime($movt->date_sent))}}
                                                        </td>
                                                        <td style="padding-top: 5px">
                                                            {{ date("F d Y", strtotime($movt->deadline_for_action))}}
                                                        </td>
                                                        <td style="padding-top: 5px; width: 35%">
                                                            {{$movt->required_action}}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </table>


                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-3 center">
                                @if(isset($isCurrentAssignee) && $isCurrentAssignee && $menu == 'letter-movement')
                                    <a href="{{ route('letter-movement.forward',['incoming_letter',$letter->id]) }}" class="clarify btn btn-primary btn-sm" title="Forward Incoming Letter">Forward Letter</a>
                                    @if(isset($showReturnToSenderBtn) && $showReturnToSenderBtn)
                                    <a href="{{ route('letter-movement.return',[$letter->id,'sender','incoming_letter']) }}" class="clarify_secondary btn btn-danger btn-sm" title="Return Incoming Letter to Sender">Return to Sender</a>
                                    @endif
                                    @if(isset($showReturnToEdBtn) && $showReturnToEdBtn)
                                    <a href="{{ route('letter-movement.return',[$letter->id,'ed','incoming_letter']) }}" class="clarify_secondary btn btn-danger btn-sm" title="Return Incoming Letter to ED's Office">Return to ED's Office</a>
                                    @endif
                                @endif
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
