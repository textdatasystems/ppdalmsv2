@extends('layouts.app')
@section('title') Letter Movements @endsection
@section('content')
    <div class="page-header">
        <h1>
            Letter Movements
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                List
            </small>
        </h1>
    </div>
    <div class="form-actions no-margin-bottom">
        <div class="float-alert center">
            <div class="float-alert-container">
                <div class="col-md-12">
                    <a href="{{ route('letter_movement.memo') }}" class="clarify btn btn-primary btn-sm" title="View Internal Memo Movements">View Internal Memo Movements</a>
                    <hr>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
    <table class="data-table letter_movements table table-striped table-bordered table-hover no-margin-bottom no-border-top hide_first_column" >
        <thead>
        <tr>
            <th></th>
            <th style="vertical-align: top;width: 15%">Entity Name</th>
            <th style="vertical-align: top;width: 20%">Subject</th>
            <th style="vertical-align: top">Date Sent</th>
            <th style="vertical-align: top">Date Received PPDA</th>
            <th style="vertical-align: top">Date Received Registry</th>
            <th style="vertical-align: top">Sender Reference No.</th>
            <th style="vertical-align: top">PPDA Reference No.</th>
            <th style="vertical-align: top">Attention To</th>
            <th style="vertical-align: top">Letter Type</th>
            <th style="vertical-align: top">Attached Document</th>
            <th style="vertical-align: top"></th>
        </tr>
        </thead>
    </table>
@endsection
