<div class="">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body smaller_accordions">

                    <form action="{{route('store')}}" class="form-horizontal" method="post" id="form_{{time()}}">

                        <input type="hidden" name="ext" value="1">
                        @csrf
                        <input type="hidden" name="table" value="LetterMovementLaunch">
                        <input type="hidden" name="r_fld[letter_id]" value="{{ $letter->id }}">

                        @if(isset($letterMovement))
                            <input type="hidden" name="fld_id" value="{{ $letterMovement->id }}">
                        @endif

                        <div id="accordion_ad_review"
                             class="accordion-style1 panel-group accordion-style2 management_letter_accordion">
                            <!-- panel 1 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle "
                                           data-parent="#accordion_ad_review" href="#section31_ad_review"
                                           aria-expanded="false">
                                            &nbsp;Letter Movement Details
                                        </a>
                                    </h4>
                                </div>

                                <div class="panel-collapse " id="section31_ad_review" aria-expanded="false">
                                    <div class="panel-body" >

                                        <div class="form-group row">
                                            <label for="from" style="font-weight: bold"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('From:') }}</label>
                                            <div class="col-md-6">
                                                <select id="from" class="form-control selectize timo-select select-fill-hidden-input "
                                                        data-hidden-input-dept="from_department" data-hidden-input-id="from_name"
                                                        name="r_fld[from]" required>
                                                    <option value="">Select User</option>
                                                    @if(isset($users))
                                                        @foreach($users as $user)
                                                            <option data-dept="{{isset($user->department)?$user->department->name:''}}"  value="{{$user->email}}" @if(isset($loggedInUser) && $loggedInUser->username == $user->email) {{ 'selected="selected"'}} @endif >{{ $user->first_name .' '.$user->last_name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <input id="from_name" name="r_fld[from_name]" type="hidden" value="@if(isset($loggedInUser)){{$loggedInUser->fullName}}@endif"/>
                                                <input id="from_department" name="r_fld[from_department]" type="hidden" value="@if(isset($loggedInUser)){{$loggedInUser->departmentName}}@endif"/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="deadline_for_action" style="font-weight: bold"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Deadline for Action:') }}</label>
                                            <div class="col-md-6">
                                                <input id="deadline_for_action" type="text"
                                                       class="form-control calendar" name="r_fld[deadline_for_action]"
                                                       value="{{ (isset($letterMovement)?full_year_format_date($letterMovement->deadline_for_action): date('j M Y') ) }}"
                                                       placeholder="" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="required_action" style="font-weight: bold"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Required Action:') }}</label>
                                            <div class="col-md-6">
                                                <textarea rows="4" id="required_action" type="text" class="form-control"
                                                          name="r_fld[required_action]">{{ (isset($letterMovement)?$letterMovement->required_action:'')}}</textarea>
                                            </div>
                                        </div>


                                        <div class="form-group row" style="margin-top: 20px">

                                            <label for="required_action" style="font-weight: bold"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Letter Movement Tags:') }}</label>
                                            <div class="col-md-6">
                                                <table class="table" style="width:100%; margin:auto;">
                                                    <tr style="background-color: #a0e4f7" >
                                                        <th class="col-md-3 text-md-center" >Department</th>
                                                        <th class="col-md-3 text-md-center" style="text-align: center">Tag</th>
                                                        <th class="col-md-6 text-md-center"  style="text-align: center">Assign To</th>
                                                    </tr>

                                                    <tr>
                                                        <td>Legal</td>
                                                        <td>
                                                            <div class="radio">
                                                                <label>
                                                                    <input data-dropdown-class= "select_legal" data-cbx-container-id = "cbx-container-legal" name="r_fld[legal_tag]" type="radio" class="ace tag-rbtns" value="1" {{ (@$letter->legal_tag == 1) ? 'checked':'' }} >
                                                                    <span class="lbl"> Yes</span>
                                                                </label>
                                                                <label>
                                                                    <input data-dropdown-class= "select_legal" data-cbx-container-id = "cbx-container-legal" name="r_fld[legal_tag]" type="radio" class="ace tag-rbtns" value="0" {{ (@$letter->legal_tag == 0 || @$letter->legal_tag === null ) ? 'checked':'' }} >
                                                                    <span class="lbl"> No</span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group row" id="cbx-container-legal" style="display: none" >
                                                                <label for="c_f_name"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Assign To:') }}</label>
                                                                <div class="col-md-8">
                                                                    <select id="select_legal" name="r_fld[assign_to_legal]" class="form-control selectize select-fill-hidden-input"
                                                                            data-hidden-input-dept="assign_to_legal_department" data-hidden-input-id="assign_to_legal_name" >
                                                                        <option value="">Select User</option>
                                                                        @if(isset($users))
                                                                            @foreach($users as $user)
                                                                                @if(isset($user->department) and $user->department->department_code == 'DEPT_22')
                                                                                    <option data-dept="{{isset($user->department)?$user->department->name:''}}" value="{{ $user->email }}" >{{ $user->first_name .' '.$user->last_name }}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                    <input id="assign_to_legal_name" name="r_fld[assign_to_legal_name]" type="hidden" />
                                                                    <input id="assign_to_legal_department" name="r_fld[assign_to_legal_department]" type="hidden" />
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Mac</td>
                                                        <td>
                                                            <div class="radio">
                                                                <label>
                                                                    <input data-dropdown-class= "select_mac" data-cbx-container-id = "cbx-container-mac" name="r_fld[mac_tag]" type="radio" class="ace tag-rbtns" value="1" {{ (@$letter->mac_tag == 1) ? 'checked':'' }} >
                                                                    <span class="lbl"> Yes</span>
                                                                </label>
                                                                <label>
                                                                    <input data-dropdown-class= "select_mac" data-cbx-container-id = "cbx-container-mac" name="r_fld[mac_tag]" type="radio" class="ace tag-rbtns" value="0" {{ (@$letter->mac_tag == 0 || @$letter->mac_tag === null ) ? 'checked':'' }} >
                                                                    <span class="lbl"> No</span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group row" id="cbx-container-mac" style="display: none">
                                                                <label for="c_f_name"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Assign To:') }}</label>
                                                                <div class="col-md-8">
                                                                    <select id="select_mac" name="r_fld[assign_to_mac]" class="form-control selectize select-fill-hidden-input"
                                                                            data-hidden-input-dept="assign_to_mac_department" data-hidden-input-id="assign_to_mac_name" >
                                                                        <option value="">Select User</option>
                                                                        @if(isset($users))
                                                                            @foreach($users as $user)
                                                                                @if(isset($user->department) and $user->department->department_code == 'DEPT_22')
                                                                                    <option data-dept="{{isset($user->department)?$user->department->name:''}}" value="{{ $user->email }}" >{{ $user->first_name .' '.$user->last_name }}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                    <input id="assign_to_mac_name" name="r_fld[assign_to_mac_name]" type="hidden" />
                                                                    <input name="r_fld[assign_to_mac_department]" value="MAC" type="hidden" />
{{--                                                                    <input id="assign_to_mac_department" name="r_fld[assign_to_mac_department]" type="hidden" />--}}
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Capacity Building</td>
                                                        <td>
                                                            <div class="radio">
                                                                <label>
                                                                    <input data-dropdown-class= "select-cbas" data-cbx-container-id = "cbx-container-cbas" name="r_fld[cbas_tag]" type="radio" class="ace tag-rbtns" value="1" {{ (@$letter->cbas_tag == 1) ? 'checked':'' }} >
                                                                    <span class="lbl"> Yes</span>
                                                                </label>
                                                                <label>
                                                                    <input data-dropdown-class= "select-cbas" data-cbx-container-id = "cbx-container-cbas" name="r_fld[cbas_tag]" type="radio" class="ace tag-rbtns" value="0" {{ (@$letter->cbas_tag == 0 || @$letter->cbas_tag === null ) ? 'checked':'' }} >
                                                                    <span class="lbl"> No</span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group row" id="cbx-container-cbas" style="display: none" >
                                                                <label for="c_f_name"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Assign To:') }}</label>
                                                                <div class="col-md-8">
                                                                    <select id="select-cbas" name="r_fld[assign_to_cbas]" class="form-control selectize select-fill-hidden-input"
                                                                            data-hidden-input-dept="assign_to_cbas_department" data-hidden-input-id="assign_to_cbas_name" >
                                                                        <option value="">Select User</option>
                                                                        @if(isset($users))
                                                                            @foreach($users as $user)
                                                                                @if(isset($user->department) and $user->department->department_code == 'DEPT_21')
                                                                                    <option data-dept="{{isset($user->department)?$user->department->name:''}}" value="{{ $user->email }}" >{{ $user->first_name .' '.$user->last_name }}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                    <input id="assign_to_cbas_name" name="r_fld[assign_to_cbas_name]" type="hidden" />
                                                                    <input id="assign_to_cbas_department" name="r_fld[assign_to_cbas_department]" type="hidden" />
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Advisory</td>
                                                        <td>
                                                            <div class="radio">
                                                                <label>
                                                                    <input data-dropdown-class= "select-advisory" data-cbx-container-id = "cbx-container-advisory" name="r_fld[advisory_tag]" type="radio" class="ace tag-rbtns" value="1" {{ (@$letter->advisory_tag == 1) ? 'checked':'' }} >
                                                                    <span class="lbl"> Yes</span>
                                                                </label>
                                                                <label>
                                                                    <input data-dropdown-class= "select-advisory" data-cbx-container-id = "cbx-container-advisory" name="r_fld[advisory_tag]" type="radio" class="ace tag-rbtns" value="0" {{ (@$letter->advisory_tag == 0 || @$letter->advisory_tag === null ) ? 'checked':'' }} >
                                                                    <span class="lbl"> No</span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>

                                                            <div class="form-group row" id="cbx-container-advisory" style="display: none" >
                                                                <label for="c_f_name"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Assign To:') }}</label>
                                                                <div class="col-md-8">
                                                                    <select id="select-advisory" name="r_fld[assign_to_advisory]" class="form-control selectize select-fill-hidden-input"
                                                                            data-hidden-input-dept="assign_to_advisory_department" data-hidden-input-id="assign_to_advisory_name">
                                                                        <option value="">Select User</option>
                                                                        @if(isset($users))
                                                                            @foreach($users as $user)
                                                                                @if(isset($user->department) and $user->department->department_code == 'DEPT_21')
                                                                                    <option data-dept="{{isset($user->department)?$user->department->name:''}}" value="{{ $user->email }}" >{{ $user->first_name .' '.$user->last_name }}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                    <input id="assign_to_advisory_name" name="r_fld[assign_to_advisory_name]" type="hidden" />
{{--                                                                    <input id="assign_to_advisory_department" name="r_fld[assign_to_advisory_department]" type="hidden" />--}}
                                                                    <input value="Advisory" name="r_fld[assign_to_advisory_department]" type="hidden" />
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Performance Monitoring</td>
                                                        <td>
                                                            <div class="radio">
                                                                <label>
                                                                    <input data-dropdown-class= "select-pm" data-cbx-container-id = "cbx-container-pm" name="r_fld[pm_tag]" type="radio" class="ace tag-rbtns" value="1" {{ (@$letter->pm_tag == 1) ? 'checked':'' }} >
                                                                    <span class="lbl"> Yes</span>
                                                                </label>
                                                                <label>
                                                                    <input data-dropdown-class= "select-pm" data-cbx-container-id = "cbx-container-pm" name="r_fld[pm_tag]" type="radio" class="ace tag-rbtns" value="0" {{ (@$letter->pm_tag == 0 || @$letter->pm_tag === null ) ? 'checked':'' }} >
                                                                    <span class="lbl"> No</span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group row" id="cbx-container-pm" style="display: none">
                                                                <label for="c_f_name"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Assign To:') }}</label>
                                                                <div class="col-md-8">
                                                                    <select id="select-pm" name="r_fld[assign_to_performance_monitoring]" class="form-control selectize select-fill-hidden-input"
                                                                            data-hidden-input-dept="assign_to_performance_monitoring_department" data-hidden-input-id="assign_to_performance_monitoring_name">
                                                                        <option value="">Select User</option>
                                                                        @if(isset($users))
                                                                            @foreach($users as $user)
                                                                                @if(isset($user->department) and $user->department->department_code == 'DEPT_24')
                                                                                    <option data-dept="{{isset($user->department)?$user->department->name:''}}" value="{{ $user->email }}" >{{ $user->first_name .' '.$user->last_name }}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                    <input id="assign_to_performance_monitoring_name" name="r_fld[assign_to_performance_monitoring_name]" type="hidden" />
                                                                    <input id="assign_to_performance_monitoring_department" name="r_fld[assign_to_performance_monitoring_department]" type="hidden" />
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Corporate</td>
                                                        <td>
                                                            <div class="radio">
                                                                <label>
                                                                    <input data-dropdown-class= "select-corporate" data-cbx-container-id = "cbx-container-corp" name="r_fld[corp_tag]" type="radio" class="ace tag-rbtns" value="1" {{ (@$letter->corp_tag == 1) ? 'checked':'' }} >
                                                                    <span class="lbl"> Yes</span>
                                                                </label>
                                                                <label>
                                                                    <input data-dropdown-class= "select-corporate" data-cbx-container-id = "cbx-container-corp" name="r_fld[corp_tag]" type="radio" class="ace tag-rbtns" value="0" {{ (@$letter->corp_tag == 0 || @$letter->corp_tag === null ) ? 'checked':'' }} >
                                                                    <span class="lbl"> No</span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group row" id="cbx-container-corp" style="display: none" >
                                                                <label for="c_f_name"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Assign To:') }}</label>
                                                                <div class="col-md-8">
                                                                    <select id="select-corporate" name="r_fld[assign_to_corporate]" class="form-control selectize select-fill-hidden-input"
                                                                            data-hidden-input-dept="assign_to_corporate_department" data-hidden-input-id="assign_to_corporate_name">
                                                                        <option value="">Select User</option>
                                                                        @if(isset($users))
                                                                            @foreach($users as $user)
                                                                                @if(isset($user->department) and $user->department->department_code == 'DEPT_6021')
                                                                                    <option data-dept="{{isset($user->department)?$user->department->name:''}}" value="{{ $user->email }}" >{{ $user->first_name .' '.$user->last_name }}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                    <input id="assign_to_corporate_name" name="r_fld[assign_to_corporate_name]" type="hidden" />
                                                                    <input id="assign_to_corporate_department" name="r_fld[assign_to_corporate_department]" type="hidden" />
                                                                </div>
                                                            </div>

                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <td>Operations</td>
                                                        <td>
                                                            <div class="radio">
                                                                <label>
                                                                    <input data-dropdown-class= "select_operations" data-cbx-container-id = "cbx-container-operations" name="r_fld[operations_tag]" type="radio" class="ace tag-rbtns" value="1" {{ (@$letter->corp_tag == 1) ? 'checked':'' }} >
                                                                    <span class="lbl"> Yes</span>
                                                                </label>
                                                                <label>
                                                                    <input data-dropdown-class= "select_operations" data-cbx-container-id = "cbx-container-operations" name="r_fld[operations_tag]" type="radio" class="ace tag-rbtns" value="0" {{ (@$letter->corp_tag == 0 || @$letter->corp_tag === null ) ? 'checked':'' }} >
                                                                    <span class="lbl"> No</span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group row" id="cbx-container-operations" style="display: none">
                                                                <label for="c_f_name"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Assign To:') }}</label>
                                                                <div class="col-md-8">
                                                                    <select id="select_operations" name="r_fld[assign_to_operations]" class="form-control selectize select-fill-hidden-input"
                                                                            data-hidden-input-dept="assign_to_operations_department" data-hidden-input-id="assign_to_operations_name">
                                                                        <option value="">Select User</option>
                                                                        @if(isset($users))
                                                                            @foreach($users as $user)
                                                                                @if(isset($user->department) and $user->department->department_code == 'DEPT_23')
                                                                                    <option data-dept="{{isset($user->department)?$user->department->name:''}}" value="{{ $user->email }}" >{{ $user->first_name .' '.$user->last_name }}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                    <input id="assign_to_operations_name" name="r_fld[assign_to_operations_name]" type="hidden" />
                                                                    <input id="assign_to_operations_department" name="r_fld[assign_to_operations_department]" type="hidden" />
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </div>


                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-3 center">
                                <input type="submit" value="Save" class="btn btn-primary btnSubmit">
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
