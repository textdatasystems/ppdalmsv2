
    <div class="page-header">
        <h1>
            Internal Memo Movements
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                List
            </small>
        </h1>
    </div>
    <div class="form-actions no-margin-bottom">
        <div class="clearfix"></div>
    </div>
    <table class="data-table memo_movements table table-striped table-bordered table-hover no-margin-bottom no-border-top hide_first_column" >
        <thead>
        <tr>
            <th></th>
            <th style="vertical-align: top;width: 15%">Sender</th>
            <th style="vertical-align: top;width: 20%">Subject</th>
            <th style="vertical-align: top">Memo Date</th>
            <th style="vertical-align: top">Date Received</th>
            <th style="vertical-align: top">Attention To</th>
            <th style="vertical-align: top">Memo Type</th>
            <th style="vertical-align: top"></th>
        </tr>
        </thead>
    </table>
