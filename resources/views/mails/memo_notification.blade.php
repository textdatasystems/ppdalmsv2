<div>

    <table style="width: 80%; border-collapse: collapse ">
        <thead>
        <tr style="background-color: #00008A; color: white"><th colspan="2" style="text-align: right; padding: 5px; width: 40%;">LETTER MOVEMENT APPLICATION</th></tr>
        <tr style="background-color: #4747FF; color: white"><th colspan="2" style="text-align: left; padding: 5px;width: 60%">{{$description}}</th></tr>
        </thead>
        <tbody>
        <tr style="background-color: #d7faf8; padding: 5px;"><td style="font-weight: bold;">From:</td><td>{{$internalMemo->from_user_full_name}}</td></tr>
        <tr style="background-color: white; padding: 5px;"><td style="font-weight: bold;">Subject of Memo:</td><td>{{$internalMemo->subject}}</td></tr>
        <tr style="background-color: #d7faf8; padding: 5px;"><td style="font-weight: bold;">Memo Date:</td><td>{{date("F d Y", strtotime($internalMemo->memo_date))}}</td></tr>
        <tr style="background-color: white; padding: 5px;"><td style="font-weight: bold;">Receipt Date:</td><td>{{date("F d Y", strtotime($internalMemo->date_received))}}</td></tr>
        <tr style="background-color: #d7faf8; padding: 5px;"><td style="font-weight: bold;">Received By:</td><td>{{$internalMemo->created_by_user_full_name}}</td></tr>
        </tbody>
    </table>

</div>

