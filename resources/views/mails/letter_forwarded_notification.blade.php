<div>

    <table style="width: 80%; border-collapse: collapse ">
        <thead>
        <tr style="background-color: #00008A; color: white"><th colspan="2" style="text-align: right; padding: 5px; width: 40%;">LETTER MOVEMENT APPLICATION</th></tr>
        <tr style="background-color: #4747FF; color: white"><th colspan="2" style="text-align: left; padding: 5px;width: 60%">Letter Details</th></tr>
        </thead>
        <tbody>
        <tr style="background-color: white; padding: 5px;"><td style="font-weight: bold;">Entity:</td><td>{{$letter->entity_name}}</td></tr>
        <tr style="background-color: #d7faf8; padding: 5px;"><td style="font-weight: bold;">Subject of Letter:</td><td>{{$letter->subject}}</td></tr>
        <tr style="background-color: white; padding: 5px;"><td style="font-weight: bold;">Letter Date:</td><td>{{date("F d Y", strtotime($letter->date_sent))}}</td></tr>
        <tr style="background-color: #d7faf8; padding: 5px;"><td style="font-weight: bold;">Receipt Date:</td><td>{{date("F d Y", strtotime($letter->date_received_ppda))}}</td></tr>
        <tr style="background-color: white; padding: 5px;"><td style="font-weight: bold;">Received By:</td><td>{{$letter->created_by_user_name}}</td></tr>

        <tr style="background-color: #4747FF; color: white"><th colspan="2" style="text-align: left; padding: 5px;width: 60%">Letter Movement Details @if(isset($department)) : Initially routed to {{$department}}  @endif</th></tr>

        <tr style="background-color: #d7faf8; padding: 5px;"><td style="font-weight: bold;">Forwarded By:</td><td>{{$senderName}}</td></tr>
        <tr style="background-color: white; padding: 5px;"><td style="font-weight: bold;">Date Forwarded:</td><td>{{$dateSent}}</td></tr>
        <tr style="background-color: #d7faf8; padding: 5px;"><td style="font-weight: bold;">Deadline:</td><td>{{$deadlineDate}}</td></tr>
        <tr style="background-color: white; padding: 5px;"><td style="font-weight: bold;">Action Required:</td><td>{{$actionRequired}}</td></tr>
        </tbody>
    </table>

</div>

