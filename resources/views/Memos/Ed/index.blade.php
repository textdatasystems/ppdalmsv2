
    <div class="page-header">
        <h1>
            Ed's Office
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Internal Memos
            </small>
        </h1>
    </div>

    <table class="data-table memos_ed table table-striped table-bordered table-hover no-margin-bottom no-border-top" >
        <thead>
            <tr>
                <th style="vertical-align: top">Memo Type</th>
                <th style="vertical-align: top">From User</th>
                <th style="vertical-align: top; width: 25%">Subject</th>
                <th style="vertical-align: top;width: 150px">Memo Date</th>
                <th style="vertical-align: top;width: 150px">Date Received</th>
                <th style="vertical-align: top">Attention To</th>
                <th style="vertical-align: top"></th>
            </tr>
        </thead>
    </table>

