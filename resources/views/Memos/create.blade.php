<div class="">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body smaller_accordions">

                    <form action="{{route('store')}}" class="form-horizontal" method="post" id="form_{{time()}}">

                        <input type="hidden" name="ext" value="1">
                        @csrf
                        <input type="hidden" name="table" value="InternalMemo">
                        <input type="hidden" name="r_fld[current_que]" value="{{ (isset($memo)?$memo->current_que:'reception') }}">

                        @if(isset($memo))
                            <input type="hidden" name="fld_id" value="{{ $memo->id }}">
                        @endif

                        <div id="accordion_ad_review"
                             class="accordion-style1 panel-group accordion-style2 management_letter_accordion">
                            <!-- panel 1 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle " data-parent="#accordion_ad_review" href="#!"
                                           aria-expanded="false">Internal Memo Details</a>
                                    </h4>
                                </div>
                                <div class="panel-collapse" id="section31_ad_review" aria-expanded="false">
                                    <div class="panel-body">

                                        <div class="form-group row">
                                            <label for="memo_type" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Memo Type:') }}</label>
                                            <div class="col-md-6">
                                                <select id="memo_type" name="r_fld[memo_type]" class="selectize form-control" required>
                                                    <option value="">Select Memo Type</option>
                                                    <option value="To ED" {{((@$memo->memo_type == 'To ED') ? 'selected="selected"':'')}} >To ED</option>
                                                    <option value="From ED" {{((@$memo->memo_type == 'From ED') ? 'selected="selected"':'')}} >From ED </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="from_user" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('From User:') }}</label>
                                            <div class="col-md-6">
                                                <select name="r_fld[from_user]" class="form-control selectize select-fill-hidden-input" data-hidden-input-id="from_user_full_name" required>
                                                    <option value="">Select User</option>
                                                    @if(isset($users))
                                                        @foreach($users as $user)
                                                            <option value="{{ $user->email }}" {{((@$memo->from_user == $user->email ) ? 'selected="selected"':'')}}>{{ $user->first_name .' '.$user->last_name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <input id="from_user_full_name" name="r_fld[from_user_full_name]" value="{{ (isset($memo)?$memo->from_user_full_name:'') }}" type="hidden"/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="subject" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Subject:') }}</label>
                                            <div class="col-md-6">
                                                <textarea name="r_fld[subject]" id="subject" class="form-control" rows="3" required>{{ (isset($memo)?$memo->subject:'') }}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="memo_date" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Memo Date:') }}</label>
                                            <div class="col-md-6">
                                                <input id="memo_date" type="text" class="form-control calendar" name="r_fld[memo_date]" value="{{ (isset($memo)?full_year_format_date($memo->memo_date): '' ) }}"
                                                       placeholder="" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="date_received" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Date Received:') }}</label>
                                            <div class="col-md-6">
                                                <input id="date_received" type="text" class="form-control calendar" name="r_fld[date_received]"
                                                       value="{{ (isset($memo)?full_year_format_date($memo->date_received): date('j M Y') ) }}"
                                                       placeholder="" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="attention_to" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Attention To:') }}</label>
                                            <div class="col-md-6">
                                                <select name="r_fld[attention_to]" class="form-control selectize select-fill-hidden-input" data-hidden-input-id="attention_to_full_name">
                                                    <option value="">Select User</option>
                                                    @if(isset($users))
                                                        @foreach($users as $user)
                                                            <option
                                                                value="{{ $user->email }}" {{((@$memo->attention_to == $user->email ) ? 'selected="selected"':'')}}>{{ $user->first_name .' '.$user->last_name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <input id="attention_to_full_name" name="r_fld[attention_to_full_name]" value="{{ (isset($memo)?$memo->attention_to_full_name:'') }}"
                                                       type="hidden"/>
                                            </div>
                                        </div>

                                        <div class="form-group row" >
                                            <label for="created_by_user" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Receiving Officer:') }}</label>
                                            <div class="col-md-6">
                                                <input id="created_by_user" type="text" class="form-control " name="r_fld[created_by_user]" value="{{ (isset($memo)?$memo->created_by_user_full_name: session('user')->fullName) }}" placeholder="" autofocus readonly >
                                                <input id="created_by_user_full_name" type="hidden" class="form-control " name="r_fld[created_by_user_full_name]" value="{{ (isset($memo)?$memo->created_by_user_full_name: session('user')->fullName) }}" placeholder=""  >
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-3 center">
                                <input type="submit" value="Save" class="btn btn-primary btnSubmit">
                            </div>
                        </div>


                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
