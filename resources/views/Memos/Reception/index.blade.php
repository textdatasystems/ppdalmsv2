
    <div class="page-header">
        <h1>
            Reception
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Internal Memos
            </small>
        </h1>
    </div>

    <div class="form-actions no-margin-bottom">
        <div class="float-alert center">
            <div class="float-alert-container">
                <div class="col-md-12">
                    <a href="{{ route('memo.create') }}" class="clarify_secondary btn btn-primary btn-sm" title="Add/Receive Internal Memo">Add/Receive Internal Memo</a>
                    <hr>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <table class="data-table memos_reception table table-striped table-bordered table-hover no-margin-bottom no-border-top" >
        <thead>
            <tr>
                <th style="vertical-align: top">Memo Type</th>
                <th style="vertical-align: top">From User</th>
                <th style="vertical-align: top; width: 25%">Subject</th>
                <th style="vertical-align: top;width: 150px">Memo Date</th>
                <th style="vertical-align: top;width: 150px">Date Received</th>
                <th style="vertical-align: top">Attention To</th>
                <th style="vertical-align: top"></th>
            </tr>
        </thead>
    </table>

