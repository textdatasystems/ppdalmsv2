
<table class="data-table old_incoming_letters {{$activeMenu}} table table-striped table-bordered table-hover no-margin-bottom no-border-top" >
    <thead>
    <tr>
        <th style="vertical-align: top;width: 11%">Letter Category</th>
        <th style="vertical-align: top;width: 15%">Entity Name</th>
        <th style="vertical-align: top;width: 10%">Memo From User</th>
        <th style="vertical-align: top; width: 15%">Subject</th>
        <th style="vertical-align: top;width: 150px">Letter Date</th>
        <th style="vertical-align: top;width: 150px">Date Received</th>
        <th style="vertical-align: top">Sender Reference No.</th>
        <th style="vertical-align: top">PPDA Reference No.</th>
        <th style="vertical-align: top">Attention To</th>
        <th style="vertical-align: top">Letter Type</th>
        <th style="vertical-align: top"></th>
    </tr>
    </thead>
</table>
