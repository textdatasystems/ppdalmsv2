@extends('layouts.app')
@section('title') Reception @endsection
@section('content')
    <div class="page-header">
        <h1>
            Reception
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Letter Management
            </small>
        </h1>
    </div>
    <div class="form-actions no-margin-bottom">
        <div class="float-alert center">
            <div class="float-alert-container">
                <div class="col-md-12">
                    <a href="{{ route('letter.create') }}" class="clarify btn btn-primary btn-sm" title="Add/Receive New Incoming Letter">Add or Receive Incoming Letter</a>
                    <a href="{{ route('letter.history.view',['reception']) }}" class="clarify btn btn-primary btn-sm" title="View Old Incoming Letters">View Old Incoming Letters</a>
                    <hr>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <table class="data-table reception_letters table table-striped table-bordered table-hover no-margin-bottom no-border-top" >
        <thead>
            <tr>
                <th style="vertical-align: top;width: 11%">Letter Category</th>
                <th style="vertical-align: top;width: 15%">Entity Name</th>
                <th style="vertical-align: top; width: 15%">Subject</th>
                <th style="vertical-align: top;width: 100px">Letter<br>Date</th>
                <th style="vertical-align: top;width: 100px">Receipt<br>Date</th>
                <th style="vertical-align: top">Sender Reference No.</th>
                <th style="vertical-align: top">PPDA Reference No.</th>
                <th style="vertical-align: top">Attention To</th>
                <th style="vertical-align: top">Letter Type</th>
                <th style="vertical-align: top; width: 4%"></th>
            </tr>
        </thead>
    </table>
@endsection
                                                                                                                                                                           
