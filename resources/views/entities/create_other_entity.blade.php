<div class="">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    <!-- open form using laravelcollective -->
                    {!! Form::open(['route'=>['store'], 'class'=>'form-horizontal', 'method'=>'post', 'id'=>'form_'.time() ]) !!}
                        {{Form::hidden('ext',null, ['value'=>'0'])}}
                        @csrf
                        
                        {{Form::hidden('table','Provider')}}

                        @if(isset($provider))
                            {{Form::hidden('fld_id',$provider->id)}}
                        @endif
                    
                        <div class="form-group row" style="display: none">
                            {{Form::label('Branch_name','Organization Type:', ['class'=>'col-md-3 col-form-label text-md-right'])}}
                            <div class="col-md-6">
                                <select name="r_fld[org_type]" class="form-control">
                                    @if(isset($org_types))
                                        @foreach($org_types as $type)
                                            <option value="{{ $type->organization_type_name }}">{{ $type->organization_type_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        @include('entities.capture_provider',['field'=>'r_fld'])

                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4 center">
                                {{Form::submit(isset($provider)?"Update":"Save", ['class'=>'btn btn-primary btnSubmit','data-origin'=>$origin])}}
                            </div>
                        </div>
                        <!-- close form -->
                    {!! Form::close() !!}

                    
                </div>
            </div>
        </div>
    </div>
</div>


