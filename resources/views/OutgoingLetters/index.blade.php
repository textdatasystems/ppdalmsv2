@extends('layouts.app')
@section('title') Outgoing Letters @endsection
@section('content')
    <div class="page-header">
        <h1>
            Outgoing Letters
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                List
            </small>
        </h1>
    </div>
    <div class="form-actions no-margin-bottom">
        <div class="float-alert center">
            <div class="float-alert-container">
                <div class="col-md-12">
                    <a href="{{ route('outgoingletter.create') }}" class="clarify btn btn-primary btn-sm" title="Add New Outgoing Letter">Add Outgoing Letter</a>
                    <hr>
                </div>
            </div>
        </div>


        <div class="clearfix"></div>
    </div>
    <table class="data-table outgoingletters table table-striped table-bordered table-hover no-margin-bottom no-border-top" >
        <thead>
            <tr>
                <th style="vertical-align: top">Entity <br>Name</th>
                <th style="vertical-align: top;width: 25%">Subject</th>
                <th style="vertical-align: top;width: 110px">Date<br>Sent</th>
                <th style="vertical-align: top;width: 110px">Receipt<br>Date</th>
                <th style="vertical-align: top">PPDA Reference No</th>
                <th style="vertical-align: top">Recipient Reference No</th>
                <th style="vertical-align: top">Recipient Title</th>
                <th style="vertical-align: top">Department of Origin</th>
                <th style="vertical-align: top">Attached <br>Doc</th>
                <th style="vertical-align: top"></th>
            </tr>
        </thead>
    </table>
@endsection
