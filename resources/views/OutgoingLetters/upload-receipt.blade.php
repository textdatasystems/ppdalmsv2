<div class="">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body smaller_accordions">


<form action="{{route('upload.outgoing-letter.receipt')}}" class="form-horizontal"  enctype="multipart/form-data" method="post" id="form_upload_image">

<input type="hidden" name="ext" value="1">
@csrf

@if(isset($letter))
    <input type="hidden" name="fld_id" value="{{ $letter->id }}">
@endif

    <div id="accordion_ad_review" class="accordion-style1 panel-group accordion-style2 management_letter_accordion">
        <!-- panel 1 -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-parent="#accordion_ad_review" href="#!" aria-expanded="false">
                        &nbsp;Upload Scanned Letter Receipt<span class="status pull-right hide">Status: Pending</span>
                    </a>
                </h4>
            </div>
            <div class="panel-collapse" id="section31_ad_review" aria-expanded="false">
                <div class="panel-body">

                    <div class="form-group row">

                        <div class="col-md-3">

                            <span id="imageFrame" class="user-image formImageInput"  style="">

                                <label class="ace-file-input">
                                    <span class="ace-file-container" data-title="Choose">
                                        <span id="fancy_file_name" class="ace-file-name" data-title="No File ...">
                                            <i class=" ace-icon fa fa-upload"></i>
                                        </span>
                                    </span>
                                    <a class="remove" href="#">
                                        <i class=" ace-icon fa fa-times"></i>
                                    </a>
                                </label>


                            </span>

                            <div class="input-control">
                                <input accept=".pdf" name = "outgoing_letter_file" data-form="form_upload_image" type="file" id="inputElement" class="uploadimg imageFrame inputElement form-control">
                            </div>

                        </div>

                        <div class="col-md-9">
                            <input class="form-control" readonly value="{{'RECEIPT_'.$letter->outgoing_letter_file_name}}" />
                        </div>

                        <input id="uploaded_by" type="hidden" class="form-control " name="uploaded_by" value="{{ session('user')->username }}">

                    </div>

                </div>
            </div>
        </div>

    </div>

    <div class="form-group row mb-0">
        <div class="col-md-12 offset-md-3 center">
            <input type="submit" value="Save" class="btn btn-primary btnUploadSubmit">
        </div>
    </div>


</form>

</div>
            </div>
        </div>
    </div>
</div>
