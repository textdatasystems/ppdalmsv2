<div class="">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body smaller_accordions">


<form action="{{route('upload.outgoing-letter.rename-attachment')}}" class="form-horizontal reload_page"  enctype="multipart/form-data" method="post" id="form_{{time()}}">

<input type="hidden" name="ext" value="1">
@csrf

@if(isset($letter))
    <input type="hidden" name="fld_id" value="{{ $letter->id }}">
@endif

    <div id="accordion_ad_review" class="accordion-style1 panel-group accordion-style2 management_letter_accordion">
        <!-- panel 1 -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-parent="#accordion_ad_review" href="#!" aria-expanded="false">Rename Document</a>
                </h4>
            </div>
            <div class="panel-collapse" id="section31_ad_review" aria-expanded="false">
                <div class="panel-body">

                    <div class="form-group row" id="container_new_upload_name" >
                        <label for="old_file_name"  style="font-weight: bold" class="col-md-3 col-form-label text-md-right">{{ __('Current File Name:') }}</label>
                        <div class="col-md-9">
                            <input id="old_file_name" name="old_file_name" type="text" class="form-control" value="{{$letter->outgoing_letter_file_name}}" placeholder="" readonly >
                        </div>
                    </div>

                    <div class="form-group row" id="container_new_upload_name" >
                        <label for="new_file_name"  style="font-weight: bold" class="col-md-3 col-form-label text-md-right">{{ __('New File Name:') }}</label>
                        <div class="col-md-9">
                            <input maxlength="100" id="new_file_name" type="text" class="form-control" name="new_file_name" value="" placeholder="" autofocus >
                        </div>
                    </div>

                </div>

            </div>
        </div>


    </div>


    <div class="form-group row mb-0">
        <div class="col-md-12 offset-md-3 center">
            <input type="submit" value="Save" class="btn btn-primary btnUploadSubmit">
        </div>
    </div>


</form>

</div>
            </div>
        </div>
    </div>
</div>
