<div class="">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body smaller_accordions">


<form action="{{route('store')}}" class="form-horizontal" method="post" id="form_{{time()}}">

<input type="hidden" name="ext" value="1">
@csrf
<input type="hidden" name="table" value="OutgoingLetter">
<input type="hidden" name="r_fld[current_que]" value="{{ (isset($letter)?$letter->current_que:'reception') }}">

@if(isset($letter))
    <input type="hidden" name="fld_id" value="{{ $letter->id }}">
@endif

    <div id="accordion_ad_review" class="accordion-style1 panel-group accordion-style2 management_letter_accordion">
        <!-- panel 1 -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_ad_review" href="#section31_ad_review" aria-expanded="false">
                        <i class="ace-icon fa fa-plus-circle bigger-110" data-icon-hide="ace-icon fa fa-minus-circle" data-icon-show="ace-icon fa fa-plus-circle"></i>
                        &nbsp;Letter Details
                        <span class="status pull-right hide">Status: Pending</span>
                    </a>
                </h4>
            </div>
            <div class="panel-collapse " id="section31_ad_review" aria-expanded="false">
                <div class="panel-body">

                    <div class="form-group row">
                        <label for="origin_department" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Department of Origin:') }}</label>
                        <div class="col-md-6">
                            <select id="select_departments" class="form-control " name="r_fld[origin_department]">
                                <option value="">Select Department of Origin</option>
                                @if(isset($departmentsOfOrigin))
                                    @foreach($departmentsOfOrigin as $department)
                                        <option value="{{ $department['name'] }}" {{((@$letter->origin_department == $department['name'] ) ? 'selected="selected"':'')}} >{{ $department['name']}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="origin_department_unit" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Department Unit:') }}</label>
                        <div class="col-md-6">
                            <select id="origin_department_unit" name="r_fld[origin_department_unit]" class="form-control selectize select-fill-hidden-input" data-hidden-input-id="origin_department_unit_name">
                                <option value="">Select Department Unit</option>
                                @if(isset($units))
                                    @foreach($units as $unit)
                                        <option data-dept="{{ $unit->deptName}}" value="{{ $unit->code }}" {{((@$letter->origin_department_unit == $unit->code ) ? 'selected="selected"':'')}}>{{ $unit->unit}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <input id="origin_department_unit_name" class="" name="r_fld[origin_department_unit_name]" value="{{ (isset($letter)?$letter->origin_department_unit_name:'') }}" type="hidden" />

                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="entity_name" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Organization Type:') }}</label>
                        <div class="col-md-6">
                            <select id="org_type_selector" class="form-control org_type_selector" name="r_fld[org_type]" required>
                                <option value="">Select Organization type</option>
                                <option value="Government Entity" {{ (@$letter->org_type == 'Government Entity') ? 'selected="selected"':'' }} >Government Entity</option>
                                <option value="Non - Government Entity" {{ (@$letter->org_type == 'Non - Government Entity') ? 'selected="selected"':'' }}>Non - Government Entity</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row" style="display:none">
                        <label for="c_f_name" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Government Entity:') }}</label>
                        <div class="col-md-6">
                            <select name="r_fld[entity_name]" class="government_entity_select selectize form-control">
                                @foreach($entities as $entity)
                                    <option value="{{ $entity->entity_name }}"
                                            data-edms-doc-path="{{$entity->edms_outgoing_letter_path}}"
                                            data-entity-id="{{$entity->id}}"
                                            {{((@$letter->entity_name == $entity->entity_name) ? 'selected="selected"':'')}} >
                                        {{ $entity->entity_name .' - '. $entity->acronym }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" id="edms_document_path_folder" name="r_fld[edms_document_path_folder]" value=""/>
                    </div>

                    {{--Begin new non govt code--}}

                    <div class="form-group row" style="display:none">
                        <label for="c_f_name"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Non Government Entity:') }}</label>
                        <div class="col-md-6 ">
                            <div class="input-group">
                                <div class="form-heading"><h5>Fill in the Entity Details below:</h5></div>
                                <select name="r_fld[entity_name]" id="provider_select" class="form-control selectize non_government_entity_select" >
                                    <option value=""></option>
                                    @if(isset($providers))
                                        @foreach($providers as $provider)
                                            <option value="{{$provider->orgname}}" {{((@$letter->entity_name == $provider->orgname) ? 'selected="selected"':'')}} >{{ $provider->orgname }}</option>
                                        @endforeach
                                    @endif
                                </select>

                                <span class="input-group-btn">
                                        <a href="{{ route('provider-create','from_form:::provider_select') }}" class="btn btn-primary btn-sm closed_caption clarify_secondary" title="Add New Organization">
                                            <span class="ace-icon fa fa-plus icon-on-right bigger-110"></span>
                                            Add New
                                        </a>
                                    </span>
                            </div>
                            <div class="tooltip-info ">Select Applicant from list above. If applicant is not in list click on Add New Button</div>
                        </div>
                    </div>

                    {{-- End new non govt code--}}



                    <div class="form-group row">
                        <label for="c_l_name" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Subject:') }}</label>
                        <div class="col-md-6">
                            <textarea name="r_fld[subject]" id="" class="form-control" rows="3" required>{{ (isset($letter)?$letter->subject:'') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="c_title"  style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Date Sent:') }}</label>
                        <div class="col-md-6">
                            <input id="c_title" type="text" class="form-control calendar" name="r_fld[date_sent]" value="{{ (isset($letter)?full_year_format_date($letter->date_sent): '' ) }}" placeholder=""  required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  style="font-weight: bold" for="date_received" class="col-md-4 col-form-label text-md-right">{{ __('Date Received/Posted:') }}</label>
                        <div class="col-md-6">
                            <input id="date_received" type="text" class="form-control calendar" name="r_fld[date_received]" value="{{ (isset($letter)?full_year_format_date($letter->date_received): '' ) }}" placeholder="" >
                        </div>
                    </div>

                    {{-- Added fields --}}

                    <div class="form-group row" >
                        <label for="ppda_reference_number" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('PPDA Reference Number:') }}</label>
                        <div class="col-md-6">
                            <input id="ppda_reference_number" type="text" class="form-control " name="r_fld[ppda_reference_number]" value="{{ (isset($letter)?$letter->ppda_reference_number:'') }}" placeholder="" autofocus required >
                        </div>
                    </div>
                    <div class="form-group row" >
                        <label for="recipient_reference_number" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Recipient Reference Number:') }}</label>
                        <div class="col-md-6">
                            <input id="recipient_reference_number" type="text" class="form-control " name="r_fld[recipient_reference_number]" value="{{ (isset($letter)?$letter->recipient_reference_number:'') }}" placeholder="" autofocus >
                        </div>
                    </div>
                    <div class="form-group row" >
                        <label for="recipient_title" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Recipient Title:') }}</label>
                        <div class="col-md-6">
                            <select class="form-control selectize" name="r_fld[recipient_title]">
                                <option value="">Select Recipient Title</option>
                                @if(isset($recipientTitles))
                                    @foreach($recipientTitles as $recipientTitle)
                                        <option value="{{ $recipientTitle['title'] }}" {{((@$letter->recipient_title == $recipientTitle['title'] ) ? 'selected="selected"':'')}} >{{ $recipientTitle['title']}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group row" >
                        <label for="signatory_title" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('PPDA Signatory Title:') }}</label>
                        <div class="col-md-6">
                            <select data-ed-username="{{(isset($ed)?$ed->username:'')}}" data-ed-name="{{(isset($ed)?$ed->fullName:'')}}" class="form-control selectize select_out_letters_signatory_title" name="r_fld[signatory_title]" required>
                                <option value="">Select Signatory Title</option>
                                @if(isset($signatoryTitles))
                                    @foreach($signatoryTitles as $signatoryTitle)
                                        <option value="{{ $signatoryTitle['title'] }}" {{((@$letter->signatory_title == $signatoryTitle['title'] ) ? 'selected="selected"':'')}} >{{ $signatoryTitle['title']}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group row" style="display: none" >
                        <label for="signatory" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('PPDA Signatory:') }}</label>
                        <div class="col-md-6">
                            <select name="r_fld[signatory_username]" class="form-control ctn_out_letter_signatory_others selectize select-fill-hidden-input" data-hidden-input-id="signatory_others">
                                <option value="">Select User</option>
                                @if(isset($users))
                                    @foreach($users as $user)
                                        <option value="{{ $user->email }}" {{((@$letter->signatory_username == $user->email ) ? 'selected="selected"':'')}}>{{ $user->first_name .' '.$user->last_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <input id="signatory_others" class="ctn_out_letter_signatory_name" name="r_fld[signatory]" value="{{ (isset($letter)?$letter->signatory:'') }}" type="hidden" />

                        </div>
                    </div>

                    <div class="form-group row" style="display: none" >
                        <label for="signatory" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('PPDA Signatory:') }}</label>
                        <div class="col-md-6">
                            <input id="signatory_ed" type="text" class="form-control ctn_out_letter_signatory_ed" name="r_fld[signatory]" value="{{ (isset($letter)?$letter->signatory:(isset($ed)?$ed->fullName:'')) }}" placeholder="" readonly >
                            <input id="signatory_username_ed" class="ctn_out_letter_signatory_username_ed" name="r_fld[signatory_username]" value="{{ (isset($letter)?$letter->signatory_username:(isset($ed)?$ed->username:'')) }}" type="hidden" />

                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="private_file" style="font-weight: bold" class="col-md-4 col-form-label text-md-right">{{ __('Private File:') }}</label>
                        <div class="col-md-6">
                            <div class="radio">
                                <label>
                                    <input name="r_fld[private_file]" type="radio" class="ace " value="1" {{ (@$letter->private_file == 1) ? 'checked':'' }} >
                                    <span class="lbl"> Yes</span>
                                </label>
                                <label>
                                    <input name="r_fld[private_file]" type="radio" class="ace " value="0" {{ (@$letter->private_file == 0 || @$letter->private_file === null ) ? 'checked':'' }} >
                                    <span class="lbl"> No</span>
                                </label>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <!-- panel 2 -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_ad_review" href="#section_tags" aria-expanded="false">
                        <i class="ace-icon fa fa-plus-circle bigger-110" data-icon-hide="ace-icon fa fa-minus-circle" data-icon-show="ace-icon fa fa-plus-circle"></i>
                        &nbsp;EMIS TAGS
                        <span class="status pull-right hide">Status: Pending</span>
                    </a>
                </h4>
            </div>
            <div class="panel-collapse collapse" id="section_tags" aria-expanded="false">
                <div class="panel-body">

                    <table class="table" style="width:50%; margin:auto;">
                        <tr>
                            <th class="col-md-6">Department</th>
                            <th class="col-md-6">Tag</th>
                        </tr>
                        <tr>
                            <td>Legal</td>
                            <td>
                                <div class="radio">
                                    <label>
                                        <input data-cbx-class= "cbx-tag-legal" data-cbx-container-id = "cbx-container-legal" name="r_fld[legal_tag]" type="radio" class="ace " value="1" {{ (@$letter->legal_tag == 1) ? 'checked':'' }} >
                                        <span class="lbl"> Yes</span>
                                    </label>
                                    <label>
                                        <input data-cbx-class= "cbx-tag-legal" data-cbx-container-id = "cbx-container-legal" name="r_fld[legal_tag]" type="radio" class="ace " value="0" {{ (@$letter->legal_tag == 0 || @$letter->legal_tag === null ) ? 'checked':'' }} >
                                        <span class="lbl"> No</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Capacity Building</td>
                            <td>
                                <div class="radio">
                                    <label>
                                        <input data-cbx-class= "cbx-tag-cbas" data-cbx-container-id = "cbx-container-cbas" name="r_fld[cbas_tag]" type="radio" class="ace " value="1" {{ (@$letter->cbas_tag == 1) ? 'checked':'' }} >
                                        <span class="lbl"> Yes</span>
                                    </label>
                                    <label>
                                        <input data-cbx-class= "cbx-tag-cbas" data-cbx-container-id = "cbx-container-cbas" name="r_fld[cbas_tag]" type="radio" class="ace " value="0" {{ (@$letter->cbas_tag == 0 || @$letter->cbas_tag === null ) ? 'checked':'' }} >
                                        <span class="lbl"> No</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Advisory</td>
                            <td>
                                <div class="radio">
                                    <label>
                                        <input data-cbx-class= "cbx-tag-advisory" data-cbx-container-id = "cbx-container-advisory" name="r_fld[advisory_tag]" type="radio" class="ace " value="1" {{ (@$letter->advisory_tag == 1) ? 'checked':'' }} >
                                        <span class="lbl"> Yes</span>
                                    </label>
                                    <label>
                                        <input data-cbx-class= "cbx-tag-advisory" data-cbx-container-id = "cbx-container-advisory" name="r_fld[advisory_tag]" type="radio" class="ace " value="0" {{ (@$letter->advisory_tag == 0 || @$letter->advisory_tag === null ) ? 'checked':'' }} >
                                        <span class="lbl"> No</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Performance Monitoring</td>
                            <td>
                                <div class="radio">
                                    <label>
                                        <input data-cbx-class= "cbx-tag-pm" data-cbx-container-id = "cbx-container-pm" name="r_fld[pm_tag]" type="radio" class="ace " value="1" {{ (@$letter->pm_tag == 1) ? 'checked':'' }} >
                                        <span class="lbl"> Yes</span>
                                    </label>
                                    <label>
                                        <input data-cbx-class= "cbx-tag-pm" data-cbx-container-id = "cbx-container-pm" name="r_fld[pm_tag]" type="radio" class="ace " value="0" {{ (@$letter->pm_tag == 0 || @$letter->pm_tag === null ) ? 'checked':'' }} >
                                        <span class="lbl"> No</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Corporate</td>
                            <td>
                                <div class="radio">
                                    <label>
                                        <input data-cbx-class= "cbx-tag-corp" data-cbx-container-id = "cbx-container-corp" name="r_fld[corp_tag]" type="radio" class="ace " value="1" {{ (@$letter->corp_tag == 1) ? 'checked':'' }} >
                                        <span class="lbl"> Yes</span>
                                    </label>
                                    <label>
                                        <input data-cbx-class= "cbx-tag-corp" data-cbx-container-id = "cbx-container-corp" name="r_fld[corp_tag]" type="radio" class="ace " value="0" {{ (@$letter->corp_tag == 0 || @$letter->corp_tag === null ) ? 'checked':'' }} >
                                        <span class="lbl"> No</span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> </td>
                        </tr>

                    </table>

                </div>
            </div>
        </div>

    </div>






        <div class="form-group row mb-0">
            <div class="col-md-12 offset-md-3 center">
                <input type="submit" value="Save" class="btn btn-primary btnSubmit">
            </div>
        </div>



</form>

</div>
            </div>
        </div>
    </div>
</div>
