@extends('layouts.app')
@section('title') ED's Office @endsection
@section('content')
    <div class="page-header">
        <h1>
            ED's Office
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Letter Management
            </small>
        </h1>
    </div>

    <table class="data-table ed_letters table table-striped table-bordered table-hover no-margin-bottom no-border-top" >
        <thead>
            <tr>
                <th style="vertical-align: top;width: 11%">Letter Category</th>
                <th style="vertical-align: top;width: 15%">Entity Name</th>
                <th style="vertical-align: top;width: 15%">Subject</th>
                <th style="vertical-align: top;width: 9%">Letter<br>Date</th>
                <th style="vertical-align: top;width: 9%">Receipt<br>Date</th>
                <th style="vertical-align: top">Sender Reference No.</th>
                <th style="vertical-align: top">PPDA Reference No.</th>
                <th style="vertical-align: top">Attention To</th>
                <th style="vertical-align: top">Letter Type</th>
                <th style="vertical-align: top">Attached Document</th>
                <th style="vertical-align: top"></th>
            </tr>
        </thead>
    </table>
@endsection
