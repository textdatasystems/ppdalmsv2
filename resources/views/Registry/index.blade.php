@extends('layouts.app')
@section('title') Registry @endsection
@section('content')
    <div class="page-header">
        <h1>
            Registry
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Letter Management
            </small>
        </h1>
    </div>

    <div class="form-actions no-margin-bottom">
        <div class="float-alert center">
            <div class="float-alert-container">
                <div class="col-md-12"> <a href="{{ route('letter.history.view',['registry']) }}" class="clarify btn btn-primary btn-sm" title="View Old Incoming Letters">View Old Incoming Letters</a>
                    <hr>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <table class="data-table registry_letters registry table table-striped table-bordered table-hover no-margin-bottom no-border-top" >
        <thead>
            <tr>
                <th style="vertical-align: top;width: 11%">Letter Category</th>
                <th style="vertical-align: top;width: 15%">Entity Name</th>
                <th style="vertical-align: top; width: 15%">Subject</th>
                <th style="vertical-align: top;width: 110px">Letter<br>Date</th>
                <th style="vertical-align: top;width: 110px">Receipt<br>Date</th>
                <th style="vertical-align: top">Sender Reference No.</th>
                <th style="vertical-align: top">PPDA Reference No.</th>
                <th style="vertical-align: top">Attention To</th>
                <th style="vertical-align: top">Letter Type</th>
                <th style="vertical-align: top">Attached Document</th>
                <th style="vertical-align: top"></th>
            </tr>
        </thead>
    </table>
@endsection
