<div class="actions">
    <i class="fa fa-list"></i>
    <div class="actions-list">
        @if($level == 'reception')

            <a class="clarify" title="View/Edit Letter - {{$letter->subject}}" href="{{ route('letter.edit',$letter->id) }}"><i class="fa fa-edit"></i> <span>View/Edit Incoming Letter Details</span></a>
            <a class="forward-action" data-id="{{ $letter->id }}" data-que="registry" title="Forward - {{$letter->subject}} to Registry" href="#!"><i class="fa fa-edit"></i> <span>Forward Letter to Registry</span></a>
{{--            <a class="forward-action" data-id="{{ $letter->id }}" data-que="ed" title="Forward - {{$letter->subject}} to ED's Office" href="{{ route('letter.edit',$letter->id) }}"><i class="fa fa-edit"></i> <span>Forward Letter to ED's Office</span></a>--}}

            @if(!isset($letter->incoming_letter_file_name) || $letter->incoming_letter_file_name == '' )
            <a class="clarify" data-id="{{ $letter->id }}" data-que="ed" title="Attach Scanned Incoming Letter" href="{{ route('letter.upload',$letter->id) }}"><i class="fa fa-edit"></i> <span>Attach Scanned Incoming Letter</span></a>
            @endif

            @if(isset($letter->incoming_letter_file_name) && $letter->incoming_letter_file_name != '' )
                <a class="pdf-link" data-remove-temp-url="{{route('delete-temp-incoming-letter-doc','')}}" data-download-url = "{{route('download-link-incoming',$letter->id)}}" data-pdf-url = "" data-id="{{ $letter->id }}" data-que="ed" title="View Attached Scanned Letter" href="#"><i class="fa fa-edit"></i> <span>View Attached Scanned Letter</span></a>
            @endif

            @if(isset($letter->incoming_letter_file_name) && $letter->incoming_letter_file_name != '' && (!isset($letter->letterMovements) || count($letter->letterMovements) == 0) )
                @if(isset($username) && $letter->incoming_letter_upload_user == $username)
                    <a class="btn-delete" data-id="{{ $letter->id }}" data-que="ed" title="Remove Attached Document" href="{{ route('letter.remove-attached-upload',$letter->id) }}"><i class="fa fa-edit"></i> <span>Remove Attached Document</span></a>
                @endif
             @endif

{{--            <a class="clarify" title="View Letter Movement - {{$letter->subject}}" href="{{ route('letter-movement.view-movement',[$letter->id, 'ed']) }}"><i class="fa fa-edit"></i> <span>View Letter Movement</span></a>--}}
{{--            <a class="clarify" data-id="{{ $letter->id }}" data-que="ed" title="Launch Letter Movement" href="{{ route('letter-movement.create',$letter->id) }}"><i class="fa fa-edit"></i> <span>Launch Letter Movement</span></a>--}}
{{--       Not shown for now <a class="clarify" data-id="{{ $letter->id }}" data-que="ed" title="View Associated Letters" href="{{ route('letter.associations',$letter->id) }}"><i class="fa fa-edit"></i> <span>View Associated Letters</span></a>--}}

            <a class="delete-row delete_letter IncomingLetter id {{ $letter->id }} danger" title="Delete Letter" href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Letter</span></a>

            @if(isset($enabledAdminDeletion) && $enabledAdminDeletion)
                <a data-url = "{{route('letters.deletion',['incoming',$letter->id])}}" data-model="Incoming Letter" class="delete-timo danger" title="Delete Letter (Admin)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Letter (Admin)</span></a>
            @endif

        @elseif($level == 'registry')

            <a class="clarify" title="View/Edit Letter - {{$letter->subject}}" href="{{ route('letter.edit',$letter->id) }}"><i class="fa fa-edit"></i> <span>View/Edit Incoming Letter Details</span></a>

{{--            <a class="forward-action" data-id="{{ $letter->id }}" data-que="registry" title="Forward - {{$letter->subject}} to Registry" href="{{ route('letter.edit',$letter->id) }}"><i class="fa fa-edit"></i> <span>Forward Letter to Registry</span></a>--}}

            @if(isset($letter->incoming_letter_file_name) && $letter->incoming_letter_file_name != '' )
                <a class="forward-action" data-id="{{ $letter->id }}" data-que="ed" title="Forward - {{$letter->subject}} to ED's Office" href="{{ route('letter.edit',$letter->id) }}"><i class="fa fa-edit"></i> <span>Forward Letter to ED's Office</span></a>
            @endif

{{--            <a class="delete-row delete_letter Letter id {{ $letter->id }} danger" title="Delete Letter" href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Letter</span></a>--}}

            @if(!isset($letter->incoming_letter_file_name) || $letter->incoming_letter_file_name == '' )
            <a class="clarify" data-id="{{ $letter->id }}" data-que="ed" title="Attach Scanned Incoming Letter" href="{{ route('letter.upload',$letter->id) }}"><i class="fa fa-edit"></i> <span>Attach Scanned Incoming Letter</span></a>
            @endif

            @if(isset($letter->incoming_letter_file_name) && $letter->incoming_letter_file_name != '' )
                <a class="pdf-link" data-enable-toolbar="yes" data-remove-temp-url="{{route('delete-temp-incoming-letter-doc','')}}" data-download-url = "{{route('download-link-incoming',$letter->id)}}" data-pdf-url = "" data-id="{{ $letter->id }}" data-que="ed" title="View Attached Scanned Letter" href="#"><i class="fa fa-edit"></i> <span>View Attached Scanned Letter</span></a>
            @endif

            @if(isset($letter->incoming_letter_file_name) && $letter->incoming_letter_file_name != '' && (!isset($letter->letterMovements) || count($letter->letterMovements) == 0) )
                @if(isset($username) && $letter->incoming_letter_upload_user == $username)
                    <a class="btn-delete" data-id="{{ $letter->id }}" data-que="ed" title="Remove Attached Document" href="{{ route('letter.remove-attached-upload',$letter->id) }}"><i class="fa fa-edit"></i> <span>Remove Attached Document</span></a>
                @endif
            @endif

            @if(isset($letter->incoming_letter_file_name) && $letter->incoming_letter_file_name != '' )
                <a class="clarify" data-id="{{ $letter->id }}" data-que="ed" title="Rename Attached Scanned Letter" href="{{ route('letter.rename-attached-doc.view',$letter->id) }}"><i class="fa fa-edit"></i> <span>Rename Attached Scanned Letter</span></a>
            @endif

{{--            <a class="clarify" title="View Letter Movement - {{$letter->subject}}" href="{{ route('letter-movement.view-movement',[$letter->id, 'ed']) }}"><i class="fa fa-edit"></i> <span>View Letter Movement</span></a>--}}
{{--            <a class="clarify" data-id="{{ $letter->id }}" data-que="ed" title="Launch Letter Movement" href="{{ route('letter-movement.create',$letter->id) }}"><i class="fa fa-edit"></i> <span>Launch Letter Movement</span></a>--}}

            <a class="clarify" data-id="{{ $letter->id }}" data-que="ed" title="View Associated Letters" href="{{ route('letter.associations',$letter->id) }}"><i class="fa fa-edit"></i> <span>View Associated Letters</span></a>

{{--            @if(session('enable_admin_deletion',false) == true)--}}
                <a data-url = "{{route('letters.deletion',['incoming',$letter->id])}}" data-model="Incoming Letter" class="delete-timo danger" title="Delete Letter (Admin)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Letter (Admin)</span></a>
{{--            @endif--}}


        @elseif($level == 'ed')

            <a class="clarify" title="View/Edit Letter - {{$letter->subject}}" href="{{ route('letter.edit',$letter->id) }}"><i class="fa fa-edit"></i> <span>View/Edit Incoming Letter Details</span></a>

{{--            <a class="forward-action" data-id="{{ $letter->id }}" data-que="registry" title="Forward - {{$letter->subject}} to Registry" href="{{ route('letter.edit',$letter->id) }}"><i class="fa fa-edit"></i> <span>Forward Letter to Registry</span></a>--}}
{{--            <a class="forward-action" data-id="{{ $letter->id }}" data-que="ed" title="Forward - {{$letter->subject}} to ED's Office" href="{{ route('letter.edit',$letter->id) }}"><i class="fa fa-edit"></i> <span>Forward Letter to ED's Office</span></a>--}}
{{--            <a class="delete-row delete_letter Letter id {{ $letter->id }} danger" title="Delete Letter" href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Letter</span></a>--}}

            @if(!isset($letter->incoming_letter_file_name) || $letter->incoming_letter_file_name == '' )
                <a class="clarify" data-id="{{ $letter->id }}" data-que="ed" title="Attach Scanned Incoming Letter" href="{{ route('letter.upload',$letter->id) }}"><i class="fa fa-edit"></i> <span>Attach Scanned Incoming Letter</span></a>
            @endif

            @if(isset($letter->incoming_letter_file_name) && $letter->incoming_letter_file_name != '' )
                <a class="pdf-link" data-enable-toolbar="yes" data-remove-temp-url="{{route('delete-temp-incoming-letter-doc','')}}" data-download-url = "{{route('download-link-incoming',$letter->id)}}" data-pdf-url = "" data-id="{{ $letter->id }}" data-que="ed" title="View Attached Scanned Letter" href="#"><i class="fa fa-edit"></i> <span>View Attached Scanned Letter</span></a>
            @endif

            @if(isset($letter->incoming_letter_file_name) && $letter->incoming_letter_file_name != '' && (!isset($letter->letterMovements) || count($letter->letterMovements) == 0) )
                @if(isset($username) && $letter->incoming_letter_upload_user == $username)
                    <a class="btn-delete" data-id="{{ $letter->id }}" data-que="ed" title="Remove Attached Document" href="{{ route('letter.remove-attached-upload',$letter->id) }}"><i class="fa fa-edit"></i> <span>Remove Attached Document</span></a>
                @endif
            @endif

            @if(!isset($letter->letterMovements) || count($letter->letterMovements) == 0 )
            <a class="clarify" data-id="{{ $letter->id }}" data-que="ed" title="Launch Letter Movement" href="{{ route('letter-movement.launch',['incoming_letter',$letter->id]) }}"><i class="fa fa-edit"></i> <span>Launch Letter Movement</span></a>
            @endif

            @if(isset($letter->letterMovements) && count($letter->letterMovements) > 0 )
                <a class="clarify" title="View Letter Movement - {{$letter->subject}}" href="{{ route('letter-movement.view-movement',[$letter->id, 'ed']) }}"><i class="fa fa-edit"></i> <span>View Letter Movement</span></a>
            @endif

            <a class="clarify" data-id="{{ $letter->id }}" data-que="ed" title="View Associated Letters" href="{{ route('letter.associations',$letter->id) }}"><i class="fa fa-edit"></i> <span>View Associated Letters</span></a>

            @if(session('enable_admin_deletion',false) == true)
                <a data-url = "{{route('letters.deletion',['incoming',$letter->id])}}" data-model="Incoming Letter" class="delete-timo danger" title="Delete Letter (Admin)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Letter (Admin)</span></a>
            @endif

        @endif

    </div>
</div>
