<div class="actions">
    <i class="fa fa-list"></i>
    <div class="actions-list">
        <a class="clarify" title="View/Edit Mailing List - {{$mailingList->list_name}}" href="{{ route('mailing_list.edit',$mailingList->id) }}"><i class="fa fa-edit"></i> <span>View/Edit Mailing List</span></a>
{{--        <a class="btn-delete" data-id="{{ $mailingList->id }}" title="Delete Mailing List" href="{{ route('mailing_list.remove_mailing_lists',$mailingList->id) }}"><i class="fa fa-edit"></i> <span>Delete Mailing List</span></a>--}}
    </div>
</div>
