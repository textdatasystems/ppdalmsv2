<div class="actions">
    <i class="fa fa-list"></i>
    <div class="actions-list">

        @if($level == 'reception')

            <a class="clarify_secondary" title="View/Edit Memo - {{$memo->subject}}" href="{{ route('memo.edit',$memo->id) }}"><i class="fa fa-edit"></i> <span>View/Edit Memo Details</span></a>
            <a class="forward-action-memo" data-id="{{ $memo->id }}" data-que="registry" title="Forward - {{$memo->subject}} to Registry" href="#!"><i class="fa fa-edit"></i> <span>Forward Memo to Registry</span></a>

            @if(!isset($memo->memo_file_name) || $memo->memo_file_name == '' )
            <a class="clarify_secondary" data-id="{{ $memo->id }}" data-que="ed" title="Attach Scanned Memo" href="{{ route('memo.upload',$memo->id) }}"><i class="fa fa-edit"></i> <span>Attach Scanned Memo</span></a>
            @endif

            @if(isset($memo->memo_file_name) && $memo->memo_file_name != '' )
                <a class="pdf-link" data-remove-temp-url="{{route('delete-temp-incoming-letter-doc','')}}" data-download-url = "{{route('download-link-memo',$memo->id)}}" data-pdf-url = "" data-id="{{ $memo->id }}" data-que="ed" title="View Attached Scanned Memo" href="#"><i class="fa fa-edit"></i> <span>View Attached Scanned Memo</span></a>
            @endif

            @if(isset($memo->memo_file_name) && $memo->memo_file_name != '' && (!isset($memo->letterMovements) || count($memo->letterMovements) == 0) )
                @if(isset($username) && $memo->memo_upload_user == $username)
                    <a class="btn-delete" data-id="{{ $memo->id }}" data-que="ed" title="Remove Attached Document" href="{{ route('memo.remove-attached-upload',$memo->id) }}"><i class="fa fa-edit"></i> <span>Remove Attached Document</span></a>
                @endif
             @endif

{{--            <a class="clarify_secondary" title="View Memo Movement - {{$memo->subject}}" href="{{ route('letter-movement.view-movement',[$memo->id, 'ed']) }}"><i class="fa fa-edit"></i> <span>View Memo Movement</span></a>--}}
{{--            <a class="clarify_secondary" data-id="{{ $memo->id }}" data-que="ed" title="Launch Memo Movement" href="{{ route('letter-movement.create',$memo->id) }}"><i class="fa fa-edit"></i> <span>Launch Memo Movement</span></a>--}}
{{--       Not shown for now <a class="clarify_secondary" data-id="{{ $memo->id }}" data-que="ed" title="View Associated Memos" href="{{ route('letter.associations',$memo->id) }}"><i class="fa fa-edit"></i> <span>View Associated Memos</span></a>--}}

            <a class="delete-row delete_memo InternalMemo id {{ $memo->id }} danger" title="Delete Memo" href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Memo</span></a>

{{--            @if(isset($enabledAdminDeletion) && $enabledAdminDeletion)
                <a data-url = "{{route('letters.deletion',['incoming',$memo->id])}}" data-model="Memo" class="delete-timo danger" title="Delete Memo (Admin)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Memo (Admin)</span></a>
            @endif--}}

        @elseif($level == 'registry')

            <a class="clarify_secondary" title="View/Edit Memo - {{$memo->subject}}" href="{{ route('memo.edit',$memo->id) }}"><i class="fa fa-edit"></i> <span>View/Edit Memo Details</span></a>

{{--            <a class="forward-action-memo" data-id="{{ $memo->id }}" data-que="registry" title="Forward - {{$memo->subject}} to Registry" href="{{ route('memo.edit',$memo->id) }}"><i class="fa fa-edit"></i> <span>Forward Memo to Registry</span></a>--}}

            @if(isset($memo->memo_file_name) && $memo->memo_file_name != '' )
                <a class="forward-action-memo" data-id="{{ $memo->id }}" data-que="ed" title="Forward - {{$memo->subject}} to ED's Office" href="#!"><i class="fa fa-edit"></i> <span>Forward Memo to ED's Office</span></a>
            @endif

{{--            <a class="delete-row delete_letter Memo id {{ $memo->id }} danger" title="Delete Memo" href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Memo</span></a>--}}

            @if(!isset($memo->memo_file_name) || $memo->memo_file_name == '' )
            <a class="clarify_secondary" data-id="{{ $memo->id }}" data-que="ed" title="Attach Scanned Memo" href="{{ route('memo.upload',$memo->id) }}"><i class="fa fa-edit"></i> <span>Attach Scanned Memo</span></a>
            @endif

            @if(isset($memo->memo_file_name) && $memo->memo_file_name != '' )
                <a class="pdf-link" data-remove-temp-url="{{route('delete-temp-incoming-letter-doc','')}}" data-download-url = "{{route('download-link-memo',$memo->id)}}" data-pdf-url = "" data-id="{{ $memo->id }}" data-que="ed" title="View Attached Scanned Memo" href="#"><i class="fa fa-edit"></i> <span>View Attached Scanned Memo</span></a>
            @endif

            @if(isset($memo->memo_file_name) && $memo->memo_file_name != '')
                @if(isset($username) && $memo->memo_upload_user == $username)
                    <a class="btn-delete" data-id="{{ $memo->id }}" data-que="ed" title="Remove Attached Document" href="{{ route('memo.remove-attached-upload',$memo->id) }}"><i class="fa fa-edit"></i> <span>Remove Attached Document</span></a>
                @endif
            @endif

            @if(isset($memo->memo_file_name) && $memo->memo_file_name != '' )
                <a class="clarify_secondary" data-id="{{ $memo->id }}" data-que="ed" title="Rename Attached Scanned Memo" href="{{ route('letter.rename-attached-doc.view',$memo->id) }}"><i class="fa fa-edit"></i> <span>Rename Attached Scanned Memo</span></a>
            @endif

{{--            <a class="clarify_secondary" title="View Memo Movement - {{$memo->subject}}" href="{{ route('letter-movement.view-movement',[$memo->id, 'ed']) }}"><i class="fa fa-edit"></i> <span>View Memo Movement</span></a>--}}
{{--            <a class="clarify_secondary" data-id="{{ $memo->id }}" data-que="ed" title="Launch Memo Movement" href="{{ route('letter-movement.create',$memo->id) }}"><i class="fa fa-edit"></i> <span>Launch Memo Movement</span></a>--}}

           {{-- @if(session('enable_admin_deletion',false) == true)
                <a data-url = "{{route('letters.deletion',['incoming',$memo->id])}}" data-model="Memo" class="delete-timo danger" title="Delete Memo (Admin)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Memo (Admin)</span></a>
            @endif--}}


        @elseif($level == 'ed')

            <a class="clarify_secondary" title="View/Edit Memo - {{$memo->subject}}" href="{{ route('memo.edit',$memo->id) }}"><i class="fa fa-edit"></i> <span>View/Edit Memo Details</span></a>

{{--            <a class="forward-action-memo" data-id="{{ $memo->id }}" data-que="registry" title="Forward - {{$memo->subject}} to Registry" href="{{ route('memo.edit',$memo->id) }}"><i class="fa fa-edit"></i> <span>Forward Memo to Registry</span></a>--}}
{{--            <a class="forward-action-memo" data-id="{{ $memo->id }}" data-que="ed" title="Forward - {{$memo->subject}} to ED's Office" href="{{ route('memo.edit',$memo->id) }}"><i class="fa fa-edit"></i> <span>Forward Memo to ED's Office</span></a>--}}
{{--            <a class="delete-row delete_letter Memo id {{ $memo->id }} danger" title="Delete Memo" href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Memo</span></a>--}}

            @if(!isset($memo->memo_file_name) || $memo->memo_file_name == '' )
                <a class="clarify_secondary" data-id="{{ $memo->id }}" data-que="ed" title="Attach Scanned Memo" href="{{ route('memo.upload',$memo->id) }}"><i class="fa fa-edit"></i> <span>Attach Scanned Memo</span></a>
            @endif

            @if(isset($memo->memo_file_name) && $memo->memo_file_name != '' )
                <a class="pdf-link" data-remove-temp-url="{{route('delete-temp-incoming-letter-doc','')}}" data-download-url = "{{route('download-link-memo',$memo->id)}}" data-pdf-url = "" data-id="{{ $memo->id }}" data-que="ed" title="View Attached Scanned Memo" href="#"><i class="fa fa-edit"></i> <span>View Attached Scanned Memo</span></a>
            @endif

            @if(isset($memo->memo_file_name) && $memo->memo_file_name != '')
                @if(isset($username) && $memo->memo_upload_user == $username)
                    <a class="btn-delete" data-id="{{ $memo->id }}" data-que="ed" title="Remove Attached Document" href="{{ route('memo.remove-attached-upload',$memo->id) }}"><i class="fa fa-edit"></i> <span>Remove Attached Document</span></a>
                @endif
            @endif

            @if(!isset($memo->letterMovements) || count($memo->letterMovements) == 0 )
                <a class="clarify_secondary" data-id="{{ $memo->id }}" data-que="ed" title="Launch Memo Movement" href="{{ route('letter-movement.launch',['internal_memo',$memo->id]) }}"><i class="fa fa-edit"></i> <span>Launch Memo Movement</span></a>
            @endif

            @if(isset($memo->letterMovements) && count($memo->letterMovements) > 0 )
                <a class="clarify_secondary" title="View Memo Movement - {{$memo->subject}}" href="{{ route('memo-movement.view-movement',[$memo->id, 'ed']) }}"><i class="fa fa-edit"></i> <span>View Memo Movement</span></a>
            @endif

{{--            @if(session('enable_admin_deletion',false) == true)--}}
{{--                <a data-url = "{{route('letters.deletion',['incoming',$memo->id])}}" data-model="Memo" class="delete-timo danger" title="Delete Memo (Admin)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Memo (Admin)</span></a>--}}
{{--            @endif--}}

        @endif

    </div>
</div>
