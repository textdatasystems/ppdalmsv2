<div class="actions">
    <i class="fa fa-list"></i>
    <div class="actions-list">
        <a class="clarify_tertiary" title="View Letter Details" href="{{ route('outgoingletter.edit',$letter->id) }}"><i class="fa fa-edit"></i> <span>View Letter Details</span></a>
        @if(isset($letter->outgoing_letter_file_name) && $letter->outgoing_letter_file_name != '')
            <a class="pdf-link " data-remove-temp-url="{{route('delete-temp-outgoing-letter-doc','')}}"  data-download-url = "{{route('download-link-outgoing',$letter->id)}}" data-id="{{ $letter->id }}" data-que="ed" title="View Attached Scanned Letter" href="#"><i class="fa fa-edit"></i> <span>View Attached Scanned Letter</span></a>
        @endif
    </div>
</div>
