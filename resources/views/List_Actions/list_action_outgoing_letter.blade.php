<div class="actions">
    <i class="fa fa-list"></i>
    <div class="actions-list">
        <a class="clarify" title="View/Edit Outgoing Letter - {{$letter->subject}}" href="{{ route('outgoingletter.edit',$letter->id) }}"><i class="fa fa-edit"></i> <span>View/Edit Outgoing Letter Details</span></a>

        @if(!isset($letter->outgoing_letter_file_name) || $letter->outgoing_letter_file_name == '')
            <a class="clarify" data-id="{{ $letter->id }}" data-que="ed" title="Attach Scanned Outgoing Letter" href="{{ route('outgoingletter.upload',$letter->id) }}"><i class="fa fa-edit"></i> <span>Attach Scanned Outgoing Letter</span></a>
        @else
            <a class="pdf-link "  data-enable-toolbar="yes"  data-remove-temp-url="{{route('delete-temp-outgoing-letter-doc','')}}"  data-download-url = "{{route('download-link-outgoing',$letter->id)}}" data-id="{{ $letter->id }}" data-que="ed" title="View Attached Scanned Letter" href="#"><i class="fa fa-edit"></i> <span>View Attached Scanned Letter</span></a>
            @if(isset($username) && $letter->outgoing_letter_upload_user == $username)
                <a class="btn-delete" data-id="{{ $letter->id }}" data-que="ed" title="Remove Attached Document" href="{{ route('outgoingletter.remove-attached-upload',$letter->id) }}"><i class="fa fa-edit"></i> <span>Remove Attached Document</span></a>
            @endif
        @endif

        @if(isset($letter->outgoing_letter_file_name) && $letter->outgoing_letter_file_name != '' )
            <a class="clarify" data-id="{{ $letter->id }}" data-que="ed" title="Rename Attached Scanned Letter" href="{{ route('outgoingletter.rename-attached-doc.view',$letter->id) }}"><i class="fa fa-edit"></i> <span>Rename Attached Scanned Letter</span></a>
        @endif

        <a class="clarify"  data-id="{{ $letter->id }}" data-que="ed" title="View Associated Letters" href="{{ route('outgoingletters.associations',$letter->id) }}"><i class="fa fa-edit"></i> <span>View Associated Letters</span></a>

        @if(session('enable_admin_deletion',false) == true)
            <a data-url = "{{route('letters.deletion',['outgoing',$letter->id])}}" data-model="Outgoing Letter" class="delete-timo danger" title="Delete Letter (Admin)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> <span>Delete Letter (Admin)</span></a>
        @endif


        {{-- Check if it has an attached document --}}
        @if(isset($letter->outgoing_letter_file_name) && $letter->outgoing_letter_file_name != '')
            {{-- Check if has no receipt attached --}}
            @if(!isset($letter->outgoing_letter_receipt_file_name) || $letter->outgoing_letter_receipt_file_name == '')
                {{-- Show option to attach receipt --}}
                <a class="clarify"  data-id="{{ $letter->id }}" data-que="ed" title="Attach Scanned Letter Receipt" href="{{ route('outgoingletter.upload.receipt',$letter->id) }}"><i class="fa fa-edit"></i> <span>Attach Scanned Letter Receipt</span></a>
            @else

            @endif
        @endif

        @if(isset($letter->outgoing_letter_receipt_file_name) && $letter->outgoing_letter_receipt_file_name != '' )
            <a class="pdf-link"  data-enable-toolbar="yes"  data-remove-temp-url="{{route('delete-temp-outgoing-letter-doc','')}}" data-download-url = "{{route('download-link-outgoing.receipt',$letter->id)}}" data-pdf-url = "" data-id="{{ $letter->id }}" data-que="ed" title="View Attached Scanned Letter Receipt" href="#"><i class="fa fa-edit"></i> <span>View Attached Scanned Letter Receipt</span></a>
        @endif

        @if(isset($letter->outgoing_letter_receipt_file_name) && $letter->outgoing_letter_receipt_file_name != '' )
            @if(isset($username) && $letter->outgoing_letter_receipt_upload_user == $username)
                <a class="btn-delete" data-id="{{ $letter->id }}" data-que="ed" title="Remove Attached  Letter Receipt" href="{{ route('outgoingletter.remove-attached-upload.receipt',$letter->id) }}"><i class="fa fa-edit"></i> <span>Remove Attached Letter Receipt</span></a>
            @endif
        @endif

    </div>
</div>
