<div class="actions">
    <i class="fa fa-list"></i>
    <div class="actions-list">
        <a class="clarify" title="View/Edit Global Value - {{$globalValue->variable_name}}" href="{{ route('global-values.edit',$globalValue->id) }}"><i class="fa fa-edit"></i> <span>View/Edit Global Value</span></a>
{{--        <a class="btn-delete" data-id="{{ $globalValue->id }}" title="Delete Global Value" href="{{ route('global-values.remove',$globalValue->id) }}"><i class="fa fa-edit"></i> <span>Delete Global Value</span></a>--}}
    </div>
</div>
