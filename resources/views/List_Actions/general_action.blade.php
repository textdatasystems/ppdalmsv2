<?php
    $actions = array(
        'edmsdoctype' => array(
            array(
                'title'=>'View/Edit EDMS Doc Type',
                'link_name'=>'View/Edit EDMS Doc Type',
                'route'=>route('edmsdoctype.edit',$record->id),
                'icon'=>'fa-edit',
                'link_class'=>'clarify'
            ),

        )
    );
?>

<div class="actions">
    <i class="fa fa-list"></i>
    <div class="actions-list">
        @foreach($actions[$model] as $action)
            <a class="{{ $action['link_class'] }}" title="{{ $action['title'] }}" href="{{ $action['route'] }}"><i class="fa {{ $action['icon'] }}"></i> <span>{{ $action['link_name'] }}</span></a>
        @endforeach
    </div>
</div>
