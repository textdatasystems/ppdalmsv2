<div class="actions">
    <i class="fa fa-list"></i>
    <div class="actions-list">
        <a class="clarify_secondary" title="View Internal Memo Movement - {{$memo->subject}}" href="{{ route('memo-movement.view-movement',[$memo->id,'letter-movement']) }}"><i class="fa fa-edit"></i> <span>View Memo Movement</span></a>
    </div>
</div>
