<div class="actions">
    <i class="fa fa-list"></i>
    <div class="actions-list">

        {{-- todo use a different variable for incoming or outgoing and render different menu items --}}
        @if(isset($associatedLetterCategory) && $associatedLetterCategory == 'Incoming')
            <a class="clarify" title="View Letter Details" href="{{ route('letter.readonly',$associatedLetterId) }}"><i class="fa fa-edit"></i> <span>View Letter Details</span></a>
            @if(isset($fileName) && $fileName != '')
                <a class="pdf-link" data-remove-temp-url="{{route('delete-temp-incoming-letter-doc','')}}" data-download-url = "{{route('download-link-incoming',$associatedLetterId)}}" data-pdf-url = "" data-id="{{ $associatedLetterId }}" data-que="ed" title="View Attached Scanned Letter" href="#"><i class="fa fa-edit"></i> <span>View Attached Scanned Letter</span></a>
            @endif

        @elseif(isset($associatedLetterCategory) && $associatedLetterCategory == 'Outgoing')
            <a class="clarify" title="View Letter Details" href="{{ route('outgoingletter.edit',$associatedLetterId) }}"><i class="fa fa-edit"></i> <span>View Letter Details</span></a>

            @if(isset($fileName) && $fileName != '')
                <a class="pdf-link " data-remove-temp-url="{{route('delete-temp-outgoing-letter-doc','')}}"  data-download-url = "{{route('download-link-outgoing',$associatedLetterId)}}" data-id="{{ $associatedLetterId }}" data-que="ed" title="View Attached Scanned Letter" href="#"><i class="fa fa-edit"></i> <span>View Attached Scanned Letter</span></a>
            @endif

        @endif

    </div>

</div>
