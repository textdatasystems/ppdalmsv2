<div class="actions">
    <i class="fa fa-list"></i>
    <div class="actions-list">
        <a class="clarify" title="View Letter Movement - {{$letter->subject}}" href="{{ route('letter-movement.view-movement',[$letter->id,'letter-movement']) }}"><i class="fa fa-edit"></i> <span>View Letter Movement</span></a>
    </div>
</div>
