@extends('layouts.app')
@section('title') Reception @endsection
@section('content')
    <div class="page-header">
        <h1>
            Reception
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Letter Management
            </small>
        </h1>
    </div>
    <div class="form-actions no-margin-bottom">
        <div class="float-alert center">
            <div class="float-alert-container">
                <div class="col-md-12">
                    <a data-pdf-url = "http://www.africau.edu/images/default/sample.pdf" href="#" class="pdf-link btn btn-primary btn-sm" title="Add/Receive New Incoming Letter">View PDF</a>
                    <hr>
                </div>
            </div>
        </div>

        <div id="pdf-dialog" style="display:none;">
            <div >
                <iframe id="iframe-pdf-viewer" style="width: 100%;height: 600px" src=""></iframe>
            </div>
        </div>


        <div class="clearfix"></div>
    </div>
    <table class="data-table reception_letters table table-striped table-bordered table-hover no-margin-bottom no-border-top" >
        <thead>
        <tr>
            <th>Entity Name</th>
            <th>Subject</th>
            <th>Date Sent</th>
            <th>Date Received PPDA</th>
            <th>Date Received Registry</th>
            <th>Sender Reference No.</th>
            <th>PPDA Reference No.</th>
            <th>Attention To</th>
            <th>Signatory</th>
            <th>Signatory Title</th>
            <th>Letter Type</th>
            <th></th>
        </tr>
        </thead>
    </table>
@endsection
